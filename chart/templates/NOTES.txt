# Application will be available at http{{ if $.Values.ingress.tls }}s{{ end }}://{{ .Values.ingress.host.name }}

# To delete app, do:
helm delete {{.Release.Name}}
kubectl delete pvc -lapp.kubernetes.io/name=postgresql
kubectl delete pvc -lapp.kubernetes.io/name=redis

# You also can delete media dir:
kubectl delete pvc -lapp.kubernetes.io/name=django