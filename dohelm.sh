#!/usr/bin/env bash

# for the first run, do an `helm dependency update chart`
# To delete the app : `helm delete $(git branch --show)`
# To delete PERMANENTLY all pvc : `kubectl delete pvc --all`

source ./tokens.sh # put `export SECRET_KEY="..."` in this file

NAMESPACE="ippidb-dev"
CI_REGISTRY_IMAGE="registry-gitlab.pasteur.fr/ippidb/ippidb-web"
CI_COMMIT_SHA=$(git log --format="%H" -n 1)
#CI_COMMIT_SHA="be552b8b33d6d10215d58e04ac80980fcccb5754"
CI_COMMIT_REF_SLUG=$(git branch --show)
INGRESS_CLASS="internal"
PUBLIC_URL="ippidb-${CI_COMMIT_REF_SLUG}.dev.pasteur.cloud"
PUBLIC_URL_2="ippidb.pasteur.fr"
CI_REGISTRY="registry-gitlab.pasteur.fr"
#POSTGRES_PASSWD="$(kubectl get secret $(echo -n $CI_COMMIT_REF_SLUG)-postgresql --output jsonpath='{.data.postgres-password}' | base64 --decode)"

export ACTION="upgrade --install"
#export ACTION="template --debug"

helm ${ACTION} --namespace=${NAMESPACE} \
    --set CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
    --set image.tag=${CI_COMMIT_SHA} \
    --set ingress.host.name=${PUBLIC_URL} \
    --set ingress.annotations."kubernetes\.io/ingress\.class"=${INGRESS_CLASS} \
    --set registry.username=${DEPLOY_USER} \
    --set registry.password=${DEPLOY_TOKEN} \
    --set registry.host=${CI_REGISTRY} \
    --set imagePullSecrets[0].name="registry-pull-secret-${CI_COMMIT_REF_SLUG}" \
    --set djangoSecrets.GACODE=$(echo "$GACODE" | base64) \
    --set djangoSecrets.MARVINJS_APIKEY=$(echo "$MARVINJS_APIKEY" | base64) \
    --set djangoSecrets.GALAXY_APIKEY=$(echo "$GALAXY_APIKEY" | base64) \
    --set djangoSecrets.GALAXY_BASE_URL=$(echo "$GALAXY_BASE_URL" | base64) \
    --set djangoSecrets.GALAXY_COMPOUNDPROPERTIES_WORKFLOWID=$(echo "$GALAXY_COMPOUNDPROPERTIES_WORKFLOWID" | base64) \
    --values ./chart/values.${NAMESPACE}.yaml \
    ${CI_COMMIT_REF_SLUG} ./chart/