from maintenance_mode.backends import AbstractStateBackend

from live_settings import live_settings


class LiveSettingsStorageBackend(AbstractStateBackend):
    def get_value(self):
        return live_settings.maintenance_mode_state__bool

    def set_value(self, value):
        live_settings.maintenance_mode_state = value
