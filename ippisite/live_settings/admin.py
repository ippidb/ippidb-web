from django.contrib import admin

from live_settings import models


class LiveSettingsAdmin(admin.ModelAdmin):
    list_display = ("key", "last_edition_date")
    readonly_fields = ["last_edition_date"]


admin.site.register(models.LiveSettings, LiveSettingsAdmin)
