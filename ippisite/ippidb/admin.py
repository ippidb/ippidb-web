"""
iPPI-DB django admin module
"""

from django.apps import apps
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.options import get_content_type_for_model
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.utils.html import format_html
from django.utils.translation import gettext, gettext_lazy
from django.utils.safestring import mark_safe

from .models import (
    Bibliography,
    Protein,
    Taxonomy,
    Domain,
    reverse,
    ProteinDomainBoundComplex,
    ProteinDomainPartnerComplex,
    Symmetry,
    Compound,
    TestActivityDescription,
    Ppi,
    ProteinDomainComplex,
    Contribution,
    Cavity,
    Chain,
    PDB,
    Ligand,
    InteractFile,
    Partner,
    Distance,
    MetaInformation,
    Job,
    HotSpot,
    LinkedPDB,
    Faq,
)
from .tasks import launch_validate_contributions


class ViewOnSiteModelAdmin(admin.ModelAdmin):
    class Media:
        js = ("/static/font-awesome/js/all.min.js",)

    def __init__(self, model, admin_site):
        if callable(getattr(model, "get_absolute_url", None)) and callable(
            self.view_on_site_in_list
        ):
            self.list_display += ("view_on_site_in_list",)
        super().__init__(model, admin_site)

    def view_on_site_in_list(self, obj):
        return format_html(
            '<center><a href="'
            + reverse(
                "admin:view_on_site",
                kwargs={
                    "content_type_id": get_content_type_for_model(obj).pk,
                    "object_id": obj.pk,
                },
            )
            + '"><i class="fa fa-external-link-alt"></i></a><center>'
        )

    view_on_site_in_list.short_description = format_html(
        "<center>" + gettext("View on site") + "<center>"
    )


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ("question", "answer", "type")


@admin.register(PDB)
class PDBAdmin(admin.ModelAdmin):
    list_display = ("code", "chains_info")
    search_fields = ("code",)

    def chains_info(self, obj):
        return "\t".join(
            [
                "{}: {}".format(chain.pdb_chain_id, chain.protein.uniprot_id)
                for chain in obj.chain_set.all()
            ]
        )


@admin.register(Chain)
class ChainAdmin(admin.ModelAdmin):
    list_display = ("pdb_chain_id", "pdb_code", "protein")
    search_fields = ("pdb_chain_id", "pdb__code", "protein__uniprot_id")
    list_filter = ("pdb_chain_id", "protein__organism__name")
    # if pdb is asked request never return
    readonly_fields = ("pdb",)

    def pdb_code(self, obj):
        return obj.pdb.code


@admin.register(HotSpot)
class HotSpotAdmin(admin.ModelAdmin):
    list_display = ("chain_info", "residu_code", "residu_number", "type")
    search_fields = ("chain__pdb_chain_id", "chain__pdb__code", "type")
    list_filter = ("residu_code", "residu_number", "type")
    # if chain is asked request never return
    readonly_fields = ("chain",)

    def chain_info(self, obj):
        return "{} {}".format(obj.chain.pdb.code, obj.chain.pdb_chain_id)


@admin.register(LinkedPDB)
class LinkedPDBAdmin(admin.ModelAdmin):
    list_display = ("id", "label")
    search_fields = ("chain__pdb__code",)

    def label(self, obj):
        return f"PDB {obj.chain.pdb.code} chain {obj.chain.pdb_chain_id} \
            ligand {obj.ligand_pl.pdb.code}  \
                {obj.ligand_pl.pdb_ligand_id}-{obj.ligand_pl.supplementary_id}"


@admin.register(InteractFile)
class InteractFileAdmin(admin.ModelAdmin):
    list_display = ("chain_info", "label")
    list_filter = ("label",)
    search_fields = ("chain__protein__uniprot_id",)
    # if chain is asked request never return
    readonly_fields = ("chain",)

    def chain_info(self, obj):
        return "{} {} {}".format(
            obj.chain.pdb.code, obj.chain.pdb_chain_id, obj.chain.protein.uniprot_id
        )


@admin.register(Ligand)
class LigandAdmin(admin.ModelAdmin):
    list_display = (
        "iupac_name",
        "pdb_ligand_id",
        "supplementary_id",
        "pdb_code",
    )
    list_filter = ("pdb_ligand_id",)
    search_fields = ("pdb_ligand_id", "supplementary_id", "pdb__code")
    readonly_fields = ("pdb",)

    def pdb_code(self, obj):
        return obj.pdb.code


@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ("partner",)
    readonly_fields = ("chain", "ligand")

    def partner(self, obj):
        if obj.ligand:
            return "Type: ligand, {}_{} {}".format(
                obj.ligand.pdb_ligand_id,
                obj.ligand.supplementary_id,
                obj.ligand.pdb.code,
            )
        else:
            return "Type: protein, {} {}".format(
                obj.chain.protein.uniprot_id, obj.chain.pdb.code
            )


@admin.register(Cavity)
class CavityAdmin(admin.ModelAdmin):
    list_display = ("full_name", "info")
    search_fields = ("full_name",)
    list_filter = ("chain__protein__organism__name",)
    readonly_fields = ("chain", "partner")

    def info(self, obj):
        if obj.partner.ligand:
            return "Cavity {} from {} with {}_{} in {}".format(
                obj.cavity_number,
                obj.chain.protein.uniprot_id,
                obj.partner.ligand.pdb_ligand_id,
                obj.partner.ligand.supplementary_id,
                obj.chain.pdb.code,
            )
        else:
            return "Cavity {} from {} with {} in {}".format(
                obj.cavity_number,
                obj.chain.protein.uniprot_id,
                obj.partner.chain.protein.uniprot_id,
                obj.chain.pdb.code,
            )


@admin.register(Distance)
class DistanceAdmin(admin.ModelAdmin):
    list_display = ("similiarity_between", "distance")
    search_fields = ("cavity1__full_name", "cavity2__full_name")

    def similiarity_between(self, obj):
        return "{} and {}".format(obj.cavity1.full_name, obj.cavity2.full_name)


@admin.register(MetaInformation)
class MetaInformationAdmin(admin.ModelAdmin):
    list_display = ("average", "std", "maximum", "minimum", "normalize_factor")


@admin.register(Job)
class JobModelAdmin(admin.ModelAdmin):
    date_hierarchy = "task_result__date_done"
    list_display = (
        "task_result_task_name",
        "task_result_task_id",
        "task_result_status",
        "task_result_date_created",
        "task_result_date_done",
        "logs",
    )
    list_filter = (
        "task_result__status",
        "task_result__date_done",
        "task_result__task_name",
    )
    readonly_fields = (
        "task_result_task_name",
        "task_result_task_id",
        "task_result_status",
        "task_result_date_created",
        "task_result_date_done",
        "logs",
    )
    search_fields = (
        "task_result__task_name",
        "task_result__task_id",
        "task_result__status",
    )
    fields = (
        ("task_result_task_name", "task_result_task_id"),
        "task_result_status",
        ("task_result_date_created", "task_result_date_done"),
        ("std_out", "std_err"),
    )

    def logs(self, obj):
        url = reverse("admin:tasklog")
        return mark_safe(
            "<a target='_blank' href='{}?jobid={}'>see log</a>".format(url, obj.id)
        )

    def task_result_task_id(self, x):
        return x.task_result.task_id

    def task_result_task_name(self, x):
        return x.task_result.task_name

    def task_result_date_done(self, x):
        return x.task_result.date_done

    def task_result_status(self, x):
        return x.task_result.status

    def task_result_date_created(self, x):
        return x.task_result.date_created

    task_result_task_id.short_description = "task_id"
    task_result_task_name.short_description = "task_name"
    task_result_date_done.short_description = "date_done"
    task_result_status.short_description = "status"
    task_result_date_created.short_description = "date_created"


@admin.register(Bibliography)
class BibliographyModelAdmin(ViewOnSiteModelAdmin):
    list_display = ("authors_list", "title", "journal_name", "biblio_year", "id_source")


@admin.register(Protein)
class ProteinModelAdmin(admin.ModelAdmin):
    list_display = ("uniprot_id", "recommended_name_long")
    filter_horizontal = ("molecular_functions", "domains")
    search_fields = (
        "uniprot_id",
        "recommended_name_long",
        "short_name",
        "gene_name",
        "entry_name",
    )
    list_filter = ("organism",)


@admin.register(Taxonomy)
class TaxonomyModelAdmin(admin.ModelAdmin):
    list_display = ("taxonomy_id", "name")


@admin.register(Domain)
class DomainModelAdmin(admin.ModelAdmin):
    list_display = ("pfam_acc", "pfam_id", "pfam_description", "domain_family")


@admin.register(ProteinDomainBoundComplex)
class ProteinDomainBoundComplexModelAdmin(ViewOnSiteModelAdmin):
    list_display = ("protein", "domain", "ppc_copy_nb", "ppp_copy_nb_per_p")
    list_display_links = ("protein", "domain", "ppc_copy_nb", "ppp_copy_nb_per_p")


@admin.register(ProteinDomainPartnerComplex)
class ProteinDomainPartnerComplexModelAdmin(ViewOnSiteModelAdmin):
    list_display = ("protein", "domain", "ppc_copy_nb")
    list_display_links = ("protein", "domain", "ppc_copy_nb")


@admin.register(Symmetry)
class SymmetryModelAdmin(admin.ModelAdmin):
    list_display = ("code", "description")


@admin.register(Compound)
class CompoundModelAdmin(ViewOnSiteModelAdmin):
    list_display = (
        "id",
        "iupac_name",
        "common_name",
        "canonical_smile",
        "is_validated",
    )
    search_fields = ("id", "iupac_name", "common_name", "canonical_smile")


@admin.register(Contribution)
class ContributionModelAdmin(ViewOnSiteModelAdmin):
    list_display = (
        "id",
        "ppi",
        "bibliography",
        "validated",
        "created_at",
        "updated_at",
    )
    search_fields = ("id", "ppi", "bibliography")

    actions = ["validate_contributions"]

    def validate_contributions(self, request, queryset):
        ids = [id for id in queryset.values_list("id", flat=True)]
        launch_validate_contributions(ids)
        self.message_user(
            request,
            f"validation started for contributions(s) "
            f"{','.join(str(id) for id in queryset.values_list('id', flat=True))}"
            f" (this might take a while).",
        )


@admin.register(TestActivityDescription)
class TextActivityDescriptionModelAdmin(ViewOnSiteModelAdmin):
    list_display = ("test_name", "test_type", "test_modulation_type")


@admin.register(Ppi)
class PpiModelAdmin(ViewOnSiteModelAdmin):
    filter_horizontal = ("diseases",)
    list_display = ("pdb_id", "name", "symmetry", "family")
    list_filter = ("diseases",)
    search_fields = ("pdb_id", "name", "symmetry", "family__name", "diseases__name")


@admin.register(ProteinDomainComplex)
class ProteinDomainComplexModelAdmin(ViewOnSiteModelAdmin):
    search_fields = (
        "id",
        "protein__id",
        "domain__id",
        "protein__uniprot_id",
        "domain__pfam_acc",
    )
    list_display = ("id", "__str__", "protein_uniprot_id", "domain_pfam_acc")

    def protein_uniprot_id(self, o):
        return o.protein.uniprot_id if o.protein is not None else None

    protein_uniprot_id.short_description = "protein"
    protein_uniprot_id.admin_order_field = "protein__uniprot_id"

    def domain_pfam_acc(self, o):
        return o.domain.pfam_acc if o.domain is not None else None

    domain_pfam_acc.short_description = "Domain"
    domain_pfam_acc.admin_order_field = "domain__pfam_acc"


for model in apps.get_app_config("ippidb").models.values():
    try:
        admin.site.register(model, ViewOnSiteModelAdmin)
    except admin.sites.AlreadyRegistered:
        continue


def grant_contribution_permission(modeladmin, request, queryset, revoke=False):
    content_type = ContentType.objects.get_for_model(Contribution)
    permission = Permission.objects.get(
        codename="add_contribution", content_type=content_type
    )
    for o in queryset:
        if revoke:
            o.user_permissions.remove(permission)
        else:
            o.user_permissions.add(permission)

    pointless = queryset.filter(Q(Q(is_superuser=True) | Q(is_staff=True))).count()
    count = queryset.count()
    msg = "Permission %s for %i user(s). "

    if revoke:
        msg = msg % ("revoked", count - pointless)
    else:
        msg = msg % ("granted", count - pointless)

    if modeladmin is None:
        return

    modeladmin.message_user(request, msg, messages.SUCCESS)

    if pointless:
        modeladmin.message_user(
            request,
            "Note that %i users are staff/superuser and thus always have the permission to contribute"
            % pointless,
            messages.WARNING,
        )


grant_contribution_permission.short_description = gettext_lazy(
    "Grant permission to contribute"
)


def revoke_contribution_permission(modeladmin, request, queryset, revoke=False):
    grant_contribution_permission(modeladmin, request, queryset, True)


revoke_contribution_permission.short_description = gettext_lazy(
    "Revoke permission to contribute"
)


class MyUserAdmin(UserAdmin):
    actions = [grant_contribution_permission, revoke_contribution_permission]
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "can_contribute",
    )

    def can_contribute(self, object):
        return object.has_perm("ippidb.add_contribution")

    can_contribute.boolean = True


admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), MyUserAdmin)
