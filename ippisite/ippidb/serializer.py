"""
Ippidb Serializer
"""

from rest_framework import serializers
from ippidb.models import (
    PDB,
    Chain,
    Ligand,
    Cavity,
    Partner,
    Distance,
    Protein,
    Taxonomy,
    InteractFile,
    Job,
    HotSpot,
    LinkedPDB,
)
from Bio.PDB import PDBParser, PDBIO
from io import StringIO
import numpy as np


class OrganismSerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomy
        fields = ("name",)


class ProteinSerializer(serializers.ModelSerializer):
    organism = OrganismSerializer(read_only=True)
    uniprot_id = serializers.CharField(max_length=10, validators=[])

    class Meta:
        model = Protein
        fields = ("id", "uniprot_id", "organism")


class JobSerializer(serializers.ModelSerializer):
    nb_neighbors = serializers.IntegerField(write_only=True)
    task_name = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    date_created = serializers.SerializerMethodField(read_only=True)
    date_done = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Job
        fields = (
            "id",
            "status",
            "task_name",
            "date_created",
            "date_done",
            "nb_neighbors",
        )
        read_only_fields = ["std_out", "std_err"]

    def get_task_name(self, obj):
        if obj.task_result:
            return obj.task_result.task_name
        else:
            return None

    def get_status(self, obj):
        if obj.task_result:
            return obj.task_result.status
        else:
            return None

    def get_date_created(self, obj):
        if obj.task_result:
            return obj.task_result.date_created
        else:
            return None

    def get_date_done(self, obj):
        if obj.task_result:
            return obj.task_result.date_done
        else:
            return None


class BuildDistancesSerializer(serializers.Serializer):
    nb_neighbors = serializers.IntegerField(write_only=True)


class PdbDistanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PDB
        fields = ("id", "code")


class LigandDistanceSerializer(serializers.ModelSerializer):
    pdb = PdbDistanceSerializer()

    class Meta:
        model = Ligand
        fields = (
            "id",
            "pdb_ligand_id",
            "supplementary_id",
            "pdb",
            "canonical_smile",
            "iupac_name",
        )


class BaseChainSerializer(serializers.ModelSerializer):
    pdb = PdbDistanceSerializer()
    protein = ProteinSerializer()

    class Meta:
        model = Chain
        fields = ("id", "pdb_chain_id", "protein", "pdb")


class CavitySummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Cavity
        fields = ["full_name", "cavity_number", "type"]


class ChainSummarySerializer(serializers.ModelSerializer):
    # cavity_set = CavitySummarySerializer(many=True, read_only=True)
    protein = ProteinSerializer()

    class Meta(BaseChainSerializer.Meta):
        # fields = ("pdb_chain_id", "protein", "cavity_set")
        fields = ("pdb_chain_id", "protein")


class ChainDistanceSerializer(BaseChainSerializer):
    class Meta(BaseChainSerializer.Meta):
        fields = BaseChainSerializer.Meta.fields + ("default_isolevel",)


class BasePartnerSerializer(serializers.ModelSerializer):
    ligand = LigandDistanceSerializer()

    class Meta:
        model = Partner
        fields = ("id", "ligand", "chain")


class PartnerSummarySerializer(BasePartnerSerializer):
    chain = ChainSummarySerializer()


class PartnerDistanceSerializer(BasePartnerSerializer):
    chain = ChainDistanceSerializer()


class CavitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Cavity
        fields = "__all__"
        write_only_fields = ("mol2",)
        read_only_fields = ("t120",)


class CavityDistanceSerializer(serializers.ModelSerializer):
    chain = ChainDistanceSerializer()
    partner = PartnerDistanceSerializer()
    type = serializers.CharField(read_only=True)

    class Meta:
        model = Cavity
        exclude = ("mol2",)

    def get_type(self, obj):
        if obj.partner:
            return obj.partner.type_binding
        else:
            return None


class DistanceSerializer(serializers.ModelSerializer):
    cavity1 = CavityDistanceSerializer()
    cavity2 = CavityDistanceSerializer()

    class Meta:
        model = Distance
        fields = "__all__"


class InteractFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = InteractFile
        fields = "__all__"


class PartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = "__all__"


class HotSpotSerializer(serializers.ModelSerializer):
    class Meta:
        model = HotSpot
        fields = "__all__"


class LinkedPDBSerializer(serializers.ModelSerializer):
    aligned_chain = serializers.SerializerMethodField(read_only=True)
    aligned_ligand = serializers.SerializerMethodField(read_only=True)
    name = serializers.SerializerMethodField(read_only=True)
    label = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = LinkedPDB
        fields = [
            "id",
            "chain",
            "ligand_pl",
            "translation",
            "rotation",
            "aligned_chain",
            "aligned_ligand",
            "name",
            "label",
        ]

    def align_coordinates(self, pdb_content, rotation, translation, ligand=None):
        parser = PDBParser(QUIET=True)
        structure = parser.get_structure("structure", StringIO(pdb_content))

        coordinates = []
        for model in structure:
            for chain in model:
                for residue in chain:
                    for atom in residue:
                        if atom.is_disordered():
                            for altloc in atom.child_dict:
                                coordinates.append(atom.child_dict[altloc].coord)
                        else:
                            coordinates.append(atom.coord)
        centroid = np.mean(coordinates, axis=0)

        if ligand:
            structure = parser.get_structure("structure", StringIO(ligand))
            coordinates = []
            for model in structure:
                for chain in model:
                    for residue in chain:
                        for atom in residue:
                            if atom.is_disordered():
                                for altloc in atom.child_dict:
                                    coordinates.append(atom.child_dict[altloc].coord)
                            else:
                                coordinates.append(atom.coord)
            aligned_coordinates = (
                np.dot(coordinates - centroid, rotation) + centroid - translation
            )
        else:
            aligned_coordinates = (
                np.dot(coordinates - centroid, rotation) + centroid - translation
            )

        atom_index = 0
        for model in structure:
            for chain in model:
                for residue in chain:
                    for atom in residue:
                        if atom.is_disordered():
                            for altloc in atom.child_dict:
                                atom.child_dict[altloc].set_coord(
                                    aligned_coordinates[atom_index]
                                )
                                atom_index += 1
                        else:
                            atom.set_coord(aligned_coordinates[atom_index])
                            atom_index += 1

        io = PDBIO()
        io.set_structure(structure)

        pdb_string = StringIO()
        io.save(pdb_string)
        new_pdb_content = pdb_string.getvalue()

        return new_pdb_content

    def get_aligned_chain(self, obj):
        chain = Chain.objects.filter(pdb=obj.ligand_pl.pdb.id)
        if chain.count() > 1:
            raise serializers.ValidationError(
                f"Protein ligand id {obj.ligand_pl.pdb.id} has more than 1 chain. Please contact the administrator."
            )

        new_pdb_content = self.align_coordinates(
            chain[0].file_content, obj.rotation, obj.translation
        )
        return new_pdb_content

    def get_aligned_ligand(self, obj):
        chain = Chain.objects.filter(pdb=obj.ligand_pl.pdb.id)
        new_pdb_content = self.align_coordinates(
            chain[0].file_content,
            obj.rotation,
            obj.translation,
            ligand=obj.ligand_pl.file_content,
        )
        return new_pdb_content

    def get_name(self, obj):
        return (
            f"{obj.chain.pdb.code}_{obj.chain.pdb_chain_id}--{obj.ligand_pl.pdb.code}"
            f"_{obj.ligand_pl.pdb_ligand_id}-{obj.ligand_pl.supplementary_id}"
        )

    def get_label(self, obj):
        return (
            f"Align {obj.ligand_pl.pdb.code} {obj.ligand_pl.pdb_ligand_id}"
            f"-{obj.ligand_pl.supplementary_id} on {obj.chain.pdb_chain_id}"
        )


class ChainSerializer(serializers.ModelSerializer):
    cavity_set = CavitySerializer(many=True, read_only=True)
    partner_set = PartnerSerializer(many=True, read_only=True)
    interactfile_set = InteractFileSerializer(many=True, read_only=True)
    hotspot_set = HotSpotSerializer(many=True, read_only=True)
    linkedpdbs = serializers.SerializerMethodField(read_only=True)
    # protein = ProteinSerializer()

    class Meta:
        model = Chain
        fields = (
            "id",
            "pdb_chain_id",
            "protein",
            "pdb",
            "file_content",
            "cavity_set",
            "partner_set",
            "mrc_file",
            "default_isolevel",
            "interactfile_set",
            "hotspot_set",
            "linkedpdbs",
        )

    def get_linkedpdbs(self, obj):
        linkedpdbs = obj.linkedpdb_set.all()
        link_pdbs = []
        for linkedpdb in linkedpdbs:
            label = (
                f"Align { linkedpdb.ligand_pl.pdb.code } {linkedpdb.ligand_pl.pdb_ligand_id}-"
                f"{linkedpdb.ligand_pl.supplementary_id} on {linkedpdb.chain.pdb_chain_id}"
            )

            dict_link_pdb = {"id": linkedpdb.id, "label": label}
            link_pdbs.append(dict_link_pdb)
        return link_pdbs


class LigandSerializer(serializers.ModelSerializer):
    partner_set = PartnerSerializer(many=True, read_only=True)

    class Meta:
        model = Ligand
        fields = "__all__"
        read_only_fields = ["canonical_smile", "iupac_name"]


class LigandSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Ligand
        fields = ["pdb_ligand_id", "supplementary_id", "canonical_smile", "iupac_name"]


class PdbSerializer(serializers.ModelSerializer):
    chain_set = ChainSerializer(many=True, read_only=True)
    ligand_set = LigandSerializer(many=True, read_only=True)

    class Meta:
        model = PDB
        fields = "__all__"


class SummarySerializer(serializers.ModelSerializer):
    chain = serializers.SerializerMethodField(read_only=True)
    partner = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Cavity
        fields = ["full_name", "cavity_number", "chain", "partner", "type"]

    def get_chain(self, obj):
        return {
            "pdb_chain_id": obj.chain.pdb_chain_id,
            "uniprot": obj.chain.protein.uniprot_id,
            "organism": obj.chain.protein.organism.name,
            "pdb": obj.chain.pdb.code,
        }

    def get_partner(self, obj):
        if obj.partner.ligand:
            return {
                "type": "ligand",
                "pdb_chain_id": "NaN",
                "uniprot": "NaN",
                "pdb_ligand_id": "{}_{}".format(
                    obj.partner.ligand.pdb_ligand_id,
                    obj.partner.ligand.supplementary_id,
                ),
                "canonical_smile": obj.partner.ligand.canonical_smile,
                "organism": "NaN",
                "pdb": obj.partner.ligand.pdb.code,
            }
        else:
            return {
                "type": "protein",
                "pdb_chain_id": obj.partner.chain.pdb_chain_id,
                "uniprot": obj.partner.chain.protein.uniprot_id,
                "pdb_ligand_id": "NaN",
                "canonical_smile": "NaN",
                "organism": obj.partner.chain.protein.organism.name,
                "pdb": obj.partner.chain.pdb.code,
            }
