from __future__ import absolute_import, unicode_literals
import json
import tempfile
import io
import base64
import itertools
from typing import Callable, List

from django.db import Error
from django.db.models import QuerySet
from ippisite.celery import app
from celery import states, chain, group, chord
from ippisite.decorator import MonitorTask
import matplotlib.pyplot as plt

import seaborn as sns
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# TODO send email from django.core.mail import mail_managers
from django.forms.models import model_to_dict
from django.core.management import call_command
from ippidb.management.commands import (
    build_distances,
    flush_targetcentric,
)

from .models import (
    Compound,
    CompoundJob,
    Contribution,
    Job,
    update_compound_cached_properties,
    LeLleBiplotData,
    PcaBiplotData,
    ProteinDomainBoundComplex,
    ProteinDomainBoundComplexGroup,
    ProteinDomainPartnerComplex,
    ProteinDomainPartnerComplexGroup,
)
from .utils import smi2sdf
from .gx import GalaxyCompoundPropertiesRunner

plt.switch_backend("Agg")


def dec(decimal_places: int) -> Callable:
    """
    generate a function to return a number rounded
    to a specific precision

    :param decimal_places: precision for the value returned by the function
    :type decimal_places: int
    :return: rounding function
    :rtype: function
    """

    def func(number):
        return round(float(number), decimal_places)

    return func


def compute_compound_properties(
    compound_ids: List[int], task: MonitorTask = None
) -> List[int]:
    """
    compute the properties for a list of compounds

    :param compound_ids: The list of compound IDs to be processed
    :type compound_ids: List[int]
    :param task: The task to which some logging can be sent
    :type task: MonitorTask
    :return: List of processed compound IDs
    :rtype: List[int]
    """
    compounds = Compound.objects.filter(id__in=compound_ids)
    runner = GalaxyCompoundPropertiesRunner()
    smiles_dict = {}
    for c in compounds:
        smiles_dict[c.id] = c.canonical_smile
    # create SDF file for the selection
    sdf_string = smi2sdf(smiles_dict)
    fh = tempfile.NamedTemporaryFile(mode="w", delete=False)
    fh.write(sdf_string)
    fh.close()
    if task is not None:
        task.write(
            f"Galaxy input SDF file for compounds {smiles_dict.keys()}: {fh.name}"
        )
    # run computations on Galaxy
    pc_properties = runner.compute_properties_for_sdf_file(fh.name)
    pc_properties_dict = {compound["Name"]: compound for compound in pc_properties}
    fh = tempfile.NamedTemporaryFile(mode="w", delete=False)
    json.dump(pc_properties_dict, fh, indent=4)
    fh.close()
    if task is not None:
        task.write(
            f"Properties added for compounds {smiles_dict.keys()} in JSON file: {fh.name}"
        )
    # report and update database
    property_mapping = {
        "CanonicalSmile": ("canonical_smile", str),
        "IUPAC": ("iupac_name", str),
        "TPSA": ("tpsa", dec(2)),
        "NbMultBonds": ("nb_multiple_bonds", int),
        "BalabanIndex": ("balaban_index", dec(2)),
        "NbDoubleBonds": ("nb_double_bonds", int),
        "RDF070m": ("rdf070m", dec(2)),
        "SumAtomPolar": ("sum_atom_polar", dec(2)),
        "SumAtomVolVdW": ("sum_atom_vol_vdw", dec(2)),
        "MolecularWeight": ("molecular_weight", dec(2)),
        "NbCircuits": ("nb_circuits", int),
        "NbAromaticsSSSR": ("nb_aromatic_sssr", int),
        "NbAcceptorH": ("nb_acceptor_h", int),
        "NbF": ("nb_f", int),
        "Ui": ("ui", dec(2)),
        "NbO": ("nb_o", int),
        "NbCl": ("nb_cl", int),
        "NbBonds": ("nb_bonds", int),
        "LogP": ("a_log_p", dec(2)),
        "RandicIndex": ("randic_index", dec(2)),
        "NbBondsNonH": ("nb_bonds_non_h", int),
        "NbAromaticsEther": ("nb_aromatic_ether", int),
        "NbChiralCenters": ("nb_chiral_centers", int),
        "NbBenzLikeRings": ("nb_benzene_like_rings", int),
        "RotatableBondFraction": ("rotatable_bond_fraction", dec(2)),
        "LogD": ("log_d", dec(2)),
        "WienerIndex": ("wiener_index", int),
        "NbN": ("nb_n", int),
        "NbC": ("nb_c", int),
        "NbAtom": ("nb_atom", int),
        "NbAromaticsBonds": ("nb_aromatic_bonds", int),
        "MeanAtomVolVdW": ("mean_atom_vol_vdw", dec(2)),
        "AromaticRatio": ("aromatic_ratio", dec(2)),
        "NbAtomNonH": ("nb_atom_non_h", int),
        "NbDonorH": ("nb_donor_h", int),
        "NbI": ("nb_i", int),
        "NbRotatableBonds": ("nb_rotatable_bonds", int),
        "NbRings": ("nb_rings", int),
        "NbCsp2": ("nb_csp2", int),
        "NbCsp3": ("nb_csp3", int),
        "NbBr": ("nb_br", int),
        "GCMolarRefractivity": ("gc_molar_refractivity", dec(2)),
        "NbAliphaticsAmines": ("nb_aliphatic_amines", int),
    }
    ippidb_convs = {value[0]: value[1] for key, value in property_mapping.items()}
    ippidb_convs["id"] = int
    output_ids = []
    for cid, item in pc_properties_dict.items():
        compound = Compound.objects.get(id=cid)
        duplicate_compounds = Compound.objects.filter(
            canonical_smile=item["CanonicalSmile"]
        )
        if len(duplicate_compounds) > 0:
            duplicate_compound = duplicate_compounds[0]
            if task is not None:
                task.write(
                    f"Replacing references to compound {compound.id}"
                    f" with existing and validated compound {duplicate_compound.id}"
                )
            compound.replace_compound_references(duplicate_compound)
            compound = duplicate_compound
        updated_properties = {}
        for galaxy_prop, prop in property_mapping.items():
            ippidb_prop = prop[0]
            ippidb_conv = prop[1]
            try:
                updated_properties[ippidb_prop] = ippidb_conv(item[galaxy_prop])
            except ValueError as ve:
                if task is not None:
                    task.write(
                        f"Error setting property {ippidb_prop} to {item[galaxy_prop]}"
                        f" in compound {compound.id} \ndetails:{ve}"
                    )
        for key, value in updated_properties.items():
            setattr(compound, key, value)
        compound.compute_fsp3()
        try:
            compound.save()
        except Error as e:
            if task is not None:
                task.write(f"Error saving compound {compound.id}")
            raise e
        output_ids.append(compound.id)
    return output_ids


def compute_drugbank_similarity(qs: QuerySet):
    """
    compute the Tanimoto similarity with Drugbank entries
    for each compound of a queryset

    :param qs: queryset over the compounds to be processed
    :type qs: QuerySet
    """

    for c in qs:
        c.save(autofill=True)


def validate(compound_ids: List[int]) -> List[int]:
    """
    validate a list of compounds

    :param compound_ids: The list of compound IDs to validate
    :type compound_ids: List[int]
    :return: List of validated compound IDs
    :rtype: List[int]
    """
    compounds = Compound.objects.filter(id__in=compound_ids)
    for c in compounds:
        for ca in c.compoundaction_set.all():
            for contribution in ca.ppi.contribution_set.filter(validated=False):
                contribution.validated = True
                contribution.save()
                for pdbc in ProteinDomainBoundComplex.objects.filter(
                    ppicomplex__ppi__contribution=contribution
                ):
                    pdbc.set_group()
                for pdpc in ProteinDomainPartnerComplex.objects.filter(
                    ppicomplex__ppi__contribution=contribution
                ):
                    pdpc.set_group()


def generate_protein_domain_complex_groups(apps_for_import=None):
    """
    Generate Protein Domain Complex groups
    for all validated contributions
    and pre-contribution PPIs
    """
    if apps_for_import is not None:
        ProteinDomainBoundComplex = apps_for_import.get_model(
            "ippidb", "ProteinDomainBoundComplex"
        )
        ProteinDomainPartnerComplex = apps_for_import.get_model(
            "ippidb", "ProteinDomainPartnerComplex"
        )
    ProteinDomainBoundComplexGroup.objects.all().delete()
    ProteinDomainPartnerComplexGroup.objects.all().delete()
    for contribution in Contribution.objects.filter(validated=True):
        for pdbc in ProteinDomainBoundComplex.objects.filter(
            ppicomplex__ppi__contribution=contribution
        ):
            pdbc.set_group()
            pdbc.save()
        for pdpc in ProteinDomainPartnerComplex.objects.filter(
            ppicomplex__ppi__contribution=contribution
        ):
            pdpc.set_group()
            pdpc.save()
    for pdbc in ProteinDomainBoundComplex.objects.filter(
        ppicomplex__ppi__contribution=None
    ):
        pdbc.set_group()
        pdbc.save()
    for pdpc in ProteinDomainPartnerComplex.objects.filter(
        ppicomplex__ppi__contribution=None
    ):
        pdpc.set_group()
        pdpc.save()


def generate_le_lle_plot():
    """
    Generate the LE-LLE plot
    for all validated compounds
    """
    print("Generating the LE vs. LLE biplot...")
    le_lle_data = []
    LeLleBiplotData.objects.all().delete()
    print("Successfully flushed LE-LLE biplot data")
    for comp in Compound.objects.validated():
        if comp.le is not None:
            le = round(comp.le, 7)
            lle = round(comp.lle, 7)
            le_lle_data.append(
                {
                    "x": le,
                    "y": lle,
                    "id": comp.id,
                    "family_name": comp.best_activity_ppi_family_name,
                    "smiles": comp.canonical_smile,
                }
            )
        else:
            print("compound %s has no LE" % comp.id)
    le_lle_json = json.dumps(le_lle_data, separators=(",", ":"))
    new = LeLleBiplotData()
    new.le_lle_biplot_data = le_lle_json
    new.save()
    print("Successfully generated LE-LLE biplot data")


def plot_circle():
    """
    Generate a circle for the PCA correlation circle
    (utility function for `generate_pca_plot` function)
    """
    theta = np.linspace(0, 2 * np.pi, 100)
    r = np.sqrt(1.0)
    x1 = r * np.cos(theta)
    x2 = r * np.sin(theta)
    return x1, x2


def generate_pca_plot():
    """
    Generate the PCA plot
    for all validated compounds
    """
    print("Generating the PCA biplot...")
    pca_data = []
    features = [
        "molecular_weight",
        "a_log_p",
        "nb_donor_h",
        "nb_acceptor_h",
        "tpsa",
        "nb_rotatable_bonds",
        "nb_benzene_like_rings",
        "fsp3",
        "nb_chiral_centers",
        "nb_csp3",
        "nb_atom",
        "nb_bonds",
        "nb_atom_non_h",
        "nb_rings",
        "nb_multiple_bonds",
        "nb_aromatic_bonds",
        "aromatic_ratio",
    ]
    PcaBiplotData.objects.all().delete()
    print("Successfully flushed PCA biplot data")
    values_list = []
    for comp in Compound.objects.validated():
        values = model_to_dict(comp, fields=features + ["id", "family"])
        values["family"] = comp.best_activity_ppi_family_name
        values_list.append(values)
    df = pd.DataFrame(values_list)
    # drop compounds with undefined property values
    df.dropna(how="any", inplace=True)
    # reset index so that it can be joined with PCA results
    df.reset_index(inplace=True)
    # prepare correlation circle figure
    plt.switch_backend("Agg")
    fig_, ax = plt.subplots(figsize=(6, 6))
    x1, x2 = plot_circle()
    plt.plot(x1, x2)
    ax.set_aspect(1)
    ax.yaxis.set_label_coords(-0.1, 0.5)
    ax.xaxis.set_label_coords(0.5, -0.08)
    # do not process the data unless there are compounds in the dataframe
    if len(df.index) > 0:
        x = df.loc[:, features].values
        y = df.loc[:, ["family"]].values
        x = StandardScaler().fit_transform(x)
        n = x.shape[0]  # retrieve number of individuals
        p = x.shape[1]  # retrieve number of variables
        pca = PCA(n_components=p)
        principal_components = pca.fit_transform(x)
        # compute correlation circle
        variance_ratio = pd.Series(pca.explained_variance_ratio_)
        coef = np.transpose(pca.components_)
        cols = ["PC-" + str(x) for x in range(len(variance_ratio))]
        pc_infos = pd.DataFrame(coef, columns=cols, index=features)
        # we might remove the line below if the PCA remains grayscale
        pal = itertools.cycle(sns.color_palette("dark", len(features)))  # noqa: F841
        # compute the length of each feature arrow in the correlation circle
        pc_infos["DIST"] = pc_infos[["PC-0", "PC-1"]].pow(2).sum(1).pow(0.5)
        # store the maximal length for normalization purposes
        best_distance = max(pc_infos["DIST"])
        # compute corvar for the correlation circle
        eigval = (float(n) - 1) / float(n) * pca.explained_variance_
        sqrt_eigval = np.sqrt(eigval)
        sqrt_eigval = np.sqrt(eigval)
        corvar = np.zeros((p, p))
        for k in range(p):
            corvar[:, k] = pca.components_[k, :] * sqrt_eigval[k]
        for idx in range(len(pc_infos["PC-0"])):
            x = corvar[idx, 0]  # use corvar to plot the variable map
            y = corvar[idx, 1]  # use corvar to plot the variable map
            color = "black"
            # alpha is the feature length normalized
            # to the longest feature's length
            alpha = pc_infos["DIST"][idx] / best_distance
            plt.arrow(0.0, 0.0, x, y, head_width=0.02, color="black", alpha=alpha)
            plt.annotate(
                Compound._meta.get_field(pc_infos.index[idx]).verbose_name,
                xy=(x, y),
                xycoords="data",
                xytext=np.asarray((x, y)) + (0.02, -0.02),
                fontsize=6,
                color=color,
                alpha=alpha,
            )
        plt.xlabel("PC-1 (%s%%)" % str(variance_ratio[0])[:4].lstrip("0."))
        plt.ylabel("PC-2 (%s%%)" % str(variance_ratio[1])[:4].lstrip("0."))
        plt.xlim((-1, 1))
        plt.ylim((-1, 1))
        principal_df = pd.DataFrame(data=principal_components)
        # only select the two first dimensions for the plot, and rename them to x and y
        principal_df = principal_df.iloc[:, 0:2]
        principal_df = principal_df.rename(columns={0: "x", 1: "y"})
        final_df = pd.concat([principal_df, df[["family", "id"]]], axis=1)
        final_df.dropna(how="any", inplace=True)
        for index, row in final_df.iterrows():
            smiles = Compound.objects.get(id=row.id).canonical_smile
            pca_data.append(
                {
                    "x": row.x,
                    "y": row.y,
                    "id": row.id,
                    "family_name": row.family,
                    "smiles": smiles,
                }
            )
    else:
        pca_data = []
    # save correlation circle PNG
    my_string_io_bytes = io.BytesIO()
    plt.savefig(my_string_io_bytes, format="png", dpi=600, bbox_inches="tight")
    my_string_io_bytes.seek(0)
    # figdata_png is the correlation circle figure, base 64-encoded
    figdata_png = base64.b64encode(my_string_io_bytes.read())
    pca_data_cc = {
        "data": pca_data,
        "correlation_circle": figdata_png.decode("utf-8"),
    }
    pca_json = json.dumps(pca_data_cc, separators=(",", ":"))
    new = PcaBiplotData()
    new.pca_biplot_data = pca_json
    new.save()
    print("Successfully generated PCA biplot data")


def set_compound_links(compound_ids: List[int]) -> List[int]:
    """
    set/update links with external databases:
    DrugBank, ChEMBL, PubChem
    for each compound of a queryset

    :param qs: queryset over the compounds to be processed
    :type qs: QuerySet
    """
    qs = Compound.objects.filter(id__in=compound_ids)
    for c in qs:
        c.set_drugbank_link()
        c.set_chembl_link()
        c.set_pubchem_link()
        c.set_ligand_link()
        c.save()
    return [c.id for c in qs]


@app.task(base=MonitorTask, bind=True)
def run_compute_compound_properties(self: MonitorTask, compound_id: int) -> int:
    """
    task "run method" to compute the properties for a compound

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    :param compound_id: the ID of the compound
    :type compound_id: int
    :return: the ID of the compound
    :rtype: int
    """
    self.update_state(state=states.STARTED)
    cj = CompoundJob()
    cj.compound = Compound.objects.get(id=compound_id)
    cj.job = Job.objects.get(task_result__task_id=self.task_id)
    cj.save()
    self.write(std_out=f"Starting computation of compound properties for {compound_id}")
    result_compound_ids = compute_compound_properties([compound_id], task=self)
    self.write(std_out=f"Finished computation of compound properties for {compound_id}")
    return result_compound_ids[0]


@app.task(base=MonitorTask, bind=True)
def run_update_compound_cached_properties(
    self: MonitorTask, compound_ids: List[int] = None
) -> List[int]:
    """
    task "run method" to cache the properties for a list of compounds

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    :param compound_ids: the list of compound IDs
    :type compound_id: List[int]
    :return: the list of compound IDs
    :rtype: List[int]
    """
    if compound_ids:
        qs = Compound.objects.filter(id__in=compound_ids)
    else:
        qs = Compound.objects.validated()
    self.update_state(state=states.STARTED)
    self.write(
        std_out=f"Starting caching of compound properties for {compound_ids or 'all compounds'}"
    )
    update_compound_cached_properties(qs)
    self.write(
        std_out=f"Finished caching of compound properties for {compound_ids or 'all compounds'}"
    )
    return compound_ids


@app.task(base=MonitorTask, bind=True)
def run_compute_drugbank_similarity(
    self: MonitorTask, compound_ids: List[int] = None
) -> List[int]:
    """
    task "run method" to compute the drugbank similarity for a list of compounds

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    :param compound_ids: the list of compound IDs
    :type compound_id: List[int]
    :return: the list of compound IDs
    :rtype: List[int]
    """
    if compound_ids:
        qs = Compound.objects.filter(id__in=compound_ids)
    else:
        qs = Compound.objects.validated()
    self.update_state(state=states.STARTED)
    self.write(
        std_out=f"Starting computing Drugbank similarity for {compound_ids or 'all compounds'}"
    )
    compute_drugbank_similarity(qs)
    self.write(
        std_out=f"Finished computing Drugbank similarity for {compound_ids or 'all compounds'}"
    )
    return compound_ids


@app.task(base=MonitorTask, bind=True)
def run_set_compound_links(self: MonitorTask, compound_id: int) -> int:
    """
    task to set/update the DB cross-links for a compound

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    :param compound_id: the ID of the compound
    :type compound_id: int
    :return: the ID of the compound
    :rtype: int
    """
    self.update_state(state=states.STARTED)
    cj = CompoundJob()
    cj.compound = Compound.objects.get(id=compound_id)
    cj.job = Job.objects.get(task_result__task_id=self.task_id)
    cj.save()
    self.write(std_out=f"Starting setting external cross-links for {compound_id}")
    result_compound_ids = set_compound_links([compound_id])
    self.write(std_out=f"Finished setting external cross-links for {compound_id}")
    return result_compound_ids[0]


@app.task(base=MonitorTask, bind=True)
def run_validate(self: MonitorTask, compound_ids: List[int] = None) -> List[int]:
    """
    task "run method" to validate a list of compounds

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    :param compound_ids: the list of compound IDs
    :type compound_id: List[int]
    :return: the list of compound IDs
    :rtype: List[int]
    """
    self.update_state(state=states.STARTED)
    self.write(std_out=f"Starting validation of compounds {compound_ids}")
    validate(compound_ids)
    self.write(std_out=f"Finished validation of compounds {compound_ids}")
    return compound_ids


@app.task(base=MonitorTask, bind=True)
def run_le_lle_plot(self: MonitorTask):
    """
    task "run method" to generate the LE-LLE plot

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    """
    self.update_state(state=states.STARTED)
    self.write(std_out="Starting computing LE-LLE plot")
    generate_le_lle_plot()
    self.write(std_out="Finished computing LE-LLE plot")


@app.task(base=MonitorTask, bind=True)
def run_pca_plot(self: MonitorTask):
    """
    task "run method" to generate the PCA plot

    :param self: the task the function is binded to as a method
    :type self: MonitorTask
    """
    self.update_state(state=states.STARTED)
    self.write(std_out="Starting computing PCA plot")
    generate_pca_plot()
    self.write(std_out="Finished computing PCA plot")


def launch_validate_contributions(contribution_ids: List[int]):
    """
    Launch the task to validate the contributions
    """
    contribution_jobs = []
    for cont in Contribution.objects.filter(id__in=contribution_ids):
        compound_ids = [
            compound_action.compound.id
            for compound_action in cont.ppi.compoundaction_set.all()
        ]
        # build a group of jobs, one job for each compound properties computation
        run_compounds_properties_computation_group = group(
            [
                run_compute_compound_properties.si(compound_id)
                for compound_id in compound_ids
            ]
        )
        # build the "main" job
        compounds_properties_computation_group = chord(
            run_compounds_properties_computation_group,
            chain(
                run_update_compound_cached_properties.s(),
                run_compute_drugbank_similarity.s(),
                run_validate.s(),
            ),
        )
        contribution_jobs.append(compounds_properties_computation_group)
        # compounds_properties_computation_group.delay()
    run_contribution_jobs = group(contribution_jobs)
    workflow = chain(run_contribution_jobs, run_le_lle_plot.si(), run_pca_plot.si())
    workflow.delay()


def launch_update_compound_cached_properties():
    """
    Launch the task to cache the properties on all compounds
    """
    run_update_compound_cached_properties.delay()


def launch_plots_computing():
    """
    Launch the task to perform the computing of LE-LLE and PCA plots.
    """
    generate_le_lle_plot()
    generate_pca_plot()
    workflow = chain(run_le_lle_plot.si(), run_pca_plot.si())
    workflow.delay()


@app.task(base=MonitorTask, bind=True)
def launch_build_matrix_distance(self: MonitorTask, params):
    """
    This task launch only the matrix distances task
    """

    command = build_distances.Command(task=self)
    self.write(std_out="Command:")
    self.write(std_out="build_distances --nb_neighbors={nb_neighbors}".format(**params))
    call_command(
        command,
        "--nb_neighbors={nb_neighbors}".format(**params),
    )


@app.task(base=MonitorTask, bind=True)
def launch_delete_targetcentric(self: MonitorTask):
    """
    This task launch the import_targetcentric command
    """

    command = flush_targetcentric.Command(task=self)
    self.write(std_out="Command:")
    self.write(std_out="flush_targetcentric")
    call_command(command, "")


def launch_set_compound_links():
    """
    Launch the tasks to set/update compound links
    """
    link_jobs_group = group(
        [
            run_set_compound_links.si(compound.id)
            for compound in Compound.objects.validated()
        ]
    )
    link_jobs_group.delay()
