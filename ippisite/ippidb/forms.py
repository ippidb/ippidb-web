"""
iPPI-DB django forms
"""
import itertools
from collections import OrderedDict
from decimal import Decimal
from math import log10

from django import forms
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db.models import Count
from django.db.models.functions import Upper
from django.forms import (
    ModelForm,
    modelformset_factory,
    formset_factory,
    inlineformset_factory,
    widgets,
)
from django.utils.translation import gettext_lazy as _, gettext

from ippidb import ws, models
from ippidb.ws import (
    get_pdb_uniprot_mapping,
    BibliographicalEntryNotFound,
    pdb_entry_exists,
    EntryNotFoundError,
)


class FieldDataList(forms.CharField):
    class Meta:
        abstract = True

    def __init__(
        self, data_class=None, data_attr=None, data_list=None, *args, **kwargs
    ):
        self.data_class = data_class
        self.data_attr = data_attr
        self.data_list = data_list
        super().__init__(*args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        if (
            self.data_class is not None
            and self.data_attr is not None
            and not widget.is_hidden
        ):
            # The HTML element is datalist, not data_list.
            attrs["datalist"] = (
                self.data_class.objects.values_list(self.data_attr, flat=True)
                .order_by(Upper(self.data_attr))
                .distinct()
            )
        if self.data_list is not None and len(self.data_list) > 0:
            attrs["datalist"] = self.data_list
        return attrs


class CharFieldDataList(FieldDataList, forms.CharField):
    pass


class IntegerFieldDataList(FieldDataList, forms.IntegerField):
    pass


class BoostrapSelectMultiple(forms.SelectMultiple):
    def __init__(
        self,
        allSelectedText,
        nonSelectedText,
        SelectedText,
        attrs=None,
        *args,
        **kwargs,
    ):
        if attrs is not None:
            attrs = attrs.copy()
        else:
            attrs = {}
        attrs.update(
            {
                "data-all-selected-text": allSelectedText,
                "data-non-selected-text": nonSelectedText,
                "data-n-selected-text": SelectedText,
                "class": "bootstrap-multiselect-applicant",
                "style": "display:none;",
            }
        )
        super().__init__(attrs=attrs, *args, **kwargs)


""" Step 1 : IdForm """


class IdForm(forms.Form):
    source = forms.ChoiceField(
        label="Bibliographic type",
        choices=models.Bibliography.SOURCES,
        initial="PM",
        widget=forms.RadioSelect,
    )
    id_source = forms.CharField(label="Bibliographic ID", max_length=255)
    allow_duplicate = forms.BooleanField(
        label=_("IdForm_allow_duplicate_label"),
        help_text=_("IdForm_allow_duplicate_help_text"),
        initial=False,
        required=False,
    )

    def __init__(self, request, display_allow_duplicate=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.display_allow_duplicate = display_allow_duplicate
        self.request = request
        if not display_allow_duplicate:
            del self.fields["allow_duplicate"]

    def clean(self):
        ret = super().clean()
        entries = (
            models.Bibliography.objects.filter(id_source=ret["id_source"])
            .filter(source=ret["source"])
            .annotate(used=Count("refcompoundbiblio"))
        )
        if entries.exists() > 0:
            if entries.filter(used=0).exists():
                # it's ok, allow to document it
                messages.debug(
                    self.request, _("IdForm_allow_duplicate_resume_contribution")
                )
            elif not self.display_allow_duplicate:
                raise ValidationError(
                    {"id_source": _("IdForm_allow_duplicate_false_validation_error")}
                )
            else:
                # it's ok, let's create a new one, and display a message
                models.Bibliography.objects.create(
                    source=self.cleaned_data["source"],
                    id_source=self.cleaned_data["id_source"],
                )
                messages.info(
                    self.request, _("IdForm_allow_duplicate_new_contribution")
                )
        else:
            models.Bibliography.validate_source_id(ret["id_source"], ret["source"])
            bibliography = models.Bibliography(
                source=self.cleaned_data["source"],
                id_source=self.cleaned_data["id_source"],
            )
            try:
                bibliography.autofill()
            except BibliographicalEntryNotFound as e:
                raise ValidationError({"id_source": str(e)})
            except Exception:
                raise ValidationError(
                    {
                        "id_source": f"Technical error, unable to retrieve data for {bibliography.id_source}."
                    }
                )
        return ret

    def get_instance(self):
        return (
            models.Bibliography.objects.filter(id_source=self.cleaned_data["id_source"])
            .filter(source=self.cleaned_data["source"])
            .annotate(used=Count("refcompoundbiblio"))
            .filter(used=0)
            .first()
        )


""" Step 2 : BibliographyForm """


class BibliographyForm(ModelForm):
    class Meta:
        model = models.Bibliography
        fields = "__all__"
        widgets = {
            "source": forms.HiddenInput(),
            "biblio_year": forms.HiddenInput(),
            "id_source": forms.HiddenInput(),
            "title": forms.HiddenInput(),
            "journal_name": forms.HiddenInput(),
            "authors_list": forms.HiddenInput(),
        }


""" Step 3 : PDBForm """


def validate_pdb_exists(value):
    if not (pdb_entry_exists(value)):
        raise ValidationError(
            "PDB entry not found: %(value)s", params={"value": value}, code="not_found"
        )


def validate_pdb_uniprot_mapping_exists(value):
    try:
        get_pdb_uniprot_mapping(value)
    except EntryNotFoundError:
        raise ValidationError(
            "No Uniprot entries for this PDB entry: %(value)s",
            params={"value": value},
            code="no_mapping",
        )


class PDBForm(forms.Form):
    pdb_id = forms.CharField(
        label="PDB ID",
        max_length=4,
        widget=forms.TextInput(
            attrs={"placeholder": "e.g 3u85", "required": "required"}
        ),
        required=True,
        validators=[
            RegexValidator(
                "^[0-9][a-zA-Z0-9]{3}$",
                message="PDB ID must be 1 numeric + 3 alphanumeric characters",
            ),
            validate_pdb_exists,
            validate_pdb_uniprot_mapping_exists,
        ],
    )

    def get_proteins(self):
        pdb_id = self.cleaned_data["pdb_id"]
        protein_ids = []
        uniprot_ids = get_pdb_uniprot_mapping(pdb_id)
        for uniprot_id in uniprot_ids:
            p, created = models.Protein.objects.get_or_create(uniprot_id=uniprot_id)
            protein_ids.append(p.id)
        return protein_ids


class ProteinForm(ModelForm):
    class Meta:
        model = models.Protein
        exclude = ["recommended_name_long", "short_name"]
        widgets = {
            "uniprot_id": forms.TextInput(
                attrs={"readonly": "readonly", "class": "readonly"}
            ),
            "gene_name": forms.TextInput(
                attrs={"readonly": "readonly", "class": "readonly"}
            ),
            "entry_name": forms.TextInput(
                attrs={"readonly": "readonly", "class": "readonly"}
            ),
            "organism": forms.TextInput(
                attrs={"readonly": "readonly", "class": "readonly"}
            ),
        }


ProteinFormSet = modelformset_factory(
    models.Protein, exclude=("recommended_name_long", "short_name"), extra=0
)

""" Step 4 : ProteinDomainComplexTypeForm aka. Architecture """
TYPE_COMPLEX = (("inhibited", "Inhibited"), ("stabilized", "Stabilized"))

TYPE_CHOICES = (
    ("Inhib_Hetero2merAB", "Inhib_Hetero 2-mer AB"),
    ("Inhib_Homo2merA2", "Inhib_Homo 2-mer A2"),
    ("Inhib_Custom", "Inhib_Custom"),
    ("Stab_Hetero2merAB", "Stab_Hetero 2-mer AB"),
    ("Stab_Homo2merA2", "Stab_Homo 2-mer A2"),
    ("Stab_HomoLike2mer", "Stab_Homo-Like 2-mer A2"),
    ("Stab_Homo3merA3", "Stab_Homo 3-mer A3"),
    ("Stab_Homo3merA2", "Stab_Homo 3-mer A3 inhibited A2-dimer"),
    ("Stab_Homo4merA4", "Stab_Homo 4-mer A4"),
    ("Stab_RingHomo3mer", "Stab_Ring-Like 3-mer A3"),
    ("Stab_RingHomo5mer", "Stab_Ring-Like 5-mer A5"),
    ("Stab_Custom", "Stab_Custom"),
)


class ProteinDomainComplexTypeForm(forms.Form):
    complexChoice = forms.CharField(
        label="PPI Complex Type", widget=forms.Select(choices=TYPE_COMPLEX)
    )
    complexType = forms.CharField(widget=forms.RadioSelect(choices=TYPE_CHOICES))


""" Step 5 : ProteinDomainComplexForm aka. Composition """


class ProteinDomainComplexForm(ModelForm):
    class Meta:
        model = models.ProteinDomainComplex
        fields = ["protein", "domain", "ppc_copy_nb"]


class ComplexCompositionForm(forms.Form):
    complex_type = forms.CharField(
        widget=forms.Select(
            choices=(), attrs={"class": "complex_readonly", "readonly": "readonly"}
        )
    )
    protein = forms.ModelChoiceField(
        queryset=models.Protein.objects.none(),
        required=True,
        widget=forms.Select(attrs={"class": "form-control"}),
        empty_label=None,
    )
    domain = forms.ModelChoiceField(
        queryset=models.Domain.objects.none(),
        required=False,
        widget=forms.Select(attrs={"class": "form-control"}),
        empty_label="Unknown",
    )
    # ppc_copy_nb is Complex>NbcopyP in the xlsx file of #33
    ppc_copy_nb = IntegerFieldDataList(
        label="Number of copies of the protein in the complex", required=True
    )
    # cc_nb is PPI>NbCopyComplex in the xlsx file of #33
    cc_nb = IntegerFieldDataList(label=_("cc_nb_verbose_name"), required=True)
    # ppp_copy_nb_per_p is Complex>NbPperPocket in the xlsx file of #33
    ppp_copy_nb_per_p = IntegerFieldDataList(
        label=_("ppp_copy_nb_per_p"),
        required=False,
        widget=forms.NumberInput(
            attrs={"class": "bound-complex-only", "data-required": True}
        ),
        # validators=[
        #     MinValueValidator(1),
        # ],
    )

    def __init__(
        self,
        protein_ids=None,
        has_bound=False,
        has_partner=False,
        nb_copy_bound=None,
        nb_per_pocket=None,
        nb_copy_partner=None,
        nb_copies_in_complex=None,
        *args,
        **kwargs,
    ):
        super(ComplexCompositionForm, self).__init__(*args, **kwargs)
        if protein_ids is not None:
            self.fields["protein"].queryset = models.Protein.objects.filter(
                pk__in=protein_ids
            )
            self.fields["domain"].queryset = models.Domain.objects.filter(
                protein__in=protein_ids
            )
        choices = []
        if has_partner:
            choices.append(("Partner", "Partner complex"))
        if has_bound:
            choices.append(("Bound", "Bound complex"))

        nb_copy = None
        if nb_copy_bound is not None:
            nb_copy = nb_copy_bound
        if nb_copy_partner is not None:
            if nb_copy is None:
                nb_copy = nb_copy_partner
            else:
                nb_copy = itertools.chain(nb_copy, nb_copy_partner)
        nb_copy = set(nb_copy)
        self.fields["ppc_copy_nb"].widget.attrs["datalist"] = set(nb_copy)
        if len(nb_copy) == 1:
            self.fields["ppc_copy_nb"].initial = next(iter(nb_copy))
            self.fields["ppc_copy_nb"].widget = forms.HiddenInput()

        self.fields["ppp_copy_nb_per_p"].widget.attrs["datalist"] = nb_per_pocket
        if len(nb_per_pocket) == 1:
            self.fields["ppp_copy_nb_per_p"].initial = next(iter(nb_per_pocket))
            self.fields["ppp_copy_nb_per_p"].widget = forms.HiddenInput()

        self.fields["cc_nb"].widget.attrs["datalist"] = nb_copies_in_complex
        if len(nb_copies_in_complex) == 1:
            self.fields["cc_nb"].initial = next(iter(nb_copies_in_complex))
            self.fields["cc_nb"].widget = forms.HiddenInput()

        self.fields["complex_type"].widget.choices = choices

    def full_clean(self):
        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        if (
            self.cleaned_data.get("complex_type", "") == "Bound"
            and self.cleaned_data.get("ppp_copy_nb_per_p", None) is None
        ):
            self.add_error("ppp_copy_nb_per_p", _("This field is required"))


class ComplexCompositionBaseFormSet(forms.BaseFormSet):
    def full_clean(self):
        """
        Ensure that the formset is sound, i.e that we have enough bound and partner
        """
        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        complex_type_dict = set()
        for form in self.forms:
            if form.cleaned_data.get("DELETE", False):
                continue
            complex_type_dict.add(form.cleaned_data.get("complex_type"))

    def __init__(
        self,
        has_bound=False,
        has_partner=False,
        form_kwargs=None,
        nb_copy_bound=None,
        nb_per_pocket=None,
        nb_copy_partner=None,
        nb_copies_in_complex=None,
        *args,
        **kwargs,
    ):
        form_kwargs = form_kwargs or {}
        form_kwargs["has_bound"] = has_bound
        form_kwargs["has_partner"] = has_partner
        form_kwargs["nb_copy_bound"] = nb_copy_bound
        form_kwargs["nb_copy_partner"] = nb_copy_partner
        form_kwargs["nb_per_pocket"] = nb_per_pocket
        form_kwargs["nb_copies_in_complex"] = nb_copies_in_complex
        self.has_partner = has_partner
        self.has_bound = has_bound
        super().__init__(form_kwargs=form_kwargs, *args, **kwargs)


ComplexCompositionFormSet = formset_factory(
    form=ComplexCompositionForm, formset=ComplexCompositionBaseFormSet, extra=0
)

""" Step 6 : PpiForm """


class PpiModelForm(ModelForm):
    pockets_nb = IntegerFieldDataList(
        label=_("Total number of pockets in the complex"), required=True
    )
    family_name = CharFieldDataList(
        data_class=models.PpiFamily,
        data_list=[],
        data_attr="name",
        label="PPI Family",
        max_length=30,
        required=True,
    )
    ols_diseases = forms.CharField(
        label=_("Search for associated diseases"),
        required=False,
        widget=forms.TextInput(
            attrs={
                "data-olswidget": "select",
                "data-suggest-header": _("Associate to"),
                "data-olsontology": "mondo",
                "data-selectpath": "https://www.ebi.ac.uk/ols/",
                "olstype": "",
                "class": "",
            }
        ),
    )
    selected_diseases = forms.CharField(
        label=_("Associated diseases"),
        widget=forms.Textarea(attrs={"class": "d-none"}),
        required=False,
    )

    class Meta:
        model = models.Ppi
        fields = (
            "id",
            "pdb_id",
            "family",
            "family_name",
            "symmetry",
            "pockets_nb",  # pockets_nb is Ppi.pockets_nb in the xlsx file of #33
            "ols_diseases",
            "selected_diseases",
        )
        widgets = {
            "id": forms.HiddenInput(),
            "family": forms.HiddenInput(),
            "diseases": BoostrapSelectMultiple(
                allSelectedText=_("All diseases used"),
                nonSelectedText=_(""),
                SelectedText=_(" diseases selected"),
            ),
        }

    def __init__(self, symmetry=None, nb_pockets=None, initial=None, *args, **kwargs):
        super().__init__(initial=initial, *args, **kwargs)
        self.fields["pdb_id"].widget.attrs["readonly"] = True
        self.fields["symmetry"].empty_choice = None
        if nb_pockets is not None:
            self.fields["pockets_nb"].widget.attrs["datalist"] = nb_pockets
            if len(nb_pockets) == 1:
                self.fields["pockets_nb"].initial = next(iter(nb_pockets))
                self.fields["pockets_nb"].widget = forms.HiddenInput()
        if symmetry is not None:
            self.fields["symmetry"].queryset = symmetry
            if symmetry.count() == 1:
                self.fields["symmetry"].initial = symmetry.first()
                self.fields["symmetry"].widget = forms.HiddenInput()

    def full_clean(self):
        super().full_clean()
        if hasattr(self, "cleaned_data") and "family_name" in self.cleaned_data:
            family, created = models.PpiFamily.objects.get_or_create(
                name=self.cleaned_data["family_name"]
            )
            self.cleaned_data["family"] = family.pk

    def save(self, commit=True):
        super().save(commit=commit)
        if not commit:
            return self.instance
        for new_disease in self.cleaned_data["selected_diseases"].split("\n"):
            new_disease = new_disease.strip()
            if len(new_disease) == 0:
                continue
            new_disease = new_disease.split(";")
            if len(new_disease) > 1:
                name = new_disease[1]
                identifier = new_disease[2]
            else:
                name = new_disease[0]
                identifier = None
            disease, created = models.Disease.objects.get_or_create(
                name=name, identifier=identifier
            )
            self.instance.diseases.add(disease)
        self.instance.family = models.PpiFamily.objects.get(
            id=self.cleaned_data["family"]
        )
        self.instance.save(autofill=True)
        return self.instance


""" Step 7 : CompoundForm """
TYPE_MOLECULE = (("smiles", "smiles"), ("iupac", "iupac"), ("sketch", "sketch"))


class CompoundForm(forms.Form):
    compound_name = forms.CharField(
        label=_("compound_name_label"),
        required=True,
        help_text=_(""),
    )
    ligand_id = forms.CharField(
        label=_("PDB Ligand ID"),
        max_length=3,  # from models.Compound.ligand_id
        required=False,  # from models.Compound.ligand_id
    )
    molecule_smiles = forms.CharField(
        label=_("molecule_smiles_label"),
        help_text=_(""),
        required=False,
        widget=forms.Textarea(
            attrs={
                "class": "molecule-composition",
                "rows": "9",
                "oninput": "showCanvas(this)",
            }
        ),
    )
    molecule_iupac = forms.CharField(
        label=_("molecule_iupac_label"),
        help_text=_(""),
        required=False,
        widget=forms.Textarea(attrs={"class": "molecule-composition", "rows": "9"}),
    )

    common_name = forms.CharField(label="Common Name", max_length=20, required=False)
    is_macrocycle = forms.BooleanField(
        label=_("is_macrocycle_verbose_name"), required=False
    )

    def full_clean(self):
        super().full_clean()
        if (
            not self.is_bound or self.cleaned_data.get("DELETE") is True
        ):  # Stop further processing.
            return
        smiles = None
        if (
            self.cleaned_data.get("molecule_smiles", "") == ""
            and self.cleaned_data.get("molecule_iupac", "") == ""
            or self.cleaned_data.get("molecule_smiles", "") != ""
            and self.cleaned_data.get("molecule_iupac", "") != ""
        ):
            self.add_error(
                "molecule_smiles",
                "You have to provide either its IUPAC or its smiles, but not both",
            )
            self.add_error(
                "molecule_iupac",
                "You have to provide either its IUPAC or its smiles, but not both",
            )
        if self.cleaned_data.get("compound_name", "") == "":
            self.add_error("compound_name", "You have to provide it")
        molecule_iupac = self.cleaned_data.get("molecule_iupac", "")
        if (
            len(molecule_iupac) > 0
            and not models.Compound.objects.filter(iupac_name=molecule_iupac).exists()
        ):
            try:
                self.cleaned_data["computed_smiles"] = ws.convert_iupac_to_smiles(
                    self.cleaned_data["molecule_iupac"]
                )
                smiles = self.cleaned_data["computed_smiles"]
            except ws.EntryNotFoundError as e:
                self.add_error("molecule_iupac", str(e))
        else:
            smiles = self.cleaned_data["molecule_smiles"]

        if smiles is not None:
            ligand_id = self.cleaned_data.get("ligand_id", None)
            c_smiles = models.Compound.objects.filter(canonical_smile=smiles).first()
            c_ligand = (
                models.Compound.objects.filter(ligand_id=ligand_id).first()
                if ligand_id is not None
                else None
            )
            if (
                c_smiles is not None
                and c_smiles.ligand_id is not None
                and ligand_id is not None
                and ligand_id != c_smiles.ligand_id
            ):
                self.add_error(
                    "ligand_id",
                    "The PDB Ligand ID differ from the one already known (%s)"
                    % c_smiles.ligand_id,
                )
            if c_ligand is not None and smiles != c_ligand.canonical_smile:
                self.add_error(
                    "ligand_id",
                    "The PDB Ligand ID you provided is associated to an other "
                    "canonical smiles: %s" % c_ligand.canonical_smile,
                )


class CompoundBaseFormSet(forms.BaseFormSet):
    def clean(self):
        """Checks that two compounds do not have the same compound_name (the name in the publication)."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        compound_names = set()
        common_names = set()
        smiles_set = set()
        iupac_set = set()
        ligand_set = set()  # noqa: F841 FIXME
        for form in self.forms:
            if form.cleaned_data.get("DELETE", False):
                continue
            compound_name = form.cleaned_data["compound_name"]
            if compound_name in compound_names:
                raise forms.ValidationError(
                    _(
                        "Compound must have distinct compound_name_label as (explanation). Incriminated value:'%s'"
                    )
                    % compound_name
                )
            compound_names.add(compound_name)
            common_name = form.cleaned_data.get("common_name", "")
            if common_name != "" and common_name in common_names:
                raise forms.ValidationError(
                    _(
                        "Compound must have distinct common_name_label as (explanation). Incriminated value:'%s'"
                    )
                    % common_name
                )
            common_names.add(common_name)

            smiles = form.cleaned_data["molecule_smiles"]
            if len(smiles) > 0:
                if smiles in smiles_set:
                    raise forms.ValidationError(
                        _("Compound must have distinct smiles. Incriminated value:'%s'")
                        % smiles
                    )
                smiles_set.add(smiles)

            iupac = form.cleaned_data["molecule_iupac"]
            if len(iupac) > 0:
                if iupac in iupac_set:
                    raise forms.ValidationError(
                        _("Compound must have distinct iupac. Incriminated value:'%s'")
                        % iupac
                    )
                iupac_set.add(iupac)

    def __init__(self, show_is_a_ligand=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.show_is_a_ligand = show_is_a_ligand

    def add_fields(self, form, index):
        super().add_fields(form, index)
        if not self.show_is_a_ligand:
            del form.fields["ligand_id"]


def compound_formset_factory(min_num: int = None, max_num: int = None):
    return formset_factory(
        form=CompoundForm,
        formset=CompoundBaseFormSet,
        can_delete=True,
        extra=0,
        min_num=min_num,
        validate_min=min_num is not None,
        max_num=max_num,
        validate_max=max_num is not None,
    )


CompoundFormSet = compound_formset_factory(min_num=1)

""" Step 8.0 : Toolkit for inlined formet """


class BaseInlineNestedFormSet(forms.BaseInlineFormSet):
    def add_fields(self, form, index):
        super().add_fields(form, index)
        delete_field = form.fields.pop("DELETE")
        delete_field.widget.attrs["onclick"] = "delete_button_clicked(this)"
        delete_field.widget.attrs["class"] = (
            delete_field.widget.attrs.get("class", "") + " formset-item-delete"
        )
        delete_field.label = _("DELETE_label")
        delete_help_text = gettext("DELETE_help_text")
        if delete_help_text != "DELETE_help_text":
            delete_field.help_text = delete_help_text
        form.fields = OrderedDict(
            itertools.chain([("DELETE", delete_field)], form.fields.items())
        )

    def is_valid(self):
        """
        Check is the forms are valid, but also all their nested forms.
        :return:
        """
        result = super().is_valid()
        if self.is_bound:
            for form in self.forms:
                if self.can_delete and self._should_delete_form(form):
                    # This form is going to be deleted so any of its errors
                    # shouldn't cause the entire formset to be invalid.
                    continue
                result = result and form.is_valid()
                if hasattr(form, "nested"):
                    result = result and form.nested.is_valid()

        return result

    def save_new(self, form, commit=True):
        # Ensure the latest copy of the related instance is present on each
        # form (it may have been saved after the formset was originally
        # instantiated).
        if isinstance(self.instance, dict):
            # if the instance is a dict, we override the normal behavior and set form attributes from the keys with
            # their associated value
            for k, v in self.instance.items():
                setattr(form.instance, k, v)
        else:
            setattr(form.instance, self.fk.name, self.instance)
        # Use commit=False so we can assign the parent key afterwards, then
        # save the object.
        obj = form.save(commit=False)
        if isinstance(self.instance, dict):
            # if the instance is a dict, we override the normal behavior and set attributes of the newly created objects
            # from the keys with their associated value
            for k, v in self.instance.items():
                setattr(obj, k, v)
        else:
            pk_value = getattr(self.instance, self.fk.remote_field.field_name)
            setattr(obj, self.fk.get_attname(), getattr(pk_value, "pk", pk_value))
        if commit:
            obj.save()
        # form.save_m2m() can be called via the formset later on if commit=False
        if commit and hasattr(form, "save_m2m"):
            form.save_m2m()
        return obj

    def save(self, commit=True):
        result = super().save(commit=commit)

        for form in self.forms:
            if hasattr(form, "nested"):
                if not self._should_delete_form(form):
                    form.nested.save(commit=commit)
        return result


""" Step 8.1 : TestActivityDescriptionForm """


class CompoundActivityResultForm(ModelForm):
    compound_name = forms.ChoiceField(choices=(), required=True)
    activity_mol = forms.DecimalField(
        label="Activity",
        required=True,
        max_digits=15,
        decimal_places=10,
        min_value=0,
    )
    activity_unit = forms.CharField(
        label="Activity unit",
        max_length=5,
        required=False,
        widget=widgets.Select(
            choices=(
                (None, "-----"),
                ("1", "mol"),
                ("1e-3", "mmol"),
                ("1e-6", "µmol"),
                ("1e-9", "nmol"),
                ("1e-12", "pmol"),
            ),
        ),
        help_text="Only required if 'activity type' is not Kd ratio.",
    )

    class Meta:
        model = models.CompoundActivityResult
        fields = (
            "compound_name",
            "modulation_type",
            "activity",
            "activity_type",
            "activity_mol",
            "activity_unit",
        )
        widgets = {
            "activity": widgets.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in self.fields.values():
            f.widget.attrs.update({"class": "col-3"})

    def has_changed(self):
        """
        Test if the form has changed, we consider that it has not changed if it is not linked to an actual instance and
        the only change is that the DELETE checkbox have been set to True
        :return:
        """
        # If only the delete button was checked, and there is no id, it has not changed and thus should be ignored.
        if (
            len(self.changed_data) == 1
            and self.changed_data[0] == "DELETE"
            and self.data.get("id", None) is None
        ):
            return False
        return super().has_changed()

    def clean(self):
        cleaned_data = super().clean()
        if "activity_mol" not in cleaned_data:
            self.add_error("activity_mol", "Must be provided")
            return cleaned_data
        if cleaned_data["activity_mol"] == 0:
            self.add_error("activity_mol", "Must be greater than 0")
            return cleaned_data
        if cleaned_data["activity_mol"] is None:
            return cleaned_data
        if (
            cleaned_data.get("activity_unit", "") == ""
            and cleaned_data.get("activity_type", None) != "KdRat"
        ):
            self.add_error("activity_unit", "Unit is required if type is not Kd ratio")
            return cleaned_data
        try:
            if self.cleaned_data["activity_type"] != "KdRat":
                d = Decimal(
                    -log10(
                        Decimal(self.cleaned_data["activity_mol"])
                        * Decimal(self.cleaned_data["activity_unit"])
                    )
                )
                d = d.quantize(
                    Decimal(10)
                    ** -self.instance._meta.get_field("activity").decimal_places
                )
                self.cleaned_data["activity"] = d
            else:
                self.cleaned_data["activity"] = self.cleaned_data["activity_mol"]
        except Exception as e:
            self.add_error(
                "activity_mol",
                "Unknown error when computing -log10(activity): %s" % str(e),
            )
            self.add_error(
                "activity_unit",
                "Unknown error when computing -log10(activity): %s" % str(e),
            )
        return cleaned_data

    def save(self, commit=True):
        # right before an actual save, we set the foreign key that have been created in the meantime from unique
        # identifier (compound_name) we where provided at the initialization of the form.
        if hasattr(self, "cleaned_data"):
            if "compound_name" in self.cleaned_data:
                try:
                    self.instance.compound = self.compounds[
                        self.cleaned_data["compound_name"]
                    ]
                except KeyError:
                    pass
        return super().save(commit=commit)


class CompoundActivityResultBaseInlineNestedFormSet(BaseInlineNestedFormSet):
    __compound_names = []
    # pIC50, pEC50, etc. activity types are labelled below as IC50, EC50, etc.
    # specifically because the user enters them in these forms as the former
    # value but they are converted to and stored in the latter.
    __activity_types = [
        ("pIC50", "IC50 (half maximal inhibitory concentration)"),
        ("pEC50", "EC50 (half maximal effective concentration)"),
        ("pKd", "Kd (dissociation constant)"),
        ("pKi", "Ki (inhibition constant)"),
        ("KdRat", "Kd ratio (Kd w/o ligand / Kd with ligand"),
    ]

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["compound_name"].choices = self.__compound_names
        form.fields["activity_type"].choices = self.__activity_types

    def set_modulation_type(self, modulation_type):
        for form in self.forms:
            form.fields["modulation_type"].choices = [
                (key, msg)
                for key, msg in form.fields["modulation_type"].choices
                if key == modulation_type
            ]

    def set_compound_names(self, compound_names):
        """
        Set the choices of the field compound_name to the compound_names provided
        :param compound_names:
        :return:
        """
        self.__compound_names = list(
            itertools.zip_longest(compound_names, compound_names)
        )

    def set_compounds(self, compounds):
        """
        Provided to the form the compounds dictionary received from the wizard

        :param complexes:
        :return:
        """
        for form in self.forms:
            form.compounds = compounds

    def full_clean(self):
        """
        Validate the unique_together = ( ('compound', 'test_activity_description', 'activity_type'),)
         of CompoundActivityResult
        """

        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        names = set()
        for form in self.forms:
            if form.cleaned_data.get("DELETE", False):
                continue

            # if the form is empty
            if not (
                form.has_changed()
                and "compound_name" in form.cleaned_data
                and "activity_type" in form.cleaned_data
            ):
                continue

            name = (
                form.cleaned_data["compound_name"]
                + "__"
                + form.cleaned_data["activity_type"]
            )
            if name in names:
                message = _(
                    "At most one result per pair of compound and activity_type is accepted. "
                    "Incriminated pair :<'%(compound_name)s', '%(activity_type)s'>"
                ) % dict(
                    compound_name=form.cleaned_data["compound_name"],
                    activity_type=form.cleaned_data["activity_type"],
                )
                self._non_form_errors.append(message)
                form.errors.setdefault("compound_name", []).append(message)
                form.errors.setdefault("activity_type", []).append(message)
            names.add(name)


CompoundActivityResultInlineFormset = inlineformset_factory(
    parent_model=models.TestActivityDescription,
    model=models.CompoundActivityResult,
    form=CompoundActivityResultForm,
    formset=CompoundActivityResultBaseInlineNestedFormSet,
    extra=1,
    can_delete=True,
)


class TestActivityDescriptionForm(forms.ModelForm):

    test_name = CharFieldDataList(
        data_class=models.TestActivityDescription,
        data_attr="test_name",
    )

    cell_line_name = CharFieldDataList(
        data_class=models.CellLine,
        data_attr="name",
        label=_("Cell line"),
        required=False,
    )

    class Meta:
        model = models.TestActivityDescription
        fields = "__all__"
        exclude = ("cell_line", "protein_domain_bound_complex", "biblio")
        widgets = {
            "protein_domain_bound_complex": forms.HiddenInput(),
            "biblio": forms.HiddenInput(),
            "cell_line": forms.HiddenInput(),
            "ppi": forms.HiddenInput(),
            "is_primary": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["test_type"].widget.attrs["onchange"] = "test_type_changed(this);"
        self.fields["test_type"].widget.attrs.update({"class": "col-2"})
        self.fields["test_name"].widget.attrs.update({"class": "col-10"})
        self.fields["test_modulation_type"].widget.attrs.update({"class": "col-2"})
        self.fields["nb_active_compounds"].widget.attrs.update({"class": "col-3"})
        self.fields["cell_line_name"].widget.attrs.update({"class": "col-7"})

    def has_changed(self):
        """
        Test if the form has changed, we consider that it has not changed if it is not linked to an actual instance and
        the only change is that the DELETE checkbox have been set to True
        :return:
        """
        # If only the delete button was checked, and there is no id, it has not changed and thus should be ignored.
        if (
            len(self.changed_data) == 1
            and self.changed_data
            and self.data.get("id", None) is None
        ):
            return False
        return super().has_changed()

    def clean(self):
        cleaned_data = super().clean()
        if "test_type" in cleaned_data and cleaned_data["test_type"] != "CELL":
            cleaned_data["cell_line_name"] = ""
        return cleaned_data

    def save(self, commit=True):
        """
        Before an actual save, we set the foreign key that have been created in the meantime from unique identifier
        (cell_line_name, protein_complex) we where provided at the initialization of the form, and/or the input of the
        user.
        :param commit:
        :return:
        """
        # right
        if hasattr(self, "cleaned_data"):
            if (
                "cell_line_name" in self.cleaned_data
                and self.cleaned_data["cell_line_name"] != ""
            ):
                cell_line, created = models.CellLine.objects.get_or_create(
                    name=self.cleaned_data["cell_line_name"]
                )
                self.instance.cell_line = cell_line
        return super().save(commit=commit)


class TestActivityDescriptionBaseModelFormSet(BaseInlineNestedFormSet):
    __compound_names = None
    __protein_subset_ids = None

    def __init__(self, modulation_type=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__modulation_type = modulation_type

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.nested = CompoundActivityResultInlineFormset(
            instance=form.instance,
            data=form.data if form.is_bound else None,
            files=form.files if form.is_bound else None,
            prefix="%s-%s-activity-results"
            % (
                form.prefix,  # .replace("__prefix__", "__parent_prefix__"),
                CompoundActivityResultInlineFormset.get_default_prefix(),
            ),
            queryset=models.CompoundActivityResult.objects.none(),
        )
        form.nested.set_compound_names(self.__compound_names)
        form.nested.set_modulation_type(self.__modulation_type)

    def set_compound_names(self, compound_names):
        """
        Provided to the nested forms the compound names received from the wizard
        :param compound_names: an array of string being the names
        :return:
        """
        self.__compound_names = compound_names

    def set_protein_subset_ids(self, protein_subset_ids):
        """
        Fill the field with he protein's ids provided byt the wizard

        :param protein_subset_ids: pk of proteins
        :return:
        """
        self.__protein_subset_ids = protein_subset_ids

    def set_complex(self, protein_domain_bound_complex):
        """
        Set the protein domain bound complex for all the form instances in the formset

        :param complexes:
        :return:
        """
        for form in self.forms:
            form.instance.protein_domain_bound_complex = protein_domain_bound_complex

    def set_compounds(self, compounds):
        """
        Provided to the nested form the complexes dictionary received from the wizard. The keys are the same as the
        compound names previously provided
        :param compounds:
        :return:
        """
        for form in self.forms:
            form.nested.set_compounds(compounds)


ActivityDescriptionFormSet = inlineformset_factory(
    parent_model=models.Bibliography,
    model=models.TestActivityDescription,
    form=TestActivityDescriptionForm,
    formset=TestActivityDescriptionBaseModelFormSet,
    can_delete=True,
    extra=0,
    min_num=1,
    validate_min=True,
)

""" Step 8.2 : TestCytotoxDescriptionForm """


class CompoundCytotoxicityResultForm(ModelForm):
    compound_name = forms.ChoiceField(choices=())

    class Meta:
        model = models.CompoundCytotoxicityResult
        fields = ("compound_name", "toxicity")
        widgets = {"toxicity": forms.Select(choices=((True, "True"), (False, "False")))}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in self.fields.values():
            f.widget.attrs.update({"class": "col-3"})

    def has_changed(self):
        """
        Test if the form has changed, we consider that it has not changed if it is not linked to an actual instance and
        the only change is that the DELETE checkbox have been set to True
        :return:
        """
        # If only the delete button was checked, and there is no id, it has not changed and thus should be ignored.
        if (
            len(self.changed_data) == 1
            and self.changed_data[0] == "DELETE"
            and self.data.get("id", None) is None
        ):
            return False
        return super().has_changed()

    def save(self, commit=True):
        # right before an actual save, we set the foreign key that have been created in the meantime from unique
        # identifier (compound_name) we where provided at the initialization of the form.
        if hasattr(self, "cleaned_data"):
            if "compound_name" in self.cleaned_data:
                try:
                    self.instance.compound = self.compounds[
                        self.cleaned_data["compound_name"]
                    ]
                except KeyError:
                    pass
        return super().save(commit=commit)


class CompoundCytotoxicityResultBaseInlineNestedFormSet(BaseInlineNestedFormSet):
    __compound_names = []

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["compound_name"].choices = self.__compound_names

    def set_compound_names(self, compound_names):
        """
        Set the choices of the field compound_name to the compound_names provided
        :param compound_names:
        :return:
        """
        self.__compound_names = list(
            itertools.zip_longest(compound_names, compound_names)
        )

    def set_compounds(self, compounds):
        """
        Provided to the form the compounds dictionary received from the wizard

        :param complexes:
        :return:
        """
        for form in self.forms:
            form.compounds = compounds

    def full_clean(self):
        """
        An example of validation across the whole formset, here no homonym.
        """

        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        names = set()
        for form in self.forms:
            if form.cleaned_data.get("DELETE", False):
                continue

            # if the form is empty
            if not (form.has_changed() and "compound_name" in form.cleaned_data):
                continue

            compound_name = form.cleaned_data["compound_name"]
            if compound_name in names:
                message = (
                    _(
                        "At most one result per compound is accepted. Incriminated entry:'%s'"
                    )
                    % compound_name
                )
                self._non_form_errors.append(message)
                form.errors.setdefault("compound_name", []).append(message)
            names.add(compound_name)


CompoundCytotoxicityResultInlineFormset = inlineformset_factory(
    parent_model=models.TestCytotoxDescription,
    model=models.CompoundCytotoxicityResult,
    form=CompoundCytotoxicityResultForm,
    formset=CompoundCytotoxicityResultBaseInlineNestedFormSet,
    extra=1,
    can_delete=True,
)


class TestCytotoxDescriptionForm(forms.ModelForm):

    test_name = CharFieldDataList(
        data_class=models.TestCytotoxDescription, data_attr="test_name"
    )

    cell_line_name = CharFieldDataList(
        data_class=models.CellLine, data_attr="name", label=_("Cell line")
    )

    class Meta:
        model = models.TestCytotoxDescription
        fields = "__all__"
        exclude = ("cell_line", "biblio")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["compound_concentration"].required = True
        self.fields["cell_line_name"].widget.attrs.update({"class": "col"})
        self.fields["compound_concentration"].widget.attrs.update({"class": "col"})
        self.fields["test_name"].widget.attrs.update({"class": "col"})

    def has_changed(self):
        """
        Test if the form has changed, we consider that it has not changed if it is not linked to an actual instance and
        the only change is that the DELETE checkbox have been set to True
        :return:
        """
        # If only the delete button was checked, and there is no id, it has not changed and thus should be ignored.
        if (
            len(self.changed_data) == 1
            and self.changed_data
            and self.data.get("id", None) is None
        ):
            return False
        return super().has_changed()

    def save(self, commit=True):
        """
        Before an actual save, we set the foreign key that have been created in the meantime from unique identifier
        (cell_line_name, protein_complex) we where provided at the initialization of the form, and/or the input of the
        user.
        :param commit:
        :return:
        """
        # right
        if hasattr(self, "cleaned_data"):
            if "cell_line_name" in self.cleaned_data:
                cell_line, created = models.CellLine.objects.get_or_create(
                    name=self.cleaned_data["cell_line_name"]
                )
                self.instance.cell_line = cell_line
        return super().save(commit=commit)


class TestCytotoxDescriptionBaseModelFormSet(BaseInlineNestedFormSet):
    __compound_names = None

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.nested = CompoundCytotoxicityResultInlineFormset(
            instance=form.instance,
            data=form.data if form.is_bound else None,
            files=form.files if form.is_bound else None,
            prefix="%s-%s-cytotox-results"
            % (
                form.prefix,  # .replace("__prefix__", "__parent_prefix__"),
                CompoundCytotoxicityResultInlineFormset.get_default_prefix(),
            ),
            queryset=models.CompoundCytotoxicityResult.objects.none(),
        )
        form.nested.set_compound_names(self.compound_names)

    def set_compound_names(self, compound_names):
        """
        Provided to the nested forms the compound names received from the wizard
        :param compound_names: an array of string being the names
        :return:
        """
        self.compound_names = compound_names

    def set_compounds(self, compounds):
        """
        Provided to the nested form the complexes dictionary received from the wizard. The keys are the same as the
        compound names previously provided
        :param compounds:
        :return:
        """
        for form in self.forms:
            form.nested.set_compounds(compounds)


TestCytotoxDescriptionFormSet = inlineformset_factory(
    parent_model=models.Bibliography,
    model=models.TestCytotoxDescription,
    form=TestCytotoxDescriptionForm,
    formset=TestCytotoxDescriptionBaseModelFormSet,
    can_delete=True,
    extra=0,
    min_num=1,
    validate_min=True,
)

""" Step 8.3 : TestPKDescriptionForm """


class CompoundPKResultForm(ModelForm):
    compound_name = forms.ChoiceField(choices=())

    class Meta:
        model = models.CompoundPKResult
        fields = "__all__"
        exclude = ("compound", "test_pk_description")
        widgets = {
            "tolerated": forms.Select(
                choices=((True, "True"), (False, "False"), (None, "Unknown"))
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in self.fields.values():
            f.widget.attrs.update({"class": "col-3"})
            if isinstance(f, forms.BooleanField) and not isinstance(
                f.widget, forms.Select
            ):
                f.widget.attrs.update({"class": "col-3 mb-4"})

    def has_changed(self):
        """
        Test if the form has changed, we consider that it has not changed if it is not linked to an actual instance and
        the only change is that the DELETE checkbox have been set to True
        :return:
        """
        # If only the delete button was checked, and there is no id, it has not changed and thus should be ignored.
        if (
            len(self.changed_data) == 1
            and self.changed_data[0] == "DELETE"
            and self.data.get("id", None) is None
        ):
            return False
        return super().has_changed()

    def save(self, commit=True):
        # right before an actual save, we set the foreign key that have been created in the meantime from unique
        # identifier (compound_name) we where provided at the initialization of the form.
        if hasattr(self, "cleaned_data"):
            if "compound_name" in self.cleaned_data:
                try:
                    self.instance.compound = self.compounds[
                        self.cleaned_data["compound_name"]
                    ]
                except KeyError:
                    pass
        return super().save(commit=commit)


class CompoundPKResultBaseInlineNestedFormSet(BaseInlineNestedFormSet):
    __compound_names = []

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["compound_name"].choices = self.__compound_names

    def set_compound_names(self, compound_names):
        """
        Set the choices of the field compound_name to the compound_names provided
        :param compound_names:
        :return:
        """
        self.__compound_names = list(
            itertools.zip_longest(compound_names, compound_names)
        )

    def set_compounds(self, compounds):
        """
        Provided to the form the compounds dictionary received from the wizard

        :param complexes:
        :return:
        """
        for form in self.forms:
            form.compounds = compounds

    def full_clean(self):
        """
        An example of validation across the whole formset, here no homonym.
        """

        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        names = set()
        for form in self.forms:
            if form.cleaned_data.get("DELETE", False):
                continue

            # if the form is empty
            if not (form.has_changed() and "compound_name" in form.cleaned_data):
                continue

            compound_name = form.cleaned_data["compound_name"]
            if compound_name in names:
                message = (
                    _(
                        "At most one result per compound is accepted. Incriminated entry:'%s'"
                    )
                    % compound_name
                )
                self._non_form_errors.append(message)
                form.errors.setdefault("compound_name", []).append(message)
            names.add(compound_name)


CompoundPKResultInlineFormset = inlineformset_factory(
    parent_model=models.TestPKDescription,
    model=models.CompoundPKResult,
    form=CompoundPKResultForm,
    formset=CompoundPKResultBaseInlineNestedFormSet,
    extra=1,
    can_delete=True,
)


class TestPKDescriptionForm(forms.ModelForm):

    test_name = CharFieldDataList(
        data_class=models.TestPKDescription, data_attr="test_name"
    )

    class Meta:
        model = models.TestPKDescription
        fields = "__all__"
        exclude = ("cell_line", "biblio")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in ["administration_mode", "dose"]:
            self.fields[f].required = True
        for f in self.fields.values():
            f.widget.attrs.update({"class": "col-3"})

    def has_changed(self):
        """
        Test if the form has changed, we consider that it has not changed if it is not linked to an actual instance and
        the only change is that the DELETE checkbox have been set to True
        :return:
        """
        # If only the delete button was checked, and there is no id, it has not changed and thus should be ignored.
        if (
            len(self.changed_data) == 1
            and self.changed_data
            and self.data.get("id", None) is None
        ):
            return False
        return super().has_changed()


class TestPKDescriptionBaseModelFormSet(BaseInlineNestedFormSet):
    __compound_names = None

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.nested = CompoundPKResultInlineFormset(
            instance=form.instance,
            data=form.data if form.is_bound else None,
            files=form.files if form.is_bound else None,
            prefix="%s-%s-pk-results"
            % (
                form.prefix,  # .replace("__prefix__", "__parent_prefix__"),
                CompoundPKResultInlineFormset.get_default_prefix(),
            ),
            queryset=models.CompoundPKResult.objects.none(),
        )
        form.nested.set_compound_names(self.compound_names)

    def set_compound_names(self, compound_names):
        """
        Provided to the nested forms the compound names received from the wizard
        :param compound_names: an array of string being the names
        :return:
        """
        self.compound_names = compound_names

    def set_compounds(self, compounds):
        """
        Provided to the nested form the complexes dictionary received from the wizard. The keys are the same as the
        compound names previously provided
        :param compounds:
        :return:
        """
        for form in self.forms:
            form.nested.set_compounds(compounds)


TestPKDescriptionFormSet = inlineformset_factory(
    parent_model=models.Bibliography,
    model=models.TestPKDescription,
    form=TestPKDescriptionForm,
    formset=TestPKDescriptionBaseModelFormSet,
    can_delete=True,
    extra=0,
    min_num=1,
    validate_min=True,
)

""" Step 8 : TestsForm """


class TestsForm(forms.Form):
    test_type = forms.CharField(
        label="Test type",
        required=False,
        widget=forms.Select(
            choices=(
                ("activity", "Activity"),
                ("cytotoxicity", "Cytotoxicity"),
                ("pk", "PK"),
            ),
            attrs={"class": "test-selector"},
        ),
    )
    activityDesc_type = forms.ModelChoiceField(
        label="Activity type",
        required=False,
        widget=forms.Select(),
        queryset=models.TestActivityDescription.objects.values_list(
            "test_type", flat=True
        ).distinct(),
        empty_label="(Toto)",
    )
    activityDesc_name = forms.ModelChoiceField(
        label="Test name",
        queryset=models.TestActivityDescription.objects.values_list(
            "test_name", flat=True
        )
        .order_by(Upper("test_name"))
        .distinct(),
        empty_label=None,
        # widget=forms.Select(),
    )
    activityDesc_cell_line = forms.ModelChoiceField(
        label="Corresponding cell line",
        queryset=models.CellLine.objects.all(),
        empty_label=None,
        # widget=forms.Select(),
    )
    activityDesc_test_modulation_type = forms.ModelChoiceField(
        label="Modulation type",
        queryset=models.TestActivityDescription.objects.values_list(
            "test_modulation_type", flat=True
        ).distinct(),
        empty_label=None,
        # widget=forms.Select(),
    )
    activityDesc_nb_active_compound = forms.IntegerField(label="Nb active compound")
    activityDesc_is_primary = forms.ModelChoiceField(
        label="Is primary",
        queryset=models.TestActivityDescription.objects.values_list(
            "is_primary", flat=True
        ).distinct(),
        empty_label=None,
        # widget=forms.Select(),
    )
    activityRes_compound = forms.ChoiceField(
        label="Compound",
        # widget=forms.Select(),
    )
    activityRes_activity_type = forms.CharField(
        label="Activity type", widget=forms.TextInput()
    )
    activityRes_activity = forms.IntegerField(
        label="Activity", widget=forms.NumberInput()
    )
    activityRes_test_modulation_type = forms.ModelChoiceField(
        label="activityRes_test_modulation_type",
        queryset=models.CompoundActivityResult.objects.values_list(
            "modulation_type", flat=True
        ).distinct(),
        empty_label=None,  # widget=forms.Select(),
    )
    activityRes_inhibition_percentage = forms.IntegerField(
        label="activityRes_inhibition_percentage", required=False
    )
    cytotoxDesc_name = forms.ModelChoiceField(
        label="Test name",
        queryset=models.TestCytotoxDescription.objects.values_list(
            "test_name", flat=True
        ).distinct(),
        empty_label=None,
        # widget=forms.Select(),
    )
    cytotoxDesc_cell_line = forms.ModelChoiceField(
        label="Corresponding cell line",
        queryset=models.CellLine.objects.all(),
        empty_label=None,
        # widget=forms.Select(),, required=False,
    )
    cytotoxDesc_compound_concentration = forms.CharField(
        label="Compound concentration",
        widget=forms.TextInput(attrs={"placeholder": "Compound concentration"}),
        required=False,
    )
    cytotoxRes_compound = forms.ChoiceField(
        label="Compound", widget=forms.TextInput(), required=False
    )
    cytotoxRes_toxicity = forms.ModelChoiceField(
        label="Toxicity",
        queryset=models.CompoundCytotoxicityResult.objects.all(),
        required=False,
    )
    pkDesc_name = forms.ModelChoiceField(
        label="Test name",
        queryset=models.TestPKDescription.objects.values_list(
            "test_name", flat=True
        ).distinct(),
        empty_label=None,
        # widget=forms.Select(),
    )
    pkDesc_organism = forms.ModelChoiceField(
        label="Organism",
        queryset=models.Taxonomy.objects.all(),
        empty_label="Choose a corresponding organism",
        required=False,
    )
    pkDesc_administration_mode = forms.CharField(
        label="Administration mode",
        widget=forms.TextInput(attrs={"placeholder": "Admnistration mode"}),
        required=False,
    )
    pkDesc_dose = forms.IntegerField(label="Dose", required=False)
    pkDesc_dose_interval = forms.IntegerField(label="Dose Interval", required=False)
    pkDesc_concentration = forms.IntegerField(label="Concentration", required=False)
    pkRes_compound = forms.ChoiceField(
        label="Compound", widget=forms.TextInput(), required=False
    )
    pkRes_tolerated = forms.ChoiceField(
        label="Tolerated", widget=forms.TextInput(), required=False
    )
    pkRes_auc_av = forms.BooleanField(label="AUC available", initial=1, required=False)
    pkRes_clearance_av = forms.BooleanField(label="Clearance available", required=False)
    pkRes_cmax_av = forms.BooleanField(
        label="Maximal concentration available", required=False
    )
    pkRes_oral_bioavailability = forms.IntegerField(
        label="Oral bio-availability", initial=1, required=False
    )
    pkRes_t_demi = forms.IntegerField(label="T 1/2", initial=1, required=False)
    pkRes_t_max = forms.IntegerField(label="T max", initial=1, required=False)
    pkRes_voldistribution_av = forms.BooleanField(
        label="Volume distribution (Vd) available", required=False
    )


UNIT_CONCENTRATION = (("micro", "µM"), ("nano", "nM"), ("pico", "pM"))
ADMINISTRATION_MODE = [
    ("Enteral", [("", "Oral"), ("", "Sublingual"), ("", "Rectal")]),
    (
        "Parenteral",
        [
            ("", "Inhalational"),
            ("", "Intravenous"),
            ("", "Intramuscular"),
            ("", "Subcutaneous"),
            ("", "Intra-arterial"),
            ("", "Intra-articular"),
            ("", "Intrathecal"),
            ("", "Intradermal"),
            ("", "Transdermal"),
        ],
    ),
    (
        "Local",
        [
            ("", "Skin topical"),
            ("", "Intranasal"),
            ("", "Ocular drops"),
            ("", "Mucosal-throat"),
            ("", "Vagina"),
            ("", "Mouth"),
            ("", "Ear"),
            ("", "Transdermal"),
        ],
    ),
]

# TestsFormSet = formset_factory(TestsForm, formset=BaseTestsFormSet, can_delete=True, can_order=True)
TestsFormSet = formset_factory(TestsForm, can_delete=True, can_order=False, extra=1)
# formset = TestsFormSet()


""" Step 9 : SaveInDB """


class SaveInDBForm(forms.Form):
    ok = forms.BooleanField(
        label=_("SaveInDBForm__ok__label"),
        # help_text=_("SaveInDBForm__ok__help_text"),
        required=True,
        initial=False,
    )
