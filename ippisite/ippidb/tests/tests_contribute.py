"""
iPPI-DB contribution module tests
"""
from requests.exceptions import HTTPError

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse


from ippidb import models
from ippidb.forms import PDBForm
from ippidb.tests.utils import TestCaseWithRequestsCache
from live_settings import live_settings


class BibliographyIDTestCase(TestCaseWithRequestsCache):
    def test_long_doi(self):
        b = models.Bibliography()
        b.source = "DO"
        b.id_source = "10.1016/j.bmcl.2013.03.013"
        try:
            b.save(autofill=True)
        except HTTPError:
            self.skipTest("Connection error, skipping test")


class ContributionViewsAccessTestCase(TestCaseWithRequestsCache):
    def setUp(self):
        super().setUp()
        self.url = reverse("admin-session-add")

        self.user = get_user_model().objects.create(username="user")

        self.user_granted = get_user_model().objects.create(username="user_granted")
        content_type = ContentType.objects.get_for_model(models.Contribution)
        permission = Permission.objects.get(
            codename="add_contribution", content_type=content_type
        )
        self.user_granted.user_permissions.add(permission)

    def test_force_auth(self):
        response = self.client.get(self.url)
        self.assertRedirects(
            response,
            # expected_url=reverse('login') + "?next=" + url
            expected_url="/accounts/login/?next=%s" % self.url,
        )

    def test_403_without_perm_and_access_closed(self):
        live_settings.open_access_to_contribution = "False"
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_ok_with_perm_and_access_closed(self):
        live_settings.open_access_to_contribution = "False"
        self.test_ok_with_perm_and_access_closed_by_default()

    def test_ok_with_perm_and_access_closed_by_default(self):
        self.client.force_login(self.user_granted)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_ok_without_perm_and_access_open(self):
        live_settings.open_access_to_contribution = "True"
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_ok_with_perm_and_access_open(self):
        live_settings.open_access_to_contribution = "True"
        self.client.force_login(self.user_granted)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_ok_with_superuser(self):
        self.user.is_superuser = True
        self.user.save()
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_ko_with_staff(self):
        live_settings.open_access_to_contribution = "False"
        self.user.is_staff = True
        self.user.save()
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)


class PDBFormTestCase(TestCaseWithRequestsCache):
    """
    Test the PDB form data validation
    """

    def test_valid(self):
        form = PDBForm({"pdb_id": "3wn7"})
        self.assertTrue(form.is_valid(), "valid PDB code should be accepted")

    def test_invalid_PDB_format(self):
        form = PDBForm({"pdb_id": "33wn7"})
        self.assertFalse(form.is_valid(), "5 char long PDB code should be rejected")
        self.assertTrue(
            form.has_error("pdb_id", code="max_length"),
            "5 char long PDB code should be rejected with max_length error code",
        )

    def test_invalid_PDB_entry(self):
        form = PDBForm({"pdb_id": "9aa9"})
        self.assertFalse(
            form.is_valid(), "inexistent PDB (9aa9) code should be rejected"
        )
        self.assertTrue(
            form.has_error("pdb_id", code="not_found"),
            "inexistent PDB (9aa9) code should be rejected with not_found error code",
        )

    def test_invalid_nouniprot(self):
        form = PDBForm({"pdb_id": "3wn8"})
        self.assertFalse(
            form.is_valid(),
            "PDB code with no Uniprot mapping (3wn8) should be rejected",
        )
        self.assertTrue(
            form.has_error("pdb_id", code="no_mapping"),
            "PDB code with no Uniprot mapping (3wn8) should be rejected with no_mappping error code",
        )
