"""
iPPI-DB unit tests
"""
import re

from django.test import TestCase
from openbabel import vectorUnsignedInt, OBFingerprint

from ippidb.utils import (
    FingerPrinter,
    mol2smi,
    smi2mol,
    smi2inchi,
    smi2inchikey,
    smi2sdf,
)


class MolSmiTestCase(TestCase):
    """
    Test MOL to SMILES and SMILES to MOL format conversion functions
    """

    def setUp(self):
        self.smiles = "C"
        # the MOL version is also a valid regexp to validate arbitrary name in
        # the openbabel-generated version
        self.mol = (
            "\n OpenBabel[0-9]{11}D\n\n  1  0  0  0  0  0  0  0 "
            " 0  0999 V2000\n    1.0000    0.0000    0.0000 C   0  0  0  0 "
            " 0  0  0  0  0  0  0  0\nM  END\n"
        )

    def test_mol2smi(self):
        self.assertEqual(mol2smi(self.mol), self.smiles)

    def test_smi2mol2smi(self):
        self.assertTrue(re.compile(self.mol).match(smi2mol(self.smiles)))


class SmiInchiTestCase(TestCase):
    """
    Test INCHI and INCHIKEY generation functions
    """

    def setUp(self):
        self.smiles_str = (
            "CC(C)C(=O)C1=C(C(=C(C(=C1)C(=O)C2=CC=C(C=C2)" "OC3=CC=CC=C3)O)O)O"
        )
        self.inchi_str = (
            "InChI=1S/C23H20O6/c1-13(2)19(24)17-12-18(22"
            "(27)23(28)21(17)26)20(25)14-8-10-16(11-9-14)29-15-6-4-3-"
            "5-7-15/h3-13,26-28H,1-2H3"
        )
        self.inchikey_str = "CVVQMBDTMYUWTR-UHFFFAOYSA-N"

    def test_smi2inchi(self):
        self.assertEqual(smi2inchi(self.smiles_str), self.inchi_str)

    def test_smi2inchikey(self):
        self.assertEqual(smi2inchikey(self.smiles_str), self.inchikey_str)


class FingerPrinterTestCase(TestCase):
    """
    Test FingerPrinter class
    """

    def setUp(self):
        self.fingerprinter = FingerPrinter("FP4")
        self.smiles = "CC"
        self.fp = vectorUnsignedInt([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.smiles_dict = {1: "CC", 2: "CCC"}
        self.fp_dict = {
            1: vectorUnsignedInt([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
            2: vectorUnsignedInt([3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
        }
        self.tanimoto_dict = {1: 1.0, 2: 0.5}

    def test_fingerprints_available(self):
        # test that all necessary fingerprints are available
        self.assertIsNotNone(OBFingerprint.FindFingerprint("FP2"))
        self.assertIsNotNone(OBFingerprint.FindFingerprint("FP4"))
        self.assertIsNotNone(OBFingerprint.FindFingerprint("ECFP4"))

    def assertEqualVUI(self, vui_one, vui_two):
        return self.assertEqual(list(vui_one), list(vui_two))

    def assertEqualVUIdict(self, vuidict_one, vuidict_two):
        vuidict_one = {id_: list(vui) for id_, vui in vuidict_one.items()}
        vuidict_two = {id_: list(vui) for id_, vui in vuidict_two.items()}
        return self.assertEqual(vuidict_one, vuidict_two)

    def test_fp(self):
        self.assertEqualVUI(self.fingerprinter.fp(self.smiles), self.fp)

    def test_fp_dict(self):
        self.assertEqualVUIdict(
            self.fingerprinter.fp_dict(self.smiles_dict), self.fp_dict
        )

    def test_tanimoto_fps(self):
        self.assertEqual(
            self.fingerprinter.tanimoto_fps(self.smiles, self.fp_dict),
            self.tanimoto_dict,
        )

    def test_tanimoto_smiles(self):
        self.assertEqual(
            self.fingerprinter.tanimoto_smiles(self.smiles, self.smiles_dict),
            self.tanimoto_dict,
        )


class FingerPrinterTestCaseCompound1ECFP4(TestCase):
    def setUp(self):
        self.fingerprinter = FingerPrinter("ECFP4")
        self.smiles = "CC(C)C(=O)c1cc(C(=O)c2ccc(Oc3ccccc3)cc2)c(O)c(O)c1O"
        self.smiles_dict = {1: "CC(C)C(=O)c1cc(C(=O)c2ccc(Oc3ccccc3)cc2)c(O)" "c(O)c1O"}
        self.tanimoto_dict = {1: 1.0}

    def test_tanimoto_smiles(self):
        self.assertEqual(
            self.fingerprinter.tanimoto_smiles(self.smiles, self.smiles_dict),
            self.tanimoto_dict,
        )


class ConvertSMILESToSDFTestCase(TestCase):
    """
    Test converting a smiles to SDF using openbabel
    """

    def setUp(self):
        self.smiles = "C"
        # the MOL version is also a valid regexp to validate arbitrary name in
        # the openbabel-generated version
        self.sdf = (
            r"1\n"
            r" OpenBabel[0-9]{11}D\n"
            r"\n"
            r"  1  0  0  0  0  0  0  0  0  0999 V2000\n"
            r"    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n"
            r"M  END\n"
            r"\$\$\$\$\n"
        )

    def test_valid(self):
        result = smi2sdf({1: self.smiles})
        self.assertTrue(re.compile(self.sdf).search(result))
