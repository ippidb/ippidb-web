"""
iPPI-DB unit tests
"""

import json
import re

from requests.exceptions import SSLError

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.urls import reverse
import requests

from ippidb.ws import (
    PubMedEntryNotFound,
    get_uniprot_info,
    get_doi_info,
    get_pfam_info,
    get_google_patent_info,
    PatentNotFound,
    get_pubmed_info,
    get_pdb_uniprot_mapping,
    get_pdb_pfam_mapping,
    EntryNotFoundError,
    convert_iupac_to_smiles_and_inchi,
    convert_smiles_to_iupac,
)
from ippidb.models import (
    Compound,
    CompoundTanimoto,
    create_tanimoto,
    update_compound_cached_properties,
    CompoundAction,
    Ppi,
    Contribution,
    LeLleBiplotData,
    PcaBiplotData,
    Protein,
    Symmetry,
)
from .utils import (
    create_dummy_compound,
    TestCaseWithRequestsCache,
    TransactionTestCaseWithRequestsCache,
)
from .. import ws


class MolSmiTestCase(TestCaseWithRequestsCache):
    """
    Test MOL to SMILES and SMILES to MOL format conversion iPPI-DB web services
    """

    def setUp(self):
        super().setUp()
        self.smiles = "C"
        # the MOL version is also a valid regexp to validate arbitrary name in
        # the openbabel-generated version
        self.mol = (
            "\n OpenBabel[0-9]{11}D\n\n  1  0  0  0  0  0  0  0 "
            " 0  0999 V2000\n    1.0000    0.0000    0.0000 C   0  0  0  0 "
            " 0  0  0  0  0  0  0  0\nM  END\n"
        )

    def test_view_smi2mol_valid(self):
        url = reverse("smi2mol")
        response = self.client.get(url, {"smiString": self.smiles})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(re.compile(self.mol).match(response.json()["mol"]))

    def test_view_smi2mol_empty(self):
        url = reverse("smi2mol")
        response = self.client.get(url, {"smiString": ""})
        self.assertEqual(response.status_code, 400)

    def test_view_smi2mol_invalid(self):
        url = reverse("smi2mol")
        response = self.client.get(url, {"smiString": "###"})
        self.assertEqual(response.status_code, 400)

    def test_view_smi2mol_withspaces(self):
        url = reverse("smi2mol")
        response = self.client.get(url, {"smiString": "\n  " + self.smiles + "  \n  "})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(re.compile(self.mol).match(response.json()["mol"]))

    def test_view_mol2smi_valid(self):
        url = reverse("mol2smi")
        response = self.client.get(url, {"molString": self.mol})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"smiles": self.smiles})

    def test_view_mol2smi_empty(self):
        url = reverse("mol2smi")
        response = self.client.get(url, {"molString": ""})
        self.assertEqual(response.status_code, 400)

    def test_view_mol2smi_invalid(self):
        url = reverse("mol2smi")
        response = self.client.get(url, {"molString": "###"})
        self.assertEqual(response.status_code, 400)


class CompoundTanimotoTestCase(TransactionTestCaseWithRequestsCache):
    reset_sequences = True

    def setUp(self):
        super().setUp()
        self.smiles_dict = {1: "CC", 2: "CCC"}
        self.query = "CC"
        for id_, smiles in self.smiles_dict.items():
            create_dummy_compound(id_, smiles)

    def test_create(self):
        create_tanimoto(self.query, "FP4")
        ct = CompoundTanimoto.objects.get(id=1, canonical_smiles=self.query)
        self.assertEqual(ct.tanimoto, 1.0)
        ct = CompoundTanimoto.objects.get(id=2, canonical_smiles=self.query)
        self.assertEqual(ct.tanimoto, 0.5)


class CompoundTanimotoTestCaseCompound1ECFP4(TransactionTestCaseWithRequestsCache):
    reset_sequences = True

    def setUp(self):
        super().setUp()
        self.smiles_dict = {
            1: "CC(C)C(=O)c1cc(C(=O)c2ccc(Oc3ccccc3)cc2)c(O)c(O)c1O",
            2: "NC(=N)N[C@H](C1CCCCC1)C(=O)NCC(=O)N1CCC(CC1)c1cc(n[nH]1)"
            "-c1ccc(Cl)cc1Cl",
        }
        self.query = "CC(C)C(=O)c1cc(C(=O)c2ccc(Oc3ccccc3)cc2)c(O)c(O)c1O"
        for id_, smiles in self.smiles_dict.items():
            create_dummy_compound(id_, smiles)

    def test_create(self):
        create_tanimoto(self.query, "ECFP4")
        ct = CompoundTanimoto.objects.get(id=1, canonical_smiles=self.query)
        ct2 = CompoundTanimoto.objects.get(id=2, canonical_smiles=self.query)
        self.assertEqual(ct.tanimoto, 1.0)
        self.assertEqual(float(ct2.tanimoto), 0.0971)


class CompoundAnnotationsTestCase(TransactionTestCaseWithRequestsCache):
    reset_sequences = True

    def test_lipinsky_ok(self):
        # c is ok for Lipinsky
        c = create_dummy_compound(1, "CC")
        # MW <= 500
        c.molecular_weight = 300
        # HBA <= 10
        c.nb_acceptor_h = 9
        # HBD <= 5
        c.nb_donor_h = 4
        # AlogP <= 5
        c.a_log_p = 4
        c.save()
        update_compound_cached_properties()
        c = Compound.objects.get(id=1)
        self.assertTrue(c.lipinsky_mw)
        self.assertTrue(c.lipinsky_hba)
        self.assertTrue(c.lipinsky_hbd)
        self.assertTrue(c.lipinsky_a_log_p)
        self.assertTrue(c.lipinsky)

    def test_lipinsky_ko(self):
        # c is ko for Lipinsky
        c = create_dummy_compound(1, "CC")
        # MW > 500
        c.molecular_weight = 3000
        # HBA > 10
        c.nb_acceptor_h = 11
        # HBD > 5
        c.nb_donor_h = 6
        # AlogP > 5
        c.a_log_p = 7
        c.save()
        update_compound_cached_properties()
        c = Compound.objects.get(id=1)
        self.assertFalse(c.lipinsky_mw)
        self.assertFalse(c.lipinsky_hba)
        self.assertFalse(c.lipinsky_hbd)
        self.assertFalse(c.lipinsky_a_log_p)
        self.assertFalse(c.lipinsky)

    def test_veber_ok(self):
        # c is ok for Veber
        c = create_dummy_compound(1, "CC")
        # HBA + HBD <=12
        c.nb_acceptor_h = 10
        c.nb_donor_h = 1
        # TPSA <= 140
        c.tpsa = 130
        # RB <= 10
        c.nb_rotatable_bonds = 9
        c.save()
        update_compound_cached_properties()
        c = Compound.objects.get(id=1)
        self.assertTrue(c.veber_hba_hbd)
        self.assertTrue(c.veber_tpsa)
        self.assertTrue(c.veber_rb)
        self.assertTrue(c.veber)

    def test_veber_ko(self):
        # c is ko for Veber
        c = create_dummy_compound(1, "CC")
        # HBA + HBD > 12
        c.nb_acceptor_h = 10
        c.nb_donor_h = 3
        # TPSA > 140
        c.tpsa = 141
        # RB > 10
        c.nb_rotatable_bonds = 11
        c.save()
        update_compound_cached_properties()
        c = Compound.objects.get(id=1)
        self.assertFalse(c.veber_hba_hbd)
        self.assertFalse(c.veber_tpsa)
        self.assertFalse(c.veber_rb)
        self.assertFalse(c.veber)

    def test_pfizer_ok(self):
        # c is ok for Pfizer
        c = create_dummy_compound(1, "CC")
        # AlogP <=3
        c.a_log_p = 2
        # TPSA >=75
        c.tpsa = 80
        c.save()
        update_compound_cached_properties()
        c = Compound.objects.get(id=1)
        self.assertTrue(c.pfizer_a_log_p)
        self.assertTrue(c.pfizer_tpsa)
        self.assertTrue(c.pfizer)

    def test_pfizer_ko(self):
        # c is ko for Pfizer
        c = create_dummy_compound(1, "CC")
        # AlogP >3
        c.a_log_p = 4
        # TPSA <75
        c.tpsa = 8
        c.save()
        update_compound_cached_properties()
        c = Compound.objects.get(id=1)
        self.assertFalse(c.pfizer_a_log_p)
        self.assertFalse(c.pfizer_tpsa)
        self.assertFalse(c.pfizer)


class QueryCompoundViewsTestCase(TestCaseWithRequestsCache):
    @classmethod
    def setUpTestData(cls):
        create_dummy_compound(1, "CC")
        create_dummy_compound(2, "CCC")
        call_command("lle_le")
        call_command("pca")

    def test_existing_compound(self):
        """
        The detail view of an existing compound
        returns a 200
        """
        url = reverse("compound_card", kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_inexisting_compound(self):
        """
        The detail view of a non-existing compound
        returns a 404 not found.
        """
        url = reverse("compound_card", kwargs={"pk": 9})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_compounds_list(self):
        """
        The compounds list view
        returns a 200
        """
        url = reverse("compound_list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


def create_dummy_user(login, password, admin=False):
    User = get_user_model()
    if admin:
        user = User.objects.create_superuser(
            username=login, email=f"{login}@ippidb.test", password=password
        )
    else:
        user = User.objects.create_user(
            username=login, email=f"{login}@ippidb.test", password=password
        )
    return user


def create_dummy_contribution(compound_id, smiles, user, symmetry, validate=False):
    c = create_dummy_compound(compound_id, smiles)
    ppi = Ppi()
    ppi.symmetry = symmetry
    ppi.save()
    ca = CompoundAction()
    ca.nb_copy_compounds = 1
    ca.compound = c
    ca.ppi = ppi
    ca.save()
    co = Contribution()
    co.ppi = ppi
    co.contributor = user
    co.validated = validate
    co.save()
    return c


def set_dummy_lelle_pca_props(c):
    c.le = 0.5
    c.lle = 0.5
    c.molecular_weight = 1
    c.a_log_p = 1
    c.nb_donor_h = 1
    c.nb_acceptor_h = 1
    c.tpsa = 1
    c.nb_rotatable_bonds = 1
    c.nb_benzene_like_rings = 1
    c.fsp3 = 1
    c.nb_chiral_centers = 1
    c.nb_csp3 = 1
    c.nb_atom = 1
    c.nb_bonds = 1
    c.nb_atom_non_h = 1
    c.nb_rings = 1
    c.nb_multiple_bonds = 1
    c.nb_aromatic_bonds = 1
    c.aromatic_ratio = 1
    c.best_activity_ppi_family_name = "dummy"
    c.save()


class QueryCompoundViewsAccessTestCase(TestCaseWithRequestsCache):
    """
    Test the visibility of compounds belonging to
    validated or unvalidated contributions
    The "visibility matrix" is the following:
    ============================================================================
    |Validation status / User| Anonymous | Creator | Another user | Admin user |
    | Unvalidated            |    No     |   Yes   |     No       |    Yes     |
    | Validated              |    Yes    |   Yes   |     Yes      |    Yes     |
    ============================================================================
    """

    @classmethod
    def setUpTestData(cls):
        """
        Test data include:
        - one "contributor" user
        - one "other" user
        - one "admin" user
        - one unvalidated contribution
        - one validated contribution
        - 20 other compounds, so that a PCA can actually be computed on the data
        """
        symmetry = Symmetry()
        symmetry.code = "AS"
        symmetry.description = "asymmetric"
        symmetry.save()
        # create contributor 1
        cls.user_c1 = create_dummy_user("contributor1", "test1")
        # create contributor 2
        cls.user_c2 = create_dummy_user("contributor2", "test2")
        # create admin
        cls.user_cA = create_dummy_user("admin", "testA", True)
        # WHAT ARE THE USE CASES TO BE TESTED?
        # user is anonymous, contributor, another contributor, admin
        # compound is not validated, validated
        cls.c1 = create_dummy_contribution(1, "CC", cls.user_c1, symmetry, False)
        cls.c2 = create_dummy_contribution(2, "CCC", cls.user_c1, symmetry, True)
        # create another 20 compounds to enable PCA
        for i in range(1, 21):
            create_dummy_compound(2 + i, "O" + i * "C")
        for c in Compound.objects.all():
            set_dummy_lelle_pca_props(c)
        call_command("lle_le")
        call_command("pca")

    def test_is_validated(self):
        """
        Test the is_validated property is valid for each compound
        """
        self.assertEqual(self.c1.is_validated(), False)
        self.assertEqual(self.c2.is_validated(), True)
        for i in range(1, 21):
            # these compounds are considered not validated because
            # are not linked to a CompoundAction
            self.assertEqual(Compound.objects.get(id=2 + i).is_validated(), False)

    def test_lelleplot_filter(self):
        """
        Test that generated LE-LLE biplot
        does not include unvalidated compounds (21 for this test dataset)
        """
        self.assertEqual(
            len(json.loads(LeLleBiplotData.objects.get().le_lle_biplot_data)), 21
        )

    def test_pcaplot_filter(self):
        """
        Test that generated PCA biplot
        does not include unvalidated compounds (21 for this test dataset)
        """
        call_command("pca")
        self.assertEqual(
            len(json.loads(PcaBiplotData.objects.get().pca_biplot_data).get("data")), 21
        )

    def test_compound_detail_unvalidated_unlogged(self):
        """
        Unvalidated compound should not be visible
        if unlogged
        """
        url = reverse("compound_card", kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_compound_detail_unvalidated_logged_creator(self):
        """
        Unvalidated compound should be visible
        if logged as the creator
        """
        url = reverse("compound_card", kwargs={"pk": 1})
        self.client.force_login(self.user_c1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def test_compound_detail_unvalidated_logged_user(self):
        """
        Unvalidated compound should not be visible
        if logged as another user
        """
        url = reverse("compound_card", kwargs={"pk": 1})
        self.client.force_login(self.user_c2)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        self.client.logout()

    def test_compound_detail_unvalidated_logged_admin(self):
        """
        Unvalidated compound should be visible
        if logged as an admin user
        """
        url = reverse("compound_card", kwargs={"pk": 1})
        self.client.force_login(self.user_cA)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def test_compound_detail_validated_unlogged(self):
        """
        Validated compound should be visible
        if unlogged
        """
        url = reverse("compound_card", kwargs={"pk": 2})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_compound_detail_validated_logged_creator(self):
        """
        Validated compound should be visible
        if logged as the creator
        """
        url = reverse("compound_card", kwargs={"pk": 2})
        self.client.force_login(self.user_c1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def test_compound_detail_validated_logged_user(self):
        """
        Validated compound should be visible
        if logged as another user
        """
        url = reverse("compound_card", kwargs={"pk": 2})
        self.client.force_login(self.user_c2)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def test_compound_detail_validated_logged_admin(self):
        """
        Validated compound should be visible
        if logged as an admin user
        """
        url = reverse("compound_card", kwargs={"pk": 2})
        self.client.force_login(self.user_cA)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.logout()


class TestGetDoiInfo(TestCaseWithRequestsCache):
    """
    Test retrieving information for a DOI entry
    """

    def test_get_doi_info(self):
        try:
            resp = get_doi_info("10.1073/pnas.0805139105")
        except requests.exceptions.HTTPError as he:
            # skip this test if the DOI server throws an error
            # (that happens)
            print(f"server-side DOI resolution error, skipping...\nError is {he}")
            return
        self.assertEqual(
            resp["title"], "A quantitative atlas of mitotic" " phosphorylation"
        )
        self.assertEqual(
            resp["journal_name"], "Proceedings of the National" " Academy of Sciences"
        )
        self.assertEqual(resp["biblio_year"], 2008)
        self.assertEqual(
            resp["authors_list"],
            "Dephoure Noah, Zhou Chunshui, Villén Judit, Beausoleil Sean A., "
            "Bakalarski Corey E., Elledge Stephen J., Gygi Steven P.",
        )


class TestGetPfamInfo(TestCaseWithRequestsCache):
    def test_get_pfam_info(self):
        resp = get_pfam_info("PF00170")
        self.assertEqual("bZIP_1", resp["id"])

        info = ws.get_pfam_info("PF13771")
        self.assertEqual("zf-HC5HC2H", info["id"])
        self.assertIn("cysteine-histidine rich region", info["description"])

        info = ws.get_pfam_info("PF00400")
        self.assertEqual("WD40", info["id"])
        self.assertIn(
            "highly symmetrical blade-shaped beta sheets", info["description"]
        )

        info = ws.get_pfam_info("PF02180")
        self.assertEqual("BH4", info["id"])
        self.assertEqual("", info["description"])


class TestGetGooglePatentInfo(TestCaseWithRequestsCache):
    def test_it(self):
        target = {
            "title": "Secure virtual machine bootstrap in untrusted"
            " cloud infrastructures",
            "journal_name": None,
            "biblio_year": "2010",
            "authors_list": "Fabio R. Maino, Pere Monclus, David A."
            " McGrew, Robert T. Bell, "
            "Steven Joseph Rich, Cisco Technology Inc",
        }
        resp = get_google_patent_info("US8856504")
        self.assertEqual(resp, target)

    def test_entry_not_found(self):
        self.assertRaises(
            PatentNotFound,
            get_google_patent_info,
            "US8856504US8856504US885US8856504US8856504",
        )


class TestProtein(TestCaseWithRequestsCache):
    def test_create_protein_no_gene_name(self):
        # this test ensures that we can save a protein with no gene name
        # in the DB
        p = Protein()
        p.uniprot_id = "P00784"
        try:
            p.save()
        except Exception as exc:
            self.fail(f"exception {exc} raised while saving protein P00784")


class TestGetPubMEDIdInfo(TestCaseWithRequestsCache):
    def test_get_existing_pubmed_info(self):
        target = {
            "title": "Gene List significance at-a-glance with" " GeneValorization.",
            "journal_name": "Bioinformatics (Oxford, England)",
            "biblio_year": "2011",
            "authors_list": "Brancotte B, Biton A, Bernard-Pierrot I, "
            "Radvanyi F, Reyal F, Cohen-Boulakia S",
        }
        resp = get_pubmed_info("21349868")
        self.assertEqual(resp, target)

    def test_get_notfound_pubmed_info(self):
        def wrong_call():
            get_pubmed_info("XXXXXXXX")

        self.assertRaises(PubMedEntryNotFound, wrong_call)


class TestGetUniprotInfo(TestCaseWithRequestsCache):
    """
    Test retrieving information for a uniprot entry
    """

    def test_get_uniprot_info(self):
        resp = get_uniprot_info("Q15286")
        self.assertEqual(resp["recommended_name"], "Ras-related protein Rab-35")
        self.assertEqual(resp["organism"], 9606)
        self.assertEqual(resp["gene_id"], 11021)
        exp_gene_names = [
            {"name": "RAB35", "type": "primary"},
            {"name": "RAB1C", "type": "synonym"},
            {"name": "RAY", "type": "synonym"},
        ]
        self.assertEqual(
            sorted(resp["gene_names"], key=lambda k: k["name"]),
            sorted(exp_gene_names, key=lambda k: k["name"]),
        )
        self.assertEqual(resp["entry_name"], "RAB35_HUMAN")
        self.assertEqual(resp["short_name"], "RAB35")
        exp_molecular_functions = [
            "GO_0003924",
            "GO_0005525",
            "GO_0005546",
            "GO_0019003",
        ]
        self.assertEqual(
            sorted(resp["molecular_functions"]), sorted(exp_molecular_functions)
        )
        exp_cellular_localisations = {
            "GO_0005829",
            "GO_0005886",
            "GO_0005905",
            "GO_0010008",
            "GO_0030669",
            "GO_0031253",
            # "GO_0030672",
            "GO_0042470",
            "GO_0045171",
            "GO_0045334",
            "GO_0055038",
            "GO_0070062",
        }
        self.assertSetEqual(
            set(resp["cellular_localisations"]), exp_cellular_localisations
        )
        exp_biological_processes = {
            "GO_0000281",
            "GO_0008104",
            "GO_0015031",
            "GO_0016197",
            "GO_0019882",
            "GO_0031175",
            "GO_0032456",
            "GO_0032482",
            "GO_0036010",
            "GO_0048227",
            "GO_1990090",
        }
        self.assertSetEqual(set(resp["biological_processes"]), exp_biological_processes)
        exp_accessions = {"Q15286", "B2R6E0", "B4E390"}
        self.assertSetEqual(set(resp["accessions"]), exp_accessions)
        exp_citations = [
            {"doi": "10.1006/bbrc.1994.2889", "pmid": "7811277"},
            {"doi": "10.1038/ng1285", "pmid": "14702039"},
            {"doi": "10.1038/nature04569", "pmid": "16541075"},
            {"doi": "10.1101/gr.2596504", "pmid": "15489334"},
            {"doi": "10.1016/j.cub.2006.07.020", "pmid": "16950109"},
            {"doi": "10.1021/pr060363j", "pmid": "17081065"},
            {"doi": "10.1074/jbc.M109.050930", "pmid": "20154091"},
            {"doi": "10.1186/1752-0509-5-17", "pmid": "21269460"},
            {"doi": "10.1038/nature10335", "pmid": "21822290"},
            {"doi": "10.1038/emboj.2012.16", "pmid": "22307087"},
            {"doi": "10.1111/j.1600-0854.2011.01294.x", "pmid": "21951725"},
            {"doi": "10.1021/pr300630k", "pmid": "23186163"},
            {"doi": "10.1002/pmic.201400617", "pmid": "25944712"},
            {"doi": "10.1073/pnas.1110415108", "pmid": "22065758"},
            {"doi": "10.7554/eLife.31012", "pmid": "29125462"},
        ]

        def to_dict(d):
            return dict([(k["pmid"], k["doi"]) for k in d])

        def values_ignore_case(d):
            # after all case does not matter for URL and doi are URL
            return dict([(k, v.lower()) for k, v in d.items()])

        # also ignore the order of the key in the dict
        self.assertDictEqual(
            values_ignore_case(to_dict(resp["citations"])),
            values_ignore_case(to_dict(exp_citations)),
        )
        exp_alternative_names = [
            {"full": "GTP-binding protein RAY"},
            {"full": "Ras-related protein Rab-1C"},
        ]
        self.assertEqual(
            sorted(exp_alternative_names, key=lambda k: k["full"]),
            sorted(resp["alternative_names"], key=lambda k: k["full"]),
        )

    def test_get_uniprot_info_domains(self):
        resp = get_uniprot_info("O00255")
        exp_domains = ["PF05053"]
        self.assertEqual(sorted(resp["domains"]), sorted(exp_domains))


class TestGetPDBUniProtMapping(TestCaseWithRequestsCache):
    """
    Test retrieving protein for a PDB entry
    """

    def test_find_info(self):
        target = sorted(["Q03164", "O00255"])
        resp = get_pdb_uniprot_mapping("3u85")
        resp = sorted(resp)
        self.assertEqual(resp, target)
        self.assertEqual(len(resp), len(set(resp)))

    def test_entry_not_found(self):
        self.assertRaises(EntryNotFoundError, get_pdb_uniprot_mapping, "Xu85")


class TestGetPDBPfamMapping(TestCaseWithRequestsCache):
    """
    Test retrieving protein for a PDB entry
    """

    def test_find_info(self):
        target = {
            "PF05053": {"identifier": "Menin", "description": "Menin", "name": "Menin"}
        }
        resp = get_pdb_pfam_mapping("3u85")
        self.assertDictEqual(resp, target)

    def test_entry_not_found(self):
        self.assertRaises(EntryNotFoundError, get_pdb_pfam_mapping, "Xu85")


class TestConvertIUPACToSMILESAndMore(TestCaseWithRequestsCache):
    """
    Test converting a IUPAC to smiles and inchi with opsin web service
    """

    def test_valid(self):
        pairs = [
            (
                "2,4,6-trinitrotoluene",
                {
                    "inchi": "InChI=1/C7H5N3O6/c1-4-6(9(13)14)2-5(8(11)12)"
                    "3-7(4)10(15)16/h2-3H,1H3",
                    "stdinchi": "InChI=1S/C7H5N3O6/c1-4-6(9(13)14)2-5(8(11)"
                    "12)3-7(4)10(15)16/h2-3H,1H3",
                    "stdinchikey": "SPSSULHKWOKEEL-UHFFFAOYSA-N",
                    "smiles": "[N+](=O)([O-])C1=C(C)C(=CC(=C1)[N+](=O)[O-])"
                    "[N+](=O)[O-]",
                },
            ),
            (
                "3-{1-oxo-6-[4-(piperidin-4-yl)butanamido]-2,3-dihydro-1H-"
                "isoindol-2-yl}propanoic acid",
                {
                    "inchi": "InChI=1/C20H27N3O4/c24-18(3-1-2-14-6-9-21-10-"
                    "7-14)22-16-5-4-15-13-23(11-8-19(25)26)20(27)17"
                    "(15)12-16/h4-5,12,14,21H,1-3,6-11,13H2,(H,22,"
                    "24)(H,25,26)/f/h22,25H",
                    "stdinchi": "InChI=1S/C20H27N3O4/c24-18(3-1-2-14-6-9-21"
                    "-10-7-14)22-16-5-4-15-13-23(11-8-19(25)26)"
                    "20(27)17(15)12-16/h4-5,12,14,21H,1-3,6-11,"
                    "13H2,(H,22,24)(H,25,26)",
                    "stdinchikey": "HWRXVANHVCXVHA-UHFFFAOYSA-N",
                    "smiles": "O=C1N(CC2=CC=C(C=C12)NC(CCCC1CCNCC1)=O)CCC(=O)O",
                },
            ),
        ]
        for iupac, dict_expected in pairs:
            dict_returned = convert_iupac_to_smiles_and_inchi(iupac)
            self.assertEqual(dict_expected["smiles"], dict_returned["smiles"])
            self.assertEqual(dict_expected["inchi"], dict_returned["inchi"])
            self.assertEqual(dict_expected["stdinchi"], dict_returned["stdinchi"])
            self.assertEqual(dict_expected["stdinchikey"], dict_returned["stdinchikey"])

    def test_invalid_entry(self):
        self.assertRaises(
            EntryNotFoundError,
            convert_iupac_to_smiles_and_inchi,
            "3-{1-oxo-6-[4-(piperid",
        )
        self.assertRaises(EntryNotFoundError, convert_iupac_to_smiles_and_inchi, None)


class TestConvertSMILESToIUPAC(TestCaseWithRequestsCache):
    """
    Test converting a smiles to IUPAC with cactus web service
    """

    def test_valid(self):
        smiles_to_iupacs = {"CCC": "propane"}
        for smiles, expected_iupac in smiles_to_iupacs.items():
            try:
                self.assertEqual(
                    convert_smiles_to_iupac(smiles).lower(), expected_iupac.lower()
                )
            except SSLError:
                self.skipTest("SSL connection issue with the CACTUS WS, skipping")


class DuplicateGeneNameTestCase(TestCaseWithRequestsCache):
    """
    Allow to have two proteins with the same gene_name and distinct uniprot_id
    """

    def test_works(self):
        Protein.objects.get_or_create(uniprot_id="P12497")
        Protein.objects.get_or_create(uniprot_id="P0C6F2")


class TestWS(TestCaseWithRequestsCache):
    def test_get_go_info(self):
        self.assertDictEqual(
            ws.get_go_info("GO_0140999"),
            {"label": "histone H3K4 trimethyltransferase activity"},
        )

    def test_get_pubchem_id(self):
        self.assertEqual(ws.get_pubchem_id("ZRIKDHLPCRURPX-CSKARUKUSA-N"), "24875307")

    def test_get_ligand_id(self):
        self.assertRaises(
            EntryNotFoundError,
            ws.get_ligand_id,
            "CC(C)C(=O)c1cc(C(=O)c2ccc(Oc3ccccc3)cc2)c(O)c(O)c1O",
        )
        self.assertEqual(
            ws.get_ligand_id(
                "FC(F)(F)c1c(Sc2ccccc2OCc2cccnc2)ccc(C=CC(=O)N2CCOCC2)c1C(F)(F)F"
            ),
            "BQN",
        )
