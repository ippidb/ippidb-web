import os
import requests_cache
from django.conf import settings

from ippidb.models import Compound, DrugBankCompound
from django.test import TestCase, TransactionTestCase


def create_dummy_compound(id_, smiles):
    c = Compound()
    c.id = id_
    c.canonical_smile = smiles
    c.is_macrocycle = True
    c.aromatic_ratio = 0.0
    c.balaban_index = 0.0
    c.fsp3 = 0.0
    c.gc_molar_refractivity = 0.0
    c.log_d = 0.0
    c.a_log_p = 0.0
    c.gc_molar_refractivity = 0.0
    c.mean_atom_vol_vdw = 0.0
    c.molecular_weight = 0.0
    c.nb_acceptor_h = 0
    c.nb_aliphatic_amines = 0
    c.nb_aromatic_bonds = 0
    c.nb_aromatic_ether = 0
    c.nb_aromatic_sssr = 0
    c.nb_atom = 0
    c.nb_atom_non_h = 0
    c.nb_benzene_like_rings = 0
    c.nb_bonds = 0
    c.nb_bonds_non_h = 0
    c.nb_br = 0
    c.nb_c = 0
    c.nb_chiral_centers = 0
    c.nb_circuits = 0
    c.nb_cl = 0
    c.nb_csp2 = 0
    c.nb_csp3 = 0
    c.nb_donor_h = 0
    c.nb_double_bonds = 0
    c.nb_f = 0
    c.nb_i = 0
    c.nb_multiple_bonds = 0
    c.nb_n = 0
    c.nb_o = 0
    c.nb_rings = 0
    c.nb_rotatable_bonds = 0
    c.randic_index = 0
    c.rdf070m = 0
    c.rotatable_bond_fraction = 0
    c.sum_atom_polar = 0
    c.sum_atom_vol_vdw = 0
    c.tpsa = 0
    c.ui = 0
    c.wiener_index = 0
    c.save(autofill=True)
    return c


def create_dummy_drugbank_compound(id_, smiles):
    dbc = DrugBankCompound()
    dbc.id = id_
    dbc.common_name = "DrugBankCompound" + str(id_)
    dbc.canonical_smiles = smiles
    dbc.save()


def my_install_cache():
    backend = requests_cache.SQLiteCache(
        os.path.join(settings.PERSISTENT_DIR, "tests_http_cache"),
    )
    requests_cache.install_cache(backend=backend)


class TestCaseWithRequestsCache(TestCase):

    def setUp(self):
        my_install_cache()


class TransactionTestCaseWithRequestsCache(TransactionTestCase):

    def setUp(self):
        my_install_cache()
