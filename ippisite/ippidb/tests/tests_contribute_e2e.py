"""
iPPI-DB contribution module "end to end" tests
"""
import math
import os
from decimal import Decimal
from tempfile import NamedTemporaryFile
import yaml
import glob

from django.contrib.auth import get_user_model
from django.db.models import Value
from django.db.models.functions import Concat
from django.test import SimpleTestCase
from django.urls import reverse
from parameterized import parameterized

from ippidb import models
from ippidb.admin import grant_contribution_permission
from ippidb.tests.utils import TransactionTestCaseWithRequestsCache
from ippidb.ws import get_uniprot_info


def compute_ppi_name(entry_data):
    """
    return the expected PPI name from a given contribution entry_data
    """
    bound_uniprots = [
        item["uniprot_id"]
        for item in entry_data["complex"]
        if item["complex_type"] == "Bound"
    ]
    partner_uniprots = [
        item["uniprot_id"]
        for item in entry_data["complex"]
        if item["complex_type"] == "Partner"
    ]
    bound_protein_names = [
        get_uniprot_info(uniprot_id)["short_name"] for uniprot_id in bound_uniprots
    ]
    partner_protein_names = [
        get_uniprot_info(uniprot_id)["short_name"] for uniprot_id in partner_uniprots
    ]
    bound_str = ",".join(sorted(list(set(bound_protein_names))))
    partner_str = ",".join(sorted(list(set(partner_protein_names))))
    name = bound_str
    if partner_str != "":
        name += " / " + partner_str
    return name


class ContributionE2ETestCase(TransactionTestCaseWithRequestsCache):
    """
    This class tests the Contribution Wizard by interacting with it
    in the same way a user would do.
    """
    reset_sequences = True
    SYMMETRIES = [
        ["AS", "asymmetric"],
        ["C2", "C2 symmetry"],
        ["D2", "D2 symmetry"],
        ["C3", "C3 symmetry"],
        ["D3", "D3 symmetry"],
        ["C4", "C4 symmetry"],
        ["D4", "D4 symmetry"],
        ["C5", "C5 symmetry"],
        ["D5", "D5 symmetry"],
    ]

    def setUp(self):
        super().setUp()
        login = "contributor"
        password = "12345"
        User = get_user_model()
        User.objects.create_user(username=login, password=password)
        self.client.login(username=login, password=password)
        grant_contribution_permission(None, None, User.objects.all())
        for item in self.SYMMETRIES:
            symmetry = models.Symmetry()
            symmetry.code = item[0]
            symmetry.description = item[1]
            symmetry.save()
        models.Taxonomy.objects.create(taxonomy_id=9606, name="Homo sapiens")

    @staticmethod
    def get_step_url(step_id):
        step_url = reverse("admin-session-add")
        if step_id is not None:
            step_url += step_id + "/"
        return step_url

    def _process_contribution_wizard(self, entry_data):
        future_expected_equals = [
            (
                models.Bibliography.objects.count,
                models.Bibliography.objects.count() + 1,
                "Bibliography count",
            ),
            (
                models.Contribution.objects.count,
                models.Contribution.objects.count() + 1,
                "Contribution count",
            ),
            (
                models.Compound.objects.count,
                models.Compound.objects.count() + len(entry_data["compounds"]),
                "Compounds count",
            ),
            (
                models.Compound.objects.validated().count,
                models.Compound.objects.validated().count(),
                "Validated Compounds count should remains the same",
            ),
            (
                models.CompoundAction.objects.count,
                models.CompoundAction.objects.count() + len(entry_data["compounds"]),
                "Compound Actions count",
            ),
            (
                models.TestActivityDescription.objects.count,
                models.TestActivityDescription.objects.count()
                + len(entry_data.get("activity_tests", [])),
                "Activity tests count",
            ),
            (
                models.TestCytotoxDescription.objects.count,
                models.TestCytotoxDescription.objects.count()
                + len(entry_data.get("cytotox_tests", [])),
                "Cytotox tests count",
            ),
            (
                models.TestPKDescription.objects.count,
                models.TestPKDescription.objects.count()
                + len(entry_data.get("pharmacokinetic_tests", [])),
                "Pharmacokinetic tests count",
            ),
            (
                lambda: models.PpiFamily.objects.get().name,
                entry_data.get("family_name"),
                "Ppi family name",
            ),
            (
                lambda: models.Ppi.objects.get().family.name,
                entry_data.get("family_name"),
                "Ppi family name from PPI",
            ),
            (
                lambda: models.Ppi.objects.get().name,
                compute_ppi_name(entry_data),
                "Ppi name",
            ),
        ]
        for activity_tests in entry_data.get("activity_tests", []):
            for compound_activity_results in activity_tests[
                "compound_activity_results"
            ]:
                if (
                    "activity_mol" in compound_activity_results
                    and compound_activity_results["activity_mol"] != ""
                ):
                    if compound_activity_results["activity_type"] != "KdRat":
                        compound_activity_results["activity"] = -math.log10(
                            Decimal(str(compound_activity_results["activity_mol"]))
                            * Decimal(str(compound_activity_results["activity_unit"]))
                        )
                    else:
                        compound_activity_results["activity"] = Decimal(
                            str(compound_activity_results["activity_mol"])
                        )

        self._process_contribution_wizard_without_sanity_check(entry_data)

        for fcn, results, msg in future_expected_equals:
            self.assertEqual(fcn(), results, msg=msg)
        post_validation_expected_equals = [
            (
                models.Compound.objects.validated().count,
                models.Compound.objects.count(),
                "Validated Compounds count should have increased",
            )
        ]
        contribution_to_be_validated = models.Contribution.objects.get(validated=False)
        contribution_to_be_validated.validated = True
        contribution_to_be_validated.save()
        for fcn, results, msg in post_validation_expected_equals:
            self.assertEqual(fcn(), results, msg=msg)

    def _process_contribution_wizard_without_sanity_check(
        self, entry_data, error_expected_in_step=None
    ):
        """
        The contribution add "wizard"
        returns a 200
        """
        wizard_data = self._generate_wizard_data(entry_data)
        for step in wizard_data:
            step_url = self.get_step_url(step.get("step-id"))
            response = self.client.get(step_url)
            # post form data
            if step.get("form-data") is not None:
                err_msg = (
                    f"Response code not ok when getting form "
                    f"for {step.get('step-id')} at {step_url}"
                )
                self.assertEqual(response.status_code, 200, err_msg)
                form_data = {
                    step["step-id"] + "-" + param_name: value
                    for param_name, value in step.get("form-data").items()
                }
                form_data["ippi_wizard-current_step"] = step["step-id"]
                response = self.client.post(step_url, form_data)
            error_is_now = (
                error_expected_in_step == step.get("step-id")
                and response.status_code != 302
            )
            if (
                response.status_code != 302
                and step.get("step-id") != "done"
                or error_is_now
            ):
                file_path = self.write_in_tmp_file(response)
                err_msg = (
                    f"Response code not ok when getting form for "
                    f"{step.get('step-id')} at {step_url}. Server"
                    f" response is stored in {file_path}"
                )
                if error_is_now:
                    self.assertEqual(response.status_code, 200, err_msg)
                    return
                self.assertEqual(response.status_code, 302, err_msg)
            # check redirect to next step (if there is one)
            if step.get("next") is not None:
                redirect_url = self.get_step_url(step.get("next"))
                self.assertEqual(
                    response.url,
                    redirect_url,
                    f"wrong redirection URL after step {step['step-id']}",
                )
        self.assertEqual(
            response.url,
            reverse("contribution-detail", kwargs={"pk": 1}),
            "wrong final URL, should be the contribution permanent URL",
        )

    def _generate_wizard_data(self, entry_data):
        """
        Generate the wizard form data dynamically based on iPPI-DB entry data
        """

        def get_id_form():
            return {
                "source": entry_data["source"],
                "id_source": entry_data["id_source"],
            }

        def get_bibliography_form():
            ret = {
                "source": entry_data["source"],
                "id_source": entry_data["id_source"],
                "title": "Opportunistic+amoebae:+challenges"
                "+in+prophylaxis+and+treatment.",
                "journal_name": "Drug+resistance+updates+:+reviews+and+"
                "commentaries+in+antimicrobial+and+anticancer"
                "+chemotherapy",
                "authors_list": "Schuster+FL,+Visvesvara+GS",
                "biblio_year": "2004",
            }
            for k in [
                "cytotox",
                "xray",
                "in_silico",
                "in_vitro",
                "in_cellulo",
                "in_vivo",
                "pharmacokinetic",
            ]:
                if entry_data.get(k, False):
                    ret[k] = "on"
            return ret

        def get_pdb_form():
            return {"pdb_id": entry_data["pdb_id"]}

        def get_complex_type_form():
            return {
                "complexType": entry_data["complexType"],
                "complexChoice": entry_data["complexChoice"],
            }

        def get_complex_form():
            data = {
                "TOTAL_FORMS": len(entry_data["complex"]),
                "INITIAL_FORMS": len(entry_data["complex"]),
                "MIN_NUM_FORMS": 0,
                "MAX_NUM_FORMS": 1000,
            }
            for idx, entry_complex in enumerate(entry_data["complex"]):
                data["{}-protein".format(idx)] = models.Protein.objects.get(
                    uniprot_id=entry_complex["uniprot_id"]
                ).id
                data["{}-complex_type".format(idx)] = entry_complex["complex_type"]
                if entry_complex["domain_pfam_acc"] is not None:
                    try:
                        data["{}-domain".format(idx)] = models.Domain.objects.get(
                            pfam_acc=entry_complex["domain_pfam_acc"]
                        ).id
                    except models.Domain.DoesNotExist as dne:
                        print(
                            "No domain in DB for PFAM ACC {}".format(
                                entry_complex["domain_pfam_acc"]
                            )
                        )
                        raise dne
                else:
                    data["{}-domain".format(idx)] = ""
                data["{}-ppc_copy_nb".format(idx)] = entry_complex["ppc_copy_nb"]
                data["{}-cc_nb".format(idx)] = entry_complex["cc_nb"]
                data["{}-ppp_copy_nb_per_p".format(idx)] = entry_complex[
                    "ppp_copy_nb_per_p"
                ]
            return data

        def get_ppi_form():
            if "symmetry" in entry_data:
                symmetry_id = models.Symmetry.objects.get(
                    code=entry_data["symmetry"]
                ).id
            return {
                "family": "",
                "pdb_id": entry_data["pdb_id"],
                "family_name": entry_data["family_name"],
                "symmetry": symmetry_id,  # FIXME
                "pockets_nb": 1,  # FIXME
                "selected_diseases": "\n".join(entry_data["diseases"]),
            }

        def get_compound_form():
            data = {
                "TOTAL_FORMS": len(entry_data["compounds"]),
                # FIXME there should be a way to list more than one compound
                "INITIAL_FORMS": len(entry_data["compounds"]),
                "MIN_NUM_FORMS": 1,
                "MAX_NUM_FORMS": 1000,
            }
            for idx, compound_data_item in enumerate(entry_data["compounds"]):
                data[f"{idx}-common_name"] = (compound_data_item["common_name"],)
                data[f"{idx}-compound_name"] = (compound_data_item["compound_name"],)
                if "molecule_smiles" in compound_data_item:
                    data[f"{idx}-molecule_smiles"] = (
                        compound_data_item["molecule_smiles"],
                    )
                elif "molecule_iupac" in compound_data_item:
                    # if a IUPAC is provided for tests assume convert it to SMILES
                    # molecule_smiles = convert_iupac_to_smiles(
                    #    compound_data_item["molecule_iupac"]
                    # )
                    data[f"{idx}-molecule_iupac"] = compound_data_item["molecule_iupac"]
                data[f"{idx}-is_macrocycle"] = compound_data_item.get(
                    "is_macrocycle", False
                )
                if "ligand_id" in compound_data_item:
                    data[f"{idx}-ligand_id"] = compound_data_item["ligand_id"]
            return data

        def get_activity_description_form():
            data = {
                "TOTAL_FORMS": len(entry_data.get("activity_tests", [])),
                "INITIAL_FORMS": 0,
                "MIN_NUM_FORMS": 0,
                "MAX_NUM_FORMS": 1000,
            }
            for idx, activity_test in enumerate(entry_data.get("activity_tests", [])):
                data[f"{idx}-ppi"] = ""
                data[f"{idx}-test_name"] = activity_test["test_name"]
                data[f"{idx}-is_primary"] = activity_test.get("is_primary", False)
                data[f"{idx}-test_type"] = activity_test["test_type"]
                data[f"{idx}-test_modulation_type"] = activity_test[
                    "test_modulation_type"
                ]
                data[f"{idx}-nb_active_compounds"] = activity_test[
                    "nb_active_compounds"
                ]
                if "cell_line_name" in activity_test:
                    data[f"{idx}-cell_line_name"] = activity_test["cell_line_name"]
                data[
                    f"{idx}-compoundactivityresult_set-activity-results-TOTAL_FORMS"
                ] = 1  # len(activity_test.get("compound_activity_results",[]))
                data[
                    f"{idx}-compoundactivityresult_set-activity-results-INITIAL_FORMS"
                ] = 0
                data[
                    f"{idx}-compoundactivityresult_set-activity-results-MIN_NUM_FORMS"
                ] = 0
                data[
                    f"{idx}-compoundactivityresult_set-activity-results-MAX_NUM_FORMS"
                ] = 1000
                for nidx, compound_activity_result in enumerate(
                    activity_test["compound_activity_results"]
                ):
                    data[
                        f"{idx}-compoundactivityresult_set-activity-results-{nidx}-compound_name"
                    ] = compound_activity_result["compound_name"]
                    data[
                        f"{idx}-compoundactivityresult_set-activity-results-{nidx}-activity_type"
                    ] = compound_activity_result["activity_type"]
                    try:
                        data[
                            f"{idx}-compoundactivityresult_set-activity-results-{nidx}-activity_mol"
                        ] = compound_activity_result["activity_mol"]
                        data[
                            f"{idx}-compoundactivityresult_set-activity-results-{nidx}-activity_unit"
                        ] = compound_activity_result["activity_unit"]
                    except KeyError:
                        pass
                    data[
                        f"{idx}-compoundactivityresult_set-activity-results-{nidx}-modulation_type"
                    ] = compound_activity_result["modulation_type"]
                    # "id": "",
                    # "test_activity_description": "",
            return data

        def get_cytotox_description_form():
            data = {
                "TOTAL_FORMS": len(entry_data.get("cytotox_tests", [])),
                "INITIAL_FORMS": 0,
                "MIN_NUM_FORMS": 1,
                "MAX_NUM_FORMS": 1000,
            }
            for idx, cytotox_test in enumerate(entry_data.get("cytotox_tests", [])):
                data[f"{idx}-test_name"] = cytotox_test["test_name"]
                data[f"{idx}-compound_concentration"] = cytotox_test[
                    "compound_concentration"
                ]
                data[f"{idx}-cell_line_name"] = cytotox_test["cell_line_name"]

                data[
                    f"{idx}-compoundcytotoxicityresult_set-cytotox-results-TOTAL_FORMS"
                ] = len(cytotox_test.get("compound_cytotox_results", []))
                data[
                    f"{idx}-compoundcytotoxicityresult_set-cytotox-results-INITIAL_FORMS"
                ] = 0
                data[
                    f"{idx}-compoundcytotoxicityresult_set-cytotox-results-MIN_NUM_FORMS"
                ] = 0
                data[
                    f"{idx}-compoundcytotoxicityresult_set-cytotox-results-MAX_NUM_FORMS"
                ] = 1000
                for nidx, result in enumerate(cytotox_test["compound_cytotox_results"]):
                    data[
                        f"{idx}-compoundcytotoxicityresult_set-cytotox-results-{nidx}-compound_name"
                    ] = result["compound_name"]
                    data[
                        f"{idx}-compoundcytotoxicityresult_set-cytotox-results-{nidx}-toxicity"
                    ] = result["toxicity"]
            return data

        def get_pk_description_form():
            data = {
                "TOTAL_FORMS": len(entry_data.get("pharmacokinetic_tests", [])),
                "INITIAL_FORMS": 0,
                "MIN_NUM_FORMS": 1,
                "MAX_NUM_FORMS": 1000,
            }
            for idx, pk_test in enumerate(entry_data.get("pharmacokinetic_tests", [])):
                data[f"{idx}-test_name"] = pk_test["test_name"]
                data[f"{idx}-organism"] = pk_test["organism"]
                data[f"{idx}-administration_mode"] = pk_test["administration_mode"]
                if "concentration" in pk_test:
                    data[f"{idx}-concentration"] = pk_test["concentration"]
                data[f"{idx}-dose"] = pk_test["dose"]
                if "dose_interval" in pk_test:
                    data[f"{idx}-dose_interval"] = pk_test["dose_interval"]
                data[f"{idx}-compoundpkresult_set-pk-results-TOTAL_FORMS"] = len(
                    pk_test.get("compound_pk_results", [])
                )
                data[f"{idx}-compoundpkresult_set-pk-results-INITIAL_FORMS"] = 0
                data[f"{idx}-compoundpkresult_set-pk-results-MIN_NUM_FORMS"] = 0
                data[f"{idx}-compoundpkresult_set-pk-results-MAX_NUM_FORMS"] = 1000
                for nidx, result in enumerate(pk_test["compound_pk_results"]):
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-tolerated"
                    ] = result["tolerated"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-auc_av"
                    ] = result["auc_av"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-clearance_av"
                    ] = result["clearance_av"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-c_max_av"
                    ] = result["c_max_av"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-oral_bioavailability"
                    ] = result["oral_bioavailability"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-t_demi"
                    ] = result["t_demi"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-t_max"
                    ] = result["t_max"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-voldistribution_av"
                    ] = result["voldistribution_av"]
                    data[
                        f"{idx}-compoundpkresult_set-pk-results-{nidx}-compound_name"
                    ] = result["compound_name"]
            return data

        def get_save_in_db_form():
            return dict(ok=True)

        form_callables = {
            None: lambda: None,
            "IdForm": get_id_form,
            "BibliographyForm": get_bibliography_form,
            "PDBForm": get_pdb_form,
            "ProteinDomainComplexTypeForm": get_complex_type_form,
            "ProteinDomainComplexForm": get_complex_form,
            "PpiForm": get_ppi_form,
            "CompoundForm": get_compound_form,
            "ActivityDescriptionFormSet": get_activity_description_form,
        }
        if entry_data.get("cytotox", False):
            form_callables[
                "TestCytotoxDescriptionFormSet"
            ] = get_cytotox_description_form
        if entry_data.get("pharmacokinetic", False):
            form_callables["TestPKDescriptionFormSet"] = get_pk_description_form

        form_callables["SaveInDB"] = get_save_in_db_form
        # Adding a step for "done" in order to also save the data in the db
        form_callables["done"] = lambda: None

        form_ids = iter(list(form_callables.keys())[1:])
        for step in form_callables.keys():
            item = {
                "step-id": step,
                "form-data": form_callables[step](),
                "next": next(form_ids, None),
            }
            yield item

    def actual_test_entry_file(self, entry_file):
        self.actual_test_entry(os.path.join(os.path.dirname(__file__), entry_file))

    def actual_test_entry(self, entry_path):
        # load the test data
        entry_data = yaml.load(
            open(entry_path, "r", encoding="utf8"), Loader=yaml.FullLoader
        )
        # process the wizard
        self._process_contribution_wizard(entry_data)
        # test diseases
        input_diseases = set(entry_data["diseases"])
        input_diseases.discard("")
        self.assertSetEqual(
            input_diseases,
            set(
                models.Ppi.objects.get(pdb_id=entry_data["pdb_id"])
                .diseases.annotate(
                    raw=Concat(Value(";"), "name", Value(";"), "identifier", Value(";"))
                )
                .values_list("raw", flat=True)
            ),
        )
        # test compounds
        for c in entry_data["compounds"]:
            if "molecule_smiles" in c:
                comp = models.Compound.objects.get(canonical_smile=c["molecule_smiles"])
            elif "molecule_iupac" in c:
                comp = models.Compound.objects.get(iupac_name=c["molecule_iupac"])
            # test ligand ID has been stored if the X ray data option is checked
            if entry_data.get("xray", False) is True:
                ligand_id = c.get("ligand_id", None)
                if ligand_id is not None:
                    ligand_id = str(ligand_id)
                self.assertEqual(comp.ligand_id, ligand_id)
        # test activity
        bibliography = models.Bibliography.objects.get(
            id_source=entry_data["id_source"].strip()
        )
        for activity_test in entry_data["activity_tests"]:
            tad_filters = {
                "test_type": activity_test["test_type"],
                "test_name": activity_test["test_name"],
                "test_modulation_type": activity_test["test_modulation_type"],
                "nb_active_compounds": activity_test["nb_active_compounds"],
            }
            if (
                "cell_line_name" in activity_test
                and activity_test["cell_line_name"] != ""
            ):
                tad_filters["cell_line__name"] = activity_test["cell_line_name"]
            test_activity_description = models.TestActivityDescription.objects.get(
                **tad_filters
            )
            for compound_activity_results in activity_test["compound_activity_results"]:
                compound = models.RefCompoundBiblio.objects.get(
                    bibliography=bibliography,
                    compound_name=compound_activity_results["compound_name"],
                ).compound
                results = models.CompoundActivityResult.objects.get(
                    activity_type=compound_activity_results["activity_type"],
                    modulation_type=compound_activity_results["modulation_type"],
                    compound=compound,
                    test_activity_description=test_activity_description,
                )
                self.assertEqual(
                    results.activity_type, compound_activity_results["activity_type"]
                )
                self.assertEqual(
                    results.modulation_type,
                    compound_activity_results["modulation_type"],
                )
                self.assertEqual(
                    Decimal(compound_activity_results["activity"]).quantize(
                        Decimal(10)
                        ** -models.CompoundActivityResult._meta.get_field(
                            "activity"
                        ).decimal_places
                    ),
                    results.activity,
                )

    def write_in_tmp_file(self, response):
        """
        Write an HTTP response contents into a temporary file for debug

        :param response: response that will be saved
        :type respons: django.http.HttpResponse
        :return: path to the temporary file created
        :rtype: str
        """
        with NamedTemporaryFile(delete=False, suffix=".html") as f:
            f.write(response.content)
            return f.name


class ContributionE2ETestCase02(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_activity_computation_and_storage_pIC50_2380_002_e-9.yaml")


class ContributionE2ETestCase03(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_activity_computation_and_storage_pIC50_4_9846073_e-6.yaml")


class ContributionE2ETestCase04(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_activity_computation_and_storage_pIC50_639_39406_e-3.yaml")


class ContributionE2ETestCase05(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_activity_computation_and_storage_pIC50_6_85_e-0.yaml")


class ContributionE2ETestCase06(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_basic_entry.yaml")


class ContributionE2ETestCase07(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_complex_no_pfam.yaml")


class ContributionE2ETestCase08(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_entry_28.yaml")


class ContributionE2ETestCase09(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_simple_heterodimer_208.yaml")


class ContributionE2ETestCase10(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_simple_heterodimer.yaml")


class ContributionE2ETestCase11(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_simple_stabilized_heterodimer.yaml")


class ContributionE2ETestCase12(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_SPD00000304.yaml")


class ContributionE2ETestCase13(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_stabilizer_204.yaml")


class ContributionE2ETestCase14(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_with_all_tests.yaml")


class ContributionE2ETestCase15(ContributionE2ETestCase):
    def test_entry(self):
        self.actual_test_entry_file(entry_file="test_with_pk_test.yaml")


class CheckAllTestAreTested(SimpleTestCase):

    @parameterized.expand(
        [
            entry_path
            for entry_path in glob.glob(
                os.path.join(os.path.dirname(__file__), "*.yaml")
            )
        ]
    )
    def test_entry_file_are_tested(self, entry_file):
        # a dummy test to check that the file name is found in the current python scripts
        # I had to remove the initial use of parametrized in order to run scenario in parallel
        entry_file = entry_file.split('/')[-1]
        with open(__file__, 'r') as src:
            self.assertTrue(any([entry_file in line for line in src.readlines()]), entry_file + " not tested")
