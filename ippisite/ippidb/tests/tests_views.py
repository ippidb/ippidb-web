"""
iPPI-DB views tests
"""
from django.urls import reverse
from django.core.management import call_command
from parameterized import parameterized

from .utils import create_dummy_compound, TestCaseWithRequestsCache

tested_urlpatterns = [
    "index",
    "general",
    "pharmacology",
    "le_lle",
    "physicochemistry",
    "pca",
    "compound_list",
    # "redirect_compound_card",
    # "compound_card",
    "tutorials",
    # "admin-session",
    # "ippidb_step",
    # "admin-session-add",
    # "admin-session-view",
    # "biblio-view",
    # "ppi-view",
    # "contribution-detail",
    # "admin-session-update",
    # "mol2smi",
    # "smi2mol",
    # "smi2iupac",
    # "iupac2smi",
    # "getoutputjob",
]


class ViewTest(TestCaseWithRequestsCache):
    @parameterized.expand([urlpattern for urlpattern in tested_urlpatterns])
    def test_url(self, url_name):
        response = self.client.get(reverse(url_name))
        self.assertEqual(response.status_code, 200)


class CompoundCardViewTest(TestCaseWithRequestsCache):
    """
    Tests for compound card
    """

    def setUp(self):
        super().setUp()
        create_dummy_compound(1, "CC")

    def test_compound_card(self):
        """
        basic test for compound card
        """

        call_command("lle_le")
        call_command("pca")
        response = self.client.get(reverse("compound_card", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 200)

    def test_compound_card_no_lelleplot_data(self):
        """
        test that compound card is ok even if LE-LLE plot
        data are missing (which can happen during LE-LLE recomputing)
        """

        response = self.client.get(reverse("compound_card", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 200)
