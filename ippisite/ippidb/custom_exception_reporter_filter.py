import re
from django.views.debug import SafeExceptionReporterFilter


class CustomExceptionReporterFilter(SafeExceptionReporterFilter):

    def __init__(self):
        super().__init__()
        self.hidden_settings = re.compile(
            pattern=f'{self.hidden_settings.pattern}|BROKER_URL',
            flags=re.IGNORECASE
        )
