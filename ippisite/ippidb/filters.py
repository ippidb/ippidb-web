from django_filters.rest_framework import FilterSet, CharFilter, OrderingFilter
from ippidb.models import Cavity


class SummaryFilter(FilterSet):
    pdb = CharFilter(
        label="PDB",
        field_name="chain__pdb__code",
    )
    type = CharFilter(
        label="Type",
        field_name="type",
    )
    organism = CharFilter(
        label="Organism",
        field_name="chain__protein__organism__name",
        lookup_expr="icontains",
    )
    chain_uniprot = CharFilter(
        label="Chain Uniprot",
        field_name="chain__protein__uniprot_id",
    )

    partner_uniprot = CharFilter(
        label="Partner Uniprot",
        field_name="partner__chain__protein__uniprot_id",
    )
    ligand = CharFilter(
        label="Ligand",
        field_name="partner__ligand__pdb_ligand_id",
        lookup_expr="contains",
    )

    ordering = OrderingFilter(
        fields=(
            ("chain__pdb__code", "chain__pdb"),
            ("chain__protein__organism__name", "chain__organism"),
            ("chain__protein__uniprot_id", "chain__uniprot"),
            ("partner__chain__protein__uniprot_id", "partner__uniprot"),
            ("partner__ligand__pdb_ligand_id", "partner__pdb_ligand_id"),
            ("type", "type"),
            ("full_name", "full_name"),
        ),
    )

    class Meta:
        model = Cavity
        fields = [
            "full_name",
            "type",
            "pdb",
            "organism",
            "chain_uniprot",
            "partner_uniprot",
            "ligand",
        ]
