# Generated by Django 2.0.10 on 2019-02-13 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0017_auto_20181220_1117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compound',
            name='a_log_p',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True, verbose_name='ALogP (Partition coefficient octanol-1/water)'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='aromatic_ratio',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=3, null=True, verbose_name='Aromatic ratio'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='balaban_index',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=3, null=True, verbose_name='Balaban index'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='fsp3',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=3, null=True, verbose_name='Fsp3'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='gc_molar_refractivity',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='GC Molar Refractivity'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='inchi',
            field=models.TextField(blank=True, null=True, verbose_name='InChi'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='inchikey',
            field=models.TextField(blank=True, null=True, verbose_name='InChiKey'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='log_d',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True, verbose_name='LogD (Partition coefficient octanol-1/water, with pKa information)'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='mean_atom_vol_vdw',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True, verbose_name='Mean atom volume computed with VdW radii'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='molecular_weight',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True, verbose_name='Molecular weight'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_acceptor_h',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of hydrogen bond acceptors'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_aliphatic_amines',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of aliphatics amines'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_aromatic_bonds',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of aromatic bonds'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_aromatic_ether',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of aromatic ethers'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_aromatic_sssr',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of aromatic Smallest Set of System Rings (SSSR)'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_atom',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_atom_non_h',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of non hydrogen atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_benzene_like_rings',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of benzene-like rings'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_bonds',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of bonds'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_bonds_non_h',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of bonds not involving a hydrogen'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_br',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of Bromine atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_c',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of Carbon atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_chiral_centers',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of chiral centers'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_circuits',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of circuits'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_cl',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of Chlorine atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_csp2',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of sp2-hybridized carbon atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_csp3',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of sp3-hybridized carbon atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_donor_h',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of hydrogen bond donors'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_double_bonds',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of double bonds'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_f',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of fluorine atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_i',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of iodine atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_multiple_bonds',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of multiple bonds'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_n',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of nitrogen atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_o',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of oxygen atoms'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_rings',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of rings'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='nb_rotatable_bonds',
            field=models.IntegerField(blank=True, null=True, verbose_name='Number of rotatable bonds'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='randic_index',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True, verbose_name='Randic index'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='rdf070m',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='RDF070m, radial distribution function weighted by the atomic masses at 7Å'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='rotatable_bond_fraction',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=3, null=True, verbose_name='Fraction of rotatable bonds'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='sum_atom_polar',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='Sum of atomic polarizabilities'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='sum_atom_vol_vdw',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True, verbose_name='Sum of atom volumes computed with VdW radii'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='tpsa',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='Topological Polar Surface Area (TPSA)'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='ui',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True, verbose_name='Unsaturation index'),
        ),
        migrations.AlterField(
            model_name='compound',
            name='wiener_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='Wiener index'),
        ),
    ]
