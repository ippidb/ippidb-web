# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-07 14:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0027_auto_20180307_1327'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testactivitydescription',
            name='protein_bound_construct',
            field=models.CharField(blank=True, choices=[('F', 'Full length'), ('U', 'Unspecified')], max_length=5, null=True, verbose_name='Protein bound construct'),
        ),
    ]
