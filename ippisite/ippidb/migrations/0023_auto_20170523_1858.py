# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-23 18:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0022_auto_20170523_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testpkdescription',
            name='administration_mode',
            field=models.CharField(blank=True, choices=[('IV', ''), ('PO', ''), ('IP', ''), ('SL', 'SL')], max_length=2, null=True, verbose_name='Administration mode'),
        ),
        migrations.DeleteModel(
            name='AdministrationMode',
        ),
    ]
