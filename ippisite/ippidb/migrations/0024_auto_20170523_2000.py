# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-23 20:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0023_auto_20170523_1858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testactivitydescription',
            name='cell_line',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ippidb.CellLine'),
        ),
    ]
