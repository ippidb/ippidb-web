# Generated by Django 2.2.1 on 2019-12-19 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0039_auto_20191216_1438'),
    ]

    operations = [
        migrations.AddField(
            model_name='compound',
            name='families',
            field=models.TextField(blank=True, null=True, verbose_name='PPI family names'),
        ),
    ]
