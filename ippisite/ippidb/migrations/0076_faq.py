# Generated by Django 4.2.10 on 2024-02-16 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0075_alter_linkedpdb_chain_alter_linkedpdb_ligand_pl'),
    ]

    operations = [
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.TextField(verbose_name='Question')),
                ('answer', models.TextField(verbose_name='Answer')),
            ],
        ),
    ]
