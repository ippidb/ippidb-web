# Generated by Django 2.2.1 on 2020-06-23 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0060_network'),
    ]

    operations = [
        migrations.AddField(
            model_name='network',
            name='number',
            field=models.PositiveIntegerField(default=1, unique=True, verbose_name='Number of the network'),
        ),
        migrations.AlterField(
            model_name='network',
            name='label',
            field=models.CharField(max_length=250, unique=True),
        ),
    ]
