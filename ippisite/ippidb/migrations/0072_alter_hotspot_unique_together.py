# Generated by Django 4.2.5 on 2023-10-05 12:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0071_hotspot'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='hotspot',
            unique_together={('chain', 'residu_code', 'residu_number', 'type')},
        ),
    ]
