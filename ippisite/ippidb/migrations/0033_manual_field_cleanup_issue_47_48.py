# Generated by Bryan on 2019-09-19

from django.db import migrations


def migration_code(apps, schema_editor):
    TestActivityDescription = apps.get_model("ippidb", "TestActivityDescription")

    TestActivityDescription.objects.filter(test_name='MTT assay').update(test_name='MTT-assay')

    CellLine = apps.get_model("ippidb", "CellLine")

    for was, should_be in [
        ('MV4;11', 'MV4-11'),
        ('RS4;11', 'RS4-11'),
        ('MOLM13', 'MOLM-13'),
    ]:
        try:
            should_be = CellLine.objects.get(name=should_be)
        except CellLine.DoesNotExist:
            CellLine.objects.filter(name=was).update(name=should_be)
            continue

        for cell in CellLine.objects.filter(name=was):
            for related_set in ['testcytotoxdescription_set', 'testactivitydescription_set']:
                for t in getattr(cell, related_set, []).all():
                    t.cell_line = should_be
                    t.save()
            cell.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('ippidb', '0032_auto_20190911_1046'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=migrations.RunPython.noop),
    ]
