# Generated by Django 2.2.1 on 2019-10-18 11:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0036_auto_20191018_1113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='compoundactivityresult',
            name='inhibition_percentage',
        ),
    ]
