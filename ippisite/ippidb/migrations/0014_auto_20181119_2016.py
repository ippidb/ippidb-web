# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-19 20:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0013_auto_20181023_1630'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='mddrcompoundimport',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='mddrcompoundimport',
            name='activity_classes',
        ),
        migrations.DeleteModel(
            name='MDDRSimilarity',
        ),
        migrations.AlterModelOptions(
            name='compound',
            options={'ordering': ['id']},
        ),
        migrations.RemoveField(
            model_name='compound',
            name='mddr_compound',
        ),
        migrations.RemoveField(
            model_name='compoundaction',
            name='pdb_id',
        ),
        migrations.DeleteModel(
            name='MDDRActivityClass',
        ),
        migrations.DeleteModel(
            name='MDDRCompoundImport',
        ),
    ]
