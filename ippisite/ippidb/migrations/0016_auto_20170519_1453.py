# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-19 14:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0015_auto_20170519_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compound',
            name='iupac_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='IUPAC name'),
        ),
    ]
