# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-27 13:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0004_auto_20170314_1449'),
    ]

    operations = [
        migrations.CreateModel(
            name='CellLine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='TestActivityDescription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_name', models.CharField(max_length=100, verbose_name='Test name')),
                ('test_type', models.CharField(choices=[('BIOCH', 'Biochemical assay'), ('CELL', 'Cellular assay')], max_length=5, verbose_name='Test type')),
                ('test_modulation_type', models.CharField(choices=[('B', 'Binding'), ('I', 'Inhibition'), ('S', 'Stabilization')], max_length=1, verbose_name='Test modulation type')),
                ('nb_active_compounds', models.IntegerField(verbose_name='Total number of active compounds')),
                ('biblio_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ippidb.Bibliography')),
                ('cell_line', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ippidb.CellLine')),
                ('complex_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ippidb.ProteinDomainBoundComplex')),
                ('ppi_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ippidb.Ppi')),
            ],
        ),
        migrations.AlterModelOptions(
            name='mddrcompoundactivityclass',
            options={'verbose_name_plural': 'MDDR compound activity classes'},
        ),
        migrations.AlterModelOptions(
            name='mddrcompoundimport',
            options={'verbose_name_plural': 'MDDR compound imports'},
        ),
        migrations.AlterModelOptions(
            name='mddrsimilarity',
            options={'verbose_name_plural': 'MDDR similarities'},
        ),
        migrations.AlterField(
            model_name='taxonomy',
            name='taxonomy_id',
            field=models.DecimalField(decimal_places=0, max_digits=9, unique=True, verbose_name='NCBI TaxID'),
        ),
    ]
