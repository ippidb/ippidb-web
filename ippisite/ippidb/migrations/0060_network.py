# Generated by Django 2.2.1 on 2020-06-23 09:23

from django.db import migrations, models
import ippidb.models.targetcentric


def contentnetwork(instance, filename):
    return "content/networks_files/{}".format(filename)


class Migration(migrations.Migration):
    dependencies = [
        ("ippidb", "0059_merge_20200424_1834"),
    ]

    operations = [
        migrations.CreateModel(
            name="Network",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "jsonfile",
                    models.FileField(upload_to=contentnetwork),
                ),
                ("default", models.BooleanField(default=False)),
                ("label", models.CharField(max_length=250)),
            ],
        ),
    ]
