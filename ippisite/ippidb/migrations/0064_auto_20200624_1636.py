# Generated by Django 2.2.1 on 2020-06-24 16:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0063_auto_20200624_1238'),
    ]

    operations = [
        migrations.AlterField(
            model_name='network',
            name='json',
            field=models.TextField(blank=True, default='{}'),
        ),
    ]
