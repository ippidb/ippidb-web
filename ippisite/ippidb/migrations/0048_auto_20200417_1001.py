# Generated by Django 2.2.1 on 2020-04-17 10:01

from django.db import migrations

def remove_compound_drugbank_duplicates(apps, schema_editor):
    DrugbankCompoundTanimoto = apps.get_model("ippidb", "DrugbankCompoundTanimoto")
    db_alias = schema_editor.connection.alias
    last = None, None
    for dct in DrugbankCompoundTanimoto.objects.using(db_alias).order_by('compound','drugbank_compound'):
        if dct.compound == last[0] and dct.drugbank_compound == last[1]:
            dct.delete()
        else:
            last = (dct.compound, dct.drugbank_compound)

class Migration(migrations.Migration):

    dependencies = [
        ('ippidb', '0047_auto_20200416_1359'),
    ]

    operations = [
        migrations.RunPython(remove_compound_drugbank_duplicates),
        migrations.AlterUniqueTogether(
            name='drugbankcompoundtanimoto',
            unique_together={('compound', 'drugbank_compound')},
        ),
    ]
