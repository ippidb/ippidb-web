"""
iPPI-DB-Galaxy communication module
"""
from datetime import datetime
import time

from bioblend.galaxy import GalaxyInstance
from django.conf import settings
import requests
import urllib3

BASE_URL = settings.GALAXY_BASE_URL
KEY = settings.GALAXY_APIKEY
WORKFLOW_ID = settings.GALAXY_COMPOUNDPROPERTIES_WORKFLOWID


urllib3.disable_warnings()


class GalaxyCompoundPropertiesRunner(object):
    def __init__(self):
        self.galaxy_instance = GalaxyInstance(url=BASE_URL, key=KEY, verify=False)
        self.galaxy_instance.nocache = True

    def compute_properties_for_sdf_file(self, sdf_file_path):
        # create a history to store the workflow results
        now = datetime.now()
        date_time = now.strftime("%Y/%m/%d-%H:%M:%S")
        history_name = "compoundpropertiesjobrun_%s" % date_time
        history = self.galaxy_instance.histories.create_history(name=history_name)
        history_id = history["id"]
        if history["state"] not in ["new", "ok"]:
            raise Exception(
                f'Error creating history "{history_name}" (id {history_id})'
            )
        # launch data upload job
        upload_response = self.galaxy_instance.tools.upload_file(
            path=sdf_file_path, file_type="sdf", history_id=history_id
        )
        upload_data_id = upload_response["outputs"][0]["id"]
        upload_job = upload_response["jobs"][0]
        upload_job_id = upload_job["id"]
        # monitor data upload until completed or on error
        while upload_job["state"] not in ["ok"]:
            time.sleep(2)
            upload_job = self.galaxy_instance.jobs.show_job(upload_job_id)
            if upload_job["state"] in ["error", "deleted", "discarded"]:
                data = self.galaxy_instance.datasets.show_dataset(upload_data_id)
                raise Exception(
                    f"Error during Galaxy data upload job - name : "
                    f"{data['name']}, id : {upload_data_id}, "
                    f"error : {data['misc_info']}"
                )
        # check uploaded dataset status
        data = self.galaxy_instance.datasets.show_dataset(upload_data_id)
        if data["state"] not in ["ok"]:
            raise Exception(
                f"Error during Galaxy data upload result - name : "
                f"{data['name']}, id : {upload_data_id}, "
                f"error : {data['misc_info']}"
            )
        # submit compound properties computation job
        dataset_map = {"0": {"src": "hda", "id": upload_data_id}}
        workflow_job = self.galaxy_instance.workflows.invoke_workflow(
            WORKFLOW_ID, inputs=dataset_map, history_id=history_id
        )
        workflow_job_id = workflow_job["id"]
        while workflow_job["state"] not in ["ok", "scheduled"]:
            time.sleep(2)
            workflow_job = self.galaxy_instance.workflows.show_invocation(
                "dad6103ff71ca4fe", workflow_job_id
            )
            if workflow_job["state"] in ["error", "deleted", "discarded"]:
                raise Exception(
                    f"Error during Galaxy workflow job - name : "
                    f"id : {workflow_job_id}, "
                )
        datasets = self.galaxy_instance.histories.show_history(
            history_id, contents=True
        )
        actual_result_dataset = None
        for dataset in datasets:
            if dataset["extension"] == "json":
                actual_result_dataset = dataset
        if actual_result_dataset is None:
            raise Exception(
                f"Result for galaxy workflow invocation {workflow_job_id} not found in"
                f" history {history_id}"
            )
        dataset = self.galaxy_instance.datasets.show_dataset(
            actual_result_dataset["id"]
        )
        while dataset["state"] not in ["ok"]:
            time.sleep(2)
            try:
                dataset = self.galaxy_instance.datasets.show_dataset(
                    actual_result_dataset["id"]
                )
            except requests.exceptions.ChunkedEncodingError:
                continue
        download_url = dataset["download_url"]
        contents_resp = requests.get(BASE_URL + download_url, verify=False)
        contents = contents_resp.json()
        return contents
