"""
iPPI-DB web-service client utility functions
"""
import json
from typing import List

from urllib import parse as urllib_parse
import requests
from bioservices import xmltools
from bioservices.eutils import EUtils
from bioservices.uniprot import UniProt
from bs4 import BeautifulSoup
from django.conf import settings
from requests.exceptions import ContentDecodingError
from chembl_webresource_client.new_client import new_client


class EntryNotFoundError(Exception):
    """
    Exception raised if an entry is not found using on of the
    Web Service functions of this module
    """

    def __init__(self, entry_id: str, status_code: str = None, msg: str = None):
        """
        :param entry_id: ID of the entry not found
        :type entry_id: str
        :param status_code: status code sent back by the web service
        :type status_code: str
        :param msg: message sent back by the web service
        :type msg: str
        """
        self.entry_id = entry_id
        _msg = f"{entry_id} not found."
        if status_code is not None:
            _msg += f" status code: {status_code})."
        if msg is not None:
            _msg += f" error message: {msg}"
        super().__init__(msg)


class BibliographicalEntryNotFound(EntryNotFoundError):
    """
    Exception raised if a Bibliography entry is not found
    """

    pass


class PubMedEntryNotFound(BibliographicalEntryNotFound):
    """
    Exception raised if a PubMed entry is not found
    """

    def __str__(self):
        return f"Bibliographical data not found for PMID {self.entry_id}"


class PatentNotFound(BibliographicalEntryNotFound):
    """
    Exception raised if a Patent entry is not found
    """

    def __str__(self):
        return f"Bibliographical data not found for Patent {self.entry_id}"


class DOIEntryNotFound(BibliographicalEntryNotFound):
    """
    Exception raised if a DOI entry is not found
    """

    def __str__(self):
        return f"Bibliographical data not found for DOI {self.entry_id}"


def get_pubmed_info(pmid: int) -> dict:
    """
    Retrieve information about a publication from  NCBI PubMed

    :param pmid: PubMed ID
    :type pmid: str
    :return: publication metadata (title, journal name, publication year, authors list).
    :rtype: dict
    """
    eu = EUtils(email=settings.EUTILS_EMAIL)
    r = eu.EFetch("pubmed", pmid, retmode="dict", rettype="abstract")
    if isinstance(r, int):
        raise PubMedEntryNotFound(pmid, r)
    if r["PubmedArticleSet"] is None:
        raise PubMedEntryNotFound(pmid)
    article = r["PubmedArticleSet"]["PubmedArticle"]["MedlineCitation"]["Article"]
    title = article["ArticleTitle"]
    authors_list = [
        a["LastName"] + " " + a["Initials"] for a in article["AuthorList"]["Author"]
    ]
    authors = ", ".join(authors_list)
    journal_name = article["Journal"]["Title"]
    biblio_date = article["Journal"]["JournalIssue"]["PubDate"]
    if "Year" in biblio_date:
        biblio_year = biblio_date["Year"]
    else:
        biblio_year = biblio_date["MedlineDate"][0:3]
    return {
        "title": title,
        "journal_name": journal_name,
        "biblio_year": biblio_year,
        "authors_list": authors,
    }


def get_google_patent_info(patent_number: str) -> dict:
    """
    Retrieve information about a patent parsing Dublin Core info in the Google HTML

    :param patent_number: patent number
    :type patent_number: str
    :return: patent metadata (title, journal name, publication year, authors list).
    :rtype: dict
    """
    url = "https://patents.google.com/patent/{}".format(patent_number)
    try:
        resp = requests.get(url)
    except ContentDecodingError as e:
        raise PatentNotFound(patent_number, msg=str(e))
    if resp.status_code != 200:
        raise PatentNotFound(patent_number, resp.status_code)
    soup = BeautifulSoup(resp.text, "html.parser")
    title = soup.find_all("meta", attrs={"name": "DC.title"})[0]["content"].strip()
    authors_list = []
    for author_meta in soup.find_all("meta", attrs={"name": "DC.contributor"}):
        authors_list.append(author_meta["content"].strip())
    authors = ", ".join(authors_list)
    biblio_year = soup.find_all("meta", attrs={"name": "DC.date"})[0][
        "content"
    ].strip()[0:4]
    return {
        "title": title,
        "journal_name": None,
        "biblio_year": biblio_year,
        "authors_list": authors,
    }


def get_doi_info(doi: str) -> dict:
    """
    Retrieve information about a publication from DOI web services

    :param doi: DOI
    :type doi: str
    :return: publication metadata (title, journal name, publication year, authors list).
    :rtype: dict
    """
    resp = requests.get(
        "http://dx.doi.org/%s" % doi,
        headers={"Accept": "application/vnd.citationstyles.csl+json"},
    )
    try:
        resp.raise_for_status()
    except requests.HTTPError as he:
        if resp.status_code == 404:
            raise DOIEntryNotFound(doi, resp.status_code, he)
        else:
            raise he
    json_data = resp.json()
    title = json_data["title"]
    journal_name = json_data.get(
        "container-title", json_data.get("original-title", None)
    )
    biblio_year = 0
    try:
        if (
            "journal-issue" in json_data
            and "published-print" in json_data["journal-issue"]
        ):
            biblio_year = json_data["journal-issue"]["published-print"]["date-parts"][
                0
            ][0]
        elif "published-print" in json_data:
            biblio_year = json_data["published-print"]["date-parts"][0][0]
        elif "issued" in json_data:
            biblio_year = json_data["issued"]["date-parts"][0][0]
        else:
            biblio_year = json_data["published-online"]["date-parts"][0][0]
    except KeyError as e:
        raise e
    authors_list = []
    for author_data in json_data["author"]:
        try:
            if "family" in author_data:
                authors_list.append(
                    "%s %s" % (author_data["family"], author_data.get("given", ""))
                )
            else:
                authors_list.append(author_data["name"])
        except KeyError as e:
            raise e
    authors = ", ".join(authors_list)
    return {
        "title": title,
        "journal_name": journal_name,
        "biblio_year": biblio_year,
        "authors_list": authors,
    }


def get_uniprot_info(uniprot_id: str) -> dict:
    """
    Retrieve information about a protein from the Uniprot database

    :param uniprot_id: Uniprot ID
    :type uniprot_id: str
    :return: protein metadata (recommended name, organism, gene, entry name,
             short name, molecular functions).
    :rtype: dict
    """
    uniprot_client = UniProt()
    ns = {"u": "http://uniprot.org/uniprot"}
    try:
        resp = uniprot_client.retrieve(uniprot_id, frmt="xml")
        resp = xmltools.easyXML(resp)
    except TypeError:
        raise EntryNotFoundError(uniprot_id)
    if resp.root == "":
        raise EntryNotFoundError(uniprot_id)
    try:
        recommended_name = resp.root.findall(
            "u:entry/u:protein/u:recommendedName/u:fullName", ns
        )[0].text
    except Exception:
        recommended_name = None
    try:
        recommended_short_name = resp.root.findall(
            "u:entry/u:protein/u:recommendedName/u:shortName", ns
        )[0].text
    except Exception:
        recommended_short_name = None
    organism = resp.root.findall(
        'u:entry/u:organism/u:dbReference[@type="NCBI Taxonomy"]', ns
    )[0].attrib["id"]
    gene_names = []
    for el in resp.root.findall("u:entry/u:gene/u:name", ns):
        gene_name = {"name": el.text, "type": el.attrib["type"]}
        gene_names.append(gene_name)
    try:
        gene_id = resp.root.findall('u:entry/u:dbReference[@type="GeneID"]', ns)[
            0
        ].attrib["id"]
    except IndexError:
        gene_id = None
    entry_name = resp.root.findall("u:entry/u:name", ns)[0].text
    go_els = resp.root.findall('u:entry/u:dbReference[@type="GO"]', ns)
    accessions = [el.text for el in resp.root.findall("u:entry/u:accession", ns)]
    molecular_functions = []
    cellular_localisations = []
    biological_processes = []
    for go_el in go_els:
        term_property_value = go_el.findall('u:property[@type="term"]', ns)[0].attrib[
            "value"
        ]
        if term_property_value[0:2] == "F:":
            molecular_functions.append("GO_" + go_el.attrib["id"][3:])
        if term_property_value[0:2] == "C:":
            cellular_localisations.append("GO_" + go_el.attrib["id"][3:])
        if term_property_value[0:2] == "P:":
            biological_processes.append("GO_" + go_el.attrib["id"][3:])
    citations = []
    for el in resp.root.findall("u:entry/u:reference", ns):
        try:
            doi = el.findall('u:citation/u:dbReference[@type="DOI"]', ns)[0].attrib[
                "id"
            ]
            pmid = el.findall('u:citation/u:dbReference[@type="PubMed"]', ns)[0].attrib[
                "id"
            ]
            citations.append({"doi": doi, "pmid": pmid})
        except IndexError:
            continue
    alternative_names = []
    for el in resp.root.findall("u:entry/u:protein/u:alternativeName", ns):
        alternative_name = {"full": el.findall("u:fullName", ns)[0].text}
        if el.findall("u:shortName", ns):
            alternative_name["short"] = el.findall("u:shortName", ns)[0].text
        alternative_names.append(alternative_name)

    db_references = resp.root.findall('u:entry/u:dbReference[@type="Pfam"]', ns)
    domains = []
    for db_reference in db_references:
        name = db_reference.attrib["id"]
        domains.append(name)

    return {
        "recommended_name": recommended_name,
        "recommended_short_name": recommended_short_name,
        "organism": int(organism),
        "gene_id": int(gene_id) if gene_id else None,
        "accessions": accessions,
        "gene_names": gene_names,
        "entry_name": entry_name,
        "short_name": entry_name.split("_")[0],
        "molecular_functions": molecular_functions,
        "domains": domains,
        "cellular_localisations": cellular_localisations,
        "biological_processes": biological_processes,
        "citations": citations,
        "alternative_names": alternative_names,
    }


def get_go_info(go_id: str) -> dict:
    """
    Retrieve information about a GO term using the EBI OLS web service

    :param go_id: Gene Ontology id
    :type go_id: str
    :return: GO metadata (label).
    :rtype: dict
    """
    resp = requests.get(
        f"https://www.ebi.ac.uk/ols4/api/ontologies/go/terms/"
        f"http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252F{go_id}"
    )
    data = resp.json()
    label = data["label"]
    return {"label": label}


def get_taxonomy_info(taxonomy_id: str) -> dict:
    """
    Retrieve information about a taxon using the NCBI Entrez web services

    :param taxonomy_id: Taxonomy ID
    :type taxonomy_id: str
    :return: Taxon metadata (scientific name).
    :rtype: dict
    """
    eu = EUtils(email=settings.EUTILS_EMAIL)
    r = eu.EFetch("taxonomy", taxonomy_id, retmode="dict")
    scientific_name = r["TaxaSet"]["Taxon"]["ScientificName"]
    return {"scientific_name": scientific_name}


def get_pfam_info(pfam_acc: str) -> dict:
    """
    Retrieve information about a protein family using the PFAM web service

    :param pfam_acc: PFAM accession number
    :type pfam_acc: str
    :return: Protein family metadata (id, description).
    :rtype: dict
    """
    resp = requests.get("https://www.ebi.ac.uk/interpro/api/entry/pfam/{}?format=json".format(pfam_acc))
    entry = resp.json()['metadata']
    pfam_id = entry["name"]["short"]
    description = ""
    if entry["description"]:
        if isinstance(entry["description"][0], dict):
            descriptions_list = []
            for desc in entry["description"]:
                descriptions_list.append(desc['text'])
            description += "".join(descriptions_list).strip()
        else:
            description += "".join(entry["description"]).strip()
    if entry["wikipedia"]:
        description += "".join(entry["wikipedia"]['extract']).strip()
    return {"id": pfam_id, "description": description}


def get_pdb_uniprot_mapping(pdb_id: str) -> List[str]:
    """
    Retrieve PDB to uniprot mappings using the PDBe web service

    :param pdb_id: PDB ID
    :type pdb_id: str
    :return: Uniprot IDs
    :rtype: List[str]
    """
    pdb_id = pdb_id.lower()
    resp = requests.get(
        "https://www.ebi.ac.uk/pdbe/api/mappings/uniprot/{}".format(pdb_id.lower())
    )
    if resp.status_code != 200:
        raise EntryNotFoundError(pdb_id, resp.status_code)
    uniprot_ids = list(resp.json()[pdb_id]["UniProt"].keys())
    if len(uniprot_ids) == 0:
        raise EntryNotFoundError(
            pdb_id,
            resp.status_code,
            msg="PDB entry does not have any existing Uniprot mapping",
        )
    return uniprot_ids


def get_pdb_pfam_mapping(pdb_id: str) -> dict:
    """
    Retrieve PDB to pfam mappings using the PDBe web service

    :param pdb_id: PDB ID
    :type pdb_id: str
    :return: Pfam along with their name identifier and description
    :rtype: dict
    """
    pdb_id = pdb_id.lower()
    resp = requests.get(
        "https://www.ebi.ac.uk/pdbe/api/mappings/pfam/{}".format(pdb_id.lower())
    )
    if "application/json" not in resp.headers.get("content-type"):
        raise EntryNotFoundError(pdb_id, resp.status_code)
    all_pfam = resp.json()[pdb_id]["Pfam"]
    for pfam in all_pfam.values():
        try:
            del pfam["mappings"]
        except KeyError:
            pass
    return all_pfam


def pdb_entry_exists(pdb_id: str) -> bool:
    """
    Test if a PDB entry exists using EBI web services

    :param pdb_id: PDB ID
    :type pdb_id: str
    :return: True/False wether the PDB entry exists
    :rtype: bool
    """
    resp = requests.get(
        "https://www.ebi.ac.uk/pdbe/api/pdb/entry/summary/{}".format(pdb_id.lower())
    )
    # EBI sends back either a 404 or an empty json if the PDB does not exist
    if not (resp.ok):
        return False
    else:
        return True


def convert_pdb_ligand_id_to_iupac(ligand_id):
    """
    return iupac from pdb ligand id
    :param ligand_id: PDB Ligand ID
    :type ligand_id: str
    :return: IUPAC, smiles
    :rtype: str
    """
    resp = requests.get(
        "https://www.ebi.ac.uk/pdbe/api/pdb/compound/summary/{}".format(
            ligand_id.lower()
        )
    )
    if resp.status_code != 200:
        raise EntryNotFoundError(ligand_id, resp.status_code)
    ret = resp.json()
    return ret[ligand_id.upper()][0]["name"]


def convert_iupac_to_smiles(iupac: str) -> str:
    """
    Convert a IUPAC name to SMILES format
    using the Opsin web service

    :param iupac: IUPAC name for the compound
    :type iupac: str
    :return: the SMILES
    :rtype: str
    """
    return convert_iupac_to_smiles_and_inchi(iupac)["smiles"]


def convert_iupac_to_smiles_and_inchi(iupac: str) -> dict:
    """
    Convert a IUPAC name to SMILES and INCHI formats
    using the Opsin web service

    :param iupac: IUPAC name for the compound
    :type iupac: str
    :return: dictionary containing the SMILES and INCHI
    :rtype: dict
    """
    if iupac is None:
        raise EntryNotFoundError(iupac, 422)
    resp = requests.get(
        "https://opsin.ch.cam.ac.uk/opsin/{}".format(urllib_parse.quote(iupac, safe=""))
    )
    if resp.status_code != 200:
        raise EntryNotFoundError(iupac, resp.status_code)
    ret = resp.json()
    ret.pop("cml")
    ret.pop("message")
    return ret


def convert_pdb_ligand_id_to_iupacandsmiles(ligand_id):
    """
    return iupac from pdb ligand id
    :param ligand_id: PDB Ligand ID
    :type ligand_id: str
    :return: IUPAC
    :rtype: str
    """
    resp = requests.get(
        "https://www.ebi.ac.uk/pdbe/api/pdb/compound/summary/{}".format(
            ligand_id.lower()
        )
    )
    if resp.status_code != 200:
        raise EntryNotFoundError(ligand_id, resp.status_code)
    ret = resp.json()
    iupac = ret[ligand_id.upper()][0]["name"]
    cansmiles = None
    for smiles in ret[ligand_id.upper()][0]["smiles"]:
        if smiles["program"] == "CACTVS":
            cansmiles = smiles["name"]
    if not cansmiles:
        try:
            cansmiles = convert_iupac_to_smiles(iupac)
        except EntryNotFoundError:
            cansmiles = ""
    return iupac, cansmiles


def convert_smiles_to_iupac(smiles: str) -> str:
    """
    Convert a compound SMILES to IUPAC name
    using the NIH Cactus Web Service

    :param smiles: IUPAC name for the compound
    :type smiles: str
    :return: the IUPAC name
    :rtype: str
    """
    return requests.get(
        f"https://cactus.nci.nih.gov/chemical/structure/{smiles}/iupac_name"
    ).text


def get_chembl_id(inchikey: str) -> str:
    """
    Retrieve the ChEMBL compound ID for a given
    compound InChi Key

    :param inchikey: InChi Key for the compound
    :type inchikey: str
    :return: the ChEMBL ID
    :rtype: str
    """
    molecule = new_client.molecule
    try:
        m = molecule.get(inchikey)
        return m["molecule_chembl_id"]
    except Exception as e:
        raise EntryNotFoundError(inchikey, msg=str(e))


def get_pubchem_id(inchikey: str) -> str:
    """
    Retrieve the PubChem compound ID for a given
    compound InChi Key
    Documentation for the corresponding web-service:
    https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest

    :param inchikey: InChi Key for the compound
    :type inchikey: str
    :return: the PubChem ID
    :rtype: str
    """
    url = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchikey/{inchikey}/json"
    resp = requests.get(url)
    json_data = resp.json()
    if "Fault" in json_data:
        raise EntryNotFoundError(
            inchikey,
            msg=str(
                f"compound for inchikey {inchikey} not found in PubChem (fault in response)"
            ),
        )
    else:
        try:
            return str(json_data["PC_Compounds"][0]["id"]["id"]["cid"])
        except KeyError:
            raise EntryNotFoundError(
                inchikey,
                msg=str(
                    f"compound for inchikey {inchikey} not found in PubChem (ID not in response)"
                ),
            )


def get_ligand_id(smiles: str) -> str:
    endpoint = "https://search.rcsb.org/rcsbsearch/v2/query"
    query = {
        "query": {
            "type": "terminal",
            "service": "chemical",
            "parameters": {
                "value": smiles,
                "type": "descriptor",
                "descriptor_type": "SMILES",
                "match_type": "graph-strict"
            }
        },
        "return_type": "mol_definition"
    }
    resp = requests.get(
        endpoint, params={"json": json.dumps(query)}
    )
    if resp.text == '':
        response = dict(total_count=0)
    else:
        response = resp.json()
    if response['total_count'] == 0:
        raise EntryNotFoundError(
            smiles, msg=str(f"ligand for smiles {smiles} not found in PDB Ligand")
        )
    results = sorted(response['result_set'], key=lambda x: -x['score'])
    return results[0]['identifier']


def get_orcid_user_details(orcid: str) -> str:
    endpoint = f"https://pub.orcid.org/v3.0/{orcid}/person"
    resp = requests.get(
        endpoint,
        headers={"Accept": "application/json"},
    )
    data = resp.json()
    details = {
        "first_name": data["name"]["given-names"]["value"],
        "last_name": data["name"]["family-name"]["value"],
    }
    try:
        details["keywords"] = data["keywords"]["keyword"][0]["content"]
    except Exception:
        details["keywords"] = None
    return details
