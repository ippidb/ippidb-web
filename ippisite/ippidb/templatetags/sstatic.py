import os
import random
import string

from django import template
from django.conf import settings

# credits : https://bitbucket.org/ad3w/django-sstatic

register = template.Library()


@register.simple_tag
def sstatic(path, always_fresh=True):
    """
    Returns absolute URL to static file with versioning.
    """
    full_path = os.path.join(settings.STATIC_URL, path[1:] if path[0] == "/" else path)
    if not settings.DEBUG or not always_fresh:
        return full_path
    try:
        # Get file modification time.
        mtime = os.path.getmtime(full_path)
        return "%s%s?%s" % (settings.STATIC_URL, path, mtime)
    except OSError:
        # Returns normal url if this file was not found in filesystem.
        return "%s%s?%s" % (
            settings.STATIC_URL[:-1],
            path,
            "".join(
                random.choice("".join((string.ascii_letters, string.digits)))
                for _ in range(32)
            ),
        )
