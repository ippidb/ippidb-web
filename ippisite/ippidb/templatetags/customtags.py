import logging
import numpy as np
import colorsys
from django import forms
from django import template
from django.db.models import F, Func
from django.core.exceptions import FieldError
from django.contrib.messages.storage.base import Message
from django.db.models import QuerySet
from django.forms.utils import ErrorList, ErrorDict
from django.utils.safestring import mark_safe
from django.utils.translation import gettext

register = template.Library()
logger = logging.getLogger(__name__)


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()
    dict_[field] = value
    return dict_.urlencode()


@register.simple_tag
def status_class(value):
    if value is True:
        return "table-success"
    elif value is False:
        return "table-danger"
    else:
        return "table-secondary"


@register.simple_tag
def rule_status_icon(value, title):
    if value is True:
        class_suffix = "success"
        icon_suffix = "check"
    elif value is False:
        class_suffix = "danger"
        icon_suffix = "times"
    else:
        class_suffix = "secondary"
        class_suffix = "question"
    return mark_safe(
        f'<i class="fas fa-{icon_suffix} text-{class_suffix}" title="{title}: failed"></i>'
    )


@register.filter
def bootstrap(object):
    return mark_safe("".join(bootstrap_core(object)))


@register.filter
def get_item(d, key):
    return d[key]


def bootstrap_core(object):
    ret = []
    if isinstance(object, forms.BoundField):
        field = object
        print_label = True
        if isinstance(field.field.widget, forms.HiddenInput):
            ret.append(field.as_widget())
            if field.errors:
                for e in field.errors:
                    ret.append(
                        """<div class="alert alert-danger">%s: %s</div>"""
                        % (field.name, e)
                    )
            return ret
        attrs = field.field.widget.attrs
        # get the class specified in the code
        css_classes = set(attrs.get("class", "").split(" "))
        # if the field has errors, we add bootstrap classes
        if field.errors:
            css_classes.add("is-invalid")
        # we propagate the "required" attribute
        if field.field.required:
            attrs["required"] = "required"
        # we fetch the "help_text" attribute from wherever we can
        help_text = getattr(field, "help_text", None) or attrs.get("help_text", None)

        if isinstance(field.field.widget, forms.widgets.CheckboxInput):
            # if it is a checkbox, we the classes are not the same
            wrapping_classes = "form-group form-check"
            label_classes = "form-check-label"
            css_classes.add("form-check-input")
        elif isinstance(field.field.widget, forms.widgets.SelectMultiple):
            # if it is a SelectMultiple, we assume that we will handle it on client side with boostrap-mutliselect
            wrapping_classes = "form-group"
            attrs["data-label"] = field.label
            print_label = False
        else:
            # usual classes
            wrapping_classes = "input_field form-group"
            label_classes = "form-control-placeholder"
            css_classes.add("form-control")
        if field.name == "DELETE":
            # HACK : If the field is the DELETE button of the formset
            attrs["onchange"] = "delete_button_clicked(this);"
            field.label = gettext("DELETE_label")
            wrapping_classes += " formset-item-delete-host delete-btn col order-1"
            css_classes.add("formset-item-delete")
        # HACK: if the css_class is a bootstrap column layout, transfer it to container element
        for css_class in css_classes:
            if (
                css_class.startswith("col")
                or css_class.startswith("order-")
                or css_class.startswith("mb-")
            ):
                wrapping_classes += f" {css_class}"
        css_classes = set(
            [
                css_class
                for css_class in css_classes
                if not (
                    css_class.startswith("col")
                    or css_class.startswith("order-")
                    or css_class.startswith("mb-")
                )
            ]
        )

        # overwrite the css classes
        attrs["class"] = " ".join(css_classes)

        if "datalist" in attrs:
            datalist = attrs.pop("datalist")
            attrs["list"] = "list__%s" % field.id_for_label
        else:
            datalist = None

        # render the field
        ret.append('<div class="%s">' % wrapping_classes)
        ret.append(field.as_widget(attrs=attrs))

        if print_label:
            # most of the use case want to print the label
            ret.append(
                """<label class="%s" for="%s">%s%s</label>"""
                % (
                    label_classes,
                    field.id_for_label,
                    field.label,
                    " *" if field.field.required else "",
                )
            )

        # If the field has a datalist attribut the input field will suggest from it, printing the element
        if datalist:
            ret.append('<datalist id="%s">' % attrs["list"])
            for option in datalist:
                ret.append('<option value="%s">' % option)
            ret.append("</datalist>")

        if help_text:
            ret.append(
                """<small class ="form-text text-muted">%s</small>""" % help_text
            )
        if field.errors:
            # render its errors
            for e in field.errors:
                ret.append("""<div class="invalid-feedback">%s</div>""" % e)
        ret.append("</div>")
        # return it as safe html code
    elif isinstance(object, forms.BaseFormSet):
        # print formset's error which are not related to a form the formset itself
        formset = object
        for error in formset.non_form_errors():
            ret.append(
                """<div class="alert alert-danger">%s</div>"""
                % error.replace("\n", "<br/>")
            )
    elif isinstance(object, forms.BaseForm):
        # print form's error which are not related to a field but the form itself
        form = object
        for field in form:
            for el in bootstrap_core(field):
                ret.append(el)
    elif isinstance(object, ErrorList):
        # print errors in a bootstrap way
        for error in object:
            ret.append(
                """<div class="alert alert-danger">%s</div>"""
                % error.replace("\n", "<br/>")
            )
    elif isinstance(object, ErrorDict):
        # print errors in a bootstrap way
        for key, error in object.items():
            ret.append('<div class="alert alert-danger"><b>%s:</b>' % key)
            for line in bootstrap_core(error):
                ret.append(line)
            ret.append("</div>")
    elif isinstance(object, Message):
        # print messages in a bootstrap way
        message = object
        level_tag = message.level_tag
        if level_tag == "error":
            level_tag = "danger"
        elif level_tag == "debug":
            level_tag = "default"
        ret.append(
            """<div class="alert alert-%s">%s</div>"""
            % (level_tag, message.message.replace("\n", "<br/>"))
        )
    elif len(str(object)) > 0:
        ret.append(
            """<div class="alert alert-danger">Can't bootstrapize object of class %s</div>"""
            % str(type(object).__name__)
        )
    return ret


@register.filter("startswith")
def startswith(text, starts):
    return text.startswith(starts)


@register.filter("endswith")
def endswith(text, starts):
    return text.endswith(starts)


@register.filter
def verbose_name(obj, field_name=None):
    if isinstance(obj, str):
        return ""
    if isinstance(obj, QuerySet):
        return obj.model._meta.verbose_name_plural
    if field_name is None:
        return obj._meta.verbose_name
    return obj._meta.get_field(field_name).verbose_name.title()


@register.filter
def get_zscore_old(distance, avg_std):
    # DEPRECATED
    return (distance - avg_std.average) / avg_std.std


@register.filter
def get_color_old(distance, avg_std):
    # DEPRECATED
    zminimum = np.arctan(float((avg_std.minimum - avg_std.average) / avg_std.std))
    zmaximum = np.arctan(float((avg_std.maximum - avg_std.average) / avg_std.std))
    value = np.arctan(float((distance - avg_std.average) / avg_std.std))
    ratio = 1 - (1 * (value - zminimum) / (zmaximum - zminimum))
    hsl = ratio * 120 / 360
    rgb_color = colorsys.hls_to_rgb(hsl, 0.6, 0.3)
    return "{}, {}, {}".format(
        rgb_color[0] * 255, rgb_color[1] * 255, rgb_color[2] * 255
    )


@register.filter
def get_zscore_threshold(avg_std):
    # DEPRECATED
    return -2 * avg_std.std


@register.filter
def get_zscore(distance, avg_std):
    factor = float(avg_std.normalize_factor)
    # mean = float(avg_std.average)
    dist = float(distance)
    score = np.exp(-((dist**2) / (2 * (factor**2))))
    return score


@register.filter
def get_color(ratio):
    hsl = ratio * 120 / 360
    rgb_color = colorsys.hls_to_rgb(hsl, 0.65, 0.7)
    return "{}, {}, {}".format(
        rgb_color[0] * 255, rgb_color[1] * 255, rgb_color[2] * 255
    )


@register.filter
def make_list(avg_std):
    step = 1 / 25
    return np.arange(0, 1 + step, step)


@register.filter
def to_minus(value):
    return value.replace("_", "-")


@register.filter
def build_name(cavity):
    if cavity.partner.is_ligand:
        return "{}-{}-{}-{}-{}_CAVITY_N{}".format(
            cavity.chain.pdb.code,
            cavity.chain.pdb_chain_id,
            cavity.chain.protein.uniprot_id,
            cavity.partner.ligand.pdb_ligand_id,
            cavity.partner.ligand.supplementary_id,
            cavity.cavity_number,
        )
    else:
        return "{}-{}{}-{}-{}-within{}_CAVITY_N{}".format(
            cavity.chain.pdb.code,
            cavity.chain.pdb_chain_id,
            cavity.partner.chain.pdb_chain_id,
            cavity.chain.protein.uniprot_id,
            cavity.partner.chain.protein.uniprot_id,
            cavity.chain.pdb_chain_id,
            cavity.cavity_number,
        )


@register.filter
def dictsort(qs, arg):
    if isinstance(qs, set):
        if "lower" in arg:
            return sorted(qs, key=str.casefold)
        else:
            return sorted(qs)
    if isinstance(qs, QuerySet):
        if "lower" in arg:
            filter_param = arg.split(".")[0]
            try:
                qs = qs.annotate(lower_property=Func(F(filter_param), function="LOWER"))
                return qs.order_by("lower_property")
            except (FieldError, AttributeError):
                return qs

        else:
            filter_param = arg
            try:
                qs = qs.annotate(property=filter_param)
                return qs.order_by("property")
            except (FieldError, AttributeError):
                return qs
    return qs
