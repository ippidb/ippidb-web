"""
iPPI-DB Django admin app config file
"""
from django.contrib.admin.apps import AdminConfig


class IppidbAdminConfig(AdminConfig):
    default_site = "ippisite.admin.IppidbAdmin"
