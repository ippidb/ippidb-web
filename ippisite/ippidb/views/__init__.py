"""
iPPI-DB views

.. autosummary::
    :toctree: _autosummary

    about
    contribute
    compound_query
    targetcentric
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .targetcentric import (
    PdbViewSet,
    DistanceViewSet,
    PDBView,
    NetworkView,
    SummaryView,
    NearCavitiesHTMLFragmentView,
    ProteinViewSet,
    ChainViewSet,
    HotSpotViewSet,
    LigandViewSet,
    PartnerViewSet,
    InteractFileViewSet,
    CavityViewSet,
    SummaryViewSet,
    build_distances,
    JobViewSet,
    pocketome_html,
    LinkedPDBViewSet,
)
from .contribute import ippidb_wizard_view, ContributionDetailView
from .compound_query import (
    CompoundListView,
    CompoundDetailView,
    CompoundCardBDCHEMRedirectView,
    convert_mol2smi,
    convert_smi2mol,
    convert_smi2iupac,
    convert_iupac2smi,
)

from .site import IppiDbCompoundSitemap, IppiDbContributorsSitemap

from .tasks import get_output_job
from .about import (
    about_general,
    about_le_lle,
    about_pca,
    about_pharmacology,
    about_physicochemistry,
    ContributorListView,
    ContributorDetailView,
)
from .faq import FaqView
from django.conf import settings


def index(request):
    return render(request, "index.html")


def credits(request):
    return render(request, "credits.html")


def citation(request):
    return render(request, "citation.html")


def news(request):
    return render(request, "news.html")


def general(request):
    return render(request, "general.html")


def tutorials(request):
    return render(request, "tutorials.html")


@login_required
def adminSession(request):
    return render(request, "admin-session.html")


def update(request):
    return render(request, "update.html")


def marvinjs(request):
    return {"marvinjs_apikey": settings.MARVINJS_APIKEY}


def google_analytics(request):
    if hasattr(settings, "GA_CODE"):
        ga_code = settings.GA_CODE
    else:
        ga_code = None
    return {"gacode": ga_code}


__all__ = [
    ippidb_wizard_view,
    ContributionDetailView,
    index,
    general,
    tutorials,
    adminSession,
    update,
    marvinjs,
    CompoundListView,
    CompoundDetailView,
    CompoundCardBDCHEMRedirectView,
    convert_smi2mol,
    convert_mol2smi,
    convert_smi2iupac,
    convert_iupac2smi,
    about_general,
    about_le_lle,
    about_pca,
    about_pharmacology,
    about_physicochemistry,
    ContributorListView,
    ContributorDetailView,
    PdbViewSet,
    ProteinViewSet,
    ChainViewSet,
    HotSpotViewSet,
    LigandViewSet,
    PartnerViewSet,
    DistanceViewSet,
    InteractFileViewSet,
    CavityViewSet,
    build_distances,
    JobViewSet,
    PDBView,
    get_output_job,
    NetworkView,
    NearCavitiesHTMLFragmentView,
    IppiDbCompoundSitemap,
    IppiDbContributorsSitemap,
    pocketome_html,
    LinkedPDBViewSet,
    SummaryView,
    SummaryViewSet,
    FaqView,
]
