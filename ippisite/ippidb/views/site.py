from django.contrib.sitemaps import Sitemap
from ippidb.models import Compound, Contributor


class IppiDbCompoundSitemap(Sitemap):
    def items(self):
        return Compound.objects.validated()

    def lastmod(self, obj):
        return obj.get_last_modification_date()


class IppiDbContributorsSitemap(Sitemap):
    def items(self):
        return (
            Contributor.objects.all()
        )

    def lastmod(self, obj):
        return obj.get_last_modification_date()
