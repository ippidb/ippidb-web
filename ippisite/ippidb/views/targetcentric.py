"""
iPPI-DB targetcentric views and related classes
"""

from itertools import product
from functools import reduce
from operator import add
from django.views.generic import ListView, DetailView, TemplateView
from django.db.models import Case, When, Value, IntegerField, F, Q
from django.shortcuts import redirect
from django.contrib import messages
from django.core.cache import cache
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.decorators import action, api_view, permission_classes
from ippidb.models import (
    PDB,
    Protein,
    Distance,
    Chain,
    Ligand,
    Partner,
    InteractFile,
    Cavity,
    MetaInformation,
    Job,
    HotSpot,
    LinkedPDB,
)
from ippidb.serializer import (
    PdbSerializer,
    DistanceSerializer,
    ChainSerializer,
    LigandSerializer,
    ProteinSerializer,
    PartnerSerializer,
    InteractFileSerializer,
    CavitySerializer,
    JobSerializer,
    HotSpotSerializer,
    LinkedPDBSerializer,
    SummarySerializer,
)
from ippidb.filters import SummaryFilter

from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAdminUser, SAFE_METHODS
from ippidb.tasks import launch_build_matrix_distance
from rest_framework.response import Response
from django.views.decorators.clickjacking import xframe_options_exempt
from django.shortcuts import render


@xframe_options_exempt
def pocketome_html(request):
    return render(request, "tmap_ippidb.html")


class IsAdminUserOrReadOnly(IsAdminUser):
    def has_permission(self, request, view):
        is_admin = super(IsAdminUserOrReadOnly, self).has_permission(request, view)
        return request.method in SAFE_METHODS or is_admin


class PdbViewSet(viewsets.ModelViewSet):
    """
    Class PDB serializer, limit with only code and id if no pk is given
    """

    serializer_class = PdbSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = PDB.objects.all().values("code", "id")
        code = self.request.GET.get("code")
        if "pk" in self.kwargs:
            queryset = PDB.objects.filter(pk=self.kwargs["pk"])
        elif code:
            queryset = PDB.objects.filter(code=code)
        return queryset


class ProteinViewSet(viewsets.ModelViewSet):
    serializer_class = ProteinSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = Protein.objects.all()
        uniprot_id = self.request.GET.get("uniprot_id")
        if uniprot_id:
            queryset = queryset.filter(uniprot_id=uniprot_id)
        return queryset


class ChainViewSet(viewsets.ModelViewSet):
    serializer_class = ChainSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = Chain.objects.all()
        pdb = self.request.GET.get("pdb_id")
        pdb_chain_id = self.request.GET.get("pdb_chain_id")
        if pdb:
            if pdb_chain_id:
                queryset = queryset.filter(pdb=pdb, pdb_chain_id=pdb_chain_id)
            else:
                queryset = queryset.filter(pdb=pdb)
        return queryset


class HotSpotViewSet(viewsets.ModelViewSet):
    serializer_class = HotSpotSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = HotSpot.objects.all()
        chain = self.request.GET.get("chain")
        type = self.request.GET.get("type")
        residu_code = self.request.GET.get("residu_code")
        residu_number = self.request.GET.get("residu_number")
        if chain:
            queryset = queryset.filter(chain=chain)
        if type:
            queryset = queryset.filter(type=type)
        if residu_code:
            queryset = queryset.filter(residu_code=residu_code)
        if residu_number:
            queryset = queryset.filter(residu_number=residu_number)
        return queryset


class LigandViewSet(viewsets.ModelViewSet):
    serializer_class = LigandSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = Ligand.objects.all()
        pdb = self.request.GET.get("pdb_id")
        pdb_ligand_id = self.request.GET.get("pdb_ligand_id")
        supplementary_id = self.request.GET.get("supplementary_id")
        if pdb:
            if pdb_ligand_id and supplementary_id:
                queryset = queryset.filter(
                    pdb=pdb,
                    pdb_ligand_id=pdb_ligand_id,
                    supplementary_id=supplementary_id,
                )
            else:
                queryset = queryset.filter(pdb=pdb)
        return queryset


class PartnerViewSet(viewsets.ModelViewSet):
    serializer_class = PartnerSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = Partner.objects.all()
        chain = self.request.GET.get("chain")
        ligand = self.request.GET.get("ligand")
        if chain:
            queryset = queryset.filter(chain=chain)
        if ligand:
            queryset = queryset.filter(ligand=ligand)
        return queryset


class InteractFileViewSet(viewsets.ModelViewSet):
    serializer_class = InteractFileSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = InteractFile.objects.all()
        chain = self.request.GET.get("chain")
        label = self.request.GET.get("label")
        if chain:
            if label:
                queryset = queryset.filter(chain=chain, label=label)
            else:
                queryset = queryset.filter(chain=chain)
        return queryset


class CavityViewSet(viewsets.ModelViewSet):
    queryset = Cavity.objects.all()
    permission_classes = [IsAdminUserOrReadOnly]
    filter_backends = [
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    ]
    filterset_fields = [
        "full_name",
        "type",
    ]
    ordering_fields = [
        "full_name",
        "type",
    ]
    serializer_class = CavitySerializer


class DistanceViewSet(viewsets.ModelViewSet):
    """
    Class Distance serializer with argument from_cavity to give near cavity of only one cavity
    """

    serializer_class = DistanceSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get"])
    def get_queryset(self):
        first_cavity = self.request.GET.get("from_cavity", None)
        number = self.request.GET.get("number", None)
        if first_cavity:
            if number:
                queryset = Distance.objects.filter(
                    Q(cavity1=first_cavity) | Q(cavity2=first_cavity)
                ).order_by("distance")[: int(number)]
            else:
                queryset = Distance.objects.filter(
                    Q(cavity1=first_cavity) | Q(cavity2=first_cavity)
                ).order_by("distance")
        elif "pk" in self.kwargs:
            queryset = Distance.objects.filter(pk=self.kwargs["pk"])
        else:
            queryset = Distance.objects.all()
        return queryset


@api_view(["POST"])
@permission_classes([IsAdminUser])
def build_distances(request):
    if request.method == "POST":
        nb_neighbors = request.data.get("nb_neighbors")
        if nb_neighbors is not None:
            try:
                params_command = {"nb_neighbors": nb_neighbors}
                launch_build_matrix_distance.delay(params_command)
                job = Job.objects.last()
                return redirect("job-detail", pk=job.id)
            except Exception:
                return Response({"error": "impossible to build distances"}, status=404)
        else:
            return Response(
                {"error": "Specify a number of neighbors with param nb_neighbors"},
                status=400,
            )


class JobViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = JobSerializer
    http_method_names = ["get"]

    def get_queryset(self):
        jobs = Job.objects.all()
        return jobs


class PDBView(ListView):
    """
    PDB view list
    """

    model = PDB
    template_name = "targetcentric.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(PDBView, self).get_context_data(**kwargs)
        searchfield = self.request.GET.get("search", "")
        searchpdb = self.request.GET.get("pdbsearch", "")
        searchprotein = self.request.GET.get("proteinsearch", "")
        searchuniprotid = self.request.GET.get("uniprotid", "")
        searchligand = self.request.GET.get("ligandcode", "")
        searchorganism = self.request.GET.get("organism", "")
        if (
            searchfield
            or searchpdb
            or searchprotein
            or searchuniprotid
            or searchligand
            or searchorganism
        ):
            context["searchfield"] = " ".join(
                [
                    searchfield,
                    searchpdb,
                    searchprotein,
                    searchuniprotid,
                    searchligand,
                    searchorganism,
                ]
            )
        cavitiescount = Cavity.objects.all().count()
        proteincount = (
            Chain.objects.all().values("protein__uniprot_id").distinct().count()
        )
        organismcount = (
            Chain.objects.all().values("protein__organism__name").distinct().count()
        )
        context["cavitiescount"] = cavitiescount
        context["proteincount"] = proteincount
        context["organismcount"] = organismcount
        infopdb0 = {}
        if context["pdb_list"]:
            # if pdb_list object get cavities and near cavities for each chain to build
            # the cavities table, this method is faster than a full django templating method
            listchainspdb0 = Chain.objects.filter(
                pdb=context["pdb_list"][0].id
            ).select_related("protein__organism")
            for chain in listchainspdb0:
                # cavities = Cavity.objects.filter(chain=chain.id).select_related(
                #     "chain__protein__organism",
                #     "partner__chain__protein__organism",
                #     "partner__ligand",
                #     "chain__protein__organism",
                #     "partner__chain__protein__organism",
                #     "partner__ligand",
                # )
                # infochain = {}
                # for cavity in cavities:
                #     nearcavities = cavity.near_cavities.select_related(
                #         "cavity1__chain__protein__organism",
                #         "cavity1__partner__chain__protein__organism",
                #         "cavity1__partner__ligand",
                #         "cavity2__chain__protein__organism",
                #         "cavity2__partner__chain__protein__organism",
                #         "cavity2__partner__ligand",
                #     )
                #     infochain[cavity] = nearcavities
                infopdb0[chain] = Cavity.objects.filter(chain=chain.id)
        context["infopdb0"] = infopdb0
        completionlistpdb = list(
            set(PDB.objects.all().values_list("code", flat=True).distinct())
        )
        completionlistpdb.sort()
        completionlistorganism = list(
            set(
                Chain.objects.filter(protein__organism__isnull=False)
                .values_list("protein__organism__name", flat=True)
                .distinct()
            )
        )
        completionlistorganism.sort()
        completionlistuniprot = list(
            set(
                Chain.objects.filter(protein__isnull=False)
                .values_list("protein__uniprot_id", flat=True)
                .distinct()
            )
        )
        completionlistuniprot.sort()
        completionlistligand = list(
            set(Ligand.objects.all().values_list("pdb_ligand_id", flat=True).distinct())
        )
        completionlistligand.sort()
        completionlistprotein = list(
            set(
                Chain.objects.filter(protein__isnull=False)
                .values_list("protein__entry_name", flat=True)
                .distinct()
            )
        )
        completionlistprotein.sort()
        completionlist = (
            completionlistpdb
            + completionlistorganism
            + completionlistuniprot
            + completionlistligand
            + completionlistprotein
        )
        context["completionlist"] = completionlist
        context["completionlistpdb"] = completionlistpdb
        context["completionlistprotein"] = completionlistprotein
        context["completionlistorganism"] = completionlistorganism
        context["completionlistuniprot"] = completionlistuniprot
        context["completionlistligand"] = completionlistligand
        avg_std = MetaInformation.objects.first()
        context["avg_std"] = avg_std
        return context

    def search_patterns(self, pattern, queryset):
        """
        Construct the sql request and order by number of match pdbs
        """
        queryset = self.model.objects.all()
        patterns = [p.strip() for p in pattern.split()]
        locations = [
            "code",
            "chain__protein__entry_name__icontains",
            "chain__protein__organism__name__icontains",
            "ligand__pdb_ligand_id",
            "chain__protein__uniprot_id",
        ]
        cpt = 0
        annotates = {}
        for pat, loc in product(patterns, locations):
            annotates["hit_%i" % cpt] = Case(
                When(**{loc: pat, "then": Value(1)}),
                default=Value(0),
                output_field=IntegerField(),
            )
            cpt += 1
        queryset = queryset.annotate(**annotates)
        queryset = queryset.annotate(hits=reduce(add, (F(col) for col in annotates)))
        queryset = (
            queryset.distinct()
            .order_by("-hits")
            .filter(hits__gt=0)
            .prefetch_related("chain_set__protein__organism")
        )
        return queryset

    def search_pdb(self, pattern, queryset):
        """
        return queryset of pdb filtered with code pdb
        """
        queryset = queryset.filter(code=pattern.strip())
        return queryset.distinct()

    def search_protein(self, pattern, queryset):
        """
        return queryset of pdb filtered with code pdb
        """
        queryset = queryset.filter(
            chain__protein__entry_name__icontains=pattern.strip()
        )
        return queryset.distinct()

    def search_uniprotid(self, pattern, queryset):
        """
        return queryset of pdb filtered with uniprot id
        """
        queryset = queryset.filter(chain__protein__uniprot_id=pattern.strip())
        return queryset.distinct()

    def search_ligand(self, pattern, queryset):
        """
        return queryset of pdb filtered with ligand id
        """
        queryset = queryset.filter(ligand__pdb_ligand_id=pattern.strip())
        return queryset.distinct()

    def search_organism(self, pattern, queryset):
        """
        return queryset of pdb filtered with organism
        """
        queryset = queryset.filter(
            chain__protein__organism__name__icontains=pattern.strip()
        )
        return queryset.distinct()

    def get_queryset(self):
        search = False
        searchfield = self.request.GET.get("search", None)
        searchpdb = self.request.GET.get("pdbsearch", None)
        searchprotein = self.request.GET.get("proteinsearch", None)
        searchuniprotid = self.request.GET.get("uniprotid", None)
        searchligand = self.request.GET.get("ligandcode", None)
        searchorganism = self.request.GET.get("organism", None)
        queryset = self.model.objects.none()
        if searchfield:
            search = True
            queryset = self.search_patterns(searchfield, queryset)
        if searchpdb:
            search = True
            queryset = self.model.objects.all()
            queryset = self.search_pdb(searchpdb, queryset)
        if searchprotein:
            search = True
            if not searchpdb:
                queryset = self.model.objects.all()
            queryset = self.search_protein(searchprotein, queryset)
        if searchuniprotid:
            search = True
            if not searchpdb and not searchprotein:
                queryset = self.model.objects.all()
            queryset = self.search_uniprotid(searchuniprotid, queryset)
        if searchligand:
            search = True
            if not searchpdb and not searchprotein and not searchuniprotid:
                queryset = self.model.objects.all()
            queryset = self.search_ligand(searchligand, queryset)
        if searchorganism:
            search = True
            if (
                not searchpdb
                and not searchprotein
                and not searchuniprotid
                and not searchligand
            ):
                queryset = self.model.objects.all()
            queryset = self.search_organism(searchorganism, queryset)

        # queryset = queryset.annotate(chain_count=Count('chain')).annotate(ligand_count=Count('ligand'))
        if queryset.count() == 0 and search is True:
            messages.add_message(
                self.request,
                messages.INFO,
                "No results found for the specified search criteria.",
            )
        if queryset.count() >= 100:
            messages.add_message(
                self.request,
                messages.WARNING,
                "There are more than 100 results.",
            )
        return queryset[:100]


class LinkedPDBViewSet(viewsets.ModelViewSet):
    serializer_class = LinkedPDBSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    @action(detail=True, methods=["get", "post"])
    def get_queryset(self):
        queryset = LinkedPDB.objects.all()
        chain = self.request.GET.get("chain")
        ligand = self.request.GET.get("ligand_pl")
        if chain:
            queryset = queryset.filter(chain=chain)
        if ligand:
            queryset = queryset.filter(ligand_pl=ligand)
        return queryset


class NetworkView(TemplateView):
    """
    NetworkView view list
    """

    template_name = "targetcentric_networks.html"

    def get(self, request, *args, **kwargs):
        cache_key = "network_view"
        # Use default caches, LocMemCache
        response = cache.get(cache_key)
        if not response:
            response = render(request, self.template_name)
            cache.set(cache_key, response.content, timeout=None)  # Never expire
        else:
            response = HttpResponse(response)
        return response


class NearCavitiesHTMLFragmentView(DetailView):
    model = Cavity
    template_name = "targetcentric_near_cavities.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        return dict(
            **super().get_context_data(**kwargs),
            chaincount=self.request.GET.get("chaincount", "0"),
            cavcount=self.request.GET.get("cavcount", "0"),
            avg_std=MetaInformation.objects.first(),
        )


class SummaryViewSet(viewsets.ModelViewSet):
    # queryset = PDB.objects.all()
    queryset = Cavity.objects.all()
    permission_classes = [IsAdminUserOrReadOnly]
    filter_backends = [
        DjangoFilterBackend,
        filters.SearchFilter,
    ]
    filterset_class = SummaryFilter
    search_fields = (
        "chain__protein__organism__name",
        "chain__protein__gene_name",
        "=type",
        "=partner__ligand__pdb_ligand_id",
        "=chain__pdb__code",
        "=chain__protein__uniprot_id",
        "=partner__chain__protein__uniprot_id",
    )

    serializer_class = SummarySerializer


class SummaryView(TemplateView):
    """
    Summary view
    """

    template_name = "summary-table.html"
