from django.views.generic import ListView
from ippidb.models import Faq


class FaqView(ListView):
    """
    faq view
    """

    queryset = Faq.objects.all()
    model = Faq
    template_name = "FAQ.html"
