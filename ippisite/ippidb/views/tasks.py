from django.http import JsonResponse, HttpResponseBadRequest
from ippidb.models import Job


def get_output_job(request):
    """
    get a task information
    """

    task_id = request.GET.get("task_id")
    if task_id in [None, ""]:
        return HttpResponseBadRequest("task_id parameter is mandatory")
    try:
        job = Job.objects.get(task_result__task_id=task_id)
        resp = {
            "task_name": job.task_result.task_name,
            "task_id": job.task_result.task_id,
            "status": job.task_result.status,
            "create_time": job.task_result.date_created,
            "complete_time": job.task_result.date_done,
            "std_out": job.std_out,
            "std_err": job.std_err,
            "traceback": job.task_result.traceback,
        }
        return JsonResponse(resp)
    except Job.DoesNotExist:
        return HttpResponseBadRequest("task with id {} doesn't exist".format(task_id))
