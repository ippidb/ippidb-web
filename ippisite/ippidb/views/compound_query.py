"""
iPPI-DB compound query view and related classes
"""

import csv
import json
from collections import OrderedDict

from django.db.models import Max, Min, F, Q
from django.urls import reverse
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.base import RedirectView

from ippidb.utils import mol2smi, smi2mol
from ippidb.models import (
    Compound,
    Ppi,
    Disease,
    Domain,
    Protein,
    Taxonomy,
    LeLleBiplotData,
    PcaBiplotData,
    PpiFamily,
    MolecularFunction,
    TestActivityDescription,
    create_tanimoto,
    ProteinDomainBoundComplexGroup,
    ProteinDomainPartnerComplexGroup,
)
from ippidb.ws import convert_iupac_to_smiles, convert_smiles_to_iupac

DEACTIVATION_MAPPING = {
    "lipinsky": set(["molecular_weight", "nb_acceptor_h", "nb_donor_h", "a_log_p"]),
    "veber": set(["nb_acceptor_h", "nb_donor_h", "tpsa", "nb_rotatable_bonds"]),
    "pfizer": set(["a_log_p", "tpsa"]),
}

DEACTIVATION_MAPPING_2 = {}
for key, entry in DEACTIVATION_MAPPING.items():
    for value in entry:
        if value not in DEACTIVATION_MAPPING_2:
            DEACTIVATION_MAPPING_2[value] = set()
        DEACTIVATION_MAPPING_2[value].add(key)
DEACTIVATION_MAPPING.update(DEACTIVATION_MAPPING_2)


class FilterHandler(object):
    """
    FilterHandler classes handle setting filter values on a given parameter
    that includes:

    * filtering the queryset when filter value(s) is/are provided
    * setting the possible values for the filter depending on other filters
      being set
    * including in the view context the selected and unselected values
    """

    def __init__(self, parameter_name, filter_context, request_get):
        """
        Constructor

        :param parameter_name: the name of the parameter the Filter is used on
        :type parameter_name: str
        :param filter_context: the context data passed through the filters
                               processing
        :type filter_context: dict
        :param request_get: the GET parameters sent through the HTTP request
        :type request_get: dict
        """
        self.parameter_name = parameter_name
        self.filter_context = filter_context
        if request_get.get(self.parameter_name):
            self.value = request_get.get(self.parameter_name).strip()
            self.filter_context[self.parameter_name] = self.value
            self.filter_context["filter_on"] = True
        else:
            self.value = None
        self.__process_deactivations__()

    def __process_deactivations__(self):
        """
        Disable other filters if this filter is on
        """
        if self.value and self.parameter_name in DEACTIVATION_MAPPING:
            self.filter_context["disabled"].update(
                DEACTIVATION_MAPPING[self.parameter_name]
            )

    def process(self, queryset):
        """
        runs the filter on the queryset if some value(s) have been set
        and sets the "selected" and "unselected" filter values available
        if relevant

        :param queryset: the queryset on which the filter is performed
        :type queryset: django.db.models.QuerySet
        """
        raise NotImplementedError()

    def post_process(self, compound_ids, queryset):
        """
        sets the "selected" and "unselected" filter values available
        if relevant and no filter values have been set

        :param compound_ids: the IDs of the current selection of Compounds,
                             on which the range of possible values need to be
                             based
        :type compound_ids: list
        :param queryset: the queryset on which the filter is performed
        :type queryset: django.db.models.QuerySet
        """
        pass


class CompoundListFilterHandler(FilterHandler):
    """
    CompoundListFilterHandler handles setting filter values on a table
    related to the Compound table
    """

    def __init__(
        self,
        filter_class,
        relation_from_compound,
        relation_to_compound,
        parameter_name,
        filter_context,
        request_get,
        additional_filter=None,
        on_value=False,
    ):
        """
        Constructor

        :param filter_class: the class of the parameter the Filter is based on
        :type filter_class: class
        :param relation_from_compound: the Django ORM relation path from Compound
                                       to the filter's class
        :type relation_from_compound: str
        :param relation_to_compound: the Django ORM relation path to the Compound
                                       from the filter's class
        :type relation_to_compound: str
        :param parameter_name: the name of the parameter the Filter is used on
        :type parameter_name: str
        :param filter_context: the context data passed through the filters
                               processing
        :type filter_context: dict
        :param request_get: the GET parameters sent through the HTTP request
        :type request_get: dict
        :param additional_filter: additional filter to run on the filter's class
                                  to get the available values
        :type additional_filter: dict, optional
        :param on_value: the property of the filter class on which the filter is
                         performed if it is not the primary key
        :type on_value: str, optional
        """
        super().__init__(parameter_name, filter_context, request_get)
        self.filter_class = filter_class
        self.relation_from_compound = relation_from_compound
        self.relation_to_compound = relation_to_compound
        self.value = request_get.getlist(parameter_name)
        self.additional_filter = additional_filter
        self.on_value = on_value

    def process(self, queryset):
        # if a value has been set for this filter
        if self.value:
            property_filtered = self.on_value or "id"
            # filter queryset on the value specified
            queryset = queryset.filter(
                **{
                    self.relation_from_compound
                    + "__"
                    + property_filtered
                    + "__in": self.value
                }
            )
            objects = self.filter_class.objects
            if self.additional_filter:
                objects = objects.filter(**self.additional_filter)
            # selected objects on the value specified
            self.filter_context["selected_" + self.parameter_name] = objects.filter(
                **{property_filtered + "__in": self.value}
            )
            # unselected objects on the value not specified
            self.filter_context[self.parameter_name] = objects.exclude(
                **{property_filtered + "__in": self.value}
            )
        return queryset

    def post_process(self, compound_ids, queryset):
        # if no value has been set for this filter
        if not self.value:
            # selected objects is an empty queryset
            self.filter_context["selected_" + self.parameter_name] = (
                self.filter_class.objects.none()
            )
            objects = self.filter_class.objects
            if self.additional_filter:
                objects = objects.filter(**self.additional_filter)
            if compound_ids:
                # unselected objects is the range related to the current queryset
                self.filter_context[self.parameter_name] = objects.filter(
                    **{self.relation_to_compound + "__id__in": compound_ids}
                ).distinct()
            else:
                # unselected objects are all objects if no filter is set
                self.filter_context[self.parameter_name] = objects.all()
        if self.on_value:
            unselected = []
            for o in self.filter_context[self.parameter_name]:
                unselected.append(getattr(o, self.on_value))
            self.filter_context[self.parameter_name] = set(unselected)
            selected = []
            for o in self.filter_context["selected_" + self.parameter_name]:
                selected.append(getattr(o, self.on_value))
            self.filter_context["selected_" + self.parameter_name] = set(selected)


class CompoundSimilarityFilterHandler(FilterHandler):
    """
    Handles setting a filter based on the Tanimoto similarity
    with a provided SMILES and a fingerprint type.
    The parameter value is formatted as [fingerprint]:[smiles]
    """

    def __init__(self, parameter_name, filter_context, request_get):
        super().__init__(parameter_name, filter_context, request_get)

    def process(self, queryset):
        if self.value:
            # here fingerprint is the type of fingerprint (e.g. FP2)
            # and query is the SMILES query to compute the similarity on
            fingerprint, query = self.value.split(":")
            self.filter_context[self.parameter_name + "_fingerprint"] = fingerprint
            self.filter_context[self.parameter_name + "_query"] = query
            # FIXME: test if exists before running
            create_tanimoto(query, fingerprint)
            queryset = queryset.filter(
                compoundtanimoto__canonical_smiles=query,
                compoundtanimoto__fingerprint=fingerprint,
            ).annotate(tanimoto=F("compoundtanimoto__tanimoto"))
        # else:
        #    self.filter_context[self.parameter_name+'_fingerprint'] = 'ECFP4'
        #    self.filter_context[self.parameter_name+'_query'] = ''
        return queryset

    def post_process(self, compound_ids, queryset):
        pass


class TextSearchFilterHandler(FilterHandler):
    """
    Handles setting a filter based on a text searched inside the
    following compound and compound-related properties:

    * Compound common name
    * PPI family name
    * bound complex protein short name
    * disease name
    """

    def __init__(self, parameter_name, filter_context, request_get):
        super().__init__(parameter_name, filter_context, request_get)

    def process(self, queryset):
        if self.value:
            # filter on:
            # compound common name
            # PPI family name
            # bound complex protein short name
            # disease name
            queryset = queryset.filter(
                Q(common_name__icontains=self.value)
                | Q(compoundaction__ppi__family__name__icontains=self.value)
                | Q(
                    compoundaction__ppi_id__ppicomplex__complex__protein__short_name__icontains=self.value
                )
                | Q(compoundaction__ppi__diseases__name__icontains=self.value)
            )
        return queryset

    def post_process(self, compound_ids, queryset):
        pass


class CompoundRangeFilterHandler(FilterHandler):
    """
    Handles setting a filter based on the range of values for
    a compound or compound-related property
    """

    def __init__(self, parameter_name, filter_context, request_get, step=1):
        super().__init__(parameter_name, filter_context, request_get)
        self.step = step or 1
        if request_get.get(parameter_name):
            self.value_min, self.value_max = self.value.split(",")
        else:
            self.value_min = None
            self.value_max = None

    def process(self, queryset):
        # if a value has been set for this filter
        if self.value:
            # filter queryset on the ID values specified
            self.filter_context[self.parameter_name + "_value_min"] = self.value_min
            self.filter_context[self.parameter_name + "_value_max"] = self.value_max
            filter_dict = {
                self.parameter_name + "__gte": self.value_min,
                self.parameter_name + "__lte": self.value_max,
            }
            queryset = queryset.filter(**filter_dict)
            # max and min value are the max and min for
            qs = Compound.objects
            self.filter_context[self.parameter_name + "_max"] = self.get_max(qs)
            self.filter_context[self.parameter_name + "_min"] = self.get_min(qs)
        return queryset

    def get_max(self, qs):
        """
        Get the maximum value available in the range

        :param qs: the queryset on which the maximum is computed
        :type qs: django.db.models.QuerySet
        """
        val = float(
            qs.aggregate(Max(self.parameter_name))[self.parameter_name + "__max"] or 0
        )
        val = val - val % self.step + self.step
        return str(val)

    def get_min(self, qs):
        """
        Get the minimum value available in the range

        :param qs: the queryset on which the minimum is computed
        :type qs: django.db.models.QuerySet
        """
        val = float(
            qs.aggregate(Min(self.parameter_name))[self.parameter_name + "__min"] or 0
        )
        val = val - val % self.step
        return str(val)

    def post_process(self, compound_ids, queryset):
        # if no value has been set for this filter
        if not self.value:
            self.filter_context[self.parameter_name + "_max"] = self.get_max(queryset)
            self.filter_context[self.parameter_name + "_min"] = self.get_min(queryset)


class ExistsFilterHandler(FilterHandler):
    """
    Handles setting a filter based on the a
    compound or compound-related property having
    a defined value
    """

    def __init__(self, parameter_name, filter_context, request_get):
        super().__init__(parameter_name, filter_context, request_get)

    def process(self, queryset):
        """to be called during queryset filtering"""
        # if a value has been set for this filter
        if self.value:
            # filter queryset on the values not being NULL
            queryset = queryset.exclude(
                **{self.parameter_name + "__isnull": (self.value == "true")}
            )
        return queryset

    def post_process(self, compound_ids, queryset):
        """to be called after queryset filtering"""
        pass


class TrueFilterHandler(FilterHandler):
    """
    Handles setting a filter based on the a
    compound or compound-related property having
    a "true" value
    """

    def __init__(self, parameter_name, filter_context, request_get):
        super().__init__(parameter_name, filter_context, request_get)

    def process(self, queryset):
        """to be called during queryset filtering"""
        # if a value has been set for this filter
        if self.value:
            # filter queryset on the values being True
            filter_dict = {self.parameter_name: self.value == "true"}
            queryset = queryset.filter(**filter_dict)
        return queryset

    def post_process(self, compound_ids, queryset):
        """to be called after queryset filtering"""
        pass


class TrueListFilterHandler(object):
    """
    Handles setting a filter based on one in a list of
    compound or compound-related property having
    a "true" value
    """

    def __init__(self, parameter_name_list, filter_context, request_get):
        self.parameter_name_list = parameter_name_list
        self.filter_context = filter_context
        self.values = {}
        for parameter_name in self.parameter_name_list:
            if request_get.get(parameter_name):
                self.values[parameter_name] = request_get.get(parameter_name).strip()
                self.filter_context[parameter_name] = self.values[parameter_name]
                self.filter_context["filter_on"] = True

    def process(self, queryset):
        """to be called during queryset filtering"""
        # if a value has been set for this filter

        if len(self.values) > 0:
            q = Q()
            for parameter_name, value in self.values.items():
                q = q | Q(**{parameter_name: value == "true"})
            queryset = queryset.filter(q)
        return queryset

    def post_process(self, compound_ids, queryset):
        """to be called after queryset filtering"""
        pass


class CompoundListView(ListView):
    """
    Compounds list view
    """

    model = Compound
    template_name = "compound_list.html"
    paginate_by = 15

    table_view_default_fields = [
        "id",
        "canonical_smiles",
        "common_name",
        "molecular_weight",
        "a_log_p",
        "ligand_id",
        "pubs",
    ]

    sort_by_option_ids = [
        "id",
        "molecular_weight",
        "a_log_p",
        "nb_aromatic_sssr",
        "nb_chiral_centers",
        "pubs",
        "le",
        "lle",
        "best_activity",
        "tests_av",
        "families",
    ]

    def get(self, request, *args, **kwargs):
        self.format = request.GET.get("format", False)
        # send back the query as CSV if the CSV format is requested in parameters
        if self.format == "csv":
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = "attachment;filename=ippidb_compounds.csv"
            fields = self.request.GET.getlist(
                "fields", self.table_view_default_fields.copy()
            )
            if "id" not in fields:
                fields.insert(0, "id")
            # FIXME Compound should have the canonical_smileS property
            fields = [
                "canonical_smile" if field == "canonical_smiles" else field
                for field in fields
            ]
            writer = csv.DictWriter(response, fields)
            writer.writeheader()
            for row in self.get_queryset().values(*fields):
                writer.writerow(row)
            return response
        return super(CompoundListView, self).get(request, *args, **kwargs)

    def get_paginate_by(self, queryset):
        return self.request.GET.get("page_size", CompoundListView.paginate_by)

    def get_ordering(self):
        # sort by options
        self.sort_by_options = OrderedDict()
        compound_fields = {
            f.name: f.verbose_name
            for f in Compound._meta.get_fields()
            if not (f.is_relation)
        }
        for sort_by_option_id in self.sort_by_option_ids:
            if sort_by_option_id == "pubs":
                name = "Number of publications"
            elif sort_by_option_id == "families":
                name = "PPI families"
            elif sort_by_option_id == "le":
                name = "Ligand Efficiency"
            elif sort_by_option_id == "lle":
                name = "Lipophilic Efficiency"
            elif sort_by_option_id == "best_activity":
                name = "Best Activity"
            elif sort_by_option_id == "tests_av":
                name = "Number of tests available"
            else:
                name = compound_fields.get(sort_by_option_id)
            self.sort_by_options[sort_by_option_id] = {
                "name": name,
                "order": "ascending",
                "id": sort_by_option_id,
            }
            self.sort_by_options["-" + sort_by_option_id] = {
                "name": name,
                "order": "descending",
                "id": sort_by_option_id,
            }
        if "similar_to" in self.request.GET:
            self.sort_by_options["tanimoto"] = {
                "name": "Tanimoto similarity",
                "order": "ascending",
                "id": "tanimoto",
            }
            self.sort_by_options["-tanimoto"] = {
                "name": "Tanimoto similarity",
                "order": "descending",
                "id": "tanimoto",
            }
        default_sort = F("-tanimoto") if "similar_to" in self.request.GET else "id"
        self.sort_by = self.request.GET.get("sort_by", default_sort)
        if self.sort_by not in self.sort_by_options.keys():
            self.sort_by = "id"
        return self.sort_by

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # process display configuration
        display = self.request.GET.get("display")
        if display not in ["l", "t"]:
            display = "v"
        context["display"] = display
        # displayed fields in "table" mode
        fields = self.request.GET.getlist(
            "fields", self.table_view_default_fields.copy()
        )
        if "id" not in fields:
            fields.append("id")
        if "similar_to" in self.request.GET:
            fields.append("tanimoto")
        context["fields"] = fields
        # filters
        context.update(self.filter_context)
        context["sort_by"] = self.sort_by
        context["sort_by_options"] = self.sort_by_options
        # return context
        return context

    def get_queryset(self):
        # compounds can be accessed only if they are validated or
        # if the current user is an admin OR their contributor
        self.queryset = self.model.objects.for_user(self.request.user)
        self.filter_context = {}
        # get queryset
        qs = super().get_queryset()
        # add filters
        self.filter_context["disabled"] = (
            set()
        )  # list of filters disabled by setting others
        cfhs = [
            CompoundListFilterHandler(
                PpiFamily,
                "compoundaction__ppi__family",
                "ppi__compoundaction__compound",
                "family",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                Ppi,
                "compoundaction__ppi",
                "compoundaction__compound",
                "ppi",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                Disease,
                "compoundaction__ppi__diseases",
                "ppi__compoundaction__compound",
                "disease",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                Taxonomy,
                "compoundaction__ppi_id__ppicomplex__complex__protein__organism",
                "protein__proteindomaincomplex__ppicomplex__ppi__compoundaction__compound",
                "taxonomy",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                Protein,
                "compoundaction__ppi_id__ppicomplex__complex__protein",
                "proteindomaincomplex__ppicomplex__ppi__compoundaction__compound",
                "uniprot_id",
                self.filter_context,
                self.request.GET,
                on_value="uniprot_id",
            ),
            CompoundListFilterHandler(
                ProteinDomainBoundComplexGroup,
                "compoundaction__ppi_id__ppicomplex__complex__group",
                "proteindomaincomplex__ppicomplex__ppi__compoundaction__compound",
                "boundcomplex",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                ProteinDomainPartnerComplexGroup,
                "compoundaction__ppi_id__ppicomplex__complex__group",
                "proteindomaincomplex__ppicomplex__ppi__compoundaction__compound",
                "partnercomplex",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                Domain,
                "compoundaction__ppi_id__ppicomplex__complex__domain",
                "proteindomaincomplex__ppicomplex__ppi__compoundaction__compound",
                "domain",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                MolecularFunction,
                "compoundaction__ppi_id__ppicomplex__complex__protein__molecular_functions",
                "protein__proteindomaincomplex__ppicomplex__ppi__compoundaction__compound",
                "molecular_function",
                self.filter_context,
                self.request.GET,
            ),
            CompoundListFilterHandler(
                TestActivityDescription,
                "compoundactivityresult__test_activity_description",
                "compoundactivityresult__compound",
                "biochtest",
                self.filter_context,
                self.request.GET,
                additional_filter={"test_type": "BIOCH"},
                on_value="test_name",
            ),
            CompoundListFilterHandler(
                TestActivityDescription,
                "compoundactivityresult__test_activity_description",
                "compoundactivityresult__compound",
                "celltest",
                self.filter_context,
                self.request.GET,
                additional_filter={"test_type": "CELL"},
                on_value="test_name",
            ),
            CompoundRangeFilterHandler(
                "molecular_weight", self.filter_context, self.request.GET, 50
            ),
            CompoundRangeFilterHandler(
                "a_log_p", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "nb_donor_h", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "nb_acceptor_h", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "tpsa", self.filter_context, self.request.GET, 50
            ),
            CompoundRangeFilterHandler(
                "nb_rotatable_bonds", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "nb_aromatic_sssr", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "nb_chiral_centers", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "fsp3", self.filter_context, self.request.GET, 0.1
            ),
            CompoundRangeFilterHandler("pubs", self.filter_context, self.request.GET),
            CompoundRangeFilterHandler(
                "best_activity", self.filter_context, self.request.GET
            ),
            CompoundRangeFilterHandler(
                "le", self.filter_context, self.request.GET, 0.1
            ),
            CompoundRangeFilterHandler("lle", self.filter_context, self.request.GET),
            ExistsFilterHandler("pubchem_id", self.filter_context, self.request.GET),
            ExistsFilterHandler("chemspider_id", self.filter_context, self.request.GET),
            ExistsFilterHandler("chembl_id", self.filter_context, self.request.GET),
            ExistsFilterHandler("ligand_id", self.filter_context, self.request.GET),
            ExistsFilterHandler("drugbank_id", self.filter_context, self.request.GET),
            TrueFilterHandler("lipinsky", self.filter_context, self.request.GET),
            TrueFilterHandler("veber", self.filter_context, self.request.GET),
            TrueFilterHandler("pfizer", self.filter_context, self.request.GET),
            CompoundSimilarityFilterHandler(
                "similar_to", self.filter_context, self.request.GET
            ),
            TextSearchFilterHandler("q", self.filter_context, self.request.GET),
            TrueFilterHandler("pdb_ligand_av", self.filter_context, self.request.GET),
            TrueListFilterHandler(
                ["inhibition_role", "stabilisation_role"],
                self.filter_context,
                self.request.GET,
            ),
            TrueFilterHandler("celltest_av", self.filter_context, self.request.GET),
            TrueFilterHandler("inhitest_av", self.filter_context, self.request.GET),
            TrueFilterHandler("stabtest_av", self.filter_context, self.request.GET),
            TrueFilterHandler("bindtest_av", self.filter_context, self.request.GET),
            TrueFilterHandler("pktest_av", self.filter_context, self.request.GET),
            TrueFilterHandler("cytoxtest_av", self.filter_context, self.request.GET),
            TrueFilterHandler("insilico_av", self.filter_context, self.request.GET),
        ]
        for cfh in cfhs:
            qs = cfh.process(qs)
        qs = qs.distinct()
        # post filter filters
        if {k: v for k, v in self.filter_context.items() if k != "disabled"}:
            # compound ids in the final selection if a selection has happened
            cids = [c for c in qs.all().values_list("id", flat=True)]
        else:
            cids = None
        for cfh in cfhs:
            cfh.post_process(cids, qs)
        # return queryset
        return qs


class CompoundDetailView(DetailView):
    """
    Compound detail view
    """

    model = Compound
    template_name = "compound_card.html"

    def get_queryset(self):
        return self.model.objects.for_user(self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context["le_lle_biplot_data"] = (
                LeLleBiplotData.objects.get().le_lle_biplot_data
            )
        except LeLleBiplotData.DoesNotExist:
            context["le_lle_biplot_data"] = []
        try:
            pca_biplot = json.loads(PcaBiplotData.objects.get().pca_biplot_data)
        except PcaBiplotData.DoesNotExist:
            pca_biplot = {"data": [], "correlation_circle": ""}
        context["pca_biplot_data"] = pca_biplot["data"]
        context["pca_biplot_cc"] = pca_biplot["correlation_circle"]
        context["bioschemas_data"] = self.object.get_bioschemas(self.request)
        return context


class CompoundCardBDCHEMRedirectView(RedirectView):
    """
    redirection of BD-CHEM-issued URLs
    """

    def get_redirect_url(self, *args, **kwargs):
        pk = str(int(kwargs["compound_id"]))
        return reverse("compound_card", kwargs={"pk": pk})


def convert_mol2smi(request):
    """
    convert MOL2 file to SMILES
    """
    mol_string = request.GET.get("molString")
    if mol_string in [None, ""]:
        return HttpResponseBadRequest("molString parameter is mandatory")
    try:
        smiles_string = mol2smi(mol_string)
    except OSError as ose:
        return HttpResponseBadRequest(
            f"invalid value for molString parameter: {mol_string}" f"\n error:{ose}"
        )
    resp = {"smiles": smiles_string}
    return JsonResponse(resp)


def convert_smi2mol(request):
    """
    convert SMILES file to MOL2
    (and generate 2D coordinates)
    """
    smi_string = request.GET.get("smiString", "").strip()
    if smi_string == "":
        return HttpResponseBadRequest("smiString parameter is mandatory")
    try:
        mol_string = smi2mol(smi_string)
    except OSError as ose:
        return HttpResponseBadRequest(
            f"invalid value for smiString parameter: {smi_string}" f"\n error:{ose}"
        )
    resp = {"mol": mol_string}
    return JsonResponse(resp)


def convert_iupac2smi(request):
    """
    convert a IUPAC name to SMILES
    """
    iupac_string = request.GET.get("iupacString", "").strip()
    if iupac_string == "":
        return HttpResponseBadRequest("iupacString parameter is mandatory")
    try:
        smiles_string = convert_iupac_to_smiles(iupac_string)
    except OSError as ose:
        return HttpResponseBadRequest(
            f"invalid value for iupacString parameter: {iupac_string}" f"\n error:{ose}"
        )
    resp = {"smiles": smiles_string}
    return JsonResponse(resp)


def convert_smi2iupac(request):
    """
    convert a SMILES to a IUPAC name
    """
    smiles_string = request.GET.get("smiString", "").strip()
    if smiles_string == "":
        return HttpResponseBadRequest("smiString parameter is mandatory")
    try:
        iupac_string = convert_smiles_to_iupac(smiles_string)
    except OSError as ose:
        return HttpResponseBadRequest(
            f"invalid value for smiString parameter: {smiles_string}" f"\n error:{ose}"
        )
    resp = {"smiles": iupac_string}
    return JsonResponse(resp)
