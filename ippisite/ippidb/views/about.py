"""
iPPI-DB global statistics views
"""

from django.db.models import F, Count, FloatField
from django.db.models.functions import Cast, Floor
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
import json


from ippidb.models import (
    Compound,
    Ppi,
    LeLleBiplotData,
    PcaBiplotData,
    PpiFamily,
    CompoundActivityResult,
    Bibliography,
    Protein,
    Contributor,
    Cavity,
    Chain,
)


def about_pi_general(request):
    context = {}

    return render(request, "about-pie.html", context=context)


def about_general(request):
    compounds_count = Compound.objects.count()
    car_count = CompoundActivityResult.objects.count()
    bioch_car_count = CompoundActivityResult.objects.filter(
        test_activity_description__test_type="BIOCH"
    ).count()
    cell_car_count = CompoundActivityResult.objects.filter(
        test_activity_description__test_type="CELL"
    ).count()
    proteins_count = Protein.objects.count()
    families_count = PpiFamily.objects.count()
    ppis_count = Ppi.objects.count()
    biblio_count = Bibliography.objects.count()
    cavitiescount = Cavity.objects.all().count()
    proteincount = Chain.objects.all().values("protein__uniprot_id").distinct().count()
    organismcount = (
        Chain.objects.all().values("protein__organism__name").distinct().count()
    )
    orthocount = Cavity.objects.filter(type="orthosteric").count()
    allostcount = Cavity.objects.filter(type="liganded_allosteric").count()
    ortho_comp = Cavity.objects.filter(type="liganded_orthosteric_competitive").count()
    ortho_nocomp = Cavity.objects.filter(
        type="liganded_orthosteric_noncompetitive"
    ).count()

    context = {
        "compounds_count": compounds_count,
        "car_count": car_count,
        "bioch_car_count": bioch_car_count,
        "cell_car_count": cell_car_count,
        "proteins_count": proteins_count,
        "families_count": families_count,
        "ppis_count": ppis_count,
        "biblio_count": biblio_count,
        "cavitiescount": cavitiescount,
        "proteincount": proteincount,
        "organismcount": organismcount,
        "orthocount": orthocount,
        "allostcount": allostcount,
        "ortho_comp": ortho_comp,
        "ortho_nocomp": ortho_nocomp,
    }
    return render(request, "about-general.html", context=context)


def convert_to_bar_chart_data(data):
    return {
        "labels": [item["label"] for item in data],
        "values": [item["value"] for item in data],
    }


def about_pharmacology(request):
    biochemical_tests_count_by_name_qs = (
        CompoundActivityResult.objects.filter(
            test_activity_description__test_type="BIOCH"
        )
        .values(label=F("test_activity_description__test_name"))
        .order_by("label")
        .annotate(value=Count("label"))
        .order_by("-value")
    )
    biochemical_tests_count_by_name = convert_to_bar_chart_data(
        biochemical_tests_count_by_name_qs
    )
    cellular_tests_count_by_name_qs = (
        CompoundActivityResult.objects.filter(
            test_activity_description__test_type="CELL"
        )
        .values(label=F("test_activity_description__test_name"))
        .order_by("label")
        .annotate(value=Count("label"))
        .order_by("-value")
    )
    cellular_tests_count_by_name = convert_to_bar_chart_data(
        cellular_tests_count_by_name_qs
    )
    compounds_per_ppi_family_raw = (
        PpiFamily.objects.annotate(
            value=Count("ppi__compoundaction__compound", distinct=True)
        )
        .values(value=F("value"), label=F("name"))
        .order_by("-value")
        .all()
    )
    compounds_per_ppi_family = convert_to_bar_chart_data(compounds_per_ppi_family_raw)
    binding_data_per_ppi_family_raw = (
        PpiFamily.objects.annotate(value=Count("ppi__compoundaction"))
        .values(value=F("value"), label=F("name"))
        .order_by("-value")
    )
    binding_data_per_ppi_family = convert_to_bar_chart_data(
        binding_data_per_ppi_family_raw
    )
    drugbank_compounds_per_ppi_family_raw = (
        PpiFamily.objects.annotate(
            value=Count("ppi__compoundaction__compound__drugbank_id", distinct=True)
        )
        .values(value=F("value"), label=F("name"))
        .order_by("-value")
        .all()
    )
    drugbank_compounds_per_ppi_family = convert_to_bar_chart_data(
        drugbank_compounds_per_ppi_family_raw
    )
    ppi_family_labels = {
        entry["name"]: entry["id"]
        for entry in PpiFamily.objects.values(*["id", "name"])
    }
    context = {
        "biochemical_tests_count_by_name": biochemical_tests_count_by_name,
        "cellular_tests_count_by_name": cellular_tests_count_by_name,
        "bioch_tests_pxc50_gte8": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="BIOCH"
        )
        .filter(activity__gte=8)
        .count(),
        "bioch_tests_pxc50_gte7_lt8": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="BIOCH"
        )
        .filter(activity__gte=7)
        .filter(activity__lt=8)
        .count(),
        "bioch_tests_pxc50_gte6_lt7": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="BIOCH"
        )
        .filter(activity__gte=6)
        .filter(activity__lt=7)
        .count(),
        "bioch_tests_pxc50_lt6": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="BIOCH"
        )
        .filter(activity__lt=6)
        .count(),
        "cell_tests_pxc50_gte8": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="CELL"
        )
        .filter(activity__gte=8)
        .count(),
        "cell_tests_pxc50_gte7_lt8": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="CELL"
        )
        .filter(activity__gte=7)
        .filter(activity__lt=8)
        .count(),
        "cell_tests_pxc50_gte6_lt7": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="CELL"
        )
        .filter(activity__gte=6)
        .filter(activity__lt=7)
        .count(),
        "cell_tests_pxc50_lt6": CompoundActivityResult.objects.filter(
            test_activity_description__test_type="CELL"
        )
        .filter(activity__lt=6)
        .count(),
        "compounds_per_ppi_family": compounds_per_ppi_family,
        "binding_data_per_ppi_family": binding_data_per_ppi_family,
        "drugbank_compounds_per_ppi_family": drugbank_compounds_per_ppi_family,
        "ppi_family_labels": ppi_family_labels,
    }
    return render(request, "about-pharmacology.html", context=context)


def about_le_lle(request):
    try:
        context = {
            "le_lle_biplot_data": LeLleBiplotData.objects.get().le_lle_biplot_data
        }
    except LeLleBiplotData.DoesNotExist:
        context = {}
    return render(request, "about-le-lle.html", context=context)


def about_physicochemistry(request):
    fsp3_counts = (
        Compound.objects.annotate(label=Cast(Floor(F("fsp3") * 10), FloatField()) / 10)
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    mw_counts = (
        Compound.objects.annotate(
            label=Cast(Floor(F("molecular_weight") / 100), FloatField()) * 100
        )
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    chiral_counts = (
        Compound.objects.annotate(label=F("nb_chiral_centers"))
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    arom_counts = (
        Compound.objects.annotate(label=F("nb_aromatic_sssr"))
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    rotbonds_counts = (
        Compound.objects.annotate(label=F("nb_rotatable_bonds"))
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    haccept_counts = (
        Compound.objects.annotate(label=F("nb_acceptor_h"))
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    hdon_counts = (
        Compound.objects.annotate(label=F("nb_donor_h"))
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    tpsa_counts = (
        Compound.objects.annotate(label=Cast(Floor(F("tpsa") / 10), FloatField()) * 10)
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    alogp_counts = (
        Compound.objects.annotate(label=Cast(Floor(F("a_log_p")), FloatField()))
        .values("label")
        .order_by("label")
        .annotate(value=Count("label"))
        .values("value", "label")
        .exclude(value=0)
    )
    context = {
        "fsp3_dist": convert_to_bar_chart_data(fsp3_counts),
        "mw_dist": convert_to_bar_chart_data(mw_counts),
        "chiral_dist": convert_to_bar_chart_data(chiral_counts),
        "arom_dist": convert_to_bar_chart_data(arom_counts),
        "rotbonds_dist": convert_to_bar_chart_data(rotbonds_counts),
        "haccept_dist": convert_to_bar_chart_data(haccept_counts),
        "hdon_dist": convert_to_bar_chart_data(hdon_counts),
        "tpsa_dist": convert_to_bar_chart_data(tpsa_counts),
        "alogp_dist": convert_to_bar_chart_data(alogp_counts),
        "lipinsky_ok_count": Compound.objects.filter(lipinsky=True).count(),
        "lipinsky_ko_count": Compound.objects.filter(lipinsky=False).count(),
        "pfizer_ok_count": Compound.objects.filter(pfizer=True).count(),
        "pfizer_ko_count": Compound.objects.filter(pfizer=False).count(),
        "veber_ok_count": Compound.objects.filter(veber=True).count(),
        "veber_ko_count": Compound.objects.filter(veber=False).count(),
    }
    return render(request, "about-physicochemistry.html", context=context)


def about_pca(request):
    try:
        context = {"pca_biplot_data": PcaBiplotData.objects.get().pca_biplot_data}
        pca_biplot = json.loads(PcaBiplotData.objects.get().pca_biplot_data)
        context["pca_biplot_cc"] = pca_biplot["correlation_circle"]
    except PcaBiplotData.DoesNotExist:
        context = {}
    return render(request, "about-pca.html", context=context)


class ContributorListView(ListView):
    """
    Contributors list view

    This view lists the users with at least one accepted contribution
    """

    model = Contributor
    template_name = "about-contributors.html"


class ContributorDetailView(DetailView):
    """
    Contributor detail view

    """

    model = Contributor
    template_name = "about-contributor-card.html"
