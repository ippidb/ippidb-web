"""
iPPI-DB contribution views
"""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db import transaction
from django.shortcuts import redirect, reverse
from django.utils.translation import gettext_lazy
from django.contrib import messages
from django.views.generic.detail import DetailView
from formtools.wizard.views import NamedUrlSessionWizardView

from ippidb import forms as ippidb_forms
from ippidb import models
from live_settings import live_settings

FORMS = [
    ("IdForm", ippidb_forms.IdForm),
    ("BibliographyForm", ippidb_forms.BibliographyForm),
    ("PDBForm", ippidb_forms.PDBForm),
    ("ProteinDomainComplexTypeForm", ippidb_forms.ProteinDomainComplexTypeForm),
    ("ProteinDomainComplexForm", ippidb_forms.ComplexCompositionFormSet),
    ("PpiForm", ippidb_forms.PpiModelForm),
    ("CompoundForm", ippidb_forms.CompoundFormSet),
    ("ActivityDescriptionFormSet", ippidb_forms.ActivityDescriptionFormSet),
    ("TestCytotoxDescriptionFormSet", ippidb_forms.TestCytotoxDescriptionFormSet),
    ("TestPKDescriptionFormSet", ippidb_forms.TestPKDescriptionFormSet),
    ("SaveInDB", ippidb_forms.SaveInDBForm),
]

FORMS_TAB_TITLE = {
    "IdForm": gettext_lazy("IdForm_tab_title"),
    "BibliographyForm": gettext_lazy("BibliographyForm_tab_title"),
    "PDBForm": gettext_lazy("PDBForm_tab_title"),
    "ProteinDomainComplexTypeForm": gettext_lazy(
        "ProteinDomainComplexTypeForm_tab_title"
    ),
    "ProteinDomainComplexForm": gettext_lazy("ProteinDomainComplexForm_tab_title"),
    "PpiForm": gettext_lazy("PpiForm_tab_title"),
    "CompoundForm": gettext_lazy("CompoundForm_tab_title"),
    "TestsForm": gettext_lazy("TestsForm_tab_title"),
    "ActivityDescriptionFormSet": gettext_lazy("ActivityDescriptionFormSet_tab_title"),
    "TestCytotoxDescriptionFormSet": gettext_lazy(
        "TestCytotoxDescriptionFormSet_tab_title"
    ),
    "TestPKDescriptionFormSet": gettext_lazy("TestPKDescriptionFormSet_tab_title"),
    "SaveInDB": gettext_lazy("SaveInDB_tab_title"),
}

FORMS_TITLE = {
    "IdForm": gettext_lazy("IdForm_title"),
    "BibliographyForm": gettext_lazy("BibliographyForm_title"),
    "PDBForm": gettext_lazy("PDBForm_title"),
    "ProteinDomainComplexTypeForm": gettext_lazy("ProteinDomainComplexTypeForm_title"),
    "ProteinDomainComplexForm": gettext_lazy("ProteinDomainComplexForm_title"),
    "PpiForm": gettext_lazy("PpiForm_title"),
    "CompoundForm": gettext_lazy("CompoundForm_title"),
    "TestsForm": gettext_lazy("TestsForm_title"),
    "ActivityDescriptionFormSet": gettext_lazy("ActivityDescriptionFormSet_title"),
    "TestCytotoxDescriptionFormSet": gettext_lazy(
        "TestCytotoxDescriptionFormSet_title"
    ),
    "TestPKDescriptionFormSet": gettext_lazy("TestPKDescriptionFormSet_title"),
    "SaveInDB": gettext_lazy("SaveInDB_title"),
}

FORMS_DESC = {
    "IdForm": gettext_lazy("IdForm_desc"),
    "BibliographyForm": gettext_lazy("BibliographyForm_desc"),
    "PDBForm": gettext_lazy("PDBForm_desc"),
    "ProteinDomainComplexTypeForm": gettext_lazy("ProteinDomainComplexTypeForm_desc"),
    "ProteinDomainComplexForm": gettext_lazy("ProteinDomainComplexForm_desc"),
    "PpiForm": gettext_lazy("PpiForm_desc"),
    "CompoundForm": gettext_lazy("CompoundForm_desc"),
    "TestsForm": gettext_lazy("TestsForm_desc"),
    "ActivityDescriptionFormSet": gettext_lazy("ActivityDescriptionFormSet_desc"),
    "TestCytotoxDescriptionFormSet": gettext_lazy("TestCytotoxDescriptionFormSet_desc"),
    "TestPKDescriptionFormSet": gettext_lazy("TestPKDescriptionFormSet_desc"),
    "SaveInDB": gettext_lazy("SaveInDB_desc"),
}

TEMPLATES = {
    "IdForm": "IdForm.html",
    "BibliographyForm": "BibliographyForm.html",
    "PDBForm": "add.html",
    "ProteinDomainComplexTypeForm": "ProteinDomainComplexTypeForm.html",
    "ProteinDomainComplexForm": "ProteinDomainComplexForm.html",
    "PpiForm": "PpiForm.html",
    "CompoundForm": "CompoundForm.html",
    "TestsForm": "TestsForm.html",
    "ActivityDescriptionFormSet": "wizard_formset_with_nested_formset.html",
    "TestCytotoxDescriptionFormSet": "wizard_formset_with_nested_formset.html",
    "TestPKDescriptionFormSet": "wizard_formset_with_nested_formset.html",
    "SaveInDB": "add.html",
}

ARCHITECTURE = {
    "Inhib_Hetero2merAB": dict(
        has_bound=True,
        nb_copy_bound=[1],
        nb_per_pocket=[1],
        has_partner=True,
        nb_copy_partner=[1],
        nb_pockets=[1],
        nb_copies_in_complex=[1],
        symmetry=["AS"],
    ),
    "Inhib_Homo2merA2": dict(
        has_bound=True,
        nb_copy_bound=[1],
        nb_per_pocket=[1],
        has_partner=False,
        nb_pockets=[1],
        nb_copies_in_complex=[2],
        symmetry=["C2", "D2"],
    ),
    "Inhib_Custom": dict(
        has_bound=True,
        nb_copy_bound=[],
        nb_per_pocket=[],
        has_partner=True,
        nb_pockets=[],
        nb_copies_in_complex=[],
        symmetry=[],
    ),
    "Stab_Hetero2merAB": dict(
        has_bound=True,
        nb_copy_bound=[1],
        nb_per_pocket=[1],
        has_partner=True,
        nb_pockets=[1],
        nb_copies_in_complex=[1],
        symmetry=["AS"],
    ),
    "Stab_Homo2merA2": dict(
        has_bound=True,
        nb_copy_bound=[2],
        nb_per_pocket=[2],
        has_partner=False,
        nb_pockets=[1, 2],
        nb_copies_in_complex=[1],
        symmetry=["C2", "D2"],
    ),
    "Stab_HomoLike2mer": dict(
        has_bound=True,
        nb_copy_bound=[1, 2],
        nb_per_pocket=[1],
        has_partner=False,
        nb_pockets=[2],
        nb_copies_in_complex=[1],
        symmetry=["C2", "D2"],
    ),
    "Stab_Homo3merA3": dict(
        has_bound=True,
        nb_copy_bound=[3],
        nb_per_pocket=[3],
        has_partner=False,
        nb_pockets=[1],
        nb_copies_in_complex=[1],
        symmetry=["C3", "D3"],
    ),
    "Stab_Homo3merA2": dict(
        has_bound=True,
        nb_copy_bound=[2],
        nb_per_pocket=[2],
        has_partner=True,
        nb_copy_partner=[1],
        nb_pockets=[1],
        nb_copies_in_complex=[1],
        symmetry=["C3", "D3"],
    ),
    "Stab_Homo4merA4": dict(
        has_bound=True,
        nb_copy_bound=[4],
        nb_per_pocket=[2],
        has_partner=False,
        nb_pockets=[1],
        nb_copies_in_complex=[1],
        symmetry=["C2", "D2", "C4", "D4"],
    ),
    "Stab_RingHomo3mer": dict(
        has_bound=True,
        nb_copy_bound=[3],
        nb_per_pocket=[2],
        has_partner=False,
        nb_pockets=[3, 6],
        nb_copies_in_complex=[1],
        symmetry=["C3", "D3"],
    ),
    "Stab_RingHomo5mer": dict(
        has_bound=True,
        nb_copy_bound=[5],
        nb_per_pocket=[2],
        has_partner=False,
        nb_pockets=[5, 10],
        nb_copies_in_complex=[1],
        symmetry=["C5", "D5"],
    ),
    "Stab_Custom": dict(
        has_bound=True,
        nb_copy_bound=[],
        nb_per_pocket=[],
        has_partner=True,
        nb_pockets=[],
        nb_copies_in_complex=[],
        symmetry=[],
    ),
}
for key, label in ippidb_forms.TYPE_CHOICES:
    assert key in ARCHITECTURE, key


class MyPermissionRequiredMixin(PermissionRequiredMixin):
    def has_permission(self):
        return (
            live_settings.open_access_to_contribution == "True"
            or super().has_permission()
        )


class IppiWizard(
    LoginRequiredMixin, MyPermissionRequiredMixin, NamedUrlSessionWizardView
):
    permission_required = "ippidb.add_contribution"

    def show_cytotox_test(self):
        data = self.storage.get_step_data("BibliographyForm")
        return data is not None and data.get("BibliographyForm-cytotox", False)

    def show_pharmacokinetic_test(self):
        data = self.storage.get_step_data("BibliographyForm")
        return data is not None and data.get("BibliographyForm-pharmacokinetic", False)

    condition_dict = {
        "TestCytotoxDescriptionFormSet": show_cytotox_test,
        "TestPKDescriptionFormSet": show_pharmacokinetic_test,
    }

    def get_context_data(self, **kwargs):
        # @formatter:off
        context = super(IppiWizard, self).get_context_data(**kwargs)
        if self.steps.current == "ProteinDomainComplexForm":
            context["complex_choice"] = self.storage.get_step_data(
                "ProteinDomainComplexTypeForm"
            ).get("ProteinDomainComplexTypeForm-complexChoice")
            context["complex_type"] = self.storage.get_step_data(
                "ProteinDomainComplexTypeForm"
            ).get("ProteinDomainComplexTypeForm-complexType")
            context["pdb"] = self.storage.get_step_data("PDBForm").get("PDBForm-pdb_id")
        if self.steps.current == "PpiForm":
            context["complex_choice"] = self.storage.get_step_data(
                "ProteinDomainComplexTypeForm"
            ).get("ProteinDomainComplexTypeForm-complexChoice")
            context["complex_type"] = self.storage.get_step_data(
                "ProteinDomainComplexTypeForm"
            ).get("ProteinDomainComplexTypeForm-complexType")
            context["pdb_id"] = self.storage.get_step_data("PDBForm").get(
                "PDBForm-pdb_id"
            )
        # @formatter:on
        return context

    def get_form(self, step=None, *args, **kwargs):
        form = super().get_form(step, *args, **kwargs)
        current_step = self.steps.current if step is None else step
        if current_step == "ActivityDescriptionFormSet":
            complex_form = super().get_form(
                "ProteinDomainComplexForm",
                data=self.storage.get_step_data("ProteinDomainComplexForm"),
            )
            protein_subset_ids = []
            for complex_json in complex_form.cleaned_data:
                complex_type = complex_json.pop("complex_type")
                if complex_type == "Bound":
                    protein_subset_ids.append(complex_json["protein"].pk)
            form.set_protein_subset_ids(protein_subset_ids)

        if current_step in [
            "ActivityDescriptionFormSet",
            "TestCytotoxDescriptionFormSet",
            "TestPKDescriptionFormSet",
        ]:
            compound_form = super().get_form(
                "CompoundForm", data=self.storage.get_step_data("CompoundForm")
            )
            compound_names = []
            for compound_json in compound_form.cleaned_data:
                if not compound_json.get("DELETE", False):
                    compound_names.append(compound_json.pop("compound_name"))

            form.set_compound_names(compound_names)
        return form

    def get_form_kwargs(self, step=None):
        # change args pass to a form
        kwargs = super().get_form_kwargs(step)
        if step == "IdForm":
            # kwargs["display_allow_duplicate"] = self.request.user.is_superuser
            kwargs["request"] = self.request
        elif step == "ProteinDomainComplexForm":
            protein_ids = self.storage.get_step_data("PDBForm").get("PDBForm-proteins")
            arch = ARCHITECTURE[
                self.storage.get_step_data("ProteinDomainComplexTypeForm").get(
                    "ProteinDomainComplexTypeForm-complexType", ""
                )
            ]
            kwargs["form_kwargs"] = {"protein_ids": protein_ids}
            for k in [
                "has_bound",
                "has_partner",
                "nb_copy_bound",
                "nb_per_pocket",
                "nb_copy_partner",
                "nb_copies_in_complex",
            ]:
                try:
                    kwargs[k] = arch[k]
                except KeyError:
                    pass
        elif step == "PpiForm":
            arch = ARCHITECTURE[
                self.storage.get_step_data("ProteinDomainComplexTypeForm").get(
                    "ProteinDomainComplexTypeForm-complexType", ""
                )
            ]
            kwargs["nb_pockets"] = arch["nb_pockets"]
            if len(arch["symmetry"]) != 0:
                kwargs["symmetry"] = models.Symmetry.objects.filter(
                    code__in=arch["symmetry"]
                )
        elif step == "ActivityDescriptionFormSet":
            kwargs["queryset"] = models.TestActivityDescription.objects.none()
            if (
                self.storage.get_step_data("ProteinDomainComplexTypeForm").get(
                    "ProteinDomainComplexTypeForm-complexChoice", ""
                )
                == "inhibited"
            ):
                kwargs["modulation_type"] = "I"
            elif (
                self.storage.get_step_data("ProteinDomainComplexTypeForm").get(
                    "ProteinDomainComplexTypeForm-complexChoice", ""
                )
                == "stabilized"
            ):
                kwargs["modulation_type"] = "S"
        elif step == "CompoundForm":
            kwargs["show_is_a_ligand"] = (
                self.storage.get_step_data("BibliographyForm").get(
                    "BibliographyForm-xray", ""
                )
                == "on"
            )
        return kwargs

    def get_form_initial(self, step):
        # Works for form.forms
        # change value pass to a form
        initial = super().get_form_initial(step)
        if step == "PpiForm":
            initial["pdb_id"] = self.storage.get_step_data("PDBForm").get(
                "PDBForm-pdb_id"
            )
        elif step == "ProteinDomainComplexForm":
            initial = []
            for protein_id in self.storage.get_step_data("PDBForm").get(
                "PDBForm-proteins"
            ):
                initial.append({"protein": protein_id})
        return initial

    def process_step(self, form):
        """
        This method overrides the one used to postprocess the form data.
        The added code just sets the form model for use in later forms
        when appropriate
        """
        data = super(IppiWizard, self).process_step(form).copy()
        if self.steps.current == "IdForm":
            bibliography = form.get_instance()
            if bibliography is not None:
                data["IdForm-id"] = bibliography.id
        elif self.steps.current == "PDBForm":
            data["PDBForm-proteins"] = form.get_proteins()
        return data

    def get_form_instance(self, step):
        # Works only for Modelform
        if self.steps.current == "BibliographyForm":
            pk = self.storage.get_step_data("IdForm").get("IdForm-id", None)
            if pk is not None:
                return models.Bibliography.objects.get(pk=pk)
            else:
                idFormData = self.storage.get_step_data("IdForm")
                bibliography = models.Bibliography(
                    source=idFormData["IdForm-source"],
                    id_source=idFormData["IdForm-id_source"],
                )
                bibliography.autofill()
                return bibliography
        if self.steps.current == "ProteinDomainComplexTypeForm":
            protein_ids = self.storage.get_step_data("PDBForm").get("PDBForm-proteins")
            return models.Protein.objects.filter(id__in=protein_ids)

    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def done(self, form_list, form_dict=None, **kwargs):
        with transaction.atomic():
            # Step 1-2 Biblio
            bibliography = form_dict["BibliographyForm"].save()
            # Step 3 & 6 Ppi
            ppi = form_dict["PpiForm"].save()
            # Step 4 Architecture
            # TODO !!!!
            # Step 5 Complexes
            complexes = dict()
            for f in form_dict["ProteinDomainComplexForm"]:
                if not f.is_valid():
                    continue
                complex_type = f.cleaned_data.pop("complex_type")
                cc_nb = f.cleaned_data.pop("cc_nb")
                if complex_type == "Partner":
                    f.cleaned_data.pop("ppp_copy_nb_per_p")
                    complex = models.ProteinDomainPartnerComplex.objects.create(
                        **f.cleaned_data
                    )
                elif complex_type == "Bound":
                    complex = models.ProteinDomainBoundComplex.objects.create(
                        **f.cleaned_data
                    )
                else:
                    raise Exception("Unknown complex type %s" % complex_type)
                models.PpiComplex.objects.create(ppi=ppi, complex=complex, cc_nb=cc_nb)
                complexes[f.cleaned_data["protein"].pk] = complex
            # Step 7 Compounds
            compounds = dict()
            created_compounds = 0
            for f in form_dict["CompoundForm"]:
                if not f.is_valid():
                    continue
                canonical_smile = (
                    f.cleaned_data["molecule_smiles"]
                    or f.cleaned_data["computed_smiles"]
                )
                if canonical_smile == "":
                    compound = models.Compound.objects.filter(
                        iupac_name=f.cleaned_data["molecule_iupac"]
                    ).first()
                    created = False
                else:
                    ligand_id = f.cleaned_data.get("ligand_id", None)
                    if ligand_id == "":
                        ligand_id = None
                    compound, created = models.Compound.objects.get_or_create(
                        canonical_smile=canonical_smile,
                        defaults=dict(
                            common_name=f.cleaned_data["common_name"],
                            is_macrocycle=f.cleaned_data["is_macrocycle"],
                            iupac_name=f.cleaned_data["molecule_iupac"],
                            ligand_id=ligand_id,
                        ),
                    )
                    if not created and ligand_id is not None:
                        compound.ligand_id = ligand_id
                        compound.save()
                models.RefCompoundBiblio.objects.create(
                    bibliography=bibliography,
                    compound=compound,
                    compound_name=f.cleaned_data["compound_name"],
                )
                if created:
                    created_compounds += 1
                compounds[f.cleaned_data["compound_name"]] = compound
            for name, compound in compounds.items():
                ca = models.CompoundAction()
                ca.ppi = ppi
                ca.compound = compound
                ca.nb_copy_compounds = 1
                ca.activation_mode = "U"
                ca.save()
            # Step 8.1 Activity Tests
            # We set the dicts to translates attributes into model
            bound_complexes = [
                protein_domain_complex
                for protein_domain_complex in complexes.values()
                if isinstance(protein_domain_complex, models.ProteinDomainBoundComplex)
            ]
            if len(bound_complexes) == 1:
                form_dict["ActivityDescriptionFormSet"].set_complex(bound_complexes[0])
            form_dict["ActivityDescriptionFormSet"].set_compounds(compounds)
            # We set the constants attributes among the instances to be created by the modelformset
            form_dict["ActivityDescriptionFormSet"].instance = dict(
                biblio=bibliography, ppi=ppi
            )
            form_dict["ActivityDescriptionFormSet"].is_valid()
            form_dict["ActivityDescriptionFormSet"].save()
            # Step 8.2 Cytotox Tests
            if self.show_cytotox_test():
                # We set the dicts to translates attributes into model
                form_dict["TestCytotoxDescriptionFormSet"].set_compounds(compounds)
                # We set the constants attributes among the instances to be created by the modelformset
                form_dict["TestCytotoxDescriptionFormSet"].instance = bibliography
                form_dict["TestCytotoxDescriptionFormSet"].is_valid()
                form_dict["TestCytotoxDescriptionFormSet"].save()
            # Step 8.3 PK Tests
            if self.show_pharmacokinetic_test():
                # We set the dicts to translates attributes into model
                form_dict["TestPKDescriptionFormSet"].set_compounds(compounds)
                # We set the constants attributes among the instances to be created by the modelformset
                form_dict["TestPKDescriptionFormSet"].instance = bibliography
                form_dict["TestPKDescriptionFormSet"].is_valid()
                form_dict["TestPKDescriptionFormSet"].save()
            # re-save the ppi to update its name            (
            ppi.save()

            # get/build the contribution object
            contribution, created = models.Contribution.objects.get_or_create(
                contributor=self.request.user, ppi=ppi, bibliography=bibliography
            )
            if not created:
                # update its date of modification
                contribution.save()
            messages.success(
                self.request,
                f"Congratulations! Dear {contribution.contributor.username},"
                f" thank you for your contribution.",
            )
        return redirect(reverse("contribution-detail", kwargs={"pk": contribution.pk}))

    def get(self, *args, **kwargs):
        """
        This overrides the renders the form or, if needed, does the http redirects.
        """
        current_step = kwargs.get("step", None)
        if current_step is not None:
            # ensure all previous steps for current step have been provided data
            # for, and otherwise redirect to the last step we provided data for
            for step in [key for key in self.get_form_list().keys()]:
                if step != current_step:
                    if self.storage.get_step_data(step) is None:
                        self.storage.current_step = step
                        return redirect(self.get_step_url(step))
                else:
                    break
        return super(IppiWizard, self).get(*args, **kwargs)


ippidb_wizard_view = IppiWizard.as_view(FORMS, url_name="ippidb_step")


class ContributionDetailView(DetailView):
    """
    Contribution detail view
    """

    model = models.Contribution
    template_name = "contribute-detail.html"

    def get_queryset(self):
        return self.model.objects.for_user(self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contribution = context["contribution"]
        bibliography = contribution.bibliography
        ppi = contribution.ppi
        context["contribution"] = contribution
        context["bibliography"] = bibliography
        context[
            "testactivitydescription"
        ] = bibliography.testactivitydescription_set.all()
        context[
            "testcytotoxdescription"
        ] = bibliography.testcytotoxdescription_set.all()
        context["testpkdescription"] = bibliography.testpkdescription_set.all()
        context["testactivityresults"] = bibliography.testactivitydescription_set.all()
        context["refcompoundbiblio"] = bibliography.refcompoundbiblio_set.all()
        context["ppi"] = ppi
        context["complexes"] = ppi.ppicomplex_set.all()
        return context
