"""
Models used in iPPI-DB
"""

from __future__ import unicode_literals

import operator
import re
import sys

from boltons.iterutils import flatten, unique_iter
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import FloatField, IntegerField, BooleanField
from django.db.models import Max, Count, F, Q, Case, When, Subquery, OuterRef, Exists
from django.db.models.functions import Cast
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django_celery_results.models import TaskResult
from django.dispatch import receiver
from django.db.models.signals import post_save
from polymorphic.models import PolymorphicModel

from ippidb.utils import FingerPrinter, smi2inchi, smi2inchikey
from ippidb.ws import (
    get_pubmed_info,
    get_google_patent_info,
    get_uniprot_info,
    get_taxonomy_info,
    get_go_info,
    get_pfam_info,
    get_doi_info,
    get_chembl_id,
    get_pubchem_id,
    get_ligand_id,
    get_orcid_user_details,
    EntryNotFoundError,
)


class AutoFillableModel(models.Model):
    """
    AutoFillableModel abstract model to enable automated model fields
    filling from external sources in the autofill() method.
    The save method allows to either include autofill or not. in autofill kwarg
    is set to True, save() will first call autofill(), otherwise it won't
    """

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        auto_fill_needed = not self.is_autofill_done()
        if kwargs.get("autofill") is True or auto_fill_needed:
            auto_fill_needed = True
            self.autofill()
        if "autofill" in kwargs:
            del kwargs["autofill"]
        super(AutoFillableModel, self).save(*args, **kwargs)
        if auto_fill_needed:
            self.autofill_post_save()

    def autofill(self):
        raise NotImplementedError()

    def autofill_post_save(self):
        """
        Method called automatically after the save is done,
        usefull for setting m2m relations
        """
        pass

    def is_autofill_done(self):
        """
        test whether or not the model has been already autofilled
        """
        return True


class Bibliography(AutoFillableModel):
    """
    Bibliography reference
    (publications or patents)
    """

    SOURCES = (("PM", "PubMed ID"), ("PT", "Patent"), ("DO", "DOI"))
    id_source_validators = dict(
        PM=re.compile(r"^[0-9]+$"),
        PT=re.compile(r"^.*$"),
        DO=re.compile(r"^10.\d{4,9}/.+$"),
    )
    source = models.CharField(
        "Bibliographic type", max_length=2, choices=SOURCES, default=SOURCES[0][0]
    )
    id_source = models.TextField("Bibliographic ID")
    title = models.TextField("Title")
    journal_name = models.TextField("Journal name", null=True, blank=True)
    authors_list = models.TextField("Authors list")
    biblio_year = models.PositiveSmallIntegerField("Year")
    cytotox = models.BooleanField("Cytotoxicity data", default=False)
    in_silico = models.BooleanField("in silico study", default=False)
    in_vitro = models.BooleanField("in vitro study", default=False)
    in_vivo = models.BooleanField("in vivo study", default=False)
    in_cellulo = models.BooleanField("in cellulo study", default=False)
    pharmacokinetic = models.BooleanField("pharmacokinetic study", default=False)
    xray = models.BooleanField("X-Ray data", default=False)

    def autofill(self):
        """
        fetch information from external services
        (Pubmed or Google patents)
        """
        if self.source == "PM":
            info = get_pubmed_info(self.id_source)
        elif self.source == "PT":
            info = get_google_patent_info(self.id_source)
        elif self.source == "DO":
            info = get_doi_info(self.id_source)
        else:
            raise NotImplementedError()
        self.title = info["title"]
        self.journal_name = info["journal_name"]
        self.authors_list = info["authors_list"]
        self.biblio_year = info["biblio_year"]

    def is_autofill_done(self):
        return len(self.title) > 0

    def clean(self):
        super().clean()
        Bibliography.validate_source_id(self.id_source, self.source)

    def has_external_url(self):
        return self.source == "PM" or self.source == "DO"

    def get_external_url(self):
        """
        Get URL to the publication
        """
        if self.source == "PM":
            return f"https://www.ncbi.nlm.nih.gov/pubmed/{self.id_source}"
        elif self.source == "DO":
            return f"https://doi.org/{self.id_source}"
        elif self.source == "PT":
            return f"https://patentscope.wipo.int/search/en/detail.jsf?docId={self.id_source}"

    @staticmethod
    def validate_source_id(id_source, source):
        id_source_validator = Bibliography.id_source_validators[source]
        if not id_source_validator.match(id_source):
            raise ValidationError(
                dict(
                    id_source=_(
                        f"Must match pattern {id_source_validator.pattern}"
                        " for this selected source"
                    )
                )
            )
        return True

    class Meta:
        verbose_name_plural = "Bibliographies"
        verbose_name = "Bibliography"

    def data_and_study(self):
        ret = []
        for f in [
            "cytotox",
            "xray",
            "in_silico",
            "in_vitro",
            "in_cellulo",
            "in_vivo",
            "pharmacokinetic",
        ]:
            if getattr(self, f, False):
                ret.append(self._meta.get_field(f).verbose_name.title())
        return ", ".join(ret)

    def __str__(self):
        return "{}, {}".format(self.source, self.id_source)

    def get_absolute_url(self):
        return reverse("biblio-view", kwargs={"biblio_pk": self.pk})


class Taxonomy(AutoFillableModel):
    """
    Taxonomy IDs (from NCBI Taxonomy)
    and the corresponding human-readable name
    """

    taxonomy_id = models.DecimalField(
        "NCBI TaxID", unique=True, max_digits=9, decimal_places=0
    )
    name = models.CharField("Organism name", max_length=200)

    def autofill(self):
        """
        fetch information from external services
        (NCBI Entrez)
        """
        info = get_taxonomy_info(self.taxonomy_id)
        self.name = info["scientific_name"]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "taxonomies"


class MolecularFunction(AutoFillableModel):
    """
    Molecular functions (from Gene Ontology)
    and the corresponding human-readable description
    """

    go_id = models.CharField("Gene Ontology ID", unique=True, max_length=10)
    # GO term id format: 'GO:0000000'
    description = models.CharField("description", max_length=500)

    def autofill(self):
        """
        fetch information from external services
        (EBI OLS)
        """
        info = get_go_info(self.go_id)
        self.description = info["label"]

    def is_autofill_done(self):
        return self.description is not None and len(self.description) > 0

    @property
    def name(self):
        return self.go_id + " " + self.description

    def __str__(self):
        return self.description


class Protein(AutoFillableModel):
    """
    Protein information (from Uniprot)
    and the corresponding human-readable name
    """

    uniprot_id = models.CharField("Uniprot ID", unique=True, max_length=10)
    recommended_name_long = models.TextField(
        "Uniprot Recommended Name (long)", blank=True, null=True
    )
    short_name = models.CharField("Short name", max_length=50)
    gene_name = models.CharField("Gene name", max_length=30, blank=True, null=True)
    entry_name = models.CharField("Entry name", max_length=30)
    organism = models.ForeignKey("Taxonomy", models.CASCADE)
    molecular_functions = models.ManyToManyField(MolecularFunction)
    domains = models.ManyToManyField("Domain")

    @transaction.atomic
    def autofill(self):
        """
        fetch information from external services
        (Uniprot) and create Taxonomy/Molecular Functions if needed
        """
        info = get_uniprot_info(self.uniprot_id)
        self.recommended_name_long = info["recommended_name"]

        gene_names = info["gene_names"]
        # put whatever name it find
        try:
            self.gene_name = gene_names[0]["name"]
        except IndexError:
            pass
        # then try to find the primary, if present
        for gene_name in gene_names:
            if gene_name["type"] == "primary":
                self.gene_name = gene_name["name"]
                break

        self.entry_name = info["entry_name"]
        self.short_name = info["short_name"]
        try:
            taxonomy = Taxonomy.objects.get(taxonomy_id=info["organism"])
        except Taxonomy.DoesNotExist:
            taxonomy = Taxonomy()
            taxonomy.taxonomy_id = info["organism"]
            taxonomy.save(autofill=True)
        self.organism = taxonomy
        self.__info = info

    def autofill_post_save(self):
        info = self.__info
        for go_id in info["molecular_functions"]:
            mol_function, created = MolecularFunction.objects.get_or_create(go_id=go_id)
            self.molecular_functions.add(mol_function)

        for domain_id in info["domains"]:
            domain, created = Domain.objects.get_or_create(pfam_acc=domain_id)
            self.domains.add(domain)

    def is_autofill_done(self):
        return len(self.entry_name) > 0

    def __str__(self):
        return "{} ({})".format(
            self.uniprot_id, self.recommended_name_long or self.short_name
        )


class Domain(AutoFillableModel):
    """
    Domain (i.e. Protein domain) information (from PFAM)
    """

    pfam_acc = models.CharField("Pfam Accession", max_length=10, unique=True)
    pfam_id = models.CharField("Pfam Family Identifier", max_length=20)
    pfam_description = models.CharField("Pfam Description", max_length=100)
    domain_family = models.CharField(
        "Domain family", max_length=25, blank=True, default=""
    )

    # TODO: what is this field? check database
    # contents

    def autofill(self):
        """
        fetch information from external services
        (PFAM)
        """
        info = get_pfam_info(self.pfam_acc)
        self.pfam_id = info["id"]
        self.pfam_description = info["description"][:100]

    def is_autofill_done(self):
        return self.pfam_id is not None and len(self.pfam_id) > 0

    @property
    def name(self):
        return self.pfam_id

    def __str__(self):
        return f"{self.pfam_acc} ({self.pfam_id}-{self.pfam_description})"


class ProteinDomainComplexGroup(models.Model):
    """
    Protein-Domain association group
    """

    protein = models.ForeignKey("Protein", models.CASCADE)
    domain = models.ForeignKey("Domain", models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name_plural = "protein domain complex groups"

    def __str__(self):
        return "{} {}-{}".format(
            self.protein.short_name,
            self.protein.uniprot_id,
            self.domain.pfam_id if self.domain else "unknow",
        )

    def name(self):
        return str(self)


class ProteinDomainComplex(PolymorphicModel):
    """
    Protein-Domain association
    """

    protein = models.ForeignKey("Protein", models.CASCADE)
    domain = models.ForeignKey("Domain", models.CASCADE, null=True, blank=True)
    ppc_copy_nb = models.IntegerField("Number of copies of the protein in the complex")
    group = models.ForeignKey(
        "ProteinDomainComplexGroup", models.SET_NULL, null=True, blank=True
    )

    class Meta:
        verbose_name_plural = "complexes"

    def __str__(self):
        return "{} {}-{} ({})".format(
            self.protein.short_name,
            self.protein.uniprot_id,
            self.domain.pfam_id if self.domain else "unknow",
            self.ppc_copy_nb,
        )

    def name(self):
        return self.protein.short_name


class ProteinDomainBoundComplexGroup(ProteinDomainComplexGroup):
    """
    Protein-Domain Bound Complexes group
    """

    pass


class ProteinDomainBoundComplex(ProteinDomainComplex):
    """
    Protein-Domain association with a "bound complex" role
    """

    ppp_copy_nb_per_p = models.IntegerField(_("ppp_copy_nb_per_p"))

    @property
    def complex_type(self):
        return "Bound"

    class Meta:
        verbose_name_plural = "bound complexes"

    def set_group(self):
        self.group = ProteinDomainBoundComplexGroup.objects.get_or_create(
            protein=self.protein, domain=self.domain
        )[0]


class ProteinDomainPartnerComplexGroup(ProteinDomainComplexGroup):
    """
    Protein-Domain Bound Complexes group
    """

    pass


class ProteinDomainPartnerComplex(ProteinDomainComplex):
    """
    Protein-Domain association with a "partner complex" role
    """

    @property
    def complex_type(self):
        return "Partner"

    class Meta:
        verbose_name_plural = "partner complexes"

    def set_group(self):
        self.group = ProteinDomainPartnerComplexGroup.objects.get_or_create(
            protein=self.protein, domain=self.domain
        )[0]


class Symmetry(models.Model):
    """
    Symmetry of a PPI
    """

    code = models.CharField("Symmetry code", max_length=2)
    description = models.CharField("Description", max_length=300)

    class Meta:
        verbose_name_plural = "symmetries"

    def __str__(self):
        return "{} ({})".format(self.code, self.description)


class Disease(models.Model):
    name = models.CharField("Disease", max_length=256)
    identifier = models.CharField("Identifier", max_length=32, null=True, blank=True)

    def __str__(self):
        return "%s (%s)" % (self.name, self.identifier)


class PpiFamily(models.Model):
    """
    PPI Family
    """

    name = models.CharField("Name", max_length=30, unique=True)

    class Meta:
        verbose_name_plural = "PPI Families"

    def __str__(self):
        return self.name


class Ppi(AutoFillableModel):
    """
    PPI
    """

    pdb_id = models.CharField("PDB ID", max_length=4, null=True, blank=True)
    pockets_nb = models.IntegerField(
        "Total number of pockets in the complex", default=1
    )
    symmetry = models.ForeignKey(Symmetry, models.CASCADE)
    diseases = models.ManyToManyField(Disease, blank=True)
    family = models.ForeignKey(PpiFamily, models.CASCADE, null=True, blank=True)
    name = models.TextField("PPI name")

    def __str__(self):
        return "PPI #{} on {}".format(self.id, self.name)

    def get_absolute_url(self):
        return reverse("ppi-view", kwargs={"ppi_pk": self.pk})

    def is_autofill_done(self):
        return self.name != ""

    def autofill(self):
        pass

    def autofill_post_save(self):
        # name is denormalized and stored in the database to reduce SQL queries in query mode
        old_name = self.name
        self.name = self.compute_name_from_protein_names()
        if old_name != self.name:
            self.save()

    def get_ppi_bound_complexes(self):
        """
        return bound ppi complexes belonging to this ppi
        """
        return PpiComplex.objects.filter(
            ppi=self, complex__in=ProteinDomainBoundComplex.objects.all()
        )

    def compute_name_from_protein_names(self):
        all_protein_names = set(
            [
                ppi_complex.complex.protein.short_name
                for ppi_complex in self.ppicomplex_set.all()
            ]
        )
        bound_protein_names = set(
            [
                ppi_complex.complex.protein.short_name
                for ppi_complex in self.get_ppi_bound_complexes()
            ]
        )
        partner_protein_names = all_protein_names - bound_protein_names
        bound_str = ",".join(sorted(bound_protein_names))
        partner_str = ",".join(sorted(partner_protein_names))
        name = bound_str
        if partner_str != "":
            name += " / " + partner_str
        return name


class PpiComplex(models.Model):
    """
    PPI Complex
    """

    ppi = models.ForeignKey(Ppi, models.CASCADE)
    complex = models.ForeignKey(ProteinDomainComplex, models.CASCADE)
    cc_nb = models.IntegerField(verbose_name=_("cc_nb_verbose_name"), default=1)

    class Meta:
        verbose_name_plural = "Ppi complexes"

    def __str__(self):
        return "PPI {}, Complex {} ({})".format(self.ppi, self.complex, self.cc_nb)


class CompoundsManager(models.Manager):
    """
    Model manager for the `Compound` class
    provides selections to `validated` or `user-visible` compounds
    """

    def for_user(self, current_user):
        """
        Get compounds visible to a given user
        i.e. validated or created by the user or
        all of them if the user is an admin
        """
        qs = self.get_queryset()
        if current_user.is_anonymous:
            qs = qs.exclude(compoundaction__ppi__contribution__validated=False)
            qs = qs.filter(replaced_with__isnull=True)
        elif not current_user.is_superuser:
            qs = qs.exclude(
                Q(compoundaction__ppi__contribution__validated=False),
                ~Q(compoundaction__ppi__contribution__contributor=current_user),
            )
            qs = qs.filter(replaced_with__isnull=True)
        return qs

    def validated(self):
        """
        Get validated compounds
        """
        return (
            super()
            .get_queryset()
            .exclude(compoundaction__ppi__contribution__validated=False)
        )


class Compound(AutoFillableModel):
    """
    Chemical compound
    """

    objects = CompoundsManager()

    canonical_smile = models.TextField(verbose_name="Canonical Smiles", unique=True)
    is_macrocycle = models.BooleanField(
        verbose_name=_("is_macrocycle_verbose_name"),
        help_text=_("is_macrocycle_help_text"),
    )
    aromatic_ratio = models.DecimalField(
        verbose_name="Aromatic ratio",
        max_digits=3,
        decimal_places=2,
        blank=True,
        null=True,
    )
    balaban_index = models.DecimalField(
        verbose_name="Balaban index",
        max_digits=3,
        decimal_places=2,
        blank=True,
        null=True,
    )
    fsp3 = models.DecimalField(
        verbose_name="Fsp3", max_digits=3, decimal_places=2, blank=True, null=True
    )
    gc_molar_refractivity = models.DecimalField(
        verbose_name="GC Molar Refractivity",
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
    )
    log_d = models.DecimalField(
        verbose_name="LogD (Partition coefficient octanol-1/water, with pKa information)",
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
    )
    a_log_p = models.DecimalField(
        verbose_name="ALogP (Partition coefficient octanol-1/water)",
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
    )
    mean_atom_vol_vdw = models.DecimalField(
        verbose_name="Mean atom volume computed with VdW radii",
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
    )
    molecular_weight = models.DecimalField(
        verbose_name="Molecular weight",
        max_digits=6,
        decimal_places=2,
        blank=True,
        null=True,
    )
    nb_acceptor_h = models.IntegerField(
        verbose_name="Number of hydrogen bond acceptors", blank=True, null=True
    )
    nb_aliphatic_amines = models.IntegerField(
        verbose_name="Number of aliphatics amines", blank=True, null=True
    )
    nb_aromatic_bonds = models.IntegerField(
        verbose_name="Number of aromatic bonds", blank=True, null=True
    )
    nb_aromatic_ether = models.IntegerField(
        verbose_name="Number of aromatic ethers", blank=True, null=True
    )
    nb_aromatic_sssr = models.IntegerField(
        verbose_name="Number of aromatic Smallest Set of System Rings (SSSR)",
        blank=True,
        null=True,
    )
    nb_atom = models.IntegerField(verbose_name="Number of atoms", blank=True, null=True)
    nb_atom_non_h = models.IntegerField(
        verbose_name="Number of non hydrogen atoms", blank=True, null=True
    )
    nb_benzene_like_rings = models.IntegerField(
        verbose_name="Number of benzene-like rings", blank=True, null=True
    )
    nb_bonds = models.IntegerField(
        verbose_name="Number of bonds", blank=True, null=True
    )
    nb_bonds_non_h = models.IntegerField(
        verbose_name="Number of bonds not involving a hydrogen", blank=True, null=True
    )
    nb_br = models.IntegerField(
        verbose_name="Number of Bromine atoms", blank=True, null=True
    )
    nb_c = models.IntegerField(
        verbose_name="Number of Carbon atoms", blank=True, null=True
    )
    nb_chiral_centers = models.IntegerField(
        verbose_name="Number of chiral centers", blank=True, null=True
    )
    nb_circuits = models.IntegerField(
        verbose_name="Number of circuits", blank=True, null=True
    )
    nb_cl = models.IntegerField(
        verbose_name="Number of Chlorine atoms", blank=True, null=True
    )
    nb_csp2 = models.IntegerField(
        verbose_name="Number of sp2-hybridized carbon atoms", blank=True, null=True
    )
    nb_csp3 = models.IntegerField(
        verbose_name="Number of sp3-hybridized carbon atoms", blank=True, null=True
    )
    nb_donor_h = models.IntegerField(
        verbose_name="Number of hydrogen bond donors", blank=True, null=True
    )
    nb_double_bonds = models.IntegerField(
        verbose_name="Number of double bonds", blank=True, null=True
    )
    nb_f = models.IntegerField(
        verbose_name="Number of fluorine atoms", blank=True, null=True
    )
    nb_i = models.IntegerField(
        verbose_name="Number of iodine atoms", blank=True, null=True
    )
    nb_multiple_bonds = models.IntegerField(
        verbose_name="Number of multiple bonds", blank=True, null=True
    )
    nb_n = models.IntegerField(
        verbose_name="Number of nitrogen atoms", blank=True, null=True
    )
    nb_o = models.IntegerField(
        verbose_name="Number of oxygen atoms", blank=True, null=True
    )
    nb_rings = models.IntegerField(
        verbose_name="Number of rings", blank=True, null=True
    )
    nb_rotatable_bonds = models.IntegerField(
        verbose_name="Number of rotatable bonds", blank=True, null=True
    )
    inchi = models.TextField(verbose_name="InChi", blank=True, null=True)
    inchikey = models.TextField(verbose_name="InChiKey", blank=True, null=True)
    randic_index = models.DecimalField(
        verbose_name="Randic index",
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
    )
    rdf070m = models.DecimalField(
        verbose_name="RDF070m, radial distribution function weighted by the atomic masses at 7Å",
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
    )
    rotatable_bond_fraction = models.DecimalField(
        verbose_name="Fraction of rotatable bonds",
        max_digits=3,
        decimal_places=2,
        blank=True,
        null=True,
    )
    sum_atom_polar = models.DecimalField(
        verbose_name="Sum of atomic polarizabilities",
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
    )
    sum_atom_vol_vdw = models.DecimalField(
        verbose_name="Sum of atom volumes computed with VdW radii",
        max_digits=6,
        decimal_places=2,
        blank=True,
        null=True,
    )
    tpsa = models.DecimalField(
        verbose_name="Topological Polar Surface Area (TPSA)",
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
    )
    ui = models.DecimalField(
        verbose_name="Unsaturation index",
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
    )
    wiener_index = models.IntegerField(
        verbose_name="Wiener index", blank=True, null=True
    )
    common_name = models.CharField(
        verbose_name="Common name", max_length=20, blank=True, null=True
    )
    pubchem_id = models.CharField(
        verbose_name="Pubchem ID", max_length=10, blank=True, null=True
    )
    chemspider_id = models.CharField(
        verbose_name="Chemspider ID", unique=True, max_length=10, blank=True, null=True
    )
    chembl_id = models.CharField(
        verbose_name="Chembl ID", max_length=30, blank=True, null=True
    )
    iupac_name = models.TextField(verbose_name="IUPAC name", blank=True, null=True)
    ligand_id = models.CharField("PDB Ligand ID", max_length=3, blank=True, null=True)
    drugbank_id = models.TextField("Drugbank ID", blank=True, null=True)
    pubs = models.IntegerField(
        verbose_name="Number of publications", null=True, blank=True
    )
    best_activity = models.DecimalField(
        "Best activity", max_digits=12, decimal_places=10, null=True, blank=True
    )
    best_activity_ppi_family_name = models.CharField(
        "Best activity PPI family name", max_length=30, null=True, blank=True
    )
    families = models.TextField("PPI family names", null=True, blank=True)

    le = models.FloatField(verbose_name="Ligand efficiency", null=True, blank=True)
    lle = models.FloatField(verbose_name="Lipophilic efficiency", null=True, blank=True)
    lipinsky_mw = models.BooleanField("MW ok for Lipinsky", null=True, blank=True)
    lipinsky_hba = models.BooleanField(
        "Hydrogen bond acceptors ok for Lipinsky", null=True, blank=True
    )
    lipinsky_hbd = models.BooleanField(
        "Hydrogen bond donors ok for Lipinsky", null=True, blank=True
    )
    lipinsky_a_log_p = models.BooleanField(
        "A log P ok for Lipinsky", null=True, blank=True
    )
    lipinsky_score = models.IntegerField(
        verbose_name="Lipinsky score", null=True, blank=True
    )
    lipinsky = models.BooleanField("Lipinsky ok", null=True, blank=True)
    hba_hbd = models.IntegerField(
        verbose_name="Sum of Hydrogen bond acceptors and donors", null=True, blank=True
    )
    veber_hba_hbd = models.BooleanField("HBA+HBD ok for Veber", null=True, blank=True)
    veber_tpsa = models.BooleanField("TPSA ok for Veber", null=True, blank=True)
    veber_rb = models.BooleanField(
        "Rotatable bonds ok for Veber", null=True, blank=True
    )
    veber = models.BooleanField("Veber ok", null=True, blank=True)
    pfizer_a_log_p = models.BooleanField("A log P ok for Pfizer", null=True, blank=True)
    pfizer_tpsa = models.BooleanField("TPSA ok for Pfizer", null=True, blank=True)
    pfizer = models.BooleanField("Pfizer ok", null=True, blank=True)
    pdb_ligand_av = models.BooleanField("PDB ligand available", null=True, blank=True)
    inhibition_role = models.BooleanField("Inhibition role", null=True, blank=True)
    binding_role = models.BooleanField("Binding role", null=True, blank=True)
    stabilisation_role = models.BooleanField(
        "Stabilisation role", null=True, blank=True
    )
    celltest_av = models.BooleanField("Cellular tests performed", null=True, blank=True)
    inhitest_av = models.BooleanField(
        "Inhibition tests performed", null=True, blank=True
    )
    stabtest_av = models.BooleanField(
        "Stabilisation tests performed", null=True, blank=True
    )
    bindtest_av = models.BooleanField("Binding tests performed", null=True, blank=True)
    pktest_av = models.BooleanField(
        "Pharmacokinetic tests performed", null=True, blank=True
    )
    cytoxtest_av = models.BooleanField(
        "Cytotoxicity tests performed", null=True, blank=True
    )
    insilico_av = models.BooleanField(
        "In silico tests performed", null=True, blank=True
    )
    tests_av = models.IntegerField(
        verbose_name="Number of tests available", null=True, blank=True
    )

    replaced_with = models.ForeignKey(
        "self",
        verbose_name="Replacement ID",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ["id"]
        indexes = [
            models.Index(fields=["molecular_weight"]),
            models.Index(fields=["a_log_p"]),
            models.Index(fields=["nb_donor_h"]),
            models.Index(fields=["nb_acceptor_h"]),
            models.Index(fields=["tpsa"]),
            models.Index(fields=["nb_rotatable_bonds"]),
            models.Index(fields=["nb_aromatic_sssr"]),
            models.Index(fields=["nb_chiral_centers"]),
            models.Index(fields=["fsp3"]),
            models.Index(fields=["pubs"]),
            models.Index(fields=["best_activity"]),
            models.Index(fields=["le"]),
            models.Index(fields=["lle"]),
        ]
        # default_manager_name = 'ippidb.models.ValidatedCompoundsManager'
        # indexes = [
        #     models.Index(fields=['lipinsky']),
        #     models.Index(fields=['veber']),
        #     models.Index(fields=['pfizer']),
        #     models.Index(fields=['pdb_ligand_av']),
        #     models.Index(fields=['inhibition_role']),
        #     models.Index(fields=['binding_role']),
        #     models.Index(fields=['stabilisation_role']),
        #     models.Index(fields=['binding_role']),
        # ]

    def compute_drugbank_compound_similarity(self):
        """
        Compute Tanimoto similarity to existing DrugBank compounds
        """
        self.save()
        # fingerprints to compute drugbank similarities are in settings module, default FP2
        fingerprinter = FingerPrinter(getattr(settings, "DRUGBANK_FINGERPRINTS", "FP2"))
        # 1. compute tanimoto for SMILES query vs all compounds
        smiles_dict = {c.id: c.canonical_smiles for c in DrugBankCompound.objects.all()}
        tanimoto_dict = fingerprinter.tanimoto_smiles(self.canonical_smile, smiles_dict)
        tanimoto_dict = dict(
            sorted(tanimoto_dict.items(), key=operator.itemgetter(1), reverse=True)[:15]
        )
        dbcts = []
        for id_, tanimoto in tanimoto_dict.items():
            dbcts.append(
                DrugbankCompoundTanimoto(
                    compound=self,
                    drugbank_compound=DrugBankCompound.objects.get(id=id_),
                    tanimoto=tanimoto,
                )
            )
        DrugbankCompoundTanimoto.objects.filter(compound=self).delete()
        DrugbankCompoundTanimoto.objects.bulk_create(dbcts)

    @property
    def biblio_refs(self):
        """
        Return all RefCompoundBiblio related to this compound
        """
        return RefCompoundBiblio.objects.filter(compound=self)

    @property
    def pfam_ids(self):
        """
        Return all PFAM ids for the domain of the proteins of the bound
        complexes in the PPIs this compound has an action on
        """
        pfam_ids = set()
        for ca in self.compoundaction_set.all():
            ca.get_complexes()
            for bound_complex in ca.ppi.get_ppi_bound_complexes():
                pfam_ids.add(bound_complex.complex.domain.pfam_id)
        return pfam_ids

    @property
    def best_pXC50_compound_activity_result(self):
        """
        Return the best pXC50 activity
        """
        best_pXC50_activity = self.best_activity
        if best_pXC50_activity is None:
            return None
        best_cars = self.compoundactivityresult_set.filter(activity=best_pXC50_activity)
        if len(best_cars) > 0:
            return best_cars[0]
        else:
            return None

    @property
    def bioch_tests_count(self):
        """
        Return the number of associated biochemical tests
        """
        return (
            self.compoundactivityresult_set.all()
            .filter(test_activity_description__test_type="BIOCH")
            .count()
        )

    @property
    def cell_tests_count(self):
        """
        Return the number of associated cell tests
        """
        return (
            self.compoundactivityresult_set.all()
            .filter(test_activity_description__test_type="CELL")
            .count()
        )

    @property
    def pk_tests_count(self):
        """
        Return the number of associated pharmacokinetic tests
        """
        return self.compoundpkresult_set.all().count()

    @property
    def cytotoxicity_tests_count(self):
        """
        Return the number of associated cytotoxicity tests
        """
        return self.compoundcytotoxicityresult_set.all().count()

    @property
    def sorted_similar_drugbank_compounds(self):
        """
        Return the similar Drugbank compounds,
        sorted by decreasing similarity
        """
        return self.drugbankcompoundtanimoto_set.order_by("-tanimoto")

    def is_validated(self):
        """
        Return the compound validation status
        """
        # if compound is not linked to any CompoundAction this is
        # because it was dereferenced because of duplication by
        # `replace_compound_references`
        if self.compoundaction_set.count() == 0:
            return False
        for ca in self.compoundaction_set.all():
            if ca.ppi.contribution_set.filter(validated=False).exists():
                return False
        return True

    is_validated.boolean = True

    def autofill(self):
        """
        Finalize the computation of the Compound
        by computing InChi, InChiKey and Drugbank similarity
        """
        # compute InChi and InChiKey
        self.inchi = smi2inchi(self.canonical_smile)
        self.inchikey = smi2inchikey(self.canonical_smile)
        self.compute_drugbank_compound_similarity()
        self.set_drugbank_link()
        self.set_chembl_link()
        self.set_pubchem_link()
        self.save()

    def set_drugbank_link(self):
        """
        Set Drugbank ID by looking up InChi Key
        in the database
        """
        drugbank_qs = DrugBankCompound.objects.filter(inchikey=self.inchikey)
        if drugbank_qs.count() > 0:
            self.drugbank_id = drugbank_qs[0].id
        else:
            self.drugbank_id = None

    def set_chembl_link(self):
        """
        Set ChEMBL ID by looking up InChi Key
        with the ChEMBL web service
        """
        try:
            self.chembl_id = get_chembl_id(self.inchikey)
        except EntryNotFoundError:
            pass

    def set_pubchem_link(self):
        """
        Set PubChem ID by looking up InChi Key
        with the PubChem web service
        """
        try:
            self.pubchem_id = get_pubchem_id(self.inchikey)
        except EntryNotFoundError:
            pass

    def set_ligand_link(self):
        """
        Set Ligand ID by looking up SMILES
        with the PDB REST service
        """
        try:
            self.ligand_id = get_ligand_id(self.canonical_smile)
        except EntryNotFoundError:
            pass

    def compute_fsp3(self):
        """
        Compute FSP3 from CSP3 and number of carbons
        """
        self.fsp3 = self.nb_csp3 / self.nb_c

    def __str__(self):
        """
        String representation
        """
        return "iPPI-DB Compound #{}".format(self.id)

    def get_absolute_url(self):
        """
        Get absolute URL to the Compound page
        """
        return reverse("compound_card", kwargs={"pk": self.pk})

    @property
    def pubchem_url(self):
        """
        Get absolute URL to the corresponding PubChem entry
        """
        return (
            f"https://pubchem.ncbi.nlm.nih.gov/compound/{ self.pubchem_id }"
            if self.pubchem_id
            else None
        )

    @property
    def chembl_url(self):
        """
        Get absolute URL to the corresponding Chembl entry
        """
        return (
            f"https://www.ebi.ac.uk/chembldb/compound/inspect/{ self.chembl_id }"
            if self.chembl_id
            else None
        )

    @property
    def chemspider_url(self):
        """
        Get absolute URL to the corresponding ChemSpider entry
        """
        return (
            f"http://www.chemspider.com/Chemical-Structure.{ self.chemspider_id}.html"
            if self.chemspider_id
            else None
        )

    @property
    def pdbligand_url(self):
        """
        Get absolute URL to the corresponding PDB ligand entry
        """
        return (
            f"https://www.rcsb.org/ligand/{ self.ligand_id}" if self.ligand_id else None
        )

    @property
    def drugbank_url(self):
        """
        Get absolute URL to the corresponding Drugbank entry
        """
        return (
            f"https://www.drugbank.ca/drugs/{ self.drugbank_id}"
            if self.drugbank_id
            else None
        )

    @property
    def sameas_urls(self):
        urls = []
        if self.pubchem_id:
            urls.append(self.pubchem_url)
        if self.chembl_id:
            urls.append(self.chembl_url)
        if self.chemspider_id:
            urls.append(self.chemspider_url)
        if self.ligand_id:
            urls.append(self.pdbligand_url)
        if self.drugbank_id:
            urls.append(self.drugbank_url)
        return urls

    def get_bioschemas(self, request):
        json_data = {"@type": "MolecularEntity", "@context": "http://schema.org"}
        json_data["name"] = str(self)
        if self.pk is not None:
            json_data["url"] = request.build_absolute_uri(self.get_absolute_url())
        if self.inchi is not None:
            json_data["inChI"] = self.inchi
        if self.iupac_name != "":
            json_data["iupacName"] = self.iupac_name
        if self.molecular_weight is not None:
            json_data["molecularWeight"] = self.molecular_weight
        if self.inchikey is not None:
            json_data["inChIKey"] = self.inchikey
        if self.chembl_id is not None:
            json_data["image"] = (
                f"https://www.ebi.ac.uk/chembl/api/data/image/{ self.chembl_id }.svg"
            )
        if self.canonical_smile is not None:
            json_data["smiles"] = self.canonical_smile
        if len(self.sameas_urls) > 0:
            json_data["sameAs"] = self.sameas_urls
        return json_data

    def clean(self):
        """
        Perform additional checks:
        - check common name for the Compound is unique
        """
        if (
            self.common_name is not None
            and self.common_name != ""
            and Compound.objects.filter(common_name=self.common_name)
            .filter(~Q(pk=self.pk))
            .exists()
        ):
            self.add_error("common_name", "A compound with this name already exists")

    def get_jobs(self):
        """
        Retrieve the jobs for the compound
        """
        return CompoundJob.objects.filter(compound=self)

    def replace_compound_references(self, replacing_compound):
        """
        Replace the references to a given compound in the data with
        references to another new compound. used to deal with
        duplicates in the database
        also delete this object
        """
        self.replaced_with = replacing_compound
        for ref in RefCompoundBiblio.objects.filter(compound=self):
            ref.compound = replacing_compound
            ref.save()
        for ca in CompoundAction.objects.filter(compound=self):
            ca.compound = replacing_compound
            ca.save()
        for car in CompoundActivityResult.objects.filter(compound=self):
            car.compound = replacing_compound
            car.save()
        for ccr in CompoundCytotoxicityResult.objects.filter(compound=self):
            ccr.compound = replacing_compound
            ccr.save()
        for cpr in CompoundPKResult.objects.filter(compound=self):
            cpr.compound = replacing_compound
            cpr.save()
        # remove similarities with current component, in case any had been
        # stored
        CompoundTanimoto.objects.filter(compound=self).delete()
        DrugbankCompoundTanimoto.objects.filter(compound=self).delete()

    def get_target_activities_table(self):
        """
        Return test activity result data as a list of
        items containing for each PPI family the compound
        was tested against, the best activity, the linked diseases
        and the modulation types
        """
        ppi_families = [
            item["test_activity_description__ppi__family"]
            for item in self.compoundactivityresult_set.filter(
                ~Q(activity_type="KdRat")
            )
            .order_by()
            .values("test_activity_description__ppi__family")
            .distinct()
        ]
        results = []
        for ppi_family in ppi_families:
            family_compound_activity_results = self.compoundactivityresult_set.filter(
                ~Q(activity_type="KdRat")
            ).filter(test_activity_description__ppi__family__id=ppi_family)
            best_activity = (
                family_compound_activity_results.order_by("-activity")
                .values("activity")
                .first()["activity"]
            )
            diseases = list(
                unique_iter(
                    flatten(
                        [
                            list(fcar.test_activity_description.ppi.diseases.all())
                            for fcar in family_compound_activity_results
                        ]
                    )
                )
            )
            modulation_types = list(
                set(
                    [
                        fcar.get_modulation_type_display()
                        for fcar in family_compound_activity_results
                    ]
                )
            )
            results.append(
                {
                    "family": PpiFamily.objects.get(id=ppi_family),
                    "best_activity": best_activity,
                    "diseases": diseases,
                    "modulation_types": modulation_types,
                }
            )
        return results

    def get_last_modification_date(self):
        """
        Return last modification date for the compound
        based on latest contribution date
        """
        return Contribution.objects.filter(
            bibliography__refcompoundbiblio__compound=self
        ).aggregate(Max("created_at"))["created_at__max"]


class CompoundTanimoto(models.Model):
    canonical_smiles = models.TextField("Canonical Smile")
    fingerprint = models.TextField("Fingerprint")
    compound = models.ForeignKey(Compound, models.CASCADE)
    tanimoto = models.DecimalField("Tanimoto value", max_digits=5, decimal_places=4)

    class Meta:
        unique_together = ("canonical_smiles", "fingerprint", "compound")


def create_tanimoto(smiles_query, fingerprint):
    """
    Compute the Tanimoto similarity between a given SMILES and the compounds
    then insert the results in CompoundTanimoto
    """
    if (
        CompoundTanimoto.objects.filter(
            canonical_smiles=smiles_query, fingerprint=fingerprint
        ).count()
        == 0
    ):
        smiles_dict = {c.id: c.canonical_smile for c in Compound.objects.all()}
        fingerprinter = FingerPrinter(fingerprint)
        # 1. compute tanimoto for SMILES query vs all compounds
        tanimoto_dict = fingerprinter.tanimoto_smiles(smiles_query, smiles_dict)
        # 2. insert results in a table with three fields: SMILES query, compound id, tanimoto index
        cts = []
        for id_, smiles in smiles_dict.items():
            cts.append(
                CompoundTanimoto(
                    canonical_smiles=smiles_query,
                    fingerprint=fingerprint,
                    compound=Compound.objects.get(id=id_),
                    tanimoto=tanimoto_dict[id_],
                )
            )
        CompoundTanimoto.objects.bulk_create(cts)


class PcaBiplotData(models.Model):
    """
    PCA biplot data
    the table contains all the data as one JSON text in one row
    """

    pca_biplot_data = models.TextField("PCA biplot JSON data", blank=True, null=True)


class LeLleBiplotData(models.Model):
    """
    LE-LLE biplot data
    the table contains all the data as one JSON text in one row
    """

    le_lle_biplot_data = models.TextField(
        "LE-LLE biplot JSON data", blank=True, null=True
    )


class CellLine(models.Model):
    """
    Cell lines
    """

    name = models.CharField("Name", max_length=50, unique=True)

    def __str__(self):
        return self.name


class TestActivityDescription(models.Model):
    """
    Activity test descriptions
    """

    TEST_TYPES = (("BIOCH", "Biochemical assay"), ("CELL", "Cellular assay"))
    TEST_MODULATION_TYPES = (
        ("B", "Binding"),
        ("I", "Inhibition"),
        ("S", "Stabilization"),
    )
    biblio = models.ForeignKey(Bibliography, on_delete=models.CASCADE)
    protein_domain_bound_complex = models.ForeignKey(
        ProteinDomainBoundComplex, on_delete=models.CASCADE, null=True, blank=True
    )
    ppi = models.ForeignKey(Ppi, models.CASCADE, blank=True, null=True)
    test_type = models.CharField(
        verbose_name="Test type", max_length=5, choices=TEST_TYPES
    )
    test_name = models.CharField(verbose_name="Test name", max_length=100)
    is_primary = models.BooleanField(
        verbose_name="Is primary", help_text=_("is_primary_help_text")
    )
    test_modulation_type = models.CharField(
        verbose_name="Test modulation type", max_length=1, choices=TEST_MODULATION_TYPES
    )
    nb_active_compounds = models.IntegerField(
        verbose_name="Total number of active compounds"
    )
    cell_line = models.ForeignKey(CellLine, on_delete=models.SET_NULL, null=True)

    def get_complexes(self):
        """
        get the complexes tested for this PPI
        depends on the modulation type
        """
        if self.test_modulation_type == "I":
            return self.ppi.ppicomplex_set.all()
        else:
            return self.ppi.get_ppi_bound_complexes()

    @property
    def protein_domain_partner_complex(self):
        for ppic in self.ppi.ppicomplex_set.all():
            if hasattr(ppic.complex, "proteindomainpartnercomplex"):
                return ppic.complex.proteindomainpartnercomplex
        return None

    @property
    def name(self):
        return self.test_name

    def __str__(self):
        return self.get_test_type_display()


class CompoundActivityResult(models.Model):
    """
    Activity test results on a compound
    """

    MODULATION_TYPES = (("I", "Inhibition"), ("S", "Stabilization"))
    ACTIVITY_TYPES = (
        ("pIC50", "pIC50 (half maximal inhibitory concentration, -log10)"),
        ("pEC50", "pEC50 (half maximal effective concentration, -log10)"),
        ("pKd", "pKd (dissociation constant, -log10)"),
        ("pKi", "pKi (inhibition constant, -log10)"),
        ("KdRat", "Kd ratio (Kd without/with ligand"),
    )
    compound = models.ForeignKey(Compound, models.CASCADE)
    test_activity_description = models.ForeignKey(
        TestActivityDescription, models.CASCADE
    )
    activity_type = models.CharField(
        "Activity type", max_length=5, choices=ACTIVITY_TYPES
    )
    activity = models.DecimalField(
        "Activity", max_digits=12, decimal_places=10, null=True, blank=True
    )
    modulation_type = models.CharField(
        "Modulation type", max_length=1, choices=MODULATION_TYPES
    )

    class Meta:
        unique_together = (("compound", "test_activity_description", "activity_type"),)

    def __str__(self):
        return "Compound activity result for {} test {} on {}".format(
            self.activity_type, self.test_activity_description.id, self.compound.id
        )

    def is_best(self):
        return (
            self.compound.best_pXC50_compound_activity_result.id == self.id
            if self.compound.best_pXC50_compound_activity_result
            else False
        )

    @property
    def ref(self):
        return RefCompoundBiblio.objects.get(
            compound=self.compound, bibliography=self.test_activity_description.biblio
        )


class TestCytotoxDescription(models.Model):
    """
    Cytotoxicity test descriptions
    """

    biblio = models.ForeignKey(Bibliography, models.CASCADE)
    test_name = models.CharField("Cytotoxicity test name", max_length=100)
    cell_line = models.ForeignKey(CellLine, models.CASCADE)
    compound_concentration = models.DecimalField(
        "Compound concentration in μM",
        max_digits=7,
        decimal_places=3,
        blank=True,
        null=True,
    )


class CompoundCytotoxicityResult(models.Model):
    """
    Cytotoxicity test results on a compound
    """

    compound = models.ForeignKey(Compound, models.CASCADE)
    test_cytotoxicity_description = models.ForeignKey(
        TestCytotoxDescription, models.CASCADE
    )
    toxicity = models.BooleanField("Toxicity", default=False)

    class Meta:
        unique_together = (("compound", "test_cytotoxicity_description"),)

    def __str__(self):
        return "Compound cytotoxicity result for test {} on {}".format(
            self.test_cytotoxicity_description.id, self.compound.id
        )

    @property
    def ref(self):
        return RefCompoundBiblio.objects.get(
            compound=self.compound,
            bibliography=self.test_cytotoxicity_description.biblio,
        )


class TestPKDescription(models.Model):
    """
    Pharmacokinetic test descriptions
    """

    ADMINISTRATION_MODES = (
        ("IV", _("ADMINISTRATION_MODES_IV")),
        ("PO", _("ADMINISTRATION_MODES_PO")),
        ("IP", _("ADMINISTRATION_MODES_IP")),
        ("SL", _("ADMINISTRATION_MODES_SL")),
    )
    biblio = models.ForeignKey(Bibliography, models.CASCADE)
    test_name = models.CharField("Pharmacokinetic test name", max_length=100)
    organism = models.ForeignKey(Taxonomy, models.CASCADE)
    administration_mode = models.CharField(
        "Administration mode",
        max_length=2,
        choices=ADMINISTRATION_MODES,
        blank=True,
        null=True,
    )
    concentration = models.DecimalField(
        "Concentration in mg/l", max_digits=7, decimal_places=3, blank=True, null=True
    )
    dose = models.DecimalField(
        "Dose in mg/kg", max_digits=9, decimal_places=4, blank=True, null=True
    )
    dose_interval = models.IntegerField(
        "Dose interval, in hours", blank=True, null=True
    )


class CompoundPKResult(models.Model):
    """
    Pharmacokinetic test results on a compound
    """

    compound = models.ForeignKey(Compound, models.CASCADE)
    test_pk_description = models.ForeignKey(TestPKDescription, models.CASCADE)
    tolerated = models.BooleanField("Tolerated", null=True)
    oral_bioavailability = models.IntegerField(
        "Oral Bioavailability (%F)", blank=True, null=True
    )
    t_demi = models.IntegerField("t½ (mn)", blank=True, null=True)
    t_max = models.IntegerField("tmax (mn)", blank=True, null=True)
    auc_av = models.BooleanField("Area under curve available")
    clearance_av = models.BooleanField("Clearance available")
    c_max_av = models.BooleanField("Maximal concentration available")
    voldistribution_av = models.BooleanField("Volume distribution (Vd) available")

    class Meta:
        unique_together = (("compound", "test_pk_description"),)

    def __str__(self):
        return "Compound PK result for test {} on {}".format(
            self.test_pk_description.id, self.compound.id
        )

    @property
    def ref(self):
        return RefCompoundBiblio.objects.get(
            compound=self.compound, bibliography=self.test_pk_description.biblio
        )


class CompoundAction(models.Model):
    """
    Compound action
    """

    ACTIVATION_MODES = (("O", "Orthosteric"), ("A", "Allosteric"), ("U", "Unspecified"))
    compound = models.ForeignKey(Compound, models.CASCADE)
    activation_mode = models.CharField(
        "Activation mode", max_length=1, choices=ACTIVATION_MODES
    )
    ppi = models.ForeignKey(Ppi, models.CASCADE)
    nb_copy_compounds = models.IntegerField("Number of copies for the compound")

    class Meta:
        ordering = ["compound", "ppi"]

    def get_complexes(self):
        """
        get the complexes involved in the compound action
        which are always the bound complexes
        """
        return self.ppi.get_ppi_bound_complexes()

    def __str__(self):
        return "Action of {} on {}".format(self.compound, self.ppi)


class RefCompoundBiblio(models.Model):
    """
    Compound-Bibliographic reference association
    """

    compound = models.ForeignKey(Compound, models.CASCADE)
    bibliography = models.ForeignKey(Bibliography, models.CASCADE)
    compound_name = models.CharField("Compound name in the publication", max_length=50)

    class Meta:
        unique_together = (("compound", "bibliography"),)
        verbose_name_plural = "Compounds in bibliography"
        verbose_name = "Compound in bibliography"

    def __str__(self):
        return "Ref. {} on {}".format(self.bibliography, self.compound)


class DrugBankCompound(AutoFillableModel):
    """
    Drugbank compound
    """

    id = models.TextField("Drugbank ID", unique=True, primary_key=True)
    common_name = models.TextField("Common name")
    canonical_smiles = models.TextField("Canonical SMILES")
    inchikey = models.TextField(verbose_name="InChiKey", blank=True, null=True)

    def autofill(self):
        """
        Computing InChiKey
        """
        # compute InChiKey
        self.inchikey = smi2inchikey(self.canonical_smiles)


class DrugbankCompoundTanimoto(models.Model):
    """
    Drugbank compound-compound tanimoto similarity
    """

    compound = models.ForeignKey(Compound, models.CASCADE)
    drugbank_compound = models.ForeignKey(DrugBankCompound, models.CASCADE)
    tanimoto = models.DecimalField("Tanimoto value", max_digits=5, decimal_places=4)

    class Meta:
        unique_together = (("compound", "drugbank_compound"),)


class ContributionManager(models.Manager):
    """
    Model manager for the `Contribution` class
    provides selections to `validated` or `user-visible` contributions
    """

    def for_user(self, current_user):
        """
        Get contributions visible to a given user
        i.e. validated or created by the user or
        all of them if the user is an admin
        """
        qs = self.get_queryset()
        if current_user.is_anonymous:
            qs = qs.exclude(validated=False)
        elif not current_user.is_superuser:
            qs = qs.exclude(
                Q(validated=False),
                ~Q(contributor=current_user),
            )
        return qs

    def validated(self):
        """
        Get validated contributions
        """
        return super().get_queryset().exclude(validated=False)


class Contribution(models.Model):
    """
    Contribution: structured information describing some
    compound(s) and related effect(s) on some PPI(s) in a
    given publication/patent
    """

    objects = ContributionManager()

    contributor = models.ForeignKey(to=get_user_model(), on_delete=models.PROTECT)
    ppi = models.ForeignKey(to=Ppi, on_delete=models.SET_NULL, null=True)
    bibliography = models.ForeignKey(
        to=Bibliography, on_delete=models.SET_NULL, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    validated = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse("contribution-detail", kwargs={"pk": self.pk})


def update_compound_cached_properties(compounds_queryset=None):
    if compounds_queryset is None:
        compounds_queryset = Compound.objects.all()
    for c in compounds_queryset:
        c.families = ", ".join(
            list(
                set(
                    [
                        ca.ppi.family.name
                        for ca in c.compoundaction_set.all()
                        if ca.ppi.family is not None
                    ]
                )
            )
        )
        c.save()
    compounds_queryset.update(
        best_activity=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _best_activity=Max(
                    "compoundactivityresult__activity",
                    filter=~Q(compoundactivityresult__activity_type="KdRat"),
                )
            )
            .values("_best_activity")[:1]
        ),
    )
    compounds_queryset.update(
        pubs=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(_pubs=Count("refcompoundbiblio", distinct=True))
            .values("_pubs")[:1]
        ),
        le=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _le=Cast(
                    1.37 * F("best_activity") / F("nb_atom_non_h"),
                    FloatField(),
                )
            )
            .values("_le")[:1]
        ),
        lle=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(_lle=Cast(F("best_activity") - F("a_log_p"), FloatField()))
            .values("_lle")[:1]
        ),
        lipinsky_mw=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _lipinsky_mw=Case(
                    When(molecular_weight__lte=500, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_lipinsky_mw")[:1]
        ),
        lipinsky_hba=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _lipinsky_hba=Case(
                    When(nb_acceptor_h__lte=10, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_lipinsky_hba")[:1]
        ),
        lipinsky_hbd=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _lipinsky_hbd=Case(
                    When(nb_donor_h__lte=5, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_lipinsky_hbd")[:1]
        ),
        lipinsky_a_log_p=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _lipinsky_a_log_p=Case(
                    When(a_log_p__lte=5, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_lipinsky_a_log_p")[:1]
        ),
        hba_hbd=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(_hba_hbd=F("nb_acceptor_h") + F("nb_donor_h"))
            .values("_hba_hbd")[:1]
        ),
        veber_tpsa=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _veber_tpsa=Case(
                    When(tpsa__lte=140, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_veber_tpsa")[:1]
        ),
        veber_rb=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _veber_rb=Case(
                    When(nb_rotatable_bonds__lte=10, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_veber_rb")[:1]
        ),
        pfizer_a_log_p=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _pfizer_a_log_p=Case(
                    When(a_log_p__lte=3, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_pfizer_a_log_p")[:1]
        ),
        pfizer_tpsa=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _pfizer_tpsa=Case(
                    When(tpsa__gte=75, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_pfizer_tpsa")[:1]
        ),
        pfizer=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _pfizer=Case(
                    When(Q(Q(a_log_p__lte=3) & Q(tpsa__gte=75)), then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_pfizer")[:1]
        ),
        pdb_ligand_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _pdb_ligand_av=Cast(
                    Max(
                        Case(
                            When(ligand_id__isnull=False, then=1),
                            default=0,
                            output_field=IntegerField(),
                        )
                    ),
                    BooleanField(),
                )
            )
            .values("_pdb_ligand_av")[:1]
        ),
        inhibition_role=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _inhibition_role=Case(
                    When(compoundactivityresult__modulation_type="I", then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_inhibition_role")[:1]
        ),
        binding_role=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _binding_role=Case(
                    When(compoundactivityresult__modulation_type="I", then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_binding_role")[:1]
        ),
        stabilisation_role=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _stabilisation_role=Case(
                    When(compoundactivityresult__modulation_type="S", then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_stabilisation_role")[:1]
        ),
        celltest_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _celltest_av=Cast(
                    Max(
                        Case(
                            When(
                                compoundactivityresult__test_activity_description__test_type="CELL",
                                then=1,
                            ),
                            default=0,
                            output_field=IntegerField(),
                        )
                    ),
                    BooleanField(),
                )
            )
            .values("_celltest_av")[:1]
        ),
        inhitest_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _inhitest_av=Cast(
                    Max(
                        Case(
                            When(
                                compoundactivityresult__test_activity_description__test_modulation_type="I",
                                then=1,
                            ),
                            default=0,
                            output_field=IntegerField(),
                        )
                    ),
                    BooleanField(),
                )
            )
            .values("_inhitest_av")[:1]
        ),
        stabtest_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _stabtest_av=Cast(
                    Max(
                        Case(
                            When(
                                compoundactivityresult__test_activity_description__test_modulation_type="S",
                                then=1,
                            ),
                            default=0,
                            output_field=IntegerField(),
                        )
                    ),
                    BooleanField(),
                )
            )
            .values("_stabtest_av")[:1]
        ),
        bindtest_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _bindtest_av=Cast(
                    Max(
                        Case(
                            When(
                                compoundactivityresult__test_activity_description__test_modulation_type="I",
                                then=1,
                            ),
                            default=0,
                            output_field=IntegerField(),
                        )
                    ),
                    BooleanField(),
                )
            )
            .values("_bindtest_av")[:1]
        ),
        pktest_av=Exists(CompoundPKResult.objects.filter(compound__id=OuterRef("id"))),
        cytoxtest_av=Exists(
            CompoundCytotoxicityResult.objects.filter(compound__id=OuterRef("id"))
        ),
        insilico_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _insilico_av=Cast(
                    Max(
                        Case(
                            When(
                                refcompoundbiblio__bibliography__in_silico=True, then=1
                            ),
                            default=0,
                            output_field=IntegerField(),
                        )
                    ),
                    BooleanField(),
                )
            )
            .values("_insilico_av")[:1]
        ),
        tests_av=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(_tests_av=Count("compoundactivityresult", distinct=True))
            .values("_tests_av")[:1]
        ),
    )
    compounds_queryset.update(
        best_activity_ppi_family_name=Subquery(
            CompoundActivityResult.objects.filter(compound_id=OuterRef("id"))
            .filter(activity=OuterRef("best_activity"))
            .annotate(
                _best_activity_ppi_family_name=F(
                    "test_activity_description__ppi__family__name"
                )
            )
            .values("_best_activity_ppi_family_name")[:1]
        ),
    )
    compounds_queryset.update(
        lipinsky_score=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _lipinsky_score=Cast(F("lipinsky_mw"), IntegerField())
                + Cast(F("lipinsky_hba"), IntegerField())
                + Cast(F("lipinsky_hbd"), IntegerField())
                + Cast(F("lipinsky_a_log_p"), IntegerField())
            )
            .values("_lipinsky_score")[:1]
        ),
        veber_hba_hbd=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _veber_hba_hbd=Case(
                    When(hba_hbd__lte=12, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_veber_hba_hbd")[:1]
        ),
        veber=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _veber=Case(
                    When(
                        Q(
                            Q(nb_rotatable_bonds__lte=10)
                            & (Q(hba_hbd__lte=12) | Q(tpsa__lte=140))
                        ),
                        then=True,
                    ),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_veber")[:1]
        ),
    )
    return compounds_queryset.update(
        lipinsky=Subquery(
            compounds_queryset.filter(id=OuterRef("id"))
            .annotate(
                _lipinsky=Case(
                    When(lipinsky_score__gte=3, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("_lipinsky")[:1]
        )
    )


class Job(models.Model):
    task_result = models.OneToOneField(
        TaskResult, on_delete=models.CASCADE, default=None
    )
    std_out = models.TextField(
        null=True, default="", blank=True, help_text="task standard output"
    )
    std_err = models.TextField(
        null=True, default="", blank=True, help_text="task error output"
    )

    def update_output(self, message, output="std_out", with_print=True):
        """
        Save log information in std_out or std_err,
        limit the log information size saved to 100 lines max
        """
        if output == "std_out":
            if with_print:
                print(message, file=sys.stdout)
            split_std_out = self.std_out.split("\n")
            split_std_out = split_std_out + str(message).split("\n")
            self.std_out = "\n".join(split_std_out[-100:])
            self.save()
        elif output == "std_err":
            if with_print:
                print(message, file=sys.stderr)
            split_std_err = self.std_err.split("\n")
            split_std_err = split_std_err + str(message).split("\n")
            self.std_err = "\n".join(split_std_err[-100:])
            self.save()
        else:
            raise Exception("output doesn't exist")


class CompoundJob(models.Model):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)


@receiver(post_save, sender=TaskResult)
def post_save_taskresult(sender, instance, created, *args, **kwargs):
    if created:
        Job.objects.create(task_result=instance)


User = get_user_model()


class ContributorManager(models.Manager):
    """
    Model manager for the `Contributor` class

    retrieves users which have a social account (i.e. an ORCID in iPPI-DB)
    and have more contributed
    """

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(
                contributions_count=Count(
                    "contribution", filter=Q(contribution__validated=True)
                )
            )
            .filter(socialaccount__isnull=False, contributions_count__gt=0)
        )


class Contributor(User):

    objects = ContributorManager()

    class Meta:
        proxy = True

    @cached_property
    def orcid_json(self):
        return get_orcid_user_details(self.get_orcid())

    def get_orcid(self):
        """
        Get contributor ORCID from their allauth social account
        """
        if self.socialaccount_set.count() > 0:
            return self.socialaccount_set.all()[0].uid
        return None

    def get_orcid_url(self):
        """
        Get contributor ORCID fURL from their allauth profile
        """
        if self.socialaccount_set.count() > 0:
            return self.socialaccount_set.all()[0].get_profile_url()
        return None

    def get_contributor_firstname(self):
        """
        Get contributor full name from their ORCID profile
        """
        orcid = self.get_orcid()
        if orcid is not None:
            data = self.orcid_json
            return data["first_name"]

    def get_contributor_lastname(self):
        """
        Get contributor full name from their ORCID profile
        """
        orcid = self.get_orcid()
        if orcid is not None:
            data = self.orcid_json
            return data["last_name"]

    def get_contributor_keywords(self):
        """
        Get contributor keywords from their ORCID profile
        """
        orcid = self.get_orcid()
        if orcid is not None:
            data = self.orcid_json
            return data["keywords"]

    def get_validated_contributions(self):
        """
        Get validated contributions for the user
        """
        return self.contribution_set.filter(validated=True)

    def get_validated_patents_count(self):
        """
        Get number of publications in validated contributions for the user
        """
        return Bibliography.objects.filter(
            source__in=["PT"],
            contribution__validated=True,
            contribution__contributor=self,
        ).count()

    def get_validated_publications_count(self):
        """
        Get number of publications in validated contributions for the user
        """
        return Bibliography.objects.filter(
            source__in=["PM", "DO"],
            contribution__validated=True,
            contribution__contributor=self,
        ).count()

    def get_validated_bibliographies(self):
        """
        Get publications/patents in validated contributions for the user
        """
        return Bibliography.objects.filter(
            contribution__validated=True,
            contribution__contributor=self,
        )

    def get_validated_compounds_count(self):
        """
        Get number of compounds in validated contributions for the user
        """
        return Compound.objects.filter(
            compoundaction__ppi__contribution__validated=True,
            compoundaction__ppi__contribution__contributor=self,
        ).count()

    def get_validated_compoundactivityresults_count(self):
        """
        Get number of compound activity results in validated contributions for the user
        """
        return CompoundActivityResult.objects.filter(
            test_activity_description__biblio__contribution__validated=True,
            test_activity_description__biblio__contribution__contributor=self,
        ).count()

    def get_validated_ppis_count(self):
        """
        Get number of PPIs in validated contributions for the user
        """
        return Ppi.objects.filter(
            contribution__validated=True, contribution__contributor=self
        ).count()

    def get_absolute_url(self):
        return reverse("contributor_card", kwargs={"pk": self.pk})

    def get_last_modification_date(self):
        """
        Return last modification date for the contributor
        based on latest contribution date
        """
        return Contribution.objects.filter(contributor=self, validated=True).aggregate(
            Max("created_at")
        )["created_at__max"]


class Faq(models.Model):
    TYPES = (
        ("compounds", "compounds"),
        ("pie", "pie"),
    )
    question = models.TextField("Question")
    answer = models.TextField("Answer")
    type = models.CharField("type", max_length=10, choices=TYPES, default=TYPES[1][0])
