from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.forms import ValidationError
from django.db.models import Q
from django.dispatch import receiver
from django.db.models.signals import pre_delete, pre_save
from .ippidb import Protein
from ippidb.ws import convert_pdb_ligand_id_to_iupacandsmiles
import os


def content(instance, filename):
    return "content/mrc_files/{}".format(filename)


def contentinteract(instance, filename):
    return "content/interact_files/{}".format(filename)


class PDB(models.Model):
    """
    PDB class, describe a pdb with file content and the code access
    """

    code = models.CharField(max_length=4, unique=True)
    file_content = models.TextField(
        verbose_name="pdb file content", default="no pdb file"
    )

    def is_heterodimer(self):
        """
        return True if pdb is a heterodimer else return False
        """
        chains = self.chain_set.all()
        if chains.count() > 1:
            return True
        else:
            return False

    def no_cavities(self):
        chains = self.chain_set.all()
        cavity_num = 0
        for chain in chains:
            cavity_num += chain.cavity_set.count()
        if cavity_num:
            return False
        else:
            return True

    def __unicode__(self):
        partners = ""
        onprot = ""
        for chain in self.chain_set.all():
            if chain.is_partner():
                partners += " {}".format(chain)
            else:
                onprot += "{}".format(chain)
        for ligand in self.ligand_set.all():
            partners += " {}".format(ligand)
        return "{}{}".format(onprot, partners)

    def __str__(self):
        partners = ""
        onprot = ""
        for chain in self.chain_set.all():
            if chain.is_partner():
                partners += " {}".format(chain)
            else:
                onprot += "{}".format(chain)
        for ligand in self.ligand_set.all():
            partners += " {}".format(ligand)
        return "{}{}".format(onprot, partners)

    class Meta:
        indexes = [models.Index(fields=["code"])]


class Chain(models.Model):
    """
    Chain class, a protein
    """

    pdb_chain_id = models.CharField(max_length=2, blank=False, null=False)
    protein = models.ForeignKey(
        Protein, on_delete=models.CASCADE, blank=True, null=True
    )
    pdb = models.ForeignKey(PDB, on_delete=models.CASCADE, blank=False, null=False)
    file_content = models.TextField(
        verbose_name="pdb file content with only one chain", default="no pdb file"
    )
    mrc_file = models.FileField(upload_to=content, blank=True)
    default_isolevel = models.FloatField(
        default=0.7, verbose_name="Default isolevel value"
    )

    class Meta:
        unique_together = ("pdb", "pdb_chain_id")
        ordering = ["pdb", "pdb_chain_id"]

    def is_partner(self):
        return self.partner_set.count()

    def __unicode__(self):
        partner_message = ""
        if self.is_partner():
            partner_message = "with partner "
        if self.protein is None:
            return "{}{}".format(partner_message, "no protein")
        else:
            return "{}{}".format(partner_message, self.protein.recommended_name_long)

    def __str__(self):
        partner_message = ""
        if self.is_partner():
            partner_message = "with partner "
        if self.protein is None:
            return "{}{}".format(partner_message, "no protein")
        else:
            return "{}{}".format(partner_message, self.protein.recommended_name_long)


class HotSpot(models.Model):
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    type = models.CharField(max_length=50, blank=False, null=False)
    residu_code = models.CharField(max_length=3, blank=False, null=False)
    residu_number = models.PositiveIntegerField(
        "Residu Number", blank=False, null=False
    )

    class Meta:
        unique_together = [["chain", "residu_code", "residu_number", "type"]]

    def __unicode__(self):
        return "{}-{} {}".format(self.residu_code, self.residu_number, self.type)

    def __str__(self):
        return "{}-{} {}".format(self.residu_code, self.residu_number, self.type)


class InteractFile(models.Model):
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    label = models.CharField(max_length=250, blank=False, null=False)
    interact_file = models.FileField(upload_to=contentinteract, blank=True)
    default_isolevel = models.FloatField(
        default=0.7, verbose_name="Default isolevel value"
    )

    class Meta:
        unique_together = [["chain", "label"]]

    def __unicode__(self):
        return "{}".format(self.interact_file)

    def __str__(self):
        return "{}".format(self.interact_file)


class Ligand(models.Model):
    """
    Ligand, class describe a ligand with file content and the code access
    """

    supplementary_id = models.CharField(
        "PDB supplementary Ligand ID", max_length=10, blank=False, null=False
    )
    pdb_ligand_id = models.CharField(
        "PDB Ligand ID", max_length=3, blank=False, null=False
    )
    file_content = models.TextField(
        verbose_name="ligand file content", default="no pdb file"
    )
    pdb = models.ForeignKey(PDB, on_delete=models.CASCADE, blank=False, null=False)
    canonical_smile = models.TextField(
        verbose_name="Canonical Smiles", blank=True, null=True
    )
    iupac_name = models.TextField(verbose_name="IUPAC name", blank=True, null=True)

    class Meta:
        unique_together = (
            "pdb",
            "pdb_ligand_id",
            "supplementary_id",
            "canonical_smile",
            "iupac_name",
        )
        indexes = [
            models.Index(fields=["pdb_ligand_id"]),
            models.Index(fields=["iupac_name"]),
            models.Index(fields=["canonical_smile"]),
        ]

    def __unicode__(self):
        partner_message = "with ligand "
        return "%s%s_%s" % (partner_message, self.pdb_ligand_id, self.supplementary_id)

    def __str__(self):
        partner_message = "with ligand "
        return "%s%s_%s" % (partner_message, self.pdb_ligand_id, self.supplementary_id)

    def save(self, *args, **kwargs):
        super(Ligand, self).save(*args, **kwargs)
        if not self.canonical_smile or not self.iupac_name:
            iupac, smiles = convert_pdb_ligand_id_to_iupacandsmiles(self.pdb_ligand_id)
            self.canonical_smile = smiles
            self.iupac_name = iupac
            self.save()


class Partner(models.Model):
    """
    Partner class, a partner can be a ligand or a chain
    """

    ligand = models.ForeignKey(Ligand, on_delete=models.CASCADE, blank=True, null=True)
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        unique_together = [["ligand", "chain"]]

    def save(self, *args, **kwargs):
        if self.ligand and self.chain:
            raise ValidationError("A partner is only a ligand or a chain not the both")
        super(Partner, self).save(*args, **kwargs)

    @property
    def is_ligand(self):
        if self.ligand:
            return True
        return False

    @property
    def is_chain(self):
        if self.chain:
            return True
        return False


class Cavity(models.Model):
    """
    Cavity descriptor class
    """

    full_name = models.CharField(max_length=100, blank=False, null=False, unique=True)
    mol2 = models.TextField(verbose_name="mol2 file content", default="no mol2 file")
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    cavity_number = models.PositiveIntegerField("Number of the pocket")
    type = models.CharField(
        "Type of pocket", max_length=50, default="orthosteric", blank=False, null=False
    )
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    asphericity = models.FloatField(verbose_name="Asphericity")
    ca = models.FloatField(verbose_name="CA")
    ca100_110 = models.FloatField(verbose_name="CA100-110")
    ca110_120 = models.FloatField(verbose_name="CA100-120")
    ca120 = models.FloatField(verbose_name="CA120")
    ca40 = models.FloatField(verbose_name="CA40")
    ca40_50 = models.FloatField(verbose_name="CA40-50")
    ca50_60 = models.FloatField(verbose_name="CA50-60")
    ca60_70 = models.FloatField(verbose_name="CA60-70")
    ca70_80 = models.FloatField(verbose_name="CA70-80")
    ca80_90 = models.FloatField(verbose_name="CA80-90")
    ca90_100 = models.FloatField(verbose_name="CA90-100")
    cz = models.FloatField(verbose_name="CZ")
    cz100_110 = models.FloatField(verbose_name="CZ100-110")
    cz110_120 = models.FloatField(verbose_name="CZ110-120")
    cz120 = models.FloatField(verbose_name="CZ120")
    cz40 = models.FloatField(verbose_name="CZ40")
    cz40_50 = models.FloatField(verbose_name="CZ40-50")
    cz50_60 = models.FloatField(verbose_name="CZ50-60")
    cz60_70 = models.FloatField(verbose_name="CZ60-70")
    cz70_80 = models.FloatField(verbose_name="CZ70-80")
    cz80_90 = models.FloatField(verbose_name="CZ80-90")
    cz90_100 = models.FloatField(verbose_name="CZ90-100")
    du = models.FloatField(verbose_name="DU")
    du100_110 = models.FloatField(verbose_name="DU100-110")
    du110_120 = models.FloatField(verbose_name="DU110-120")
    du120 = models.FloatField(verbose_name="DU120")
    du40 = models.FloatField(verbose_name="DU40")
    du40_50 = models.FloatField(verbose_name="DU40-50")
    du50_60 = models.FloatField(verbose_name="DU50-60")
    du60_70 = models.FloatField(verbose_name="DU60-70")
    du70_80 = models.FloatField(verbose_name="DU70-80")
    du80_90 = models.FloatField(verbose_name="DU80-90")
    du90_100 = models.FloatField(verbose_name="DU90-100")
    eccentricity = models.FloatField(verbose_name="Eccentricity")
    inertialshapefactor = models.FloatField(verbose_name="InertialShapeFactor")
    n = models.FloatField(verbose_name="N")
    n100_110 = models.FloatField(verbose_name="N100-110")
    n110_120 = models.FloatField(verbose_name="N110-120")
    n120 = models.FloatField(verbose_name="N120")
    n40 = models.FloatField(verbose_name="N40")
    n40_50 = models.FloatField(verbose_name="N40-50")
    n50_60 = models.FloatField(verbose_name="N50-60")
    n60_70 = models.FloatField(verbose_name="N60-70")
    n70_80 = models.FloatField(verbose_name="N70-80")
    n80_90 = models.FloatField(verbose_name="N80-90")
    n90_100 = models.FloatField(verbose_name="N90-100")
    npr1 = models.FloatField(verbose_name="NPR1")
    npr2 = models.FloatField(verbose_name="NPR2")
    nz = models.FloatField(verbose_name="NZ")
    nz100_110 = models.FloatField(verbose_name="NZ100-110")
    nz110_120 = models.FloatField(verbose_name="NZ110-120")
    nz120 = models.FloatField(verbose_name="NZ120")
    nz40 = models.FloatField(verbose_name="NZ40")
    nz40_50 = models.FloatField(verbose_name="NZ40-50")
    nz50_60 = models.FloatField(verbose_name="NZ50-60")
    nz60_70 = models.FloatField(verbose_name="NZ60-70")
    nz70_80 = models.FloatField(verbose_name="NZ70-80")
    nz80_90 = models.FloatField(verbose_name="NZ80-90")
    nz90_100 = models.FloatField(verbose_name="NZ90-100")
    o = models.FloatField(verbose_name="O")
    o100_110 = models.FloatField(verbose_name="O100-110")
    o110_120 = models.FloatField(verbose_name="O110-120")
    o120 = models.FloatField(verbose_name="O120")
    o40 = models.FloatField(verbose_name="O40")
    o40_50 = models.FloatField(verbose_name="O40-50")
    o50_60 = models.FloatField(verbose_name="O50-60")
    o60_70 = models.FloatField(verbose_name="O60-70")
    o70_80 = models.FloatField(verbose_name="O70-80")
    o80_90 = models.FloatField(verbose_name="O80-90")
    o90_100 = models.FloatField(verbose_name="O90-100")
    od1 = models.FloatField(verbose_name="OD1")
    od1100_110 = models.FloatField(verbose_name="OD1100-110")
    od1110_120 = models.FloatField(verbose_name="OD1110-120")
    od1120 = models.FloatField(verbose_name="OD1120")
    od140 = models.FloatField(verbose_name="OD140")
    od140_50 = models.FloatField(verbose_name="OD140-50")
    od150_60 = models.FloatField(verbose_name="OD150-60")
    od160_70 = models.FloatField(verbose_name="OD160-70")
    od170_80 = models.FloatField(verbose_name="OD170-80")
    od180_90 = models.FloatField(verbose_name="OD180-90")
    od190_100 = models.FloatField(verbose_name="OD190-100")
    og = models.FloatField(verbose_name="OG")
    og100_110 = models.FloatField(verbose_name="OG100-110")
    og110_120 = models.FloatField(verbose_name="OG110-120")
    og120 = models.FloatField(verbose_name="OG120")
    og40 = models.FloatField(verbose_name="OG40")
    og40_50 = models.FloatField(verbose_name="OG40-50")
    og50_60 = models.FloatField(verbose_name="OG50-60")
    og60_70 = models.FloatField(verbose_name="OG60-70")
    og70_80 = models.FloatField(verbose_name="OG70-80")
    og80_90 = models.FloatField(verbose_name="OG80-90")
    og90_100 = models.FloatField(verbose_name="OG90-100")
    pmi1 = models.FloatField(verbose_name="PMI1")
    pmi2 = models.FloatField(verbose_name="PMI2")
    pmi3 = models.FloatField(verbose_name="PMI3")
    rgyr = models.FloatField(verbose_name="RGYR")
    spherocityindex = models.FloatField(verbose_name="SpherocityIndex")
    t100_110 = models.FloatField(verbose_name="T100-110")
    t110_120 = models.FloatField(verbose_name="T110-120")
    t120 = models.FloatField(verbose_name="T120")
    t40 = models.FloatField(verbose_name="T40")
    t40_50 = models.FloatField(verbose_name="T40-50")
    t50_60 = models.FloatField(verbose_name="T50-60")
    t60_70 = models.FloatField(verbose_name="T60-70")
    t70_80 = models.FloatField(verbose_name="T70-80")
    t80_90 = models.FloatField(verbose_name="T80-90")
    t90_100 = models.FloatField(verbose_name="T90-100")
    volume = models.FloatField(verbose_name="Volume")

    class Meta:
        unique_together = ("full_name", "chain", "cavity_number", "partner")
        verbose_name_plural = "Cavities"
        ordering = ["chain", "cavity_number"]

    @property
    def near_cavities(self):
        """
        return a queryset of distance with the 5 more little distance
        """

        queryset_pl = Distance.objects.filter(
            (Q(cavity1=self) & Q(cavity2__partner__ligand__isnull=False))
            | (Q(cavity2=self) & Q(cavity1__partner__ligand__isnull=False))
        ).order_by("distance")[:5]
        queryset_hd = Distance.objects.filter(
            (Q(cavity1=self) & Q(cavity2__partner__chain__isnull=False))
            | (Q(cavity2=self) & Q(cavity1__partner__chain__isnull=False))
        ).order_by("distance")[:5]
        queryset = queryset_pl | queryset_hd
        return queryset.order_by("distance")

    def save(self, *args, **kwargs):
        if not self.t120:
            self.t120 = 100 - (
                self.t40
                + self.t40_50
                + self.t50_60
                + self.t60_70
                + self.t70_80
                + self.t80_90
                + self.t90_100
                + self.t100_110
                + self.t110_120
            )
        super(Cavity, self).save(*args, **kwargs)


class LinkedPDB(models.Model):
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    ligand_pl = models.ForeignKey(Ligand, on_delete=models.CASCADE)
    translation = ArrayField(models.FloatField(), size=3)
    rotation = ArrayField(ArrayField(models.FloatField(), size=3), size=3)

    class Meta:
        unique_together = ("chain", "ligand_pl")

    def __unicode__(self):
        return "PDB %s chain %s ligand %s %s-%s" % (
            self.chain.pdb.code,
            self.chain.pdb_chain_id,
            self.ligand_pl.pdb.code,
            self.ligand_pl.pdb_ligand_id,
            self.ligand_pl.supplementary_id,
        )

    def __str__(self):
        return "PDB %s chain %s ligand %s %s-%s" % (
            self.chain.pdb.code,
            self.chain.pdb_chain_id,
            self.ligand_pl.pdb.code,
            self.ligand_pl.pdb_ligand_id,
            self.ligand_pl.supplementary_id,
        )


class Distance(models.Model):
    """
    Distance between two cavity
    """

    cavity1 = models.ForeignKey(
        Cavity, on_delete=models.CASCADE, related_name="first_cavity"
    )
    cavity2 = models.ForeignKey(
        Cavity, on_delete=models.CASCADE, related_name="second_cavity"
    )
    distance = models.DecimalField(
        verbose_name="Distance", max_digits=11, decimal_places=8
    )

    class Meta:
        unique_together = ("cavity1", "cavity2")


class MetaInformation(models.Model):
    """
    Average and Standard deviation from the Distances Matrix
    """

    average = models.DecimalField(
        verbose_name="Average",
        max_digits=11,
        decimal_places=8,
        help_text="partial matrix mean value",
    )
    std = models.DecimalField(
        verbose_name="Standard Deviation",
        max_digits=11,
        decimal_places=8,
        help_text="partial matrix standard deviation",
    )
    maximum = models.DecimalField(
        verbose_name="Maximum",
        max_digits=11,
        decimal_places=8,
        help_text="partial matrix maximum value",
    )
    minimum = models.DecimalField(
        verbose_name="Minimum",
        max_digits=11,
        decimal_places=8,
        help_text="partial matrix minimum value",
    )
    normalize_factor = models.DecimalField(
        blank=True,
        null=True,
        default=4.58599730,
        verbose_name="Normalize Factor",
        help_text="complet matrix standard deviation",
        max_digits=11,
        decimal_places=8,
    )

    class Meta:
        verbose_name_plural = "MetaInformation"


@receiver(pre_delete, sender=Partner)
@receiver(pre_delete, sender=Chain)
@receiver(pre_delete, sender=PDB)
def mrcdelete(sender, instance, **kwargs):
    if isinstance(instance, PDB):
        chains = instance.chain_set.all()
    elif isinstance(instance, Chain):
        chains = [instance]
    elif isinstance(instance, Partner):
        if instance.chain:
            chains = [instance.chain]
        else:
            chains = []

    for chain in chains:
        if chain.mrc_file.storage.exists(chain.mrc_file.name):
            chain.mrc_file.delete()


@receiver(pre_delete, sender=Partner)
@receiver(pre_delete, sender=Chain)
@receiver(pre_delete, sender=PDB)
def interactdelete(sender, instance, **kwargs):
    if isinstance(instance, PDB):
        chains = instance.chain_set.all()
    elif isinstance(instance, Chain):
        chains = [instance]
    elif isinstance(instance, Partner):
        if instance.chain:
            chains = [instance.chain]
        else:
            chains = []

    for chain in chains:
        for interactfile in chain.interactfile_set.all():
            if interactfile.interact_file.storage.exists(
                interactfile.interact_file.name
            ):
                interactfile.interact_file.delete()


@receiver(pre_save, sender=Chain)
def updatemrcfile(sender, instance, **kwargs):
    try:
        old_chain = sender.objects.get(id=instance.id)
    except sender.DoesNotExist:
        old_chain = None
    if instance.mrc_file:
        if old_chain:
            if old_chain.mrc_file:
                path_mrc_file = old_chain.mrc_file.path
                if os.path.exists(path_mrc_file):
                    os.remove(path_mrc_file)


@receiver(pre_save, sender=InteractFile)
def updateinteractfile(sender, instance, **kwargs):
    try:
        old_interact = sender.objects.get(id=instance.id)
    except sender.DoesNotExist:
        old_interact = None
    if instance.interact_file:
        if old_interact:
            if old_interact.interact_file:
                path_interact_file = old_interact.interact_file.path
                if os.path.exists(path_interact_file):
                    os.remove(path_interact_file)
