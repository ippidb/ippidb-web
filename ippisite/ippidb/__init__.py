"""
This is the main ippidb module

.. autosummary::
    :toctree: _autosummary

    admin
    apps
    forms
    gx
    models
    tasks
    tests
    urls
    utils
    views
    ws
    management
    migrations
    templatetags
"""
