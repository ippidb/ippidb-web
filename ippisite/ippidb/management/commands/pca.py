from django.core.management import BaseCommand

from ippidb.tasks import generate_pca_plot


class Command(BaseCommand):

    help = "Generate the data for the compound PCA biplot"

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS("Generating the PCA biplot..."))
        generate_pca_plot()
        self.stdout.write(self.style.SUCCESS("Successfully generated PCA biplot data"))
