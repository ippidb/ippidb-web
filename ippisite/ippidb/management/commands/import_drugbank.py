import pandas as pd

from django.core.management import BaseCommand, CommandError

from ippidb.models import DrugBankCompound, Compound


class Command(BaseCommand):

    help = "Import data from a Drugbank file TSV file"

    def add_arguments(self, parser):
        parser.add_argument("path", type=str, help="Drugbank TSV file path")

    def handle(self, *args, **options):
        df = pd.read_csv(options["path"], sep=",")
        DrugBankCompound.objects.all().delete()
        self.stdout.write(
            self.style.SUCCESS("Successfully flushed DrugBank Compound table")
        )
        for index, row in df.iterrows():
            # insert all drugbank compounds in the DB
            try:
                dbc = DrugBankCompound()
                dbc.id = row.loc["DRUGBANK_ID"]
                dbc.common_name = row.loc["GENERIC_NAME"]
                dbc.canonical_smiles = row.loc["SMILES"]
                dbc.save(autofill=True)
            except Exception:
                self.stdout.write(
                    self.style.ERROR(
                        "Failed inserting {}".format(row.loc["DRUGBANK_ID"])
                    )
                )
                import traceback

                self.stderr.write(traceback.format_exc())
                raise CommandError("Failed inserting {}".format(row.loc["DRUGBANK_ID"]))
            else:
                self.stdout.write(
                    self.style.SUCCESS(
                        "Successfully inserted {}".format(row.loc["DRUGBANK_ID"])
                    )
                )
        for c in Compound.objects.validated():
            # for each iPPI-DB compound compute the most similar drugbank compounds
            c.set_drugbank_link()
            c.compute_drugbank_compound_similarity()
            c.save()
            self.stdout.write(
                self.style.SUCCESS(
                    "Successfully computed 15 most similar compounds for {}".format(
                        c.id
                    )
                )
            )
