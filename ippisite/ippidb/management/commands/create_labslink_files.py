# coding: utf-8
from django.db.models import CharField, Value as V
from django.db.models.functions import Concat
from django.db.models import F, Q, Case, When

from ippidb.models import RefCompoundBiblio
from biodblinks.lablinks.command import LabsLinkCommand


class Command(LabsLinkCommand):

    queryset = (
        RefCompoundBiblio.objects.exclude(Q(bibliography__source="PT"))
        .annotate(
            resource_title=Concat(
                V("iPPI-DB compound #"), F("compound__id"), output_field=CharField()
            )
        )
        .annotate(
            resource_url=Concat(
                V("https://ippidb.pasteur.fr/compounds/"),
                F("compound__id"),
                output_field=CharField(),
            )
        )
        .annotate(
            doi=Case(
                When(Q(bibliography__source="DO"), then=F("bibliography__id_source")),
                default=None,
                output_field=CharField(),
            )
        )
        .annotate(
            record_id=Case(
                When(Q(bibliography__source="PM"), then=F("bibliography__id_source")),
                default=None,
                output_field=CharField(),
            )
        )
        .annotate(
            record_source=Case(
                When(Q(bibliography__source="PM"), then=V("MED")),
                default=None,
                output_field=CharField(),
            )
        )
    )
