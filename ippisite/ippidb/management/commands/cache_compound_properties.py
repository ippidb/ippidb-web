from django.core.management import BaseCommand

from ippidb.models import update_compound_cached_properties


class Command(BaseCommand):

    help = "Cache compound properties in the main 'Compound' table"

    def handle(self, *args, **options):
        self.stdout.write(
            self.style.SUCCESS("Generating the compound properties cache...")
        )
        n = update_compound_cached_properties()
        self.stdout.write(
            self.style.SUCCESS(
                "Successfully generated compound properties cache for %s compounds" % n
            )
        )
