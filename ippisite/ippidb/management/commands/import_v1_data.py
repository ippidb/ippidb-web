import re

from django_diu.import_command import (
    ImportCommand,
    ImportTask,
    ListImportTask,
    MysqlImportTask,
    MyConverter,
)
import mysql.connector

from ippidb.models import (
    Bibliography,
    Protein,
    Taxonomy,
    MolecularFunction,
    Domain,
    ProteinDomainBoundComplex,
    ProteinDomainPartnerComplex,
    Symmetry,
    Ppi,
    PpiComplex,
    Disease,
    Compound,
    TestActivityDescription,
    CellLine,
    RefCompoundBiblio,
    CompoundAction,
    TestCytotoxDescription,
    TestPKDescription,
    CompoundActivityResult,
    CompoundCytotoxicityResult,
    CompoundPKResult,
    PpiFamily,
)


class SymmetriesImportTask(ListImportTask):

    description = "Symmetries import"

    option = "symmetries"

    target_classes = [Symmetry]

    main_class = Symmetry

    DATA = [
        ["AS", "asymmetric"],
        ["C2", "C2 symmetry"],
        ["D2", "D2 symmetry"],
        ["C3", "C3 symmetry"],
        ["D3", "D3 symmetry"],
        ["C4", "C4 symmetry"],
        ["D4", "D4 symmetry"],
        ["C5", "C5 symmetry"],
        ["D5", "D5 symmetry"],
    ]

    def migrate_row(self, row):
        symmetry = Symmetry()
        symmetry.code = row[0]
        symmetry.description = row[1]
        symmetry.save()
        return symmetry


class IppiDBMySQLImportTask(MysqlImportTask):
    def __init__(self, command, **kwargs):
        super().__init__(command, **kwargs)
        self.conn = command.ippidb_source_conn


class ProteinsImportTask(IppiDBMySQLImportTask):

    description = "Proteins import"

    outer_sql = "SELECT * FROM protein"

    option = "prot"

    target_classes = [Protein]

    main_class = Protein

    def migrate_row(self, row):
        p = Protein()
        p.id = row[0]
        p.uniprot_id = row[1]
        p.save(autofill=True)
        return p


class BibliographyImportTask(IppiDBMySQLImportTask):

    description = "Bibliography references import"

    outer_sql = "SELECT * FROM biblio"

    option = "bib"

    target_classes = [Bibliography]

    main_class = Bibliography

    def migrate_row(self, row):
        b = Bibliography()
        b.id = row[0]
        if row[1] == "article":
            b.source = "PM"
        else:
            b.source = "PT"
        b.id_source = row[2]
        b.cytotox = row[6] == "Y"
        b.in_silico = row[7] == "Y"
        b.in_vitro = row[8] == "Y"
        b.in_vivo = row[9] == "Y"
        b.in_cellulo = row[10] == "Y"
        b.pharmacokinetic = row[11] == "Y"
        b.xray = row[12] == "Y"
        b.save(autofill=True)
        return b


class DomainImportTask(IppiDBMySQLImportTask):

    description = "Domains import"

    outer_sql = "SELECT * FROM domain"

    option = "dom"

    target_classes = [Domain, Taxonomy, MolecularFunction]

    main_class = Domain

    def migrate_row(self, row):
        d = Domain()
        d.id = row[0]
        d.pfam_acc = row[2]
        d.domain_family = row[4]
        d.save(autofill=True)
        return d


class CompoundImportTask(IppiDBMySQLImportTask):

    description = "Compound import"

    outer_sql = "SELECT * FROM compound"

    option = "com"

    target_classes = [Compound]

    main_class = Compound

    def migrate_row(self, row):
        compound = Compound()
        compound.id = row[0]
        compound.canonical_smile = row[1]
        compound.is_macrocycle = row[4] == "Y"
        compound.aromatic_ratio = row[5]
        compound.balaban_index = row[6]
        compound.fsp3 = row[7]  # Csp3Ratio
        compound.gc_molar_refractivity = row[10]  # GCMolarRefractivity
        compound.log_d = row[13]  # LogD
        compound.a_log_p = row[14]  # ALogP
        compound.mean_atom_vol_vdw = row[15]  # MeanAtomVolVdW
        compound.molecular_weight = row[16]  # MolecularWeight
        compound.nb_acceptor_h = row[17]  # NbAcceptorH
        compound.nb_aliphatic_amines = row[18]  # NbAliphaticsAmines
        compound.nb_aromatic_bonds = row[19]  # NbAromaticBonds
        compound.nb_aromatic_ether = row[20]  # NbAromaticsEther
        compound.nb_aromatic_sssr = row[21]  # NbAromaticsSSSR
        compound.nb_atom = row[22]  # NbAtom
        compound.nb_atom_non_h = row[23]  # NbAtomNonH
        compound.nb_benzene_like_rings = row[24]  # NbBenzLikeRings
        compound.nb_bonds = row[25]  # NbBonds
        compound.nb_bonds_non_h = row[26]  # NbBondsNonH
        compound.nb_br = row[27]  # NbBr
        compound.nb_c = row[28]  # NbC
        compound.nb_chiral_centers = row[29]  # NbChiralCenters
        compound.nb_circuits = row[30]  # NbCircuits
        compound.nb_cl = row[31]  # NbCl
        compound.nb_csp2 = row[32]  # NbCsp2
        compound.nb_csp3 = row[33]  # NbCsp3
        compound.nb_donor_h = row[34]  # NbDonorH
        compound.nb_double_bonds = row[35]  # NbDoubleBonds
        compound.nb_f = row[36]  # NbF
        compound.nb_i = row[37]  # NbI
        compound.nb_multiple_bonds = row[38]  # NbMultBonds
        compound.nb_n = row[39]  # NbN
        compound.nb_o = row[40]  # NbO
        compound.nb_rings = row[41]  # NbRings
        compound.nb_rotatable_bonds = row[42]  # NbRotatableBonds
        compound.randic_index = row[44]  # RandicIndex
        compound.rdf070m = row[45]  # RDF070m
        compound.rotatable_bond_fraction = row[46]  # RotatableBondFraction
        compound.sum_atom_polar = row[47]  # SumAtomPolar
        compound.sum_atom_vol_vdw = row[48]  # SumAtomVolVdW
        compound.tpsa = row[51]  # TPSA
        compound.ui = row[52]  # Ui
        compound.wiener_index = row[54]  # WienerIndex
        if row[55] != "N":
            compound.common_name = row[55]  # CmpdNameSh
        compound.pubchem_id = row[56]  # IdPubchem
        if row[57] != "N":
            compound.chemspider_id = row[57]  # IdPubchem
        compound.chembl_id = row[58]
        compound.iupac_name = row[59]
        compound.save(autofill=True)
        return compound


class RefCompoundBiblioImportTask(IppiDBMySQLImportTask):

    description = "RefCompoundBiblio import"

    outer_sql = """
        select r.CmpdNameInBiblio, c.CanonicalSmile, b.IDSource
        from refCmpdBiblio as r inner join compound as c
        on r.IDCompound=c.IDCompound
        inner join biblio as b on r.IDBiblio=b.IDBiblio;"""

    option = "rcb"

    target_classes = [RefCompoundBiblio]

    main_class = RefCompoundBiblio

    depends_on = [BibliographyImportTask, CompoundImportTask]

    def migrate_row(self, row):
        c = Compound.objects.get(canonical_smile=row[1])
        b = Bibliography.objects.get(id_source=row[2])
        r = RefCompoundBiblio()
        r.compound_id = c.id
        r.bibliography_id = b.id
        r.compound_name = "-".join(re.split("[-.]", row[0])[1:])
        # mysql format for this field is [PMID/WIPOID]-name: we remove the first part
        # sometimes there is a . instead of the dash
        # sometimes we have more than one -
        r.save()
        return r


class PpiImportTask(IppiDBMySQLImportTask):

    description = "PPIs import"

    outer_sql = """
        select distinct protein.NumUniprot, domain.PfamNumAccession, complexe.NbCopy,
            cmpdAction.IDComplexeBound, bindingSiteEvidence.CodePDB,
            'part1', ppi.IDPPI, disease.Disease, complexe.IDComplexe, ppi.Family
            from bindingSite inner join ppi
            on (bindingSite.IDBindingSite=ppi.IDBindingSite1)
            inner join complexe on (ppi.IDComplexe1=complexe.IDComplexe)
            left outer join cmpdAction
            on (complexe.IDComplexe=cmpdAction.IDComplexeBound)
            inner join protein on (bindingSite.IDProtein=protein.IDProtein)
            inner join domain on (bindingSite.IDDomain=domain.IDDomain)
            inner join disease on (disease.IDPPI=ppi.IDPPI)
            left outer join bindingSiteEvidence on (ppi.IDPPI=bindingSiteEvidence.IDPPI)
        union
        select distinct protein.NumUniprot, domain.PfamNumAccession  , complexe.NbCopy,
            cmpdAction.IDComplexeBound, null,
            'part2', ppi.IDPPI, disease.Disease, complexe.IDComplexe, ppi.Family
            from bindingSite inner join ppi
            on (bindingSite.IDBindingSite=ppi.IDBindingSite2)
            inner join complexe on (ppi.IDComplexe2=complexe.IDComplexe)
            left outer join cmpdAction
            on (complexe.IDComplexe=cmpdAction.IDComplexeBound)
            inner join protein on (bindingSite.IDProtein=protein.IDProtein)
            inner join domain on (bindingSite.IDDomain=domain.IDDomain)
            inner join disease on (disease.IDPPI=ppi.IDPPI)
    """

    option = "ppi"

    target_classes = [
        ProteinDomainBoundComplex,
        ProteinDomainPartnerComplex,
        Disease,
        Ppi,
        PpiFamily,
        PpiComplex,
        CompoundAction,
    ]

    main_class = Ppi

    depends_on = [ProteinsImportTask, DomainImportTask, CompoundImportTask]

    def migrate_row(self, row):
        # create or retrieve Ppi object
        if row[5] == "part1":
            ppi = Ppi()
            ppi.id = row[6]
            disease, created = Disease.objects.get_or_create(name=row[7])
            ppi_family, created = PpiFamily.objects.get_or_create(name=row[9].strip())
            ppi.family = ppi_family
            ppi.pdb_id = row[4]
            ppi.pockets_nb = 1
            ppi.symmetry = Symmetry.objects.get(code="AS")
            ppi.save()
            ppi.diseases.add(disease)
            ppi.save()
        else:
            ppi = Ppi.objects.get(id=row[6])
        # create a complex
        if row[3] is None:
            c = ProteinDomainPartnerComplex()
        else:
            c = ProteinDomainBoundComplex()
        c.id = row[8]
        protein = Protein.objects.get(uniprot_id=row[0])
        c.protein = protein
        domain = Domain.objects.get(pfam_acc=row[1])
        c.domain = domain
        c.ppc_copy_nb = row[2]
        if isinstance(c, ProteinDomainBoundComplex):
            c.ppp_copy_nb_per_p = 1
        c.save()
        # create the PpiComplex object
        ppi_complex = PpiComplex()
        ppi_complex.ppi = ppi
        ppi_complex.complex = c
        ppi_complex.cc_nb = 1
        ppi_complex.save()
        return ppi

    def post_process(self):
        # add names to PPIs automatically once the PPI complexes which the names
        # depend on have been inserted
        for ppi in Ppi.objects.all():
            ppi.save(autofill=True)

    def check_final_count(self):
        self.source_count = int(self.source_count / 2)
        super().check_final_count()


class TestActivityDescriptionImportTask(IppiDBMySQLImportTask):

    description = "TestActivityDescription import"

    outer_sql = """
        SELECT * FROM testActivityDescription
    """

    option = "tad"

    target_classes = [TestActivityDescription]

    main_class = TestActivityDescription

    depends_on = [BibliographyImportTask, PpiImportTask]

    def migrate_row(self, row):
        tad = TestActivityDescription()
        tad.id = row[0]
        cursor_biblio = self.get_cursor()
        cursor_biblio.execute(
            """select IDSource from biblio where IDBiblio={}""".format(row[2])
        )
        biblio_row = cursor_biblio.fetchone()
        biblio = Bibliography.objects.get(id_source=biblio_row[0])
        tad.biblio = biblio
        tad.protein_domain_bound_complex = ProteinDomainBoundComplex.objects.get(
            id=row[1]
        )
        tad.ppi = Ppi.objects.get(id=row[3])
        tad.test_name = row[4]
        tad.is_primary = row[5] == "Y"
        tad.test_type = row[7].upper()
        tad.test_modulation_type = row[8][0]
        tad.nb_active_compounds = row[9]
        if row[16] is not None:
            tad.cell_line, created = CellLine.objects.get_or_create(name=row[16])
        tad.save()
        return tad


class CompoundActivityResultImportTask(IppiDBMySQLImportTask):

    description = "CompoundActivityResult import"

    outer_sql = """
        select c.CanonicalSmile, r.IDTestActivity, r.ActivityType,
        r.Activity, r.PourcentInhib from cmpdActiveResult as r
        inner join compound as c on r.IDCompound = c.IDCompound;
    """

    option = "car"

    target_classes = [CompoundActivityResult]

    main_class = CompoundActivityResult

    depends_on = [TestActivityDescriptionImportTask, CompoundImportTask]

    def migrate_row(self, row):
        car = CompoundActivityResult()
        car.test_activity_description = TestActivityDescription.objects.get(id=row[1])
        car.compound = Compound.objects.get(canonical_smile=row[0])
        car.activity_type = row[2]
        car.activity = row[3]
        car.inhibition_percentage = row[4]
        car.modulation_type = "I"  # because previous DB is only about inhibitors
        car.save()
        return car


class CompoundActionImportTask(ImportTask):

    description = "Compound Actions creation from the CompoundActionResults"

    option = "cac"

    target_classes = [CompoundAction]

    main_class = CompoundAction

    depends_on = [CompoundActivityResultImportTask]

    def open_data_source(self):
        self.rows = CompoundActivityResult.objects.all()

    def migrate_row(self, row):
        ca = CompoundAction()
        ca.compound = row.compound
        ca.ppi = row.test_activity_description.ppi
        ca.activation_mode = "U"
        ca.nb_copy_compounds = 1
        ca.save()
        return ca


class TestCytotoxDescriptionImportTask(IppiDBMySQLImportTask):

    description = "TestCytotoxDescription import"

    outer_sql = """
        SELECT * FROM testCytotoxDescription
    """

    option = "tcd"

    target_classes = [TestCytotoxDescription]

    main_class = TestCytotoxDescription

    depends_on = [BibliographyImportTask, CompoundImportTask]

    def migrate_row(self, row):
        tcd = TestCytotoxDescription()
        tcd.id = row[0]
        cursor_biblio = self.get_cursor()
        cursor_biblio.execute(
            """select IDSource from biblio where IDBiblio={}""".format(row[1])
        )
        biblio_row = cursor_biblio.fetchone()
        biblio = Bibliography.objects.get(id_source=biblio_row[0])
        tcd.biblio = biblio
        tcd.test_name = row[2]
        tcd.compound_concentration = row[4]
        if row[3] is not None:
            tcd.cell_line, created = CellLine.objects.get_or_create(name=row[3])
        tcd.save()
        return tcd


class CompoundCytotoxicityResultImportTask(IppiDBMySQLImportTask):

    description = "CompoundCytotoxicityResult import"

    outer_sql = """
        select c.CanonicalSmile, r.IDTestCytotox, r.IDTestCytotox
        from cmpdCytotoxResult as r inner join compound as c
        on r.IDCompound = c.IDCompound;
    """

    option = "ccr"

    target_classes = [CompoundCytotoxicityResult]

    main_class = CompoundCytotoxicityResult

    depends_on = [CompoundImportTask, TestCytotoxDescriptionImportTask]

    def migrate_row(self, row):
        ccr = CompoundCytotoxicityResult()
        ccr.test_cytotoxicity_description = TestCytotoxDescription.objects.get(
            id=row[1]
        )
        ccr.compound = Compound.objects.get(canonical_smile=row[0])
        ccr.toxicity = row[2] == "Y"
        ccr.save()
        return ccr


class TestPKDescriptionImportTask(IppiDBMySQLImportTask):

    description = "TestPKDescription import"

    outer_sql = """
        SELECT * FROM testPKDescription
    """

    option = "tpd"

    target_classes = [TestPKDescription]

    main_class = TestPKDescription

    depends_on = [BibliographyImportTask, CompoundImportTask]

    def migrate_row(self, row):
        tpd = TestPKDescription()
        tpd.id = row[0]
        cursor_biblio = self.get_cursor()
        cursor_biblio.execute(
            """select IDSource from biblio where IDBiblio={}""".format(row[1])
        )
        biblio_row = cursor_biblio.fetchone()
        biblio = Bibliography.objects.get(id_source=biblio_row[0])
        tpd.biblio = biblio
        tpd.test_name = row[2]
        # tcd.compound_concentration = row[4]
        try:
            taxonomy = Taxonomy.objects.get(taxonomy_id=10090)
        except Taxonomy.DoesNotExist:
            taxonomy = Taxonomy()
            taxonomy.taxonomy_id = 10090
            # dirty hack: all organisms in this table are "mice",
            # hence assuming Mus musculus
            taxonomy.save(autofill=True)
        tpd.organism = taxonomy
        tpd.administration_mode = row[4]
        tpd.concentration = row[5]
        tpd.dose = row[6]
        tpd.dose_interval = row[7]
        tpd.save()
        return tpd


class CompoundPKResultImportTask(IppiDBMySQLImportTask):

    description = "CompoundPKResult import"

    outer_sql = """
        select c.CanonicalSmile, r.IDTestPK, r.Tolerated, r.AUC, r.Clearance,
        r.Cmax, r.OralBioavailability, r.Tdemi, r.Tmax, r.VolDistribution
        from cmpdPKResult as r inner join compound as c
        on r.IDCompound = c.IDCompound;
    """

    option = "cpr"

    target_classes = [CompoundPKResult]

    main_class = CompoundPKResult

    depends_on = [CompoundImportTask, TestPKDescriptionImportTask]

    def migrate_row(self, row):
        cpr = CompoundPKResult()
        cpr.test_pk_description = TestPKDescription.objects.get(id=row[1])
        cpr.compound = Compound.objects.get(canonical_smile=row[0])
        cpr.tolerated = row[2] == "Y"
        cpr.auc = row[3]
        cpr.clearance = row[4]
        cpr.c_max = row[5]
        cpr.oral_bioavailability = row[6]
        cpr.t_demi = row[7]
        cpr.t_max = row[8]
        cpr.voldistribution = row[9]
        cpr.save()
        return cpr


class Command(ImportCommand):
    help = "Import iPPI-DB data from the MySQL database"

    task_classes = [
        ProteinsImportTask,
        BibliographyImportTask,
        DomainImportTask,
        CompoundImportTask,
        SymmetriesImportTask,
        PpiImportTask,
        RefCompoundBiblioImportTask,
        CompoundActionImportTask,
        #                    AdditionalCasImportTask,
        TestActivityDescriptionImportTask,
        CompoundActivityResultImportTask,
        TestCytotoxDescriptionImportTask,
        CompoundCytotoxicityResultImportTask,
        TestPKDescriptionImportTask,
        CompoundPKResultImportTask,
    ]

    def handle(self, *args, **options):
        self.ippidb_source_conn = mysql.connector.connect(
            converter_class=MyConverter,
            host="localhost",
            user="root",
            password="ippidb",
            database="ippidb",
        )
        super().handle(*args, **options)
