from django.core.management.base import OutputWrapper


class TaskOutWrapper(OutputWrapper):
    """
    OutWrapper with celery task object, allow to update
    std_out and std_err in django database Job
    """

    def __init__(
        self, out, style_func=None, ending="\n", task=None, std_out=None, std_err=None
    ):
        super(TaskOutWrapper, self).__init__(out, ending=ending)
        self.task = task
        self.style_func = style_func
        self.std_out = std_out
        self.std_err = std_err

    def write(self, msg, style_func=None, ending=None):
        super(TaskOutWrapper, self).write(msg, style_func=style_func, ending=ending)
        if self.task:
            if self.std_out:
                self.task.write(std_out=msg, with_print=False)
            else:
                self.task.write(std_err=msg, with_print=False)
