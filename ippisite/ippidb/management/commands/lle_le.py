from django.core.management import BaseCommand

from ippidb.tasks import generate_le_lle_plot


class Command(BaseCommand):

    help = "Generate the data for the compound LE vs LLE biplot"

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS("Generating the LE vs. LLE biplot..."))
        generate_le_lle_plot()
        self.stdout.write(
            self.style.SUCCESS("Successfully generated LE-LLE biplot data")
        )
