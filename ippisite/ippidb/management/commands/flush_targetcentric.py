"""
Command to import cavity descriptors
"""

import sys
from celery import states
from django.core.management.base import AppCommand

# from django.db import transaction
from ippidb.models.targetcentric import (
    PDB,
)
from ippidb.management.commands import TaskOutWrapper


class Command(AppCommand):
    """
    Command to import cavity descriptors,
    we use TaskOutWrapper to manage update std_out and std_err
    """

    help = """Load Cavity descriptors from global csv file and complet directory tree.
    Please before import cavity descriptors clean the old data with the command python manage.py clean_targetcentric"""

    def __init__(
        self,
        stdout=None,
        stderr=None,
        no_color=False,
        force_color=False,
        task=None,
    ):
        super(Command, self).__init__(
            stdout=stdout, stderr=stderr, no_color=no_color, force_color=force_color
        )
        if task:
            task.update_state(state=states.STARTED)
            self.stdout = TaskOutWrapper(stdout or sys.stdout, task=task, std_out=True)
            self.stderr = TaskOutWrapper(stderr or sys.stderr, task=task, std_err=True)

    def handle(self, *args, **options):
        """
        Perform the command's actions
        """
        self.stdout.write(self.style.SUCCESS("Delete targetcentric data preparation"))
        for pdb in PDB.objects.all():
            pdb.delete()
            self.stdout.write(self.style.SUCCESS(f"{pdb.code} is delete"))
        self.stdout.write(
            self.style.SUCCESS("Successfully flushed targetcentric tables")
        )
