"""
Command to build matrix distances
"""

import os
import sys
import datetime
import numpy as np
import pandas as pd
from celery import states
from django.core.management.base import AppCommand

# from django.db import transaction
from django.db.models import Q
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
from django.db.models.aggregates import Avg, StdDev, Max, Min
from ippidb.models.targetcentric import (
    Cavity,
    Distance,
    MetaInformation,
)
from ippidb.management.commands import TaskOutWrapper


def normalize_data(indata):
    std = indata.std(axis=0)
    std[std == 0] = 1.0
    mu = indata.mean(axis=0)
    return (indata - mu) / std


def load_data_deprecated(ppc):
    """
    >>> data, name = load_data("PPC-HD-PL_dataset_07-01-2019PDBe_matrix.csv")
    >>> data.shape
    (57, 109)
    """
    colnames = set(ppc.columns) - set(["full_name"])
    sel_log = set(["pmi1", "pmi2", "pmi3", "rgyr", "volume"])
    sel_atanh = set(
        [
            "npr1",
            "npr2",
            "asphericity",
            "eccentricity",
            "inertialshapefactor",
            "spherocityindex",
        ]
    )
    sel_atanh_100 = colnames - sel_log - sel_atanh
    ppc[list(sel_log)] = np.log10(ppc[list(sel_log)])
    ppc[list(sel_atanh)] = np.arctanh(ppc[list(sel_atanh)])
    ppc[list(sel_atanh_100)] = np.arctanh(ppc[list(sel_atanh_100)] / 100.0)

    names = ppc.full_name
    data = ppc[list(colnames)].values
    error_indices = []
    for cav in range(len(data)):
        if np.isinf(data[cav]).sum():
            error_indices.append(cav)
        if np.isnan(data[cav]).sum():
            error_indices.append(cav)
    # Clean error entry data
    new_data = [i for j, i in enumerate(data) if j not in error_indices]
    new_names = [i for j, i in enumerate(names) if j not in error_indices]
    cavitynames_error = []
    for ind in error_indices:
        cavitynames_error.append(names[ind])

    data = np.array(new_data)
    names = new_names
    data = normalize_data(data)
    return data, names, cavitynames_error


def load_data(ppc):
    col = list(set(ppc.columns) - set(["full_name"]))
    names = ppc.full_name
    descriptors = ppc[col]
    size = np.shape(descriptors)
    n_descr = size[1]
    # Remove descriptors that are == 0 for > 95% of pockets
    for i in range(0, n_descr):
        col_dat = descriptors[col[i]]
        cd0 = [i for i in col_dat if i == 0]
        if (len(cd0) / len(col_dat)) > 0.95:
            descriptors.drop(col[i], axis=1, inplace=True)

    col_red = descriptors.columns
    for i in range(len(col_red)):
        d = descriptors[col_red[i]]

        # Condition to take the log of a descriptor
        if np.mean(d) < 0.13 * np.max(d) and np.median(d) < 0.65 * np.mean(d):
            d = np.log(d)  # not log10

    scaler = StandardScaler().fit(descriptors)
    descriptors = scaler.transform(descriptors)
    cavitynames_error = []
    return descriptors, names, cavitynames_error


def normalize_cavity_table():
    cavities = Cavity.objects.all()
    df = pd.DataFrame(list(cavities.values()))
    df = df.drop(
        labels=["id", "mol2", "cavity_number", "partner_id", "chain_id", "type"], axis=1
    )
    return load_data(df)


def knn(data, nb_neighbors):
    nbrs = NearestNeighbors(n_neighbors=nb_neighbors, algorithm="ball_tree").fit(data)
    distances, indices = nbrs.kneighbors(data)
    return distances, indices


def dir_path(string):
    """
    Test if a path is a directory
    """
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)


def file_exist(string):
    """
    Test if a file exist
    """
    if os.path.exists(string):
        return string
    else:
        raise FileNotFoundError(string)


class Command(AppCommand):
    """
    Command to import cavity descriptors,
    we use TaskOutWrapper to manage update std_out and std_err
    """

    help = """Load Cavity descriptors from global csv file and complet directory tree.
    Please before import cavity descriptors clean the old data with the command python manage.py clean_targetcentric"""

    def __init__(
        self,
        stdout=None,
        stderr=None,
        no_color=False,
        force_color=False,
        task=None,
    ):
        super(Command, self).__init__(
            stdout=stdout, stderr=stderr, no_color=no_color, force_color=force_color
        )
        if task:
            task.update_state(state=states.STARTED)
            self.stdout = TaskOutWrapper(stdout or sys.stdout, task=task, std_out=True)
            self.stderr = TaskOutWrapper(stderr or sys.stderr, task=task, std_err=True)

    def add_arguments(self, parser):
        parser.add_argument(
            "-n",
            "--nb_neighbors",
            type=int,
            help="Number of nearest neighbors to use for saved distances between cavities",
        )

    def get_iupac_from_pdb(self, content_file, code_ligand):
        """
        Parse Hetnam from pdb by using this record forma:
        COLUMNS       DATA  TYPE    FIELD           DEFINITION
        ----------------------------------------------------------------------------
        1 -  6       Record name   "HETNAM"
        9 - 10       Continuation  continuation    Allows concatenation of multiple records.
        12 - 14       LString(3)    hetID           Het identifier, right-justified.
        16 - 70       String        text            Chemical name.
        """
        lines = content_file.split("\n")
        iupac = []
        for line in lines:
            if line[0:6] == "HETNAM" and line[11:14] == code_ligand:
                index = line[8:10].strip()
                if index:
                    iupac.insert(int(index) - 1, line[15:70].strip())
                else:
                    iupac.append(line[15:70].strip())
        return "".join(iupac)

    def handle(self, *args, **options):
        """
        Perform the command's actions
        """
        self.stdout.write(self.style.SUCCESS("Matrix preparation"))
        data, names, cavities_error = normalize_cavity_table()
        for cavity_error in cavities_error:
            self.stdout.write(
                self.style.WARNING(
                    """WARNING Cavity {}: an error appeared during the normalization matrix.
                        This cavity was not taken into account in order to normalize the matrix.
                        Please check the cavity before recalculating the distance matrix.
                        You can use the clean_targetcentric command to clean up the database
                        if you delete the cavity or the import_targetcentric command
                        to update the matrix if you correct the cavity""".format(
                        cavity_error
                    )
                )
            )
        self.stdout.write(
            self.style.SUCCESS(
                "{}: Cleaning, old distances".format(datetime.datetime.now())
            )
        )
        Distance.objects.all().delete()
        distances, indices = knn(data, options["nb_neighbors"])
        for cav1_index, name in enumerate(names):
            bulk_distance_create = []
            bulk_distance_update = []
            try:
                cavity1 = Cavity.objects.get(full_name=name)
            except Cavity.DoesNotExist:
                cavity1 = None
            if cavity1:
                self.stdout.write(
                    self.style.SUCCESS(
                        "{}: Distances for cavity {}".format(
                            datetime.datetime.now(), name
                        )
                    )
                )
                for dist_index, cav2_index in enumerate(indices[cav1_index]):
                    try:
                        cavity2 = Cavity.objects.get(full_name=names[cav2_index])
                    except Cavity.DoesNotExist:
                        cavity2 = None
                    if cavity2 and cavity1 != cavity2:
                        if distances[cav1_index][dist_index] == 0:
                            self.stdout.write(
                                self.style.WARNING(
                                    "WARNING Distance between {} and {}, is null".format(
                                        cavity1.full_name, cavity2.full_name
                                    )
                                )
                            )
                        try:
                            dist = Distance.objects.get(
                                Q(cavity1=cavity1, cavity2=cavity2)
                                | Q(cavity1=cavity2, cavity2=cavity1)
                            )
                            if dist.distance != distances[cav1_index][dist_index]:
                                dist.distance = distances[cav1_index][dist_index]
                                bulk_distance_update.append(dist)
                        except Distance.DoesNotExist:
                            dist = Distance(
                                cavity1=cavity1,
                                cavity2=cavity2,
                                distance=distances[cav1_index][dist_index],
                            )
                            bulk_distance_create.append(dist)
                        except Distance.MultipleObjectsReturned as err:
                            self.stdout.write(
                                self.style.ERROR(
                                    "ERROR: Multiple distances for {}, {}: {}".format(
                                        name, names[cav2_index], err
                                    )
                                )
                            )
                Distance.objects.bulk_create(bulk_distance_create)
                Distance.objects.bulk_update(bulk_distance_update, ["distance"])
        average_std = Distance.objects.aggregate(
            average=Avg("distance"),
            std=StdDev("distance"),
            maximum=Max("distance"),
            minimum=Min("distance"),
        )
        metainfo = MetaInformation.objects.all()
        if metainfo.count() > 1:
            metainfo.delete()
        elif metainfo.count() == 0:
            MetaInformation.objects.create(
                average=average_std["average"],
                std=average_std["std"],
                maximum=average_std["maximum"],
                minimum=average_std["minimum"],
            )
        else:
            metainfofirst = metainfo.first()
            metainfofirst.average = average_std["average"]
            metainfofirst.std = average_std["std"]
            metainfofirst.maximum = average_std["maximum"]
            metainfofirst.minimum = average_std["minimum"]
            metainfofirst.save()
