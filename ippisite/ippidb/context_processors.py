from django.conf import settings


def additionnal_settings_variable(request):
    return {
        "PUBLIC_HOST": settings.PUBLIC_HOST,
    }
