"""
iPPI-DB Django app config file
"""

from django.apps import AppConfig


class IppidbConfig(AppConfig):
    name = "ippidb"
