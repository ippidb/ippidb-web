"""
iPPI-DB URLs routing module
"""

from django.urls import include, re_path
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import Sitemap
from django.urls import path
from django.urls import reverse
from rest_framework import routers
from . import views


ROUTER = routers.DefaultRouter()
ROUTER.register(r"pdb", views.PdbViewSet, basename="pdb")
ROUTER.register(r"distance", views.DistanceViewSet, basename="distance")
ROUTER.register(r"protein", views.ProteinViewSet, basename="protein")
ROUTER.register(r"chain", views.ChainViewSet, basename="chain")
ROUTER.register(r"hotspot", views.HotSpotViewSet, basename="hotspot")
ROUTER.register(r"ligand", views.LigandViewSet, basename="ligand")
ROUTER.register(r"partner", views.PartnerViewSet, basename="partner")
ROUTER.register(r"interactfile", views.InteractFileViewSet, basename="interactfile")
ROUTER.register(r"cavity", views.CavityViewSet, basename="cavity")
ROUTER.register(r"job", views.JobViewSet, basename="job")
ROUTER.register(r"linkedpdb", views.LinkedPDBViewSet, basename="linkedpdb")
ROUTER.register(r"summary", views.SummaryViewSet, basename="summary")


class IppiDbStaticSitemap(Sitemap):
    changefreq = "never"
    priority = 1

    def items(self):
        return [
            "index",
            "credits",
            "general",
            "pharmacology",
            "le_lle",
            "physicochemistry",
            "pca",
            "compound_list",
        ]

    def location(self, item):
        return reverse(item)

    def lastmod(self, obj):
        return None


sitemaps = {
    "static": IppiDbStaticSitemap,
    "compounds": views.IppiDbCompoundSitemap,
    "contributors": views.IppiDbContributorsSitemap,
}

urlpatterns = [
    re_path(r"^$", views.index, name="index"),
    re_path(r"^credits/$", views.credits, name="credits"),
    re_path(r"^citation/$", views.citation, name="citation"),
    re_path(r"^news/$", views.news, name="news"),
    re_path(r"^about-general/$", views.about_general, name="general"),
    re_path(r"^about-pharmacology/$", views.about_pharmacology, name="pharmacology"),
    re_path(r"^about-le-lle/$", views.about_le_lle, name="le_lle"),
    re_path(
        r"^about-physicochemistry/$",
        views.about_physicochemistry,
        name="physicochemistry",
    ),
    re_path(r"^about-pca/$", views.about_pca, name="pca"),
    re_path(
        r"^about-contributors/$",
        views.ContributorListView.as_view(),
        name="contributors",
    ),
    re_path(
        r"^about-contributors/(?P<pk>\d+)$",
        views.ContributorDetailView.as_view(),
        name="contributor_card",
    ),
    re_path(r"^targetcentric/$", views.PDBView.as_view(), name="cavities"),
    re_path(r"^targetcentric/summary$", views.SummaryView.as_view(), name="summary"),
    re_path(r"^targetcentric/tmaps$", views.pocketome_html, name="pocketome_html"),
    re_path(r"^targetcentric/networks$", views.NetworkView.as_view(), name="networks"),
    re_path(
        r"^cavity/(?P<pk>\d+)/near-cavities$",
        views.NearCavitiesHTMLFragmentView.as_view(),
        name="near_cavities",
    ),
    re_path(r"^api/", include(ROUTER.urls)),
    re_path(
        r"^api/builddistances$",
        views.build_distances,
        name="builddistances",
    ),
    re_path(r"^compounds/$", views.CompoundListView.as_view(), name="compound_list"),
    re_path(
        r"^compounds/IPPIDB-(?P<compound_id>\d+)$",
        views.CompoundCardBDCHEMRedirectView.as_view(),
        name="redirect_compound_card",
    ),
    re_path(
        r"^compounds/(?P<pk>\d+)$",
        views.CompoundDetailView.as_view(),
        name="compound_card",
    ),
    re_path(r"^tutorials$", views.tutorials, name="tutorials"),
    re_path(r"^contribute$", views.adminSession, name="admin-session"),
    re_path(
        r"^contribute/add/(?P<step>.+)/$", views.ippidb_wizard_view, name="ippidb_step"
    ),
    re_path(r"^contribute/add/$", views.ippidb_wizard_view, name="admin-session-add"),
    re_path(
        r"^contributions/(?P<pk>\d+)$",
        views.ContributionDetailView.as_view(),
        name="contribution-detail",
    ),
    re_path(r"^faq/$", views.FaqView.as_view(), name="faq"),
    re_path(r"^utils/mol2smi$", views.convert_mol2smi, name="mol2smi"),
    re_path(r"^utils/smi2mol$", views.convert_smi2mol, name="smi2mol"),
    re_path(r"^utils/smi2iupac$", views.convert_smi2iupac, name="smi2iupac"),
    re_path(r"^utils/iupac2smi$", views.convert_iupac2smi, name="iupac2smi"),
    re_path(r"^utils/getoutputjob$", views.get_output_job, name="getoutputjob"),
    path(
        "sitemap.xml",
        sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [re_path(r"^__debug__/", include(debug_toolbar.urls))] + urlpatterns
