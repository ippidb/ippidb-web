var molToSmiles = function(molStr, callback){
    $.ajax({
        url: '/utils/mol2smi',
        data: {'molString':molStr},
        success: function(data){
            callback(data.smiles);
        }
    });
};

var smilesToMol = function(smiStr, callback){
    $.ajax({
        url: '/utils/smi2mol',
        data: {'smiString':smiStr},
        success: function(data){
          var molStr = data.mol;
          callback(molStr);
        }
    });
};