function update_molecule_composition_fields() {
    let $this = $(this);
    let elt_id = $this.attr('id');
    let that = $this.closest(".compound").find(".molecule-composition:not('#" + elt_id + "')");
    if ($this.val() == '') {
        if (that.val() == '') {
            $this.add(that).prop("disabled", false);
        }
    } else {
        that.val('').prop("disabled", true);
    }
    if ($this.attr("name").indexOf("smiles") > 0) {
        showCanvas(this);
        let canvas = $("#canvas-" + $this.attr("name"));
        //console.log("canvas name:",canvas);
        canvas.attr("data-smiles", $this.val());
        canvas[0].getContext('2d').clearRect(0, 0, canvas.attr("width"), canvas.attr("height"));
        SmilesDrawer.parse($this.val(), function (tree) {
            smilesDrawer.draw(tree, canvas.attr("id"), 'light', false);
        });
    }
}

function showModalFromMe(src) {
    $("#marvin-div").attr("data-smiles", $(src).closest(".smiles-input-and-edit").find(".molecule-composition").val());
    $("#smiles-textarea").attr("data-smiles", $(src).closest(".smiles-input-and-edit").find(".molecule-composition").val());
    $("#smiles-textarea").attr("data-smile-target", $(src).closest(".smiles-input-and-edit").find(".molecule-composition").attr("id"));
    $("#modal-marvin").modal();
    return false;
}

function apply_molecule_composition_tuning(src) {
    $(src).find(".molecule-composition:visible")
        .keyup(update_molecule_composition_fields)
        .prop("required", true)
        .change(update_molecule_composition_fields)
        .change();
    return src;
}

smilesDrawer = new SmilesDrawer.Drawer({ width: 250, height: 200 });
window.addEventListener("load", function () {
    apply_molecule_composition_tuning(".formset-item");
    var marvinModal = new MarvinModal(
        "marvin-div",
        'modal-marvin',
        'smiles-textarea',
        'marvinApplyButton',
        function (smilesString) {
            $("#" + $("#smiles-textarea").attr("data-smile-target")).val(smilesString).change();
        }
    );
});



$(document).ready(function () {
    $('.molecule_draw').on('click', '.compo_input', function () {

        $(this).closest('.compo_input').find('.compo_box').removeClass('compo_box');
        $(this).addClass('compo_box');
    }).on('click', '.compo_box', function () {
        $(this).removeClass('compo_box');
    })
});


function displayMoleculeCode(Compound) {
    var x = document.getElementById('draw_smile-'.concat(Compound));
    var y = document.getElementById('smile_button-'.concat(Compound));
    var z = document.getElementById('id_'.concat(Compound));
        if (x.style.display === "none") {
          x.style.display = "flex";
          y.className = 'molecule_code_button_active';
          showCanvas(z);
        } else {
          x.style.display = "none";
          y.className = 'molecule_code_button';
        }
};

function showCanvas(Compound){
    var z = Compound.value;
    var x = document.getElementById('canvas_window-'.concat(Compound.name));
    var y = document.getElementById('canvas_placeholder-'.concat(Compound.name));
    if (z!="") {
        x.style.display = "flex";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "flex";
    }
};
