var drawSmiles = function() {
    $('canvas[data-smiles]').each(function() {
        var el = this;
        // only try to draw if the element is visible in the DOM
        if (el.offsetParent !== null) {
            var options = { width: $(el).attr('width'), height: $(el).attr('height'), bondThickness: 1 };
            var theme = $(el).attr('data-draw-theme') || 'light';
            var smilesDrawer = new SmilesDrawer.Drawer(options);
            SmilesDrawer.parse($(el).attr('data-smiles'), function(tree) {
                // Draw to the canvas
                smilesDrawer.draw(tree, el, theme, false);
            });
            // remove the data-smiles attr so we do not try to draw it again unless full reload
            $(el).removeAttr('data-smiles');
        }
    })
};

window.addEventListener("load", drawSmiles);