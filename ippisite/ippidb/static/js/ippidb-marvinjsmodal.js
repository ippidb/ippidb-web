class MarvinModal {
    constructor(iframeId, modalId, textAreaId, applyId, exportCallback) {
        this.iframeEl = $('#' + iframeId);
        this.modalEl = $('#' + modalId);
        this.applyEl = $('#' + applyId);
        this.textAreaEl = $('#' + textAreaId);
        this.exportCallback = exportCallback;
        this.marvinSketcherInstance = null;
        if (typeof ChemicalizeMarvinJs !== "undefined") {
            ChemicalizeMarvinJs.createEditor('#' + iframeId).then(function(sketcherInstance) {
                this.marvinSketcherInstance = sketcherInstance;
                this.marvinSketcherInstance.on('molchange', function() {
                    this.exportSmiles(function(smilesString) {
                        this.textAreaEl.val(smilesString);
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        this.modalEl.on('shown.bs.modal', function() {
            //NB: this must be setup only once the modal is shown otherwise
            // it doesn't work with Firefox (see https://bugzilla.mozilla.org/show_bug.cgi?id=941146)
            var query = this.iframeEl.attr('data-smiles') || this.textAreaEl.attr('data-smiles');
            var fingerprint = this.iframeEl.attr('data-fingerprint') || this.textAreaEl.attr('data-fingerprint');
            $("input[name=fingerprint][value='" + fingerprint + "']").attr('checked', 'checked')
            this.importSmiles(query);
            this.textAreaEl.val(query);
            this.textAreaEl.focus();
        }.bind(this));
        this.applyEl.click(function() {
            this.modalEl.modal('hide');
            this.exportSmiles(this.exportCallback.bind(this));
        }.bind(this));
        this.textAreaEl.on('input', function() {
            this.importSmiles(this.textAreaEl.val())
        }.bind(this));
    }

    exportSmiles(callback) {
        if (this.marvinSketcherInstance != null) {
            $("#marvinApplyButton").prop("disabled", false);
            this.marvinSketcherInstance.exportStructure("mol").then(function(molStr) {
                molToSmiles(molStr, callback);
            }.bind(this));
        } else {
            $("#marvinApplyButton").prop("disabled", false);
            callback(this.textAreaEl.val());
        }
    }

    importSmiles(smilesString) {
        if (this.marvinSketcherInstance != null) {
            if (smilesString !== undefined && smilesString !== '') {
                $("#marvinApplyButton").prop("disabled", true);
                smilesToMol(smilesString,
                    function(molString) {
                        $("#marvinApplyButton").prop("disabled", false);
                        return this.marvinSketcherInstance.importStructure("mol", molString);
                    }.bind(this));
            } else {
                return this.marvinSketcherInstance.clear();
            }
        }
    }
}