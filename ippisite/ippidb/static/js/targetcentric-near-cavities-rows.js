$(document).ready(function(){
    $(".asnyc-construction.now[data-cavity-id]").each(function () {
        loadNearCavityRows($(this));
    });
});

function loadNearCavityRows($row){
    if($row.length==0)
        return;
    $row.addClass("loading");
    $.ajax({
        url: '/cavity/'+$row.data("cavityId")+'/near-cavities',
        data: {'chaincount':$row.data("chaincount"), 'cavcount':$row.data("cavcount")},
        success: function(data){
            var $data=$(data);
            if ($row.hasClass("show"))
                $data.addClass("show");
            $data.insertAfter($row);
            $row.remove();
        }
    });
}