$(document).ready(function () {
    // DataTable initialization
    var dataTable = new DataTable('#summarytable', {
        ajax: {
            url: '/api/summary/',
            type: "GET",
            //dataSrc: 'results',

            // Add other parameters like headers, etc., if needed
            data: function (data) {
                data.page = data.draw;
                delete data.draw;
                data.limit = data.length;
                data.page_size = data.limit;
                delete data.length;
                data.search = data.search.value;
                data.offset = data.start;
                var ordering = data.columns[data.order[0].column].data;
                if (ordering.includes('.')) {
                    ordering = ordering.replace('.', '__');
                }
                if (data.order[0].dir === 'asc') {
                    data.ordering = ordering;
                } else {
                    data.ordering = "-" + ordering;
                }
                delete data.start;
                return data;
            },

            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                json.data = json.results;
                delete json.results;
                return JSON.stringify(json); // return JSON string
            },
        },
        columns: [
            // Define your columns here (matching the JSON structure)
            { data: 'full_name' },
            { data: 'cavity_number' },
            {
                data: 'chain.pdb', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html("<a target='_blank' rel='noopener noreferrer' href='/targetcentric?pdbsearch=" + oData.chain.pdb + "'>" + oData.chain.pdb + "</a>");
                }
            },
            {
                data: 'chain.organism', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html("<a target='_blank' rel='noopener noreferrer' href='/targetcentric?organism=" + oData.chain.organism + "'>" + oData.chain.organism + "</a>");
                },
            },
            { data: 'chain.pdb_chain_id' },
            {
                data: 'chain.uniprot', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html("<a target='_blank' rel='noopener noreferrer' href='/targetcentric?uniprotid=" + oData.chain.uniprot + "'>" + oData.chain.uniprot + "</a>");
                },
            },
            { data: 'partner.pdb_chain_id' },
            {
                data: 'partner.uniprot', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    if (oData.partner.uniprot === 'NaN') {
                        oData.partner.uniprot
                    } else {
                        $(nTd).html("<a target='_blank' rel='noopener noreferrer' href='/targetcentric?uniprotid=" + oData.partner.uniprot + "'>" + oData.partner.uniprot + "</a>");
                    }
                },
            },
            {
                data: 'partner.pdb_ligand_id', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    if (oData.partner.pdb_ligand_id === 'NaN') {
                        oData.partner.pdb_ligand_id
                    } else {
                        $(nTd).html("<a target='_blank' rel='noopener noreferrer' href='/targetcentric?ligandcode=" + oData.partner.pdb_ligand_id.split('_')[0] + "'>" + oData.partner.pdb_ligand_id + "</a>");
                    }
                },
            },
            { data: 'type' },
        ],
        serverSide: true, // Enable server-side processing for pagination
        paging: true,
        searching: true,

        //pagingType: "simple",
        processing: true,
        // Add additional DataTable options as needed
        rowReorder: true,
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 0 },
            { orderable: false, targets: 1 },
            { orderable: true, className: 'reorder', targets: 2 },
            { orderable: true, className: 'reorder', targets: 3 },
            { orderable: false, targets: 4 },
            { orderable: true, className: 'reorder', targets: 5 },
            { orderable: false, targets: 6 },
            { orderable: true, className: 'reorder', targets: 7 },
            { orderable: true, className: 'reorder', targets: 8 },
            { orderable: true, className: 'reorder', targets: 9 },
        ],

    });
});