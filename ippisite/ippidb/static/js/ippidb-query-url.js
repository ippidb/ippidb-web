class QueryUrl {

    constructor(loadingEl) {
        this.url = new URL(location.href);
        this.loadingEl = loadingEl
    }

    unPage() {
        this.url.searchParams.delete('page');
    }

    modify(paramName, paramValue) {
        if (Array.isArray(paramValue)) {
            // set multiple values for one parameter
            this.url.searchParams.delete(paramName);
            paramValue.forEach(function(value) {
                this.url.searchParams.append(paramName, value);
            }.bind(this));
        } else if (paramValue != null) {
            // set one value for one parameter
            this.url.searchParams.set(paramName, paramValue);
        } else if (Array.isArray(paramName)) {
            // unset value for multiple parameters
            paramName.forEach(function(name) {
                this.url.searchParams.delete(name);
            }.bind(this));
        } else {
            // unset value for one parameter
            this.url.searchParams.delete(paramName);
        }
        this.go();
    }

    changeSelection(paramName, values) {
        values = values || $('input[name=' + paramName + ']:checked').map(function(_, el) { return $(el).val(); }).get();
        this.modify(paramName, values);
    }

    removeFromSelection(paramName, value) {
        var selectedValues = this.url.searchParams.getAll(paramName);
        selectedValues.splice(selectedValues.indexOf(value), 1);
        this.modify(paramName, selectedValues);
    }

    go() {
        var modifiedUrl = this.url.toString();
        window.location.assign(modifiedUrl);
        if (this.loadingEl) {
            $('#loadingModal').modal();
        }
    }

}