var drawCompoundDescriptorRadarChart = function (canvasId, mw, alogP, hbd, hba, tpsa, rb, ar, fsp3, rs) {
    var radarChartData = {
        "labels": ["MW", "ALogP", "HBD", "HBA", "TPSA", "RB", "Ar", "Fsp3", "R/S"],
        "datasets": [
            {
                "label": "Descriptor's threshold",
                "data": [500 / 1200, (5 + 3) / 15, 5 / 15, 10 / 15, 140 / 250, 10 / 20, 4 / 10, 1 - (0.4 / 1), 1 - (1 / 10)],
                "realData": [500, 5, 5, 10, 140, 10, 4, 0.4, 1],
                "fill": true,
                "backgroundColor": "rgba(54, 162, 235, 0.2)",
                "borderColor": "rgb(54, 162, 235)",
                "pointBackgroundColor": "rgb(54, 162, 235)",
                "pointBorderColor": "#fff",
                "pointHoverBackgroundColor": "#fff",
                "pointHoverBorderColor": "rgb(54, 162, 235)"
            },
            {
                "label": "Descriptor's value for the compound",
                "data": [mw / 1200, (alogP + 3) / 15, hbd / 15, hba / 15, tpsa / 250, rb / 20, ar / 10, 1 - (fsp3 / 1), 1 - (rs / 10)],
                "realData": [mw, alogP, hbd, hba, tpsa, rb, ar, fsp3, rs],
                "fill": true,
                "backgroundColor": "rgba(255, 99, 132, 0.2)",
                "borderColor": "rgb(255, 99, 132)",
                "pointBackgroundColor": "rgb(255, 99, 132)",
                "pointBorderColor": "#fff",
                "pointHoverBackgroundColor": "#fff",
                "pointHoverBorderColor": "rgb(255, 99, 132)"
            }],
        "options": { "elements": { "line": { "tension": 0, "borderWidth": 3 } } }
    };
    var canvasEl = document.getElementById(canvasId);
    var height = canvasEl.getAttribute('data-radarh');
    var width = canvasEl.getAttribute('data-radarw');
    canvasEl.height = height;
    canvasEl.width = width;
    canvasEl.style.height = height;
    canvasEl.style.width = width;
    var myRadar = new Chart(document.getElementById(canvasId).getContext("2d"),
        {
            type: 'radar',
            data: radarChartData,
            options: {
                scaleLineWidth: 1,
                pointLabelFontFamily: "'Helvetica Neue'",
                pointLabelFontSize: 12,
                scaleOverride: true,
                scaleSteps: 5,
                scaleStepWidth: 0.2,
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var label = data.labels[tooltipItem.index];
                            label += tooltipItem.datasetIndex == 0 ? " threshold" : "";
                            label += " value:";
                            label += data.datasets[tooltipItem.datasetIndex].realData[tooltipItem.index];
                            return label;
                        }
                    }
                },
                maintainAspectRatio: false
            }
        });
};
var prepareCompoundFamilyBiplotData = function (compoundId, compoundFamily, plotData) {
    var currentCompoundData = [];
    var currentFamilyData = [];
    var otherFamiliesData = [];
    plotData.forEach(function (item) {
        if (item.id == compoundId) {
            currentCompoundData.push(item);
        } else if (item.family_name == compoundFamily) {
            currentFamilyData.push(item);
        } else {
            otherFamiliesData.push(item)
        }
    });
    var scatterData = {
        'datasets': [
            {
                label: 'Other PPI families',
                borderColor: "rgba(211,211,211, 0.3)",
                backgroundColor: "rgba(211,211,211, 0.3)",
                data: otherFamiliesData
            },
            {
                label: compoundFamily,
                borderColor: "rgba(54, 162, 235, 1)",
                backgroundColor: "rgba(54, 162, 235, 0.5)",
                data: currentFamilyData
            },
            {
                label: 'Compound',
                radius: 6,
                borderColor: "rgba(194, 0, 0, 1)",
                backgroundColor: "rgba(194, 0, 0, 1)",
                data: currentCompoundData
            }
        ]
    };
    return scatterData;
}

var getPpiFamilyColor = function (ppiName) {
    var hash = 0;
    if (ppiName == null || ppiName.length === 0) return hash;
    for (var i = 0; i < ppiName.length; i++) {
        hash = ppiName.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    var color = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 255;
        color += ('00' + value.toString(16)).substr(-2);
    }
    return color;
}

var preparePerFamilyBiplotData = function (plotData) {
    var property = 'family_name';
    var datasets = plotData.reduce(function (acc, obj) {
        var family = obj[property];
        if (!acc[family]) {
            acc[family] = {
                label: family,
                borderColor: getPpiFamilyColor(family),
                backgroundColor: getPpiFamilyColor(family),
                data: []
            };
        }
        acc[family].data.push(obj);
        return acc;
    }, {});
    var scatterData = {
        'datasets': Object.values(datasets)
    };
    return scatterData;
}

var drawCompoundsBiplotChart = function (canvasId, scatterData, tabHash, options) {
    var ctx = document.getElementById(canvasId).getContext('2d');
    var biplotOptions = {
        aspectRatio: true,
        maintainAspectRatio: true,
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        },
        tooltips: {
            enabled: false,
            mode: 'point',
            custom: function (tooltipModel) {
                // Tooltip Element
                var tooltipEl = document.getElementById('chartjs-tooltip');
                // Create element on first render
                if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    document.body.appendChild(tooltipEl);
                }
                var data = this._data;
                var props;
                if (tooltipModel.dataPoints && tooltipModel.dataPoints.length > 0) {
                    tooltipModel.dataPoints.forEach(function (dataPoint) {
                        var dataItem = data.datasets[dataPoint.datasetIndex].data[dataPoint.index];
                        props = { 'id': dataItem.id, 'family': dataItem.family_name, 'smiles': dataItem.smiles };
                    });
                    var canvasStr = '<canvas id="canvas-' + props.id + '" width="300" height="250" data-smiles="' + props.smiles + '"></canvas>'
                    var tooltipStr = '<div class="card" style="width:250">' + canvasStr
                        + '<div class="card-body"><h5 class="card-title">Compound ' + props.id + '</h5>'
                        + '<p class="card-text">PPI Family: ' + props.family + '</p></div></div>';
                    tooltipEl.innerHTML = tooltipStr;
                }
                // Hide if no tooltip
                if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                }
                // Set caret Position
                tooltipEl.classList.remove('above', 'below', 'no-transform');
                if (tooltipModel.yAlign) {
                    tooltipEl.classList.add(tooltipModel.yAlign);
                } else {
                    tooltipEl.classList.add('no-transform');
                }
                // `this` will be the overall tooltip
                var position = this._chart.canvas.getBoundingClientRect();
                // Display, position, and set styles for font
                tooltipEl.style.opacity = 1;
                tooltipEl.style.position = 'absolute';
                tooltipEl.style.left = position.left + window.scrollX + tooltipModel.caretX + 'px';
                tooltipEl.style.top = position.top + window.scrollY + tooltipModel.caretY + 'px';
                drawSmiles();
            }
        }
    }
    if (options) {
        Object.assign(biplotOptions, options);
    }
    var scatterChart = new Chart(ctx, {
        type: 'scatter',
        data: scatterData,
        options: biplotOptions
    });
    document.getElementById(canvasId).onclick = function (evt) {
        var activePoints = scatterChart.getElementAtEvent(evt);
        var datasetIndex = activePoints[0]._datasetIndex;
        var selectedIndex = activePoints[0]._index;
        var id = scatterChart.data.datasets[datasetIndex].data[selectedIndex].id;
        window.location = '/compounds/' + id + '#' + tabHash;
    };
};


var drawBarChart = function (elementId, data, caption, queryFilter, orientation, labelIdMap) {
    var ctx = document.getElementById(elementId).getContext('2d');
    if (orientation == null) {
        orientation = 'horizontalBar'
    }
    var myHorizontalBar = new Chart(ctx, {
        type: orientation,
        data: data,
        options: {
            elements: {
                rectangle: {
                    borderWidth: 20,
                }
            },
            responsive: true,
            aspectRatio: true,
            maintainAspectRatio: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: caption
            }
        }
    });
    if (queryFilter != null) {
        document.getElementById(elementId).onclick = function (evt) {
            let activePoints = myHorizontalBar.getElementsAtEvent(evt);
            let index = 0;
            if(activePoints.length>0){
                index = activePoints[0]._index;
                let label = myHorizontalBar.data.labels[index];
                let id = labelIdMap[label];
                window.location = '/compounds/?' + queryFilter + '=' + id;    
            }
            return false;
        };
    }
}


var drawHorizontalStackedBarChart = function (elementId, data, caption, queryFilter) {
    var ctx = document.getElementById(elementId).getContext('2d');
    var myHorizontalBar = new Chart(ctx, {
        type: 'horizontalBar',
        data: data,
        options: {
            elements: {
                rectangle: {
                    borderWidth: 20,
                }
            },
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: caption
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });
    if (queryFilter != null) {
        document.getElementById(elementId).onclick = function (evt) {
            var activePoints = myHorizontalBar.getElementAtEvent(evt);
            var datasetIndex = activePoints[0]._datasetIndex;
            var selectedIndex = activePoints[0]._index;
            var id = myHorizontalBar.data.labels[datasetIndex];
            window.location = '/compounds/?' + queryFilter + '=' + id;
        };
    }
}
