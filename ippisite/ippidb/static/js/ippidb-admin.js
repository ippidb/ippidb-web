function get_logs(taskid) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/utils/getoutputjob?task_id=' + taskid, true);
    xhr.onload = function() {
        const cls = ["border-info", "border-warning", "border-danger"]
        var outputs = JSON.parse(xhr.response);
        var auth = document.getElementById('auth');
        var createtime = document.getElementById('createtime');
        var updatetime = document.getElementById('updatetime');
        var traceback = document.getElementById('traceback');
        var stderr = document.getElementById('stderr');
        var stdout = document.getElementById('stdout');
        var status = document.getElementById('status');
        auth.innerHTML = outputs.task_name + " || " + outputs.task_id;
        datecreate = new Date(outputs.create_time);
        createtime.innerHTML = datecreate;
        status.innerHTML = outputs.status;
        if (outputs.status == "SUCCESS") {
            status.closest('div').classList.remove(...cls);
            status.closest('div').classList.add("border-success");
        } else if (outputs.status == "FAILURE") {
            status.closest('div').classList.remove(...cls);
            status.closest('div').classList.add("border-danger");
        } else if (outputs.status == "REVOKED") {
            status.closest('div').classList.remove(...cls);
            status.closest('div').classList.add("border-warning");
        } else {
            status.closest('div').classList.remove(...cls);
            status.closest('div').classList.add("border-info");
        };
        dateupdate = new Date(outputs.complete_time);
        updatetime.innerHTML = dateupdate;
        traceback.innerHTML = outputs.traceback;
        stderr.innerHTML = outputs.std_err;
        stdout.innerHTML = outputs.std_out;
    };
    xhr.send();
}

(function() {
    let taskid = String(document.getElementById("loginfo").getAttribute("taskid"));
    get_logs(taskid);
    setInterval(get_logs, 2000, taskid);
})();