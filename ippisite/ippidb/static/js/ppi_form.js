$(document).ready(function(){
	var app = require("ols-autocomplete");
	var instance = new app();
    options={
        action : function(relativePath, suggestion_ontology, type, iri, data){
            if ($('[name*="selected_diseases"]').val().indexOf(";"+data['shortForm']+";")!=-1){
                return;
            }
            console.log(data);
            label=data["label"];
            while (label.indexOf("<b>")!=-1){
                label=label.replace("<b>","").replace("</b>","");
            }
            $('[name*="selected_diseases"]').val(
                $('[name*="selected_diseases"]').val()+";"+label+";"+data['shortForm']+";\n"
            );
            create_entry(label,data['shortForm']);
            $('[name*="ols_diseases"]').val("");
        }
    }
    instance.start(options);
    $('[name*="selected_diseases"]')
        .parent().addClass("")
        .append($('[name*="ols_diseases"]').closest(".form-group"))
        .closest("form").find(".twitter-typeahead").addClass("d-block");
    var current_entries = $('[name*="selected_diseases"]').val().split("\n");
    for(var i=0;i<current_entries.length;i++){
        if(current_entries[i].trim()!=""){
            let entry = current_entries[i].split(";");
            create_entry(entry[1],entry[2]);
        }
    }
});

function remove_selected_diseases(id){
    $("[data-disease="+id+"]").remove();
    var entries = $('[name*="selected_diseases"]').val().split("\n");
    entries[id]="";
    entries=entries.join("\n");
    $('[name*="selected_diseases"]').val(entries);
    if (entries.trim() == ""){
        $('[data-disease="-1"]').show();
    }
}

function create_entry(label,identifier){
    $('[data-disease="-1"]').hide();
    var cpt=$('[name*="selected_diseases"]').data("cpt") || 0;
    $('[name*="selected_diseases"]').data("cpt", cpt+1);
    $(
        '<h6 data-disease="'+cpt+'">'
        + label
        +' <span style="cursor: pointer; font-size:18px;" class="badge-pill float-right"'
        +'onclick="remove_selected_diseases('+cpt+')"'
        +'>x</span>'
        + '</h6>'
    ).insertBefore($('[name*="ols_diseases"]').parent().parent());
}