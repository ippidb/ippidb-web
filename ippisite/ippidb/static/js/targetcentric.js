function autocomplete(inp, arr) {
    //DEPRECATED TODO: remove it
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        if (val.length <= 2) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/

            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function createElement(name, properties, style, classlist = false, attrs = false) {
    var el = document.createElement(name);
    Object.assign(el, properties);
    Object.assign(el.style, style);
    if (classlist) {
        var splitlist = classlist.split(" ");
        for (var i = 0; i < splitlist.length; i++) {
            el.classList.add(splitlist[i]);
        };
    };
    if (attrs) {
        for (var attr in attrs) {
            el.setAttribute(attr, attrs[attr])
        };
    };
    return el
};

function addElement(stage, el) {
    /*stage.viewer.container.appendChild(el);*/
    $("#viewportcard").append(el);
};


var DefaultColor = 0xC0DB01
var ProbeTypeColors = {
    "DU": 0xFFFFFF,
    'C': 0x909090,
    "CA": 0x737373,
    "CZ": 0xA6A6A6,
    'N': 0x3050F8,
    'NZ': 0x2640C6,
    "OD1": 0xFF6464,
    "OG": 0xA30808,
    'O': 0xFF0D0D,
};
var ElementColors = {
    'H': 0xFFFFFF,
    'HE': 0xD9FFFF,
    'LI': 0xCC80FF,
    'BE': 0xC2FF00,
    'B': 0xFFB5B5,
    'C': 0x909090,
    'N': 0x3050F8,
    'O': 0xFF0D0D,
    'F': 0x90E050,
    'NE': 0xB3E3F5,
    'NA': 0xAB5CF2,
    'MG': 0x8AFF00,
    'AL': 0xBFA6A6,
    'SI': 0xF0C8A0,
    'P': 0xFF8000,
    'S': 0xFFFF30,
    'CL': 0x1FF01F,
    'AR': 0x80D1E3,
    'K': 0x8F40D4,
    'CA': 0x3DFF00,
    'SC': 0xE6E6E6,
    'TI': 0xBFC2C7,
    'V': 0xA6A6AB,
    'CR': 0x8A99C7,
    'MN': 0x9C7AC7,
    'FE': 0xE06633,
    'CO': 0xF090A0,
    'NI': 0x50D050,
    'CU': 0xC88033,
    'ZN': 0x7D80B0,
    'GA': 0xC28F8F,
    'GE': 0x668F8F,
    'AS': 0xBD80E3,
    'SE': 0xFFA100,
    'BR': 0xA62929,
    'KR': 0x5CB8D1,
    'RB': 0x702EB0,
    'SR': 0x00FF00,
    'Y': 0x94FFFF,
    'ZR': 0x94E0E0,
    'NB': 0x73C2C9,
    'MO': 0x54B5B5,
    'TC': 0x3B9E9E,
    'RU': 0x248F8F,
    'RH': 0x0A7D8C,
    'PD': 0x006985,
    'AG': 0xC0C0C0,
    'CD': 0xFFD98F,
    'IN': 0xA67573,
    'SN': 0x668080,
    'SB': 0x9E63B5,
    'TE': 0xD47A00,
    'I': 0x940094,
    'XE': 0x940094,
    'CS': 0x57178F,
    'BA': 0x00C900,
    'LA': 0x70D4FF,
    'CE': 0xFFFFC7,
    'PR': 0xD9FFC7,
    'ND': 0xC7FFC7,
    'PM': 0xA3FFC7,
    'SM': 0x8FFFC7,
    'EU': 0x61FFC7,
    'GD': 0x45FFC7,
    'TB': 0x30FFC7,
    'DY': 0x1FFFC7,
    'HO': 0x00FF9C,
    'ER': 0x00E675,
    'TM': 0x00D452,
    'YB': 0x00BF38,
    'LU': 0x00AB24,
    'HF': 0x4DC2FF,
    'TA': 0x4DA6FF,
    'W': 0x2194D6,
    'RE': 0x267DAB,
    'OS': 0x266696,
    'IR': 0x175487,
    'PT': 0xD0D0E0,
    'AU': 0xFFD123,
    'HG': 0xB8B8D0,
    'TL': 0xA6544D,
    'PB': 0x575961,
    'BI': 0x9E4FB5,
    'PO': 0xAB5C00,
    'AT': 0x754F45,
    'RN': 0x428296,
    'FR': 0x420066,
    'RA': 0x007D00,
    'AC': 0x70ABFA,
    'TH': 0x00BAFF,
    'PA': 0x00A1FF,
    'U': 0x008FFF,
    'NP': 0x0080FF,
    'PU': 0x006BFF,
    'AM': 0x545CF2,
    'CM': 0x785CE3,
    'BK': 0x8A4FE3,
    'CF': 0xA136D4,
    'ES': 0xB31FD4,
    'FM': 0xB31FBA,
    'MD': 0xB30DA6,
    'NO': 0xBD0D87,
    'LR': 0xC70066,
    'RF': 0xCC0059,
    'DB': 0xD1004F,
    'SG': 0xD90045,
    'BH': 0xE00038,
    'HS': 0xE6002E,
    'MT': 0xEB0026,
    'DS': 0xFFFFFF,
    'RG': 0xFFFFFF,
    'CN': 0xFFFFFF,
    'UUT': 0xFFFFFF,
    'FL': 0xFFFFFF,
    'UUP': 0xFFFFFF,
    'LV': 0xFFFFFF,
    'UUH': 0xFFFFFF,

    'D': 0xFFFFC0,
    'T': 0xFFFFA0
};

var ChainColors = {
    'A': 0x313695,
    'B': 0xA50026,
    'C': 0x1b7837,
    'D': 0xB35806,
    'E': 0x542788,
    'F': 0xF46D43,
    'G': 0x4575B4,
    'H': 0x5AAE61,
    'I': 0xF4A582,
    'J': 0x74ADD1,
    'K': 0xFDAE61,
    'L': 0xE6F5D0,
    'M': 0xBFA6A6,
    'N': 0xF0C8A0,
    'O': 0xFF8000,
    'P': 0xFFFF30,
    'Q': 0x1FF01F,
    'R': 0x80D1E3,
    'S': 0x8F40D4,
    'T': 0x3DFF00,
    'U': 0xE6E6E6,
    'V': 0xBFC2C7,
    'W': 0xA6A6AB,
    'X': 0x8A99C7,
    'Y': 0x9C7AC7,
    'Z': 0xE06633,
};

var ColorsList = [
    0xE06633, 0x9C7AC7, 0x8A99C7, 0xA6A6AB, 0xBFC2C7, 0xE6E6E6,
    0x3DFF00, 0x8F40D4, 0x80D1E3, 0x1FF01F, 0xFFFF30, 0xFF8000,
    0xF0C8A0, 0xBFA6A6, 0xE6F5D0, 0xFDAE61, 0x74ADD1, 0xF4A582,
    0x5AAE61, 0x4575B4, 0xF46D43, 0x542788, 0xB35806, 0x1b7837,
    0xA50026, 0x313695
];

var probetype = NGL.ColormakerRegistry.addScheme(function (params) {
    this.atomColor = function (atom) {
        return ProbeTypeColors[atom.atomname] || ElementColors[atom.element] || DefaultColor
    };
});

var chaincolor = NGL.ColormakerRegistry.addScheme(function (params) {
    this.atomColor = function (atom) {
        return ChainColors[atom.chainname] || DefaultColor;
    };
});

FileColors = {
    1: '#00AAB2',
    2: '#e56e6e',
    3: '#FFF8E7',
    4: '#b3b652',
    5: '#189846',
    6: '#d782d1',
    7: '#7bdca2',
    8: '#7087e4',
    9: '#f6a33e',
    10: '#3748b3',
    11: '#79bd7a',
    12: '#bd799a',
};

var filecolor = function (indexchain, indexfile) {
    return FileColors[indexchain + 1 + indexfile + 1] || DefaultColor;
};

var tooltip = document.createElement("div");
Object.assign(tooltip.style, {
    track: true,
    display: "none",
    position: "relative",
    zIndex: 10,
    pointerEvents: "none",
    backgroundColor: "rgba(0, 0, 0, 0.6)",
    color: "lightgrey",
    padding: "0.5em",
    fontFamily: "sans-serif",
    width: "auto"
});

function baseName(str) {
    var base = new String(str).substring(str.lastIndexOf('/') + 1);
    if (base.lastIndexOf(".") != -1)
        base = base.substring(0, base.lastIndexOf("."));
    return base;
}

function loadNGL(url, callback) {
    $.ajax({
        url: url,
        success: function (pdb) {
            var stage = new NGL.Stage("viewport", {
                backgroundColor: "white",
                fogNear: 50,
                fogFar: 65,
            });
            stage.mouseControls.remove("hoverPick", NGL.MouseActions.tooltipPick);
            stage.mouseControls.remove("drag-ctrl-left", NGL.MouseActions.rotateComponentDrag);
            stage.mouseControls.remove("drag-ctrl-right", NGL.MouseActions.panComponentDrag);
            $("#viewport").append(tooltip);
            stage.signals.hovered.add(function (pickingProxy) {
                if (pickingProxy && (pickingProxy.atom)) {
                    var cp = pickingProxy.canvasPosition;

                    tooltip.innerText = pickingProxy.getLabel();
                    tooltip.style.bottom = (cp.y + 48) + "px";
                    tooltip.style.left = (cp.x) + 'px';
                    tooltip.style.display = "inline-block";
                } else {
                    tooltip.style.display = "none";
                }
            });
            // Handle window resizing
            var divflex = createElement("div", {}, {}, "d-flex justify-content-between flex-row-reverse bd-highlight");
            var divmultiButtonright = createElement("div", { role: "group" }, {}, "d-flex align-items-end flex-column");
            var divcavitiesButton = createElement("div", { role: "group" }, {}, "infobulle btn-group-vertical btn-group-sm btn-group-toggle p-2");
            var spaninfocav = createElement("span", {}, {}, '');
            spaninfocav.innerHTML = "Load a pocket detected with Volsite (Desaphy et al., 2012).";
            divcavitiesButton.appendChild(spaninfocav);
            divmultiButtonright.appendChild(divcavitiesButton);
            var divmultiButtoncenter = createElement("div", {}, {}, "btn-group-vertical align-self-start");
            var divmultiButtonleft = createElement("div", {}, {}, "btn-group align-self-start");
            divflex.appendChild(divmultiButtonright);
            divflex.appendChild(divmultiButtoncenter);
            divflex.appendChild(divmultiButtonleft);
            var divslider = createElement("div", {}, {}, 'infobulle form-group border border-info rounded p-2');
            var spaninfodrug = createElement("span", {}, {}, '');
            spaninfodrug.innerHTML = "Druggability has been determined by our Deep Learning tool InDeep trained on protein/ligand interactions. They are represented as isolevel probabilities, which you can adjust with the slider (0,1)."
            divslider.appendChild(spaninfodrug);
            titledruggable = createElement("strong", {}, {}, '');
            titledruggable.innerHTML = "Predicted druggability";
            divslider.appendChild(titledruggable);
            var divsliderinteractibility = createElement("div", {}, {}, 'infobulle form-group border border-info rounded p-2 mr-2');
            var spaninfoint = createElement("span", {}, { "right": "50%" }, '');
            spaninfoint.innerHTML = "Interactability has been determined by our Deep Learning tool InDeep trained on protein-protein interactions. They are represented as isolevel probabilities, which you can adjust with the slider (0,1)."
            divsliderinteractibility.appendChild(spaninfoint);
            titleinteractible = createElement("strong", {}, {}, '');
            titleinteractible.innerHTML = "Predicted Interactability";
            divsliderinteractibility.appendChild(titleinteractible);
            divmultiButtonleft.appendChild(divsliderinteractibility);
            divmultiButtonleft.appendChild(divslider);
            window.addEventListener("resize", function (event) {
                stage.handleResize();
            }, false);
            var hasmrc = 0
            var hasinteract = 0
            var colorindex = 0
            pdb.chain_set.forEach(function (chain, indexchain) {
                var blobchain = new Blob([chain.file_content], { type: 'text/plain' });
                var componentname = pdb.code + '_' + chain.pdb_chain_id;
                var hascritical = false
                var haswarm = false
                chain.hotspot_set.forEach(function (residue) {
                    if (residue.type == "critical") {
                        hascritical = true
                    } else {
                        haswarm = true
                    };
                });
                stage.loadFile(blobchain, { ext: "pdb", name: componentname }).then(function (o) {
                    o.addRepresentation("cartoon", { color: chaincolor, visible: true });
                    o.addRepresentation("licorice", { visible: false, sele: 'not _H' });
                    o.addRepresentation("surface", {
                        visible: false,
                        colorScheme: 'electrostatic',
                        surfaceType: 'av',
                        probeRadius: 1.4,
                        smooth: 2,
                        scaleFactor: 2.0,
                        cutoff: 0.02,
                    });


                    if (chain.hotspot_set.length != 0) {
                        var selection_critical = ":" + chain.pdb_chain_id + " and ( "
                        var selection_warm = ":" + chain.pdb_chain_id + " and ( "
                        chain.hotspot_set.forEach(function (residue) {
                            if (residue.type == "critical") {
                                selection_critical += residue.residu_number + ", ";
                            } else {
                                selection_warm += residue.residu_number + ", ";
                            };

                        });
                        selection_critical = selection_critical.slice(0, - 2);
                        selection_critical = selection_critical + " )";
                        selection_warm = selection_warm.slice(0, - 2);
                        selection_warm = selection_warm + " )";


                        var colorhotspotwarm = "#ffbd92";
                        if (indexchain === 0) {
                            colorhotspotwarm = "#00b6e8"
                        }

                        var colorhotspotcritical = "#ffd700";
                        if (indexchain === 0) {
                            colorhotspotcritical = "#00b394"
                        }
                        if (hascritical) {
                            o.addRepresentation("ball+stick", {
                                name: componentname + "critical",
                                sele: selection_critical,
                                color: colorhotspotcritical,
                                visible: false,
                            });
                        }
                        if (haswarm) {
                            o.addRepresentation("ball+stick", {
                                name: componentname + "warm",
                                sele: selection_warm,
                                color: colorhotspotwarm,
                                visible: false,
                            });
                        }
                    }

                    stage.autoView();
                });

                var divchain = createElement("div", {}, {}, "");
                divsliderinteractibility.appendChild(divchain);
                if (chain.interactfile_set.length) {

                    hasinteract = 1;
                    var labelsliderinterac = createElement("label", {}, {}, "", { 'for': 'isolevel' + chain.pdb_chain_id });
                    labelsliderinterac.innerHTML = 'Adjust predicted interactability surface on chain ' + chain.pdb_chain_id;
                    divchain.appendChild(labelsliderinterac);
                    divchain.appendChild(createElement('br'));
                    var divdropdowninterac = createElement("div", {}, {}, "dropdown");
                    var buttondorpdowninterac = createElement("button", {
                        type: "button",
                        id: componentname + "_interact"
                    }, {}, "btn btn-outline-info btn-sm dropdown-toggle", {
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                    });
                    buttondorpdowninterac.append("Select interactability model, chain ".concat(chain.pdb_chain_id));
                    divdropdowninterac.appendChild(buttondorpdowninterac);
                    var dropdowninteracmenu = createElement("div", {}, {}, "dropdown-menu", { 'aria-labelledby': componentname + "_interact" });
                    divdropdowninterac.appendChild(dropdowninteracmenu);
                    divchain.appendChild(divdropdowninterac);
                    chain.interactfile_set.forEach(function (interactfile, indexfile) {
                        var span = createElement("label", {}, { display: "inline-block", width: "250px" }, "dropdown-item");
                        span.innerHTML = baseName(interactfile.label);
                        var checkbox = createElement("input", {
                            type: "checkbox",
                            checked: false,
                            onchange: function (e) {
                                stage.compList.forEach(function (component) {
                                    if (component.name.includes("_interact")) {
                                        if (!document.getElementById(component.name).checked) {
                                            stage.removeComponent(component);
                                            elemdiv = document.getElementById(baseName(interactfile.interact_file) + "_interact_isolevel");
                                            elemdiv.parentNode.removeChild(elemdiv);
                                            elemslide = document.getElementById(baseName(interactfile.interact_file) + "_interact_isolevelslide");
                                            elemslide.parentNode.removeChild(elemslide);
                                        };
                                    };
                                });
                                if (e.target.checked) {
                                    stage.loadFile(interactfile.interact_file, { ext: "mrc", name: baseName(interactfile.interact_file) + "_interact" }).then(function (o) {
                                        interactrepresentation = o.addRepresentation("surface", {
                                            opacity: 0.7,
                                            opaqueBack: false,
                                            smooth: 2,
                                            color: filecolor(indexchain, indexfile),
                                            isolevel: interactfile.default_isolevel,
                                            isolevelType: 'value',
                                        });
                                    });
                                    var divvalueslider = createElement("div", {}, {}, "", { 'id': baseName(interactfile.interact_file) + "_interact_isolevel" });
                                    divvalueslider.innerHTML = 'current value for ' + interactfile.label + ': ' + interactfile.default_isolevel;
                                    divchain.appendChild(divvalueslider);
                                    var isolevel = createElement("input", { type: "range", value: interactfile.default_isolevel * 100, min: 0, max: 100, step: 1 },
                                        {}, "form-control-range", { 'id': baseName(interactfile.interact_file) + "_interact_isolevelslide" });
                                    isolevel.oninput = function (e) {
                                        $('#' + baseName(interactfile.interact_file) + "_interact_isolevel").html('current value for ' + interactfile.label + ': ' + parseFloat(e.target.value) / 100);
                                        stage.getRepresentationsByName("surface").list.forEach(function (rep) {
                                            if (rep.parent.name === baseName(interactfile.interact_file) + "_interact") {
                                                rep.setParameters({
                                                    isolevel: parseFloat(e.target.value) / 100,
                                                    isolevelType: "value"
                                                })
                                            };
                                        });
                                    };
                                    divchain.appendChild(isolevel);
                                };
                            }
                        }, { float: "right", margin: "3px" }, "align-middle, interact", { "target": interactfile.interact_file, "id": baseName(interactfile.interact_file) + "_interact" });
                        span.appendChild(checkbox);
                        dropdowninteracmenu.appendChild(span);
                    });

                };
                var divchainmrc = createElement("div", {}, {}, "");
                divslider.appendChild(divchainmrc);
                if (chain.mrc_file) {
                    hasmrc = 1;
                    stage.loadFile(chain.mrc_file, { ext: "mrc", name: componentname + "_mrc" }).then(function (o) {
                        mrcrepresentation = o.addRepresentation("surface", {
                            opacity: 0.7,
                            opaqueBack: false,
                            smooth: 2,
                            color: filecolor(indexchain, -1),
                            isolevel: chain.default_isolevel,
                            isolevelType: 'value',
                        });
                    });


                    var labelslider = createElement("label", {}, {}, "", { 'for': 'isolevel' + chain.pdb_chain_id });
                    labelslider.innerHTML = 'Adjust predicted druggability surface on chain ' + chain.pdb_chain_id;
                    var divvalueslider = createElement("div", {}, {}, "", { 'id': 'valueisolevel' + chain.pdb_chain_id });
                    divvalueslider.innerHTML = 'current value: ' + chain.default_isolevel
                    divchainmrc.appendChild(labelslider);
                    divchainmrc.appendChild(divvalueslider);
                    var isolevel = createElement("input", {
                        type: "range", value: chain.default_isolevel * 100, min: 0, max: 100, step: 1
                    }, {}, "form-control-range", { 'id': 'isolevel' + chain.pdb_chain_id });
                    isolevel.oninput = function (e) {
                        $('#valueisolevel' + chain.pdb_chain_id).html('current value: ' + parseFloat(e.target.value) / 100);
                        stage.getRepresentationsByName("surface").list.forEach(function (rep) {
                            if (rep.parent.name === componentname + '_mrc') {
                                rep.setParameters({
                                    isolevel: parseFloat(e.target.value) / 100,
                                    isolevelType: "value"
                                })
                            }
                        });
                    };
                    divchainmrc.appendChild(isolevel);
                };
                var reprcartoonButton = createElement("input", {
                    type: "checkbox",
                    checked: true,
                    onchange: function (e) {
                        stage.getRepresentationsByName("cartoon").list.forEach(function (rep) {
                            if (rep.parent.name === componentname) {
                                rep.setVisibility(e.target.checked);
                            };
                        });
                    }
                }, { float: "right", margin: "3px" }, "align-middle");
                var reprstickButton = createElement("input", {
                    type: "checkbox",
                    checked: false,
                    onchange: function (e) {
                        stage.getRepresentationsByName("licorice").list.forEach(function (rep) {
                            if (rep.parent.name === componentname) {
                                rep.setVisibility(e.target.checked);
                            };
                        });
                    }
                }, { float: "right", margin: "3px" }, "align-middle");
                var reprsurfaceButton = createElement("input", {
                    type: "checkbox",
                    checked: false,
                    onchange: function (e) {
                        stage.getRepresentationsByName("surface").list.forEach(function (rep) {
                            if (rep.parent.name === componentname) {
                                rep.setVisibility(e.target.checked);
                            };
                        });
                    }
                }, { float: "right", margin: "3px" }, "align-middle");

                if (chain.hotspot_set.length != 0) {
                    if (hascritical) {
                        var reprHotSpotButtonCritical = createElement("input", {
                            type: "checkbox",
                            checked: false,
                            onchange: function (e) {
                                stage.getRepresentationsByName(componentname + "critical").list.forEach(function (rep) {
                                    if (rep.parent.name === componentname) {
                                        rep.setVisibility(e.target.checked);
                                    };
                                });
                            }
                        }, { float: "right", margin: "3px" }, "align-middle");
                    }
                    if (haswarm) {
                        var reprHotSpotButtonWarm = createElement("input", {
                            type: "checkbox",
                            checked: false,
                            onchange: function (e) {
                                stage.getRepresentationsByName(componentname + "warm").list.forEach(function (rep) {
                                    if (rep.parent.name === componentname) {
                                        rep.setVisibility(e.target.checked);
                                    };
                                });
                            }
                        }, { float: "right", margin: "3px" }, "align-middle");
                    }
                }

                var divdropdown = createElement("div", {}, {}, "dropdown");
                var buttondorpdown = createElement("button", {
                    type: "button",
                    id: componentname
                }, {}, "btn btn-outline-primary dropdown-toggle", {
                    "data-toggle": "dropdown",
                    "aria-haspopup": "true",
                    "aria-expanded": "false"
                });
                buttondorpdown.append("chain ".concat(chain.pdb_chain_id))
                divdropdown.appendChild(buttondorpdown);

                if (chain.hotspot_set.length != 0) {
                    if (hascritical) {
                        var spanhotspotcritical = createElement("label", {}, { display: "inline-block" }, "dropdown-item");
                        spanhotspotcritical.innerHTML = "critical&nbsp;";
                        spanhotspotcritical.appendChild(reprHotSpotButtonCritical);
                    }
                    if (haswarm) {
                        var spanhotspotwarm = createElement("label", {}, { display: "inline-block" }, "dropdown-item");
                        spanhotspotwarm.innerHTML = "warm&nbsp;";
                        spanhotspotwarm.appendChild(reprHotSpotButtonWarm);
                    }
                }
                var chainreprButton = createElement("div", {}, {}, "dropdown-menu", { 'aria-labelledby': componentname });
                var divheader = createElement("div", {}, {}, "dropdown-header");
                divheader.innerHTML = "Representations"
                chainreprButton.appendChild(divheader)

                var spancartoon = createElement("label", {}, { display: "inline-block" }, "dropdown-item");
                spancartoon.innerHTML = "cartoon&nbsp;";
                spancartoon.appendChild(reprcartoonButton);
                chainreprButton.appendChild(spancartoon);

                var spanstick = createElement("label", {}, { display: "inline-block" }, "dropdown-item");
                spanstick.innerHTML = "licorice&nbsp;"
                spanstick.appendChild(reprstickButton);
                chainreprButton.appendChild(spanstick);

                var spansurface = createElement("label", {}, { display: "inline-block" }, "dropdown-item");
                spansurface.innerHTML = "surface&nbsp;"
                spansurface.appendChild(reprsurfaceButton);
                chainreprButton.appendChild(spansurface);

                if (chain.hotspot_set.length != 0) {
                    var divdividerhotspot = createElement("div", {}, {}, "dropdown-divider");
                    chainreprButton.appendChild(divdividerhotspot);
                    var divheaderhotspot = createElement("div", {}, {}, "dropdown-header");
                    divheaderhotspot.innerHTML = "Hotspots"
                    chainreprButton.appendChild(divheaderhotspot);
                    console.log("critical", hascritical, "warm", haswarm)
                    if (haswarm) {
                        chainreprButton.appendChild(spanhotspotwarm);
                    }
                    if (hascritical) {
                        chainreprButton.appendChild(spanhotspotcritical);
                    }
                }

                divdropdown.appendChild(chainreprButton);
                divmultiButtoncenter.appendChild(divdropdown);
                chain.cavity_set.forEach(function (cavity) {
                    var blobcavity = new Blob([cavity.mol2], { type: 'text/plain' });
                    var multiButton = createElement("input", {
                        type: "button",
                        value: "load pocket " + cavity.cavity_number + " from " + chain.pdb_chain_id,
                        onclick: function (e) {
                            stage.compList.forEach(function (component) {
                                if (component.name.includes("CAVITY")) {
                                    stage.removeComponent(component);
                                }
                            });
                            document.querySelectorAll('[id^=cavity_]').forEach(function (cav) {
                                cav.classList.add('table-light');
                                cav.classList.remove('table-info');
                            });
                            if (e.target.classList.contains("active")) {
                                e.target.classList.remove('active');
                            } else {
                                e.target.parentNode.childNodes.forEach(function (button) {
                                    button.classList.remove('active');
                                });
                                e.target.classList.add('active');
                                stage.loadFile(blobcavity, { ext: "mol2", name: cavity.full_name }).then(function (o) {
                                    o.addRepresentation("spacefill", {
                                        radius: 0.2,
                                        color: probetype,
                                    });
                                });
                                document.querySelectorAll('[id^=cavity_]').forEach(function (cav) {
                                    if (cav.id == 'cavity_' + cavity.id) {
                                        cav.classList.add('table-info');
                                        cav.classList.remove('table-light');
                                    };
                                });
                            };
                        }
                    }, {}, "btn btn-outline-info btn-sm");
                    divcavitiesButton.appendChild(multiButton);
                });
            });
            if (hasmrc == 0) {
                var labelslider = createElement("label", {}, {}, "", {});
                labelslider.innerHTML = 'No surface druggable predict';
                divslider.appendChild(labelslider);
            };
            if (hasinteract == 0) {
                var labelsliderinteractibility = createElement("label", {}, {}, "", {});
                labelsliderinteractibility.innerHTML = 'No interactable surface predict';
                divsliderinteractibility.appendChild(labelsliderinteractibility);
            };
            pdb.ligand_set.forEach(function (ligand) {
                var blobligand = new Blob([ligand.file_content], { type: 'text/plain' });
                var componentname = pdb.code.concat('_', ligand.pdb_ligand_id, '_', ligand.supplementary_id)
                stage.loadFile(blobligand, { ext: "pdb", name: componentname }).then(function (o) {
                    o.addRepresentation("licorice");
                });
                var reprstickButton = createElement("input", {
                    type: "checkbox",
                    checked: true,
                    onchange: function (e) {
                        stage.getRepresentationsByName("licorice").list.forEach(function (rep) {
                            if (rep.parent.name === componentname) {
                                rep.setVisibility(e.target.checked);
                            };
                        });
                    }
                }, { float: "right", margin: "3px" }, "align-middle");
                var divdropdown = createElement("div", {}, {}, "dropdown");
                var buttondorpdown = createElement("button", {
                    type: "button",
                    id: componentname
                }, {}, "btn btn-outline-primary dropdown-toggle", {
                    "data-toggle": "dropdown",
                    "aria-haspopup": "true",
                    "aria-expanded": "false"
                });
                buttondorpdown.append("ligand ".concat(ligand.pdb_ligand_id, '_', ligand.supplementary_id))
                divdropdown.appendChild(buttondorpdown);
                var chainreprButton = createElement("div", {}, {}, "dropdown-menu", { 'aria-labelledby': componentname });
                var spanstick = createElement("label", {}, { display: "inline-block" }, "dropdown-item");
                spanstick.innerHTML = "licorice&nbsp;"
                spanstick.appendChild(reprstickButton);
                chainreprButton.appendChild(spanstick);
                divdropdown.appendChild(chainreprButton);
                divmultiButtoncenter.appendChild(divdropdown);
            });
            var divmultiButtoncam = createElement("div", { role: "group" }, {}, "btn-group btn-group-sm mt-auto p-2");
            divmultiButtonright.appendChild(divmultiButtoncam);
            var centerButton = createElement("button", {

                onclick: function () {
                    stage.autoView()
                }
            }, {}, "btn btn-secondary shadow-none");
            var fullscreenButton = createElement("button", {

                onclick: function () {
                    elem = document.getElementById('viewportfull');
                    stage.toggleFullscreen(elem);
                }
            }, {}, "btn btn-secondary shadow-none buttonfullscreen");
            var rockButton = createElement("button", {

                onclick: function () {
                    stage.toggleRock();
                }
            }, {}, "btn btn-secondary shadow-none");
            var spinButton = createElement("button", {

                onclick: function () {
                    stage.toggleSpin();
                }
            }, {}, "btn btn-secondary shadow-none");
            var backgroundcolorButton = createElement("button", {

                onclick: function () {
                    /* old ngl version use stage.parameters.backgroundColor.value */
                    if (stage.parameters.backgroundColor == 'white') {
                        stage.setParameters({ backgroundColor: 'black' });
                    } else {
                        stage.setParameters({ backgroundColor: 'white' });
                    };
                }
            }, {}, "btn btn-secondary shadow-none");
            backgroundcolorButton.appendChild(createElement("i", {}, {}, "fas fa-adjust"));
            spinButton.appendChild(createElement("i", {}, {}, "fas fa-sync-alt"));
            rockButton.appendChild(createElement("i", {}, {}, "fas fa-exchange-alt"));
            fullscreenButton.appendChild(createElement("i", {}, {}, "fas fa-expand"));
            centerButton.appendChild(createElement("i", {}, {}, "fas fa-dot-circle"));
            divmultiButtoncam.appendChild(centerButton);
            divmultiButtoncam.appendChild(spinButton);
            divmultiButtoncam.appendChild(rockButton);
            divmultiButtoncam.appendChild(backgroundcolorButton);
            divmultiButtoncam.appendChild(fullscreenButton);
            addElement(stage, divflex)
            pdb.chain_set.forEach(function (chain, indexchain) {
                if (chain.linkedpdbs.length > 0) {
                    var selectPL = document.getElementById("selectPL");
                    var btnPL = document.getElementById("loadButton");
                    btnPL.removeAttribute('disabled', false);
                    chain.linkedpdbs.forEach(function (linkedpdb, indexlinkedpdb) {
                        var newoption = document.createElement("option");
                        newoption.value = linkedpdb.id;
                        newoption.text = linkedpdb.label;
                        selectPL.appendChild(newoption);
                    })
                }
            });
            linkedPLs(stage, colorindex);
            var optionsCount = selectPL.options.length;
            var maxSize = Math.min(optionsCount, 10);
            selectPL.size = maxSize;

        }
    });
};

function linkedPLs(stage, colorindex) {
    var loadStructureButton = document.getElementById("loadStructureButton");
    loadStructureButton.addEventListener("click", function () {
        var selectPL = document.getElementById("selectPL");
        $.ajax({
            url: "/api/linkedpdb/" + selectPL.value,
            success: function (linkedpdb) {
                const alignedPDB = linkedpdb.aligned_chain

                const name = linkedpdb.name.concat("_chain")
                const blobalignchain = new Blob([alignedPDB], { type: 'text/plain' });
                stage.loadFile(blobalignchain, { ext: "pdb", name: name }).then(function (o) {
                    o.addRepresentation("cartoon", { color: ColorsList[colorindex], visible: true });
                    o.addRepresentation("licorice", { visible: false, sele: 'not _H' });
                    o.addRepresentation("surface", {
                        visible: false,
                        colorScheme: 'electrostatic',
                        surfaceType: 'av',
                        probeRadius: 1.4,
                        smooth: 2,
                        scaleFactor: 2.0,
                        cutoff: 0.02,
                    });
                });
                colorindex = colorindex + 1;
                const alignedLigand = linkedpdb.aligned_ligand;
                const ligandname = linkedpdb.name.concat("_ligand");
                const blobalignligand = new Blob([alignedLigand], { type: 'text/plain' });
                stage.loadFile(blobalignligand, { ext: "pdb", name: ligandname }).then(function (o) {
                    o.addRepresentation("licorice");
                });
                var loadedButton = document.getElementById('loadedButton');
                if (loadedButton.classList.contains("disabled")) {
                    loadedButton.classList.remove("disabled");
                };
                var loadedMenu = document.getElementById('loadedMenu');
                item = createDropdownitem(stage, linkedpdb);
                loadedMenu.append(item);
                // Auto-view pour afficher les deux structures dans la scène
                //stage.autoView();
            },
            error: function () {
                console.error("Error during second structure loading.");
            }
        });
        $('#loadPLmodal').modal('toggle');
    });
};

function createDropdownitem(stage, linkedpdb) {
    divitem = createElement("div", {}, {}, "dropdown-item");
    row = createElement("div", {}, {}, "row");
    ligandlabel = createElement("h5", {}, {}, "col");
    var partslabel = linkedpdb.name.split("--");
    partslabel = partslabel[1].split("_")
    var newlabel = partslabel[0] + " " + partslabel[1];
    ligandlabel.innerHTML = newlabel;
    row.appendChild(ligandlabel);

    divrepcartoon = createElement("div", {}, {}, "col");
    labelcartoon = createElement("label", {}, {}, "mr-2");
    labelcartoon.innerHTML = 'cartoon';
    divrepcartoon.appendChild(labelcartoon);
    inputcartoon = createElement("input", {
        type: "checkbox",
        checked: true,
        onchange: function (e) {
            stage.getRepresentationsByName("cartoon").list.forEach(function (rep) {
                if (rep.parent.name === linkedpdb.name.concat("_chain")) {
                    rep.setVisibility(e.target.checked);
                };
            });
            setLigandVisibility(linkedpdb, stage);
        }

    }, {}, "mb-2");
    divrepcartoon.appendChild(inputcartoon);

    divreplicorice = createElement("div", {}, {}, "col");
    labellicorice = createElement("label", {}, {}, "mr-2");
    labellicorice.innerHTML = 'licorice';
    divreplicorice.appendChild(labellicorice);
    inputlicorice = createElement("input", {
        type: "checkbox",
        checked: false,
        onchange: function (e) {
            stage.getRepresentationsByName("licorice").list.forEach(function (rep) {
                if (rep.parent.name === linkedpdb.name.concat("_chain")) {
                    rep.setVisibility(e.target.checked);
                };
            });
            setLigandVisibility(linkedpdb, stage);
        }

    }, {}, "mb-2");
    divreplicorice.appendChild(inputlicorice);

    divrepsurface = createElement("div", {}, {}, "col");
    labelsurface = createElement("label", {}, {}, "mr-2");
    labelsurface.innerHTML = 'surface';
    divrepsurface.appendChild(labelsurface);
    inputsurface = createElement("input", {
        type: "checkbox",
        checked: false,
        onchange: function (e) {
            stage.getRepresentationsByName("surface").list.forEach(function (rep) {
                if (rep.parent.name === linkedpdb.name.concat("_chain")) {
                    rep.setVisibility(e.target.checked);
                };
            });
            setLigandVisibility(linkedpdb, stage);
        }

    }, {}, "mb-2");
    divrepsurface.appendChild(inputsurface);

    row.appendChild(divrepcartoon);
    row.appendChild(divreplicorice);
    row.appendChild(divrepsurface);

    divitem.appendChild(row);

    return divitem
}

function setLigandVisibility(linkedpdb, stage) {
    var ligandvisibility = false;
    stage.eachRepresentation(function (repalt) {
        if (repalt.parent.name === linkedpdb.name.concat("_chain")) {
            if (repalt.getVisibility() === true) {
                ligandvisibility = true;
            };
        };
    });
    stage.getRepresentationsByName("licorice").list.forEach(function (replig) {
        if (replig.parent.name === linkedpdb.name.concat("_ligand")) {
            replig.setVisibility(ligandvisibility);
        };
    });
}
