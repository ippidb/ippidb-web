(function () {
    // Get all buttons that opens the modal

    var btns = document.getElementsByClassName("modal-btn");
    for (var j = 0; j < btns.length; j++) {
        // When the user clicks the button, open the modal 
        btns[j].onclick = function () {
            var modalname = this.getAttribute('for');
            var modal = document.getElementById(modalname);
            modal.style.display = "block";
            var buttonclose = modal.getElementsByClassName("close")[0];
            buttonclose.onclick = function () {
                modal.style.display = "none";
            }
        }
    };

    window.onclick = function () {
        modals = document.getElementsByClassName("modal")
        for (var m of modals) {
            if (m.style.display == "block") {
                var modal = m;
                break;
            } else {
                var modal = "none"
            };
        };
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
})();