$(document).ready(function() {
    var marvinModal = new MarvinModal(
        'marvin-div',
        'modal-marvin',
        'smiles-textarea',
        'marvinApplyButton',
        function(smilesString) {
            var fingerprint = '';
            if (smilesString.trim() !== '') {
                fingerprint = $('input[name=fingerprint]:checked').val();
                queryUrl.modify('similar_to', fingerprint + ':' + smilesString);
            } else {
                queryUrl.modify('similar_to', null);
            }
        }
    );
});