// Function to hide/show infos contain in a div


// ProteinDomainComplexTypeForm - Function to hide/show architectures according to the chosen type of complex
$(document).ready(function () {
    $("#id_ProteinDomainComplexTypeForm-complexChoice").change(function () {
        $(this).find("option:checked").each(function () {
            //console.log("test:",$(this).attr("value"));
            if ($(this).attr("value") == "inhibited") {
                $(".boxs").not(".inhibited").hide();
                $(".inhibited").show();
            } else if ($(this).attr("value") == "stabilized") {
                $(".boxs").not(".stabilized").hide();
                $(".stabilized").show();
            } else {
                $(".boxs").hide();
            }
        });
    }).change();
});
// Function to change Complex type value according to an architecture


//Function to load different template setting on Composition page
 function onloadTest(type, choice) {
    console.log("Onload Test:", type, choice);
    var total = $('#id_' + 'ProteinDomainComplexForm' + '-TOTAL_FORMS').val();
    console.log("total: ",total);
    if (type == "Hetero2merAB" && choice == "inhibited" && total < 2) {
        console.log(document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value);
        cloneMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "1";
        document.getElementById('id_ProteinDomainComplexForm-1-complex_type_1').value = "Partner";
    } else if (type == "Homo2merA2" && choice == "inhibited" && total < 2) {
        cloneMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-0-complex_type_0').value = "Bound";
    } else if (type == "Hetero2merAB" && choice == "stabilized" && total < 2) {
        console.log(document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value);
        cloneMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "1";
        document.getElementById('id_ProteinDomainComplexForm-1-complex_type_1').value = "Partner";
    } else if (type == "HomoLike2mer" && choice == "stabilized" && total < 2) {
        cloneMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-1-complex_type_1').value = "Bound";
    } else if (type == "Homo3merA2" && choice == "stabilized" && total < 2) {
        cloneMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-1-complex_type_1').value = "Partner";
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "2";
        document.getElementById('id_ProteinDomainComplexForm-1-ppc_copy_nb_1').value = "1";
    } else if (type == "custom" && total < 2) {
        cloneMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-1-complex_type_1').value = "Partner";
    } else if (type == "Homo3merA3" || type == "RingHomo3mer" && choice == "stabilized") {
        removeMoreFull('.inline_box_complex_long:last', 'ProteinDomainComplexForm');
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "3";
    } else if (type == "Homo4merA4" && choice == "stabilized") {
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "4";
    } else if (type == "Homo3merA3" && choice == "stabilized") {
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "3";
    } else if (type == "RingHomo5mer" && choice == "stabilized") {
        document.getElementById('id_ProteinDomainComplexForm-0-ppc_copy_nb').value = "5";
    }
} 

//Function to load different template setting on PPI page
/* function onloadTestPPI(type, choice) {
    console.log("Onload Test PPI:", type, choice);
    if (type == "Homo2merA2" && choice == "inhibited") {
        document.getElementById('id_PpiForm-cc_nb').value = "2";
        document.getElementById('id_PpiForm-symmetry').value = "2";
    } else if (type == "Homo2merA2" && choice == "stabilized") {
        document.getElementById('id_PpiForm-cc_nb').value = "1";
        document.getElementById('id_PpiForm-symmetry').value = "2";
    } else if (type == "HomoLike2mer" && choice == "stabilized" && total < 2) {
        document.getElementById('id_PpiForm-pockets_nb').value = "2";
        document.getElementById('id_PpiForm-cc_nb').value = "1";
        document.getElementById('id_PpiForm-symmetry').value = "3";
    } else if (type == "Homo3merA2" && choice == "stabilized") {
        console.log("Onload Test2 PPI:", type, choice);
        document.getElementById('id_PpiForm-pockets_nb').value = "1";
        document.getElementById('id_PpiForm-symmetry').value = "4";
    } else if (type == "Homo3merA3" && choice == "stabilized") {
        console.log("Onload Test3 PPI:", type, choice);
        document.getElementById('id_PpiForm-pockets_nb').value = "1";
        document.getElementById('id_PpiForm-symmetry').value = "4";
    } else if (type == "RingHomo3mer" && choice == "stabilized") {
        console.log("Onload Test4 PPI:", type, choice);
        document.getElementById('id_PpiForm-pockets_nb').value = "3";
        document.getElementById('id_PpiForm-symmetry').value = "4";
    } else if (type == "Homo4merA4" && choice == "stabilized") {
        document.getElementById('id_PpiForm-pockets_nb').value = "2";
        document.getElementById('id_PpiForm-cc_nb').value = "1";
        document.getElementById('id_PpiForm-symmetry').value = "6";
    } else if (type == "RingHomo5mer" && choice == "stabilized") {
        document.getElementById('id_PpiForm-pockets_nb').value = "5";
        document.getElementById('id_PpiForm-cc_nb').value = "1";
        document.getElementById('id_PpiForm-symmetry').value = "8";
    }
} */

// Function to duplicate form and increment ids
function cloneMoreFull(selector, prefix) {
    var newElement = $(selector).clone(true);
    var total = $('#id_' + prefix + '-TOTAL_FORMS').val();
    console.log('total: ', total);
    newElement.find(':input').each(function () {
        var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
        var id = 'id_' + name + '_' + total;
        $(this).attr({ 'name': name, 'id': id, 'for': id });
    });
    total++;
    console.log('total incr: ', total);
    $('#id_' + prefix + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
}


// Function to duplicate form, increment ids and empty input
function cloneMoreEmpty(selector, prefix) {
    var newElement = $(selector).clone(true);
    var total = $('#id_' + prefix + '-TOTAL_FORMS').val();
    console.log('total: ', total);
    console.log('new Element: ', newElement.find('input'));
    newElement.find(':input').each(function () {
        var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
        console.log('Element name: ', name);
        var id = 'id_' + name + '_' + total;
        $(this).attr({ 'name': name, 'id': id, 'for': id }).val('').removeAttr('checked');
    });
    $('#compound_mol').find('label').each(function () {
        var name = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
        console.log("new element: ", name);
        this.setAttribute("for", $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-'));
    });
    total++;
    console.log('total incr: ', total);
    $('#id_' + prefix + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
}

// Function to enlarge some part of the main page
$(document).ready(function () {
    $('.box').on('click', '.box__inner', function () {
        $(this).closest('.box').find('.box--active').removeClass('box--active');
        $(this).addClass('box--active');
    }).on('click', '.box--active', function () {
        $(this).removeClass('box--active');
    })
});


// Function to switch test type in TestsForm -- CHANGE FONCTION
$(document).ready(function () {
    $("select.test-selector").change(change_test_selector);
});

function change_test_selector() {
    var test_type = $(this).val();
    var root = $(this).closest(".div_test");
    root.find(".test-selector").val(test_type);
    if (test_type == "cytotoxicity") {
        $(root).find(".div_test_activity").hide();
        $(root).find(".div_test_pk").hide();
        $(root).find(".div_test_cytotoxicity").show();
    } else if (test_type == "pk") {
        $(root).find(".div_test_activity").hide();
        $(root).find(".div_test_pk").show();
        $(root).find(".div_test_cytotoxicity").hide();
    } else {
        $(root).find(".div_test_activity").show();
        $(root).find(".div_test_pk").hide();
        $(root).find(".div_test_cytotoxicity").hide();
    }
}

function add_form_to_formset(source, prefix) {
    let input_total = $(source).closest(".formset-container").find("[name='" + prefix + "-TOTAL_FORMS']");
    let empty_form_as_str = $(source).closest(".formset-container").find(".empty_form").prop('innerHTML');
    empty_form_as_str = empty_form_as_str.replace(
        /__prefix__/g,
        input_total.val()
    );
    empty_form = $(empty_form_as_str).removeClass("empty_form");
    empty_form.insertBefore($(source).parent());
    empty_form.find("select.test-selector").change(change_test_selector);
    input_total.val(parseInt(input_total.val()) + 1);
    empty_form.find("input.form-control")
        .add(empty_form.find("select.form-control"))
        .focusout(function (e) { $(e.target).addClass("has-been-focused"); });
    return empty_form;
}

function delete_button_clicked(source) {
    let item = $(source).closest(".form-group");
    $(item).addClass("formset-item-delete-host");
    item = $(item).closest(".formset-nested-item");
    if (item.length == 0) {
        item = $(source).closest(".formset-item");
    }
    if ($(source).prop("checked")) {
        $(item).find("select").prop("disabled", true);
        $(item).find("textarea").prop("disabled", true);
        $(item).find(":not(.formset-item-delete-host)>input:visible").prop("disabled", true);
        $(item).addClass("delete-checked");
    } else {
        $(item).removeClass("delete-checked");
        $(item).find("select").prop("disabled", false);
        $(item).find("textarea").prop("disabled", false);
        $(item).find("input").prop("disabled", false);
    }
}

function create_bootstrap_multiselect_from_applicant(source) {
    $(source).multiselect({
        allSelectedText: $(source).attr("data-all-selected-text"),
        nonSelectedText: $(source).attr("data-non-selected-text"),
        nSelectedText: $(source).attr("data-n-selected-text"),
        enableFiltering: true,
        buttonWidth: '100%',
        numberDisplayed: 4,
        maxHeight: 600,
        templates: {
            button: '<button type="button" class="multiselect dropdown-toggle btn btn-secondary btn-default multi-btn " data-toggle="dropdown"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
            filterClearBtn: '<div class="input-group-append"><button class="btn btn-outline-primary multiselect-clear-filter" type="button"><i class="fa fa-times-circle"></i></button></div>',
            filter: '<li class="multiselect-item multiselect-filter"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span></div><input class="form-control multiselect-search" type="text" /></div></li>',
        }
    }).show();
}
function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
    localStorage.setItem('mySidenav', 'true');
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0px";
    localStorage.setItem('mySidenav', 'false');
}  

$(document).ready(function () {
    $("select[multiple].bootstrap-multiselect-applicant").each(function (i, e) {
        create_bootstrap_multiselect_from_applicant(e);
    });
});

var multiselectTypeAhead = function (idSearch, selection, onSelect) {
    var bh = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: selection,
        identify: function (obj) { return obj.id; }
    });
    $('#' + idSearch).typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },
        {
            name: 'source' + idSearch,
            display: 'name',
            source: bh
        });
    $('#' + idSearch).on('typeahead:select', onSelect);
};

var setUpSliderForModal = function (paramName) {
    $('#' + paramName).slider();
    $('#' + paramName).on('slide', function (slideEvt) {
        $('#' + paramName + '_textvalue_min').text(slideEvt.value[0]);
        $('#' + paramName + '_textvalue_max').text(slideEvt.value[1]);
    });
}

var toggleCheckBox = function (id) {
    var cb = $('#' + id);
    queryUrl.unPage();
    if (cb.prop('checked') == true) {
        queryUrl.changeSelection(id, cb.prop('checked'));
    } else {
        queryUrl.changeSelection(id, null);
    }
}

$(document).ready(function () {
    $(".modalhref").click(function(e){
        $(".modal").modal('hide');
        window.open($(this).find("a").last()[0].href, '_self');
    });
});