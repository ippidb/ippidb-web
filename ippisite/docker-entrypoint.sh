#!/bin/bash

function msg_info {
   echo -e "\033[1;32m$1\033[0m"
}

function msg_warning {
   echo -e "\033[1;33m$1\033[0m"
}

function msg_error {
   echo -e "\033[1;31m$1\033[0m"
}

cd /code

if [ "$1" == "hold_on" ]; then
    msg_info "holding on util you delete /tmp/hold_on"
    touch /tmp/hold_on
    while [ -e "/tmp/hold_on" ]; do
        sleep 1 ;
        echo "holding on" ;
    done
fi

if [ "$1" == "test" ]; then
    if [ "$2" != "" ]; then
      msg_info "Running specific tests"
        python manage.py $@
        exit 0
    fi
    msg_info "Running tests"
    # python manage.py makemigrations || exit 7 # fails in migrations are missing
    coverage run --concurrency=multiprocessing || exit 3
    msg_info "Combine"
    coverage combine
    msg_info "report"
    coverage report --skip-covered --omit=*/wsgi.py,*/asgi.py,manage.py,*/apps.py
    msg_info "xml"
    coverage xml
    msg_info "html"
    coverage html --omit=*/wsgi.py,*/asgi.py,manage.py,*/apps.py
    exit 0
fi

msg_info "Applying database migrations"
python manage.py migrate || exit 4

if [ -e $STATIC_ROOT_ON_STARTUP_COPY ]; then
  msg_info "Copying static files to $STATIC_ROOT_ON_STARTUP_COPY"
  cp -rf ${STATIC_ROOT}/* ${STATIC_ROOT_ON_STARTUP_COPY} || exit 6
else
  msg_info "$STATIC_ROOT_ON_STARTUP_COPY absent, cannot copy static files on startup to this directory"
fi

exec "$@"
