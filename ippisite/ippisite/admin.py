import os
from django.contrib import admin
from django.urls import path
from django.shortcuts import redirect
from django.contrib import messages
from django.http import JsonResponse
from ippidb.tasks import (
    launch_update_compound_cached_properties,
    run_compute_drugbank_similarity,
    launch_plots_computing,
    launch_delete_targetcentric,
    launch_set_compound_links,
)
from django.shortcuts import render
from ippidb.models import Job


class IppidbAdmin(admin.AdminSite):
    site_header = "iPPI-DB website administration"

    site_title = site_header

    index_header = "iPPI-DB administration"

    index_title = index_header

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "launch_compound_properties_caching/",
                self.admin_view(self.launch_compound_properties_caching_view),
            ),
            path(
                "launch_drugbank_similarity_computing/",
                self.admin_view(self.launch_drugbank_similarity_computing_view),
            ),
            path(
                "launch_plots_computing/",
                self.admin_view(self.launch_plots_computing_view),
            ),
            path(
                "delete_targetcentric/",
                self.admin_view(self.delete_targetcentric),
            ),
            path(
                "check_path/",
                self.admin_view(self.check_path),
            ),
            path(
                "launch_set_compound_links/",
                self.admin_view(self.launch_set_compound_links_view),
            ),
            path("tasklog/", self.admin_view(self.tasklog), name="tasklog"),
        ]
        return my_urls + urls

    def tasklog(self, request):
        jobid = request.GET.get("jobid", None)
        if jobid:
            try:
                job = Job.objects.get(id=jobid)
            except Job.DoesNotExist:
                messages.add_message(
                    request,
                    messages.INFO,
                    "job with id: {} doesn't exist".format(jobid),
                )
                return redirect("/admin/ippidb/job")
            return render(
                request,
                "admin/ippidb/loginfo.html",
                context={"taskid": job.task_result.task_id},
            )
        else:
            messages.add_message(request, messages.INFO, "Please specify a jobid")
            return redirect("/admin/ippidb/job")

    def launch_compound_properties_caching_view(self, request):
        """
        This view launches the task to perform, for all already validated compounds,
        the caching of the properties.
        """
        launch_update_compound_cached_properties()
        messages.add_message(
            request, messages.INFO, "Compound properties caching launched"
        )
        return redirect("/admin/ippidb/job")

    def launch_drugbank_similarity_computing_view(self, request):
        """
        This view launches the task to perform, for all already validated compounds,
        the computing of drugbank similarity.
        """
        run_compute_drugbank_similarity.delay()
        messages.add_message(
            request, messages.INFO, "DrugBank similarity computing launched"
        )
        return redirect("/admin/ippidb/job")

    def launch_plots_computing_view(self, request):
        """
        This view launches the task to perform the computing of LE-LLE and PCA plots.
        """
        launch_plots_computing()
        messages.add_message(request, messages.INFO, "Plots computing launched")
        return redirect("/admin/ippidb/job")

    def delete_targetcentric(self, request):
        """
        This view delete all pdb in the database.
        """
        launch_delete_targetcentric.delay()
        messages.add_message(request, messages.INFO, "Delete targetcentric launched")
        return redirect("/admin/")

    def check_path(self, request):
        """
        This view check if path exist on file system server.
        """
        path = request.GET.get("path", "/")
        accept_type = request.GET.get("type")
        dirname = os.path.dirname(path)
        basename = os.path.basename(path)
        listdir = []
        if self.check_permission(dirname):
            listdir = [
                os.path.join(dirname, elem)
                for elem in os.listdir(dirname)
                if os.path.isdir(os.path.join(dirname, elem))
                and not elem.startswith(".")
                and elem.startswith(basename)
            ]
            listdir = self.check_permissions(listdir)
            if accept_type == "dir":
                if os.path.isdir(path) and self.check_permission(path):
                    return JsonResponse({"listpath": listdir[:50]}, status=200)
                else:
                    return JsonResponse({"listpath": listdir[:50]}, status=404)
            else:
                listfiles = [
                    os.path.join(dirname, elem)
                    for elem in os.listdir(dirname)
                    if not os.path.isdir(os.path.join(dirname, elem))
                    and not elem.startswith(".")
                    and elem.startswith(basename)
                    and elem.endswith(accept_type)
                ]
                listfiles = self.check_permissions(listfiles)
                listdir = listfiles + listdir
                if os.path.exists(path) and self.check_permission(path):
                    return JsonResponse({"listpath": listdir[:50]}, status=200)
                else:
                    return JsonResponse({"listpath": listdir[:50]}, status=404)
        return JsonResponse({"listpath": listdir[:50]}, status=404)

    def check_permission(self, elem):
        """
        check linux permission for one path
        """
        if os.path.isdir(elem):
            if os.access(elem, os.X_OK) or os.access(elem, os.R_OK):
                return True
        else:
            if os.access(elem, os.R_OK):
                return True
        return False

    def check_permissions(self, listdir):
        """
        check linux permission for a list of paths
        """
        listelem = []
        for elem in listdir:
            if self.check_permission(elem):
                listelem.append(elem)
        listelem.sort()
        return listelem

    def launch_set_compound_links_view(self, request):
        """
        This view launches the task to set/update compound DB cross-links.
        """
        launch_set_compound_links()
        messages.add_message(request, messages.INFO, "DB links update launched")
        return redirect("/admin/ippidb/job")
