import traceback

from celery import Task, states
from django_celery_results.models import TaskResult

from ippidb.models import Job


class AlreadyExistError(Exception):
    pass


class MonitorTask(Task):
    """
    Ippidb custom task
    """

    def __call__(self, *args, **kwargs):
        """In celery task this function call the run method, here you can
        set some environment variable before the run of the task"""
        tasks = TaskResult.objects.filter(
            task_name=self.request.task,
            status__in=[states.STARTED, states.PENDING, states.RETRY, states.RECEIVED],
            task_args=str(args),
            task_kwargs=str(kwargs),
        )
        count_tasks = tasks.count()
        if not count_tasks:
            self.update_state(state=states.PENDING)
            return self.run(*args, **kwargs)
        else:
            message_exc = "Job {} in state {} already exist".format(
                tasks[0].task_name, tasks[0].status
            )
            raise AlreadyExistError(message_exc)

    def write(self, std_out=None, std_err=None, with_print=True):
        job = Job.objects.get(task_result__task_id=self.task_id)
        if std_out:
            job.update_output(std_out, output="std_out", with_print=with_print)
        elif std_err:
            job.update_output(std_err, output="std_err", with_print=with_print)

    def on_success(self, retval, task_id, args, kwargs):
        self.write(std_out="SUCCESS")
        super(MonitorTask, self).on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        super(MonitorTask, self).on_failure(exc, task_id, args, kwargs, einfo)
        if isinstance(exc, AlreadyExistError):
            self.update_state(state=states.REVOKED)
        else:
            self.update_state(state=states.FAILURE)
        self.write(std_err="".join(traceback.format_tb(einfo.tb)))

    def update_state(self, task_id=None, state=None, meta=None, **kwargs):
        self.state = state
        if task_id is None:
            self.task_id = self.request.id
        else:
            self.task_id = task_id
        super(MonitorTask, self).update_state(
            task_id=task_id, state=state, meta=meta, **kwargs
        )
