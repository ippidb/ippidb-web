# How to build and run the app

```shell
docker build . -t test_ippidb --target app-container && docker run -it -e ALLOWED_HOSTS=localhost -e SECRET_KEY=a -e "STATIC_URL=/static" -e "POSTGRES_NAME=postgres" -e "POSTGRES_USER=postgres" -e "POSTGRES_PASSWORD=postgres" -e "POSTGRES_HOST=db" -e "USE_SQLITE_AS_DB=True" -v $(pwd):/code -p 8095:8000 test_ippidb
```

# How to import the data
_To dump the db you can do `/usr/pgsql-11/bin/pg_dump --clean --format plain | gzip -c - > /tmp/db.sql.gz`_

```shell
# get a shell inside the db
docker exec -it -u postgres ippidb-web_db-local_1 bash
# check that the dump is here 
ls -lah /code/db.sql.gz
# import it 5 minutes
gzip -d -c /code/db.sql.gz | sed 's/OWNER TO ippidb/OWNER TO postgres/' | psql 1>/tmp/log
```
    