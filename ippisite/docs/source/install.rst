Installation
============

Basic installation and run
--------------------------

You need **python3** to install and use ippidb-web.

.. code-block:: shell

    git clone https://gitlab.pasteur.fr/hub/ippidb-web.git
    virtualenv -p python3 venv
    . venv/bin/activate
    cd ippisite
    pip install -r requirements.txt

Important note: until recently openbabel distributions were bugged, especially
for Tanimoto functionalities. The best solution for local deployments is to
clone openbabel, compile it and copy the generated python modules to the
virtualenv you have created.

.. code-block:: shell

    sudo apt install cmake
    git clone https://github.com/openbabel/openbabel.git
    cd openbabel
    mkdir build
    cd build
    cmake .. -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_BINDINGS=ON -DRUN_SWIG=ON
    make
    sudo make install
    # then we copy the python library installed by make install to the virtualenv
    cp -r /usr/local/lib/python3.7/site-packages/openbabel ~/ippidb-web/venv/lib/python3.7/site-packages/

Public server deployment
------------------------

Ansible playbooks are available in the `ansible folder of the source repository
<https://gitlab.pasteur.fr/hub/ippidb-web/tree/master/ansible>`_. These
playbooks automate the initial configuration of the system (for a server using
CentOS), and the initial installation or the update of the code from the repository.
