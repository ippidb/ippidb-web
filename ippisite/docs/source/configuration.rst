Configuration
=============

Although most of the basic functionalities work directly in iPPI-DB, some of
them require some configuration, either in the django settings or in the
database.

Some of these settings can be automatically configured upon deployment if you
use the Ansible scripts provided.

Authentication with allauth
---------------------------

Users can login to iPPI-DB with an ORCID or a GitHub account. 

ORCID authentication configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to login to https://orcid.org and configure the public API in the `Developers tools page <https://orcid.org/developer-tools>`_.
On this screen, click on `Register for the free ORCID public API`, and then consent to the terms of service.

.. image:: img/ORCID_conf_1.png
   :width: 100%

.. image:: img/ORCID_conf_2.png
   :width: 50%

The following screen allows you to authorize the ORCID API to be accessed from the iPPI-DB website to authenticate users.
Provide, as shown below, a name, the website URL, the description of the application, and the redirect URL to your website (which should be `[ROOT URL]/accounts/orcid/login/callback`).

.. image:: img/ORCID_conf_3.png
   :width: 100%

Save this configuration by clicking on the floppy disk icon at the bottom right of the screen.

Once this is done, you can access the generate *Client ID* and *Client secret* variables from the main `Developers tools page <https://orcid.org/developer-tools>`_.

.. image:: img/ORCID_conf_4.png
   :width: 100%

Click on the `Show details` button there, and you will access the variables you need to configure ORCID authentication on the Django admin interface of the iPPI-DB website.

.. image:: img/ORCID_conf_5.png
   :width: 100%

You can then navigate to the django admin interface of iPPI-DB, and create an entry in the `Social applications` table of the `Social accounts` app.

.. image:: img/ORCID_conf_6.png
   :width: 50%

.. image:: img/ORCID_conf_7.png
   :width: 100%

.. GitHub authentication is commented because it does not currently work
.. GitHub authentication configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. The configuration for GitHub authentication follows the same principle: configure the app on the provider website, then enter the provided `Client ID` and `Client secret`
   on the django admin interface website.
..
   .. image:: img/Github_conf_1.png
      :width: 100%
..
   .. image:: img/Github_conf_2.png
      :width: 100%
..
   .. image:: img/Github_conf_3.png
      :width: 100%



Galaxy configuration
--------------------

MarvinJS configuration
----------------------
The MarvinJS widget distributed by Chemaxon can be used to draw chemical
compounds instead of typing their SMILES or IUPAC name, but this component
requires a license. This license is associated with an API key, which can be
obtained on the `Settings page of the Chemicalize website
<https://pro.chemicalize.com/app/marvin/settings>`_. To be functional on your
website, this API key must be associate with the domain URL of the server. This
API key has to be specified in the settings module of iPPI-DB, as the value of
the ``MARVINJS_APIKEY`` variable.

