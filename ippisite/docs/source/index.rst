.. iPPI-DB documentation master file, created by
   sphinx-quickstart on Wed Nov 28 16:32:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the iPPI-DB technical documentation!
===============================================

iPPI-DB is an online database of modulators of protein-protein interactions.
The database itself and its description are hosted at https://ippidb.pasteur.fr.

These pages contain the documentation for technical maintenance and development purposes.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   configuration
   database
   webservices
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
