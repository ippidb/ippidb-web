How to update and build the documentation for iPPI-DB
=====================================================

- update the database graphical representation
python ../manage.py graph_models ippidb --pygraphviz --output source/db.dot
- build the docs
make html

The main page of the documentation is in build/html/index.html.
